package com.wdc.www.wd360;

import android.media.ExifInterface;

import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * Created by Biswapratap Chatterjee on 6/24/2017.
 */

class BlockedQueueBasedMiner {

    private static String TAG = BlockedQueueBasedMiner.class.getSimpleName();

    private void indexDocuments(File file, WD360TessBaseApi tessBaseAPI, WD360Indexer indexer) {
        try {
            IndexItem[] ii = new IndexItem[1];
            ii[0] = null;
            IndexItem idxItem = null;
            if (0 == TextExtractor.getIndexItem(file, tessBaseAPI, ii)) {
                return;
            }
            else {
                idxItem = ii[0];
            }
            if (idxItem != null) {
                indexer.index(idxItem);
                if (idxItem.getContent().isEmpty())
                    Log.d(TAG, "Indexed with empty content : " + Long.toString(idxItem.getId()) + " : " + idxItem.getTitle() + " : " + idxItem.getContent());
                else
                    Log.d(TAG, "Indexed with content : " + Long.toString(idxItem.getId()) + " : " + idxItem.getTitle());
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    synchronized private void runindexer(String userName,
                                         String fileAbsolutePath,
                                         WD360TessBaseApi tessApi,
                                         WD360Indexer indexer) {
        try {
            Boolean lookForNewFaces = false;
            int group = 0;
            int dog = 0;
            int cat = 0;
            String underscore_delimite_height_width_area = "";
            int height = 0;
            int width = 0;
            int area = 0;

            MasterDBHelper mdDHelper = new MasterDBHelper(Globals.gContext);
            mdDHelper.setUserName(userName);
            String userAbsFolder = mdDHelper.getTableAbsoluteRootFolerPath();

            File file = new File(fileAbsolutePath);
            if (null != file && file.exists()) {
                Log.d(TAG, "runindexer() : FILE TO BE DOC INDEXED = " + fileAbsolutePath);
                indexDocuments(file, tessApi, indexer);
                mdDHelper.insertRowIfNotExist(fileAbsolutePath,
                        null,
                        "NA",
                        null,
                        0.0,
                        0.0,
                        "NA",
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        "NA",
                        1,
                        0);

                if (Utility.isTypeFile(file, "image")) {
                    File theFile = new File(fileAbsolutePath);
                    if(900000 < theFile.length()) {
                        Log.d(TAG, "runindexer() : Size greated than 900 KB, Skipping : " + fileAbsolutePath);
                    } else {
                        WD360FaceDetection wdfd = new WD360FaceDetection(
                                Globals.FACE_RECOGNIZER_TYPE_LBPH,
                                userAbsFolder + Globals.WD360_NATIVE_LOG_FOLDER + File.separator + "WD360.log",
                                userAbsFolder + Globals.WD360_ETC_FOLDER + File.separator + Globals.WD360_FACE_DETECTOR_XML_FILE_NAME,
                                userAbsFolder + Globals.WD360_ETC_FOLDER + File.separator + Globals.WD360_FACE_DETECTOR_DOG_XML_FILE_NAME,
                                userAbsFolder + Globals.WD360_ETC_FOLDER + File.separator + Globals.WD360_FACE_DETECTOR_CAT_XML_FILE_NAME,
                                userAbsFolder + Globals.WD360_FACE_RECOGNIZER_MODEL_FOLDER,
                                1
                        );
                        String predictedFaces = wdfd.detect(fileAbsolutePath);
                        String predictedFacesFromDB = mdDHelper.getPredictedFacesFromDB(fileAbsolutePath);
                        if (predictedFaces.isEmpty() && predictedFacesFromDB.equals("NA")) {
                            mdDHelper.incrementFaceTagAge(fileAbsolutePath);
                        } else if ((false == predictedFaces.isEmpty()) && (false == predictedFacesFromDB.equalsIgnoreCase(predictedFaces))) {
                            mdDHelper.updateFace(fileAbsolutePath, predictedFaces);
                            mdDHelper.incrementFaceTagAge(fileAbsolutePath);
                            Log.d(TAG, fileAbsolutePath + " successfully identified faces : " + predictedFaces);
                        } else {
                            mdDHelper.incrementFaceTagAge(fileAbsolutePath);
                        }
                        wdfd.release();
                    }

                    WD360FaceDetection wdfd = new WD360FaceDetection(
                            Globals.FACE_RECOGNIZER_TYPE_LBPH,
                            userAbsFolder + Globals.WD360_NATIVE_LOG_FOLDER + File.separator + "WD360.log",
                            userAbsFolder + Globals.WD360_ETC_FOLDER + File.separator + Globals.WD360_FACE_DETECTOR_XML_FILE_NAME,
                            userAbsFolder + Globals.WD360_ETC_FOLDER + File.separator + Globals.WD360_FACE_DETECTOR_DOG_XML_FILE_NAME,
                            userAbsFolder + Globals.WD360_ETC_FOLDER + File.separator + Globals.WD360_FACE_DETECTOR_CAT_XML_FILE_NAME,
                            userAbsFolder + Globals.WD360_FACE_RECOGNIZER_MODEL_FOLDER,
                            1
                    );

                    float[] latLong = new float[2];
                    String hashHexValue = "";
                    final ExifInterface exifInterface = new ExifInterface(fileAbsolutePath);
                    exifInterface.getLatLong(latLong);
                    hashHexValue = Utility.calculateMD5(new File(fileAbsolutePath));
                    group = wdfd.howManyFaces(fileAbsolutePath, 0);
                    Log.d(TAG, fileAbsolutePath + " : found = " + group + " faces");
                    cat = wdfd.howManyFaces(fileAbsolutePath, 2);
                    Log.d(TAG, fileAbsolutePath + " : found = " + cat + " cat faces");
                    underscore_delimite_height_width_area = wdfd.getDimensions(fileAbsolutePath);

                    if (null != underscore_delimite_height_width_area) {
                        String[] data = underscore_delimite_height_width_area.split("_");
                        height = Integer.parseInt(data[0]);
                        width = Integer.parseInt(data[1]);
                        area = Integer.parseInt(data[2]);
                    }

                    mdDHelper.updateImageConstantValues(fileAbsolutePath,
                            latLong[0],
                            latLong[1],
                            dog,
                            cat,
                            group,
                            height,
                            width,
                            area,
                            hashHexValue);
                    wdfd.release();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    synchronized public QueueItem observerGetItem() throws Exception {
        return Globals.OBSERVER_MINER_QUEUE.take();
    }

    void mine() throws Exception {
        while (true) {
            QueueItem item = null;
            item = observerGetItem();
            if (null == item) {
                break;
            }
            try {
                runindexer(item.m_userName,
                           item.m_absFilePath,
                           item.m_tessApi,
                           item.m_indexer);
            } catch (Exception e) {
                Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
                continue;
            }
        }
    }

    synchronized public QueueItem periodicGetItem() throws Exception {
        return Globals.PERIODIC_MINER_QUEUE.poll(5, TimeUnit.SECONDS);
    }

    void mineWithTimeout() throws Exception {
        String un = "";
        while (true) {
            QueueItem item = null;
            item = periodicGetItem();
            if (null == item) {
                break;
            }
            try {
                un = item.m_userName;
                runindexer(item.m_userName,
                           item.m_absFilePath,
                           item.m_tessApi,
                           item.m_indexer);
            } catch (Exception e) {
                Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
                continue;
            }
        }
    }
}

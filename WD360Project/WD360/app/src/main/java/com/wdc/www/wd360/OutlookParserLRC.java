package com.wdc.www.wd360;

import org.apache.poi.hsmf.MAPIMessage;
import org.apache.poi.hsmf.datatypes.AttachmentChunks;
import org.apache.poi.hsmf.exceptions.ChunkNotFoundException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.concurrent.Callable;

/**
 * Created by Biswapratap Chatterjee on 7/3/2017.
 */

public class OutlookParserLRC implements Callable<IndexItem> {
    private MAPIMessage msg = null;
    private final File f;
    private final String fileNameStem;
    private PrintWriter txtOut = null;

    public OutlookParserLRC(File file) {
        this.f = file;
        if(this.f.getName().endsWith(".msg") || this.f.getName().endsWith(".MSG")) {
            this.fileNameStem = this.f.getName().substring(0, this.f.getName().length() - 4);
        }
        else
            this.fileNameStem = "";
    }

    /**
     * Processes the message.
     *
     * @throws IOException if an exception occurs while writing the message out
     */
    private void processMessage() throws IOException {
        String txtFileName = fileNameStem + ".txt";
        String attDirName = fileNameStem + "-att";
        txtOut = null;
        try {
            txtOut = new PrintWriter(txtFileName);
            try {
                String displayFrom = msg.getDisplayFrom();
                txtOut.println("From: "+displayFrom);
            } catch (ChunkNotFoundException e) {
                // ignore
            }
            try {
                String displayTo = msg.getDisplayTo();
                txtOut.println("To: "+displayTo);
            } catch (ChunkNotFoundException e) {
                // ignore
            }
            try {
                String displayCC = msg.getDisplayCC();
                txtOut.println("CC: "+displayCC);
            } catch (ChunkNotFoundException e) {
                // ignore
            }
            try {
                String displayBCC = msg.getDisplayBCC();
                txtOut.println("BCC: "+displayBCC);
            } catch (ChunkNotFoundException e) {
                // ignore
            }
            try {
                String subject = msg.getSubject();
                txtOut.println("Subject: "+subject);
            } catch (ChunkNotFoundException e) {
                // ignore
            }
            try {
                String body = msg.getTextBody();
                txtOut.println(body);
            } catch (ChunkNotFoundException e) {
                System.err.println("No message body");
            }

            AttachmentChunks[] attachments = msg.getAttachmentFiles();
            if(attachments.length > 0) {
                File d = new File(attDirName);
                if(d.mkdir()) {
                    for(AttachmentChunks attachment : attachments) {
                        processAttachment(attachment, d);
                    }
                } else {
                    System.err.println("Can't create directory "+attDirName);
                }
            }
        } finally {
            if(txtOut != null) {
                txtOut.close();
            }
        }
    }

    /**
     * Processes a single attachment: reads it from the Outlook MSG file and
     * writes it to disk as an individual file.
     *
     * @param attachment the chunk group describing the attachment
     * @param dir the directory in which to write the attachment file
     * @throws IOException when any of the file operations fails
     */
    private void processAttachment(AttachmentChunks attachment,
                                  File dir) throws IOException {
        BufferedReader buf = null;
        String fileName = attachment.getAttachFileName().toString();
        if(attachment.getAttachLongFileName() != null) {
            fileName = attachment.getAttachLongFileName().toString();
        }

        try {
            File f = new File(dir, fileName);
            InputStream is = new FileInputStream(f);
            buf = new BufferedReader(new InputStreamReader(is));
            String line = buf.readLine();
            StringBuilder sb = new StringBuilder();
            while(line != null){
                sb.append(line).append("\n");
                line = buf.readLine();
            }
            buf.close();
            txtOut.println(sb.toString());
            String content = sb.toString();
        } finally {
            if(buf != null) {
                buf.close();
            }
        }
    }

    public IndexItem call() {
        IndexItem idxItm = new IndexItem((long)f.getName().hashCode(), f.getAbsolutePath(), "");
        try {
            this.msg = new MAPIMessage(f.getAbsolutePath());
            processMessage();
            idxItm.setContent(txtOut.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return idxItm;
    }
}
package com.wdc.www.wd360;

import android.app.Service;
import android.content.Intent;
import android.os.FileObserver;

import com.wdc.nassdk.BaseStartupService;
import com.wdc.nassdk.MyCloudUIServer;

import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;

/**
 * Created by Biswapratap Chatterjee on 5/12/2017.
 */

public class StartupService extends BaseStartupService {
    private static String TAG = StartupService.class.getSimpleName();
    public static FileObserver observer;

    @Override
    public MyCloudUIServer createMyCloudUIServer() {
        Log.d(TAG, "[INIT] StartupService:createMyCloudUIServer() Entering");
        Log.d(TAG, "[INIT] StartupService:createMyCloudUIServer() Exiting");
        return new WD360AppUI(getApplicationContext());
        /*
        try {
            String ip = "127.0.0.1";
            Enumeration e = null;
            try {
                e = NetworkInterface.getNetworkInterfaces();
            } catch (SocketException e1) {
                e1.printStackTrace();
            }
            while(e.hasMoreElements()) {
                NetworkInterface n = (NetworkInterface) e.nextElement();
                String neteth = n.getDisplayName();
                if (neteth.equals("eth0")) {
                    Enumeration ee = n.getInetAddresses();
                    while (ee.hasMoreElements()) {
                        InetAddress i = (InetAddress) ee.nextElement();
                        String ipaddr = i.getHostAddress();
                        if ((i instanceof Inet4Address) && (!ipaddr.equals("127.0.0.1"))) {
                            if (ipaddr.equals("10.0.2.15")) {
                                ip = "127.0.0.1";
                                break;
                            }
                            ip = ipaddr;
                            break;
                        }
                    }
                }
            }
            Log.d(TAG, "StartupService:createMyCloudUIServer() called");
            return new WD360AppUI(ip , Globals.gContext);
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        return null;
        */
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "[INIT] StartupService:onStartCommand() Entering");
        Globals.myCloudUserId = "MyCloudOwner";
        flags = Service.START_STICKY;

        if (false == StartupServiceStatus.getServiceStatus()) {
            StartupServiceStatus.setServiceStatus(true);
            Globals.gContext = getApplicationContext();
            FileUtils.deleteQuietly(Globals.gContext.getCacheDir());

            try {
                Globals.ROOT_FOLDER_NAME = WD360CloudUIServer.getRootFolder(Globals.gContext, "");
                if (Globals.ROOT_FOLDER_NAME.endsWith("/")) {
                    Globals.ROOT_FOLDER_NAME = WD360CloudUIServer.getRootFolder(Globals.gContext, "");
                } else {
                    Globals.ROOT_FOLDER_NAME = WD360CloudUIServer.getRootFolder(Globals.gContext, "") + File.separator;
                }

                if (null == Globals.gContext) {
                    Log.e(TAG, "******************* Globals.gContext is NULL");
                    Log.d(TAG, "[INIT} StartupService:onStartCommand() Exiting");
                    super.onStartCommand(intent, flags, startId);
                }

                BufferedReader br = null;
                Globals.WD360_FACE_TRAINING_AGE_INFO = Globals.ROOT_FOLDER_NAME + "wd360FaceTrainingAge.inf";

                APP_CONTEXT_DATA.getFaceTrainingAge();

                String response = "200";
                File userRootFolder = new File(Globals.ROOT_FOLDER_NAME + Globals.myCloudUserId);
                if ((null == userRootFolder) || (false == userRootFolder.exists())) {
                    String[] keyValues = new String[3];
                    keyValues[1] = "username=" + Globals.myCloudUserId;
                    keyValues[0] = "cmd=user_login";
                    keyValues[2] = "passwd=1234";
                    String cmd_opcode = "user_login";
                    response = ExecuteUserLoginCommand.executeUserLogin(keyValues, cmd_opcode);
                }

                File yml = new File(Globals.ROOT_FOLDER_NAME + Globals.myCloudUserId +
                        File.separator + Globals.WD360_FACE_RECOGNIZER_MODEL_FOLDER +
                        File.separator + Globals.WD360_FACE_RECOGNIZER_MODEL_YML_FILE_NAME);
                if ((null == yml) || (false == yml.exists())) {
                    ExecuteUserLoginCommand.putSampleDataSet(Globals.ROOT_FOLDER_NAME + Globals.myCloudUserId);
                    if (false == ExecuteUserLoginCommand.createPreLoadedFaceTrainingInfrastructure(
                            Globals.ROOT_FOLDER_NAME + Globals.myCloudUserId)) {
                        response = "FAILURE";
                    }
                }

                if (response.contains("200")) {
                    Thread periodicThread = new Thread(new PeriodicMinerThread());
                    periodicThread.start();

                    Thread observerThread = new Thread(new ObserverMinerThread());
                    observerThread.start();

                    //periodicThread.join();
                    //observerThread.join();
                } else {
                    Log.e(TAG, "Failed to start Server");
                }
            } catch (Exception e) {
                Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            }
        } else {
            Log.d(TAG, "[INIT] StartupService:onStartCommand() ALREADY STARTED");
        }
        Log.d(TAG, "[INIT] StartupService:onStartCommand() Exiting");
        return super.onStartCommand(intent, flags, startId);
    }
}

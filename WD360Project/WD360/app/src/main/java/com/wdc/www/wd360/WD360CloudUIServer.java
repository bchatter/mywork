package com.wdc.www.wd360;

import android.content.Context;
import android.os.Environment;
import android.support.annotation.NonNull;


import com.wdc.nassdk.Utils;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD;


/**
 * Created by Biswapratap Chatterjee on 6/6/2017.
 */

public abstract class WD360CloudUIServer extends NanoHTTPD {

    private static final String TAG = "WD360CloudUIServer";
    private Context mContext;

    public WD360CloudUIServer(Context context) {
        super(getPort(context));
        this.mContext = context;
    }

    public WD360CloudUIServer(String hostname, Context context) {
        super(hostname, getPort(context));
        this.mContext = context;
    }

    public static int getPort(Context context) {
        return 9092;
    }

    public static String getRootFolder(@NonNull Context context, @NonNull String myCloudUserId) {
        String pathname;
        if(Utils.isNotWdNas()) {
            pathname = Environment.getExternalStorageDirectory() + File.separator + myCloudUserId + File.separator;
            File file = new File(pathname);
            file.mkdir();
            try {
                return file.getCanonicalPath();
            } catch (IOException e) {
                Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + e.getMessage());
                return pathname;
            }
        } else {
            pathname = context.getApplicationInfo().dataDir + File.separator + myCloudUserId + File.separator;
            return pathname;
        }
    }

    public Response serve(IHTTPSession session) {
        Log.d("MyCloudUIServer", "HTTP Method-> " + session.getMethod().name());
        Log.d("MyCloudUIServer", "Query Parameter String-> " + session.getQueryParameterString());
        Log.d("MyCloudUIServer", "<-- Headers start -->");
        Iterator var2 = session.getHeaders().entrySet().iterator();

        while(var2.hasNext()) {
            Map.Entry entry = (Map.Entry)var2.next();
            Log.i("MyCloudUIServer", entry.getKey() + ", " + entry.getValue());
        }

        Log.d("MyCloudUIServer", "<-- Headers end -->");
        switch(session.getMethod()) {
            case GET:
                return this.get(session);
            case POST:
                return this.post(session);
            case DELETE:
                return this.delete(session);
            case HEAD:
                return this.head(session);
            case PUT:
                return this.put(session);
            default:
                return super.serve(session);
        }
    }

    public abstract Response get(IHTTPSession var1);

    public abstract Response post(IHTTPSession var1);

    public Response put(IHTTPSession session) {
        return newFixedLengthResponse(new Response.IStatus() {
            public int getRequestStatus() {
                return 200;
            }

            public String getDescription() {
                return "OK";
            }
        }, "text/html", "<html><body>Sucess<body></html>");
    }

    public Response delete(IHTTPSession session) {
        return newFixedLengthResponse(new Response.IStatus() {
            public int getRequestStatus() {
                return 200;
            }

            public String getDescription() {
                return "OK";
            }
        }, "text/html", "<html><body>Sucess<body></html>");
    }

    public Response head(IHTTPSession session) {
        return newFixedLengthResponse(new Response.IStatus() {
            public int getRequestStatus() {
                return 200;
            }

            public String getDescription() {
                return "OK";
            }
        }, "text/html", "<html><body>Sucess<body></html>");
    }

    public static String getId(IHTTPSession session) {
        if(session == null) {
            return "0";
        } else {
            try {
                return (String)session.getHeaders().get("x-wd-user-id");
            } catch (Exception var2) {
                return "0";
            }
        }
    }

    public static String getBaseUrl(IHTTPSession session) {
        String baseUrl = (String)session.getParms().get("redirect-ip");
        if(baseUrl != null && baseUrl.length() != 0) {
            Log.d("MyCloudUIServer", "baseUrl " + baseUrl);
            String postURL = baseUrl + "?access_token=" + getAccessToken(session);
            return postURL;
        } else {
            Log.d("MyCloudUIServer", "##### baseUrl is empty");
            return null;
        }
    }

    public static String getAccessToken(IHTTPSession session) {
        try {
            String e = (String)session.getParms().get("access_token");
            return e;
        } catch (Exception var2) {
            return null;
        }
    }

    public static String getMyCloudUserId(IHTTPSession session) {
        try {
            String e = (String)session.getHeaders().get("x-wd-mycloud-id");
            return e;
        } catch (Exception var2) {
            return null;
        }
    }
}

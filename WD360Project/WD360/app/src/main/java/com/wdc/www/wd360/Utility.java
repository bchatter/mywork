package com.wdc.www.wd360;

import android.content.ContentResolver;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Biswapratap Chatterjee on 7/5/2017.
 */

public class Utility {
    private static final String TAG = Utility.class.getSimpleName();
    public static final String newline = System.lineSeparator();
    private static WD360FaceDetection temp_wdfd;

    public static WD360FaceDetection getTempWDFD(String userAbsRootFolder, Boolean force) {
        if (false == force) {
            if (null != temp_wdfd)
                return temp_wdfd;
        }
        temp_wdfd = new WD360FaceDetection(
                Globals.FACE_RECOGNIZER_TYPE_LBPH,
                userAbsRootFolder + Globals.WD360_NATIVE_LOG_FOLDER + File.separator + "WD360.log",
                "NA",
                "NA",
                "NA",
                userAbsRootFolder + Globals.WD360_FACE_RECOGNIZER_MODEL_FOLDER,
                0);
        return temp_wdfd;
    }

    public static String calculateMD5(File updateFile) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            Log.e("calculateMD5", "Exception while getting Digest" + e.getMessage());
            return null;
        }

        InputStream is;
        try {
            is = new FileInputStream(updateFile);
        } catch (FileNotFoundException e) {
            Log.e("calculateMD5", "Exception while getting FileInputStream" + e.getMessage());
            return null;
        }

        byte[] buffer = new byte[8192];
        int read;
        try {
            while ((read = is.read(buffer)) > 0) {
                digest.update(buffer, 0, read);
            }
            byte[] md5sum = digest.digest();
            BigInteger bigInt = new BigInteger(1, md5sum);
            String output = bigInt.toString(16);
            // Fill to 32 chars
            output = String.format("%32s", output).replace(' ', '0');
            return output;
        } catch (IOException e) {
            throw new RuntimeException("Unable to process file for MD5", e);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                Log.e("calculateMD5", "Exception on closing MD5 input stream" + e.getMessage());
            }
        }
    }

    synchronized public static String getExceptionDetailedInfo(Exception e, String TAG) {
        String msg = "";
        try {
            for (int i = 0; i < e.getStackTrace().length; i++) {
                if (e.getStackTrace()[i].getClassName().contains(TAG)) {
                    msg = e.getStackTrace()[i].getFileName() + " : " +
                            e.getStackTrace()[i].getMethodName() + " : " +
                            e.getStackTrace()[i].getLineNumber();
                }
            }
        } catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        }
        return msg;
    }

    @Nullable
    synchronized private static String getTheMimeType(Uri uri) {
        try {
            String mimeType = "";
            if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
                ContentResolver cr = Globals.gContext.getContentResolver();
                if (cr == null) {
                    Log.d(TAG, "Utility.java : getTheMimeType() : getContentResolver() : returned null for file " + uri.toString());
                    return null;
                }
                mimeType = cr.getType(uri);
            } else {
                String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri.toString());
                if (null == fileExtension) {
                    Log.d(TAG, "Utility.java : getTheMimeType() : MimeTypeMap.getFileExtensionFromUrl() : returned null for file " + uri.toString());
                    return null;
                }
                mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase());
                if (null == mimeType) {
                    Log.d(TAG, "Utility.java : getTheMimeType() : MimeTypeMap.getSingleton().getMimeTypeFromExtension() : returned null for file " + uri.toString());
                    return null;
                }
            }
            return mimeType;
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return null;
        }
    }

    public static Boolean isTypeFile(File f, String type) {
        try {
            String retType = "";
            if (f.isDirectory()) {
                Log.d(TAG, "Utility.java : isTypeFile() : is a directory " + f.getAbsolutePath());
                return false;
            }
            retType = getTheMimeType(Uri.parse(f.toURI().toString()));
            if (null == retType) {
                Log.d(TAG, "Utility.java : isTypeFile() : returned null for file " + f.getAbsolutePath());
                return false;
            }
            if (retType.contains(type)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return false;
        }
    }

    @Nullable
    synchronized public static File putFileFromAsset(String dirifany, String fileUrl, File folder, String newFileName) {
        try {
            if (newFileName.isEmpty()) {
                newFileName = fileUrl;
            }
            File file = getWritableFile(folder, newFileName);
            if (null == file) {
                Log.e(TAG, "Utility.java : putFileFromAsset() : getWritableFile() : returned null for folder " + dirifany + " and file " + newFileName);
                return null;
            }
            InputStream is = Globals.gContext.getAssets().open(dirifany + File.separator + fileUrl);
            if (null == is) {
                Log.e(TAG, "Utility.java : putFileFromAsset() : Globals.gContext.getAssets().open() : returned null for folder " + dirifany + " and file " + fileUrl);
                return null;
            }
            if (!writeContentToFile(file, is)) {
                Log.e(TAG, "Utility.java : putFileFromAsset() : writeContentToFile() : returned false for folder " + dirifany + " and file " + fileUrl);
                return null;
            }
            return file;
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return null;
        }
    }

    @Nullable
    synchronized public static File putFile(InputStream is, String fileUrl, File folder) {
        try {
            File file = getWritableFile(folder, fileUrl);
            if (null == file) {
                Log.e(TAG, "Utility.java : putFile() : getWritableFile() : returned null for folder " + folder + " and file " + fileUrl);
                return null;
            }
            if (!writeContentToFile(file, is)) {
                Log.e(TAG, "Utility.java : putFile() : writeContentToFile() : returned false for folder " + folder + " and file " + fileUrl);
                return null;
            }
            return file;
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return null;
        }
    }

    @Nullable
    synchronized public static File getWritableFile(File folder, String fileName) {
        try {
            File imageFile = new File(folder.getAbsolutePath(), fileName);
            if (null == imageFile) {
                Log.e(TAG, "Utility.java : getWritableFile() : new File() : returned null for folder " + folder + " and file " + fileName);
                return null;
            }
            if (!imageFile.exists()) {
                imageFile.createNewFile();
            }
            return imageFile;
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return null;
        }
    }

    synchronized public static Boolean writeContentToFile(File destFile, InputStream is) {
        try {
            FileOutputStream fos = new FileOutputStream(destFile);
            if (null == fos) {
                Log.e(TAG, "Utility.java : writeContentToFile() : new FileOutputStream() : returned null for file " + destFile);
                return false;
            }
            int read = 0;
            byte[] buffer = new byte[32768];
            if (null == buffer) {
                Log.e(TAG, "Utility.java : writeContentToFile() : new byte[32768] : returned null for file " + destFile);
                return false;
            }
            while ((read = is.read(buffer)) > 0) {
                fos.write(buffer, 0, read);
            }
            fos.flush();
            fos.close();
            return true;
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return false;
        }
    }

    @Nullable
    synchronized public static File createFolder(String folder_name) {
        try {
            File newFolder = null;
            newFolder = new File(folder_name);
            if (null == newFolder) {
                Log.e(TAG, "Utility.java : createFolder() : new File() : returned null for folder " + folder_name);
                return null;
            }
            if (!newFolder.exists()) {
                newFolder.mkdir();
            }
            return newFolder;
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return null;
        }
    }

    @Nullable
    synchronized public static String getFileNameOnly(String url) {
        try {
            File f = new File(url);
            return f.getName();
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        return null;
    }

    @Nullable
    synchronized public static List<File> listfiles(String username, String directoryName) {
        File directory = new File(directoryName);
        List<File> resultList = new ArrayList<File>();
        File[] fList = directory.listFiles();
        resultList.addAll(Arrays.asList(fList));
        for (File file : fList) {
            if (file.isFile()) {
            } else if (file.isDirectory()) {
                resultList.remove(file);
                if ((false == file.getAbsolutePath().contains(Globals.TESS_DATA_FOLDER)) &&
                    (false == file.getAbsolutePath().contains(Globals.DOC_INDEXER_FOLDER)) &&
                    (false == file.getAbsolutePath().contains(Globals.WD360_ETC_FOLDER)) &&
                    (false == file.getAbsolutePath().contains(Globals.WD360_NATIVE_LOG_FOLDER)) &&
                    (false == file.getAbsolutePath().contains(Globals.WD360_USER_RESULTS_ROOT_FOLDER)) &&
                    (false == file.getAbsolutePath().contains(Globals.USER_FACE_TRAINING_FOLDER)) &&
                    (false == file.getAbsolutePath().contains(Globals.WD360_FACE_RECOGNIZER_MODEL_FOLDER))) {
                    resultList.addAll(listfiles(username, file.getAbsolutePath()));
                }
            }
        }
        return resultList;
    }

    @Nullable
    synchronized public static List<File> listMineablefiles(String username, String directoryName) {
        File directory = new File(directoryName);
        List<File> resultList = new ArrayList<File>();
        File[] fList = directory.listFiles();
        resultList.addAll(Arrays.asList(fList));
        boolean imageYes = false;
        boolean docYes = false;
        for (File file : fList) {
            imageYes = false;
            docYes = false;
            if (file.isFile()) {
                MasterDBHelper mdDHelper = new MasterDBHelper(Globals.gContext);
                mdDHelper.setUserName(username);

                String fileName = file.getName();
                String extension = fileName.substring(fileName.lastIndexOf('.') + 1);

                if (extension.equalsIgnoreCase("csv")) {
                    resultList.remove(file);
                }

                if (true == mdDHelper.isFileKnownToDB(file.getAbsolutePath())) {
                    if (true == mdDHelper.doesImageContainAnyFace(file.getAbsolutePath())) {
                        int appContextFaceTrainingAge = APP_CONTEXT_DATA.getFaceTrainingAge();
                        int fileDBFaceTrainingAge = mdDHelper.getImageFileFaceTaggedCurrentAge(file.getAbsolutePath());
                        if (appContextFaceTrainingAge > fileDBFaceTrainingAge) {
                            imageYes = true;
                        }
                    }

                    if ((false == mdDHelper.isFileAlreadyDocIndexed(file.getAbsolutePath())) ||
                        (false == mdDHelper.isFileConstantsAlreadyTagged(file.getAbsolutePath()))) {
                            docYes = true;
                    }
                } else {
                    imageYes = true;
                    docYes = true;
                }

                if ((imageYes == false) && (docYes == false)) {
                    resultList.remove(file);
                }
            } else if (file.isDirectory()) {
                resultList.remove(file);
                if ((false == file.getAbsolutePath().contains(Globals.TESS_DATA_FOLDER)) &&
                    (false == file.getAbsolutePath().contains(Globals.DOC_INDEXER_FOLDER)) &&
                    (false == file.getAbsolutePath().contains(Globals.WD360_ETC_FOLDER)) &&
                    (false == file.getAbsolutePath().contains(Globals.WD360_NATIVE_LOG_FOLDER)) &&
                    (false == file.getAbsolutePath().contains(Globals.WD360_USER_RESULTS_ROOT_FOLDER)) &&
                    (false == file.getAbsolutePath().contains(Globals.WD360_FACE_RECOGNIZER_MODEL_FOLDER))) {
                    resultList.addAll(listMineablefiles(username, file.getAbsolutePath()));
                }
            }
        }
        return resultList;
    }
}

package com.wdc.www.wd360;



import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Biswapratap Chatterjee on 7/14/2017.
 */

public class PeriodicMinerThread implements Runnable {
    private static String TAG = PeriodicMinerThread.class.getSimpleName();

    synchronized public void periodicPutItem(String un,
                                             String path,
                                             WD360Indexer indexer,
                                             WD360TessBaseApi tessapi) {
        try {
            Globals.PERIODIC_MINER_QUEUE.put(new QueueItem(un, path, indexer, tessapi));
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    @Override
    public void run() {
        int iteration = 1;
        MasterDBHelper mDBHlpr = new MasterDBHelper(Globals.gContext);
        int count_of_items = 0;
        while (true) {
            try {
                ServerBusy.setServerStatus(true);
                List<Object> clearObjs = new ArrayList<Object>();
                List<String> existingUserNames = mDBHlpr.getAllTableNames();
                if (null != existingUserNames) {
                    for (String oneExistingUser : existingUserNames) {
                        mDBHlpr.setUserName(oneExistingUser);
                        String absUserRootPath = mDBHlpr.getTableAbsoluteRootFolerPath();
                        WD360TessBaseApi tessBaseAPI = mDBHlpr.getTessBaseApi();
                        WD360Indexer indexer = mDBHlpr.getDocIndexer();
                        if ((null == tessBaseAPI) || (null == indexer))
                            continue;
                        /*
                        Re-initing Tess Base Api and Indexer here
                         */
                        tessBaseAPI.init(absUserRootPath, Globals.TRAINING_LANGUAGE);
                        indexer.setIndexDir(absUserRootPath + Globals.DOC_INDEXER_FOLDER);
                        /*
                        End of reiniting
                         */
                        clearObjs.add(tessBaseAPI);
                        clearObjs.add(indexer);
                        if (null != absUserRootPath) {
                            List<File> files = Utility.listMineablefiles(oneExistingUser, absUserRootPath);
                            Log.d(TAG, "###################### ITEMS TO BE MINED NOW FOR USER [ " + oneExistingUser + " ] IN IREATION [ " + iteration + " ] = " + files.size());
                            int burst_size = Globals.MINING_BURST_SIZE;
                            if (files.size() < Globals.MINING_BURST_SIZE) {
                                burst_size = files.size();
                            }
                            for (File file : files) {
                                periodicPutItem(oneExistingUser,
                                        file.getAbsolutePath(),
                                        indexer,
                                        tessBaseAPI);
                                count_of_items++;
                                if (count_of_items >= burst_size) {
                                    if (Globals.PERIODIC_MINER_QUEUE.size() > 0) {
                                        new BlockedQueueBasedMiner().mineWithTimeout();
                                    }
                                    count_of_items = 0;
                                    Thread.sleep(Globals.JAVA_GC_CLEANUP_TIME);
                                }
                            }
                        }
                        int items_doc_indexed_until_now = mDBHlpr.ItemsDocIndexedUntilNow();
                        int items_image_constants_indexed_until_now = mDBHlpr.ItemsImageConstantIndexedUntilNow();
                        int items_image_face_tagged_until_now = mDBHlpr.ItemsImageFaceTaggedUntilNow();
                        Log.d(TAG, "###################### ITEMS DOC INDEXED FOR USER [ " + oneExistingUser + " ] IN IREATION [ " + iteration + " ] = " + items_doc_indexed_until_now);
                        Log.d(TAG, "###################### ITEMS IMAGED CONSTANTS INDEXED FOR USER [ " + oneExistingUser + " ] IN IREATION [ " + iteration + " ] = " + items_image_constants_indexed_until_now);
                        Log.d(TAG, "###################### ITEMS IMAGE FACE TAGGED FOR USER [ " + oneExistingUser + " ] IN IREATION [ " + iteration + " ] = " + items_image_face_tagged_until_now);
                    }
                    for (int i = 0; i < clearObjs.size(); i+=2) {
                        ((TessBaseAPI)clearObjs.get(i)).clear();
                        ((WD360Indexer)clearObjs.get(i + 1)).close();
                    }
                }
                ServerBusy.setServerStatus(false);
                //Thread.sleep(Globals.NEXT_MININING_ATTEMPT_TIME);
                Thread.sleep(Globals.NEXT_MININING_ATTEMPT_TIME);
                iteration++;
            } catch (Exception e) {
                Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            }
        }
    }
}

package com.wdc.www.wd360;



import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Biswapratap Chatterjee on 7/21/2017.
 */

public class WD360FaceDetection {
    private String m_type;
    private String m_logpath;
    private String m_xmlpath;
    private String m_dogxmlpath;
    private String m_catxmlpath;
    private String m_frmfolderpath;
    private String m_frmfilename;
    private Map<Integer, String> m_trainedData;
    private int m_load;
    private static String TAG = WD360FaceDetection.class.getSimpleName();

    static {
        System.loadLibrary("native-lib");
    }

    public WD360FaceDetection(String type,
                              String logpath,
                              String xmlpath,
                              String dogxmlpath,
                              String catxmlpath,
                              String frmfolderpath,
                              int load) {
        m_type = type;
        m_logpath = logpath;
        m_xmlpath = xmlpath;
        m_dogxmlpath = dogxmlpath;
        m_catxmlpath = catxmlpath;
        m_frmfolderpath = frmfolderpath;
        m_frmfilename = Globals.WD360_FACE_RECOGNIZER_MODEL_YML_FILE_NAME;
        m_load = load;
        m_trainedData = new HashMap<Integer, String>();
        mNativeObj = nativeCreateObject(m_type,
                m_logpath,
                m_xmlpath,
                m_dogxmlpath,
                m_catxmlpath,
                m_frmfolderpath + File.separator + m_frmfilename,
                m_load);
    }

    public Map<Integer, String> getTrainedData() {
        try {
            String result = nativeGetAllTrainedFacesAndTheirLabelsUntilNow(mNativeObj, m_frmfolderpath, m_type);
            String[] items = result.split(":");
            for (String item : items) {
                String[] subitems = item.split("_");
                if (subitems.length != 2)
                    break;
                String lbl = subitems[1];
                Integer label = Integer.parseInt(lbl);
                String name = subitems[0];
                m_trainedData.put(label, name);
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        return m_trainedData;
    }

    public void train(String csvfile) {
        try {
            nativeTrain(mNativeObj, csvfile);
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    public void update(String newcsvfile) {
        try {
            nativeUpdate(mNativeObj, newcsvfile);
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    public String detect(String imagefilepath) {
        String predictedfaces = "";
        try {
            predictedfaces = nativePredict(mNativeObj, imagefilepath);
            Log.d(TAG, "Predicted faces for : " + imagefilepath + " = " + predictedfaces);
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        return predictedfaces;
    }

    /*
     * type = 0, Human.
     * type = 1, Dog,
     * type = 2, Cat,
     */
    public int howManyFaces(String imagefilepath, int type) {
        int ret = 0;
        try {
            ret = nativeHowManyFaces(mNativeObj, imagefilepath, type);
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        return ret;
    }

    public String getDimensions(String imagefilepath) {
        String ret = "";
        try {
            ret = nativeGetDimension(mNativeObj, imagefilepath);
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        return ret;
    }

    public void release() {
        try {
            nativeDestroyObject(mNativeObj);
            mNativeObj = 0;
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    private static long mNativeObj = 0;
    private native long nativeCreateObject(String type,
                                           String logpath,
                                           String xmlpath,
                                           String dogXmlpath,
                                           String catXmlpath,
                                           String modelsavepath,
                                           int load);
    private native void nativeLogOn(long thiz);
    private native void nativeLogOff(long thiz);
    private native void nativeDestroyObject(long thiz);
    private native void nativeTrain(long thiz, String csvfile);
    private native void nativeUpdate(long thiz, String csvfile);
    private native String nativePredict(long thiz, String imagefilepath);
    private native int nativeHowManyFaces(long thiz, String imagefilepath, int type);
    private native String nativeGetAllTrainedFacesAndTheirLabelsUntilNow(long thiz, String frmfolder, String type);
    private native String nativeGetDimension(long thiz, String imagefilepath);
}

package com.wdc.www.wd360;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Biswapratap Chatterjee on 7/11/2017.
 */

public class ExecuteUserImageQueryCommands {
    private static String TAG = ExecuteUserImageQueryCommands.class.getSimpleName();
    private static String MESSAGE = "Failed to query";

    public static String executeUserTrainedImagesQuery(String[] keyValues, String cmd_opcode) {
        try {
            String ret = "";
            String username = "";
            Log.d(TAG, "executeUserTrainedImagesQuery() cmd_opcode = " + cmd_opcode);
            ret = JSONUtils.commandSpecificChecks(keyValues, cmd_opcode, 1);
            if (ret.contains("ASSERT"))
                return ret;
            else {
                username = Globals.myCloudUserId;
                MasterDBHelper masterDBHelper = new MasterDBHelper(Globals.gContext);
                masterDBHelper.setUserName(username);
                Log.d(TAG, "executeUserTrainedImagesQuery() username = " + username);
                String userAbsFolder = masterDBHelper.getTableAbsoluteRootFolerPath();
                Log.d(TAG, "executeUserTrainedImagesQuery() userAbsFolder = " + userAbsFolder);
                WD360FaceDetection temp_wdfd = Utility.getTempWDFD(userAbsFolder,true);
                Map<Integer, String> fl = temp_wdfd.getTrainedData();
                Log.d(TAG, "executeUserTrainedImagesQuery() Num of faces trained  = " + fl.size());
                temp_wdfd.release();

                ret = "";
                if (fl.size() == 0) {
                    ret = "No faces trained yet";
                }

                for (String name : fl.values()) {
                    ret = ret + name + ":";
                }

                ret = ret.substring(0, ret.lastIndexOf(":"));
                Log.d(TAG, "executeUserTrainedImagesQuery() Faces  = " + ret);

                return JSONUtils.formResponseValues("200",
                        username,
                        cmd_opcode,
                        "",
                        "",
                        "0",
                        ret,
                        Globals.emptyResponse);
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return JSONUtils.formResponseValues("500",
                    "",
                    "",
                    "",
                    "",
                    "",
                    e.getMessage(),
                    Globals.emptyResponse);
        }
    }

    public static String executeUserImageQuery(String[] keyValues, String cmd_opcode) {
        try {
            String ret = "";
            String username = "";
            ret = JSONUtils.commandSpecificChecks(keyValues, cmd_opcode, 2);
            if (ret.contains("ASSERT"))
                return ret;
            else {
                username = Globals.myCloudUserId;
                String[] query = keyValues[1].split("=");

                MasterDBHelper masterDBHelper = new MasterDBHelper(Globals.gContext);
                masterDBHelper.setUserName(username);

                if (query[0].equalsIgnoreCase("group")) {
                    String groupBaseFolder = masterDBHelper.getTableAbsoluteRootFolerPath() +
                            Globals.WD360_USER_RESULTS_ROOT_FOLDER + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_BASE_FOLDER + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_GROUP_FOLDER + File.separator;
                    FileUtils.cleanDirectory(new File(groupBaseFolder));
                    String[] results = new String[Globals.RESULT_SIZE];
                    Map<String, Integer> groups = masterDBHelper.getAllGroupPics();
                    String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date());
                    String folderName = groupBaseFolder + timeStamp + File.separator;
                    File destination = Utility.createFolder(folderName);
                    if (null != destination) {
                        int idx = 0;
                        int index = 0;
                        for (Integer i : groups.values()) {
                            String srcFile = (String)groups.keySet().toArray()[index];
                            index += 1;
                            FileUtils.copyFile(new File(srcFile), new File(destination.getAbsoluteFile() + File.separator + Utility.getFileNameOnly(srcFile)), true);
                            results[idx++] = new File(destination.getAbsoluteFile() + File.separator + Utility.getFileNameOnly(srcFile)).getAbsolutePath();
                            if (idx >= Globals.RESULT_SIZE)
                                break;
                        }
                    }
                    MESSAGE = destination.getAbsolutePath();
                    return JSONUtils.formResponseValues("200",
                            username,
                            cmd_opcode,
                            "",
                            "",
                            "0",
                            MESSAGE,
                            results);
                } else if (query[0].equalsIgnoreCase("duplicate")) {
                    String duplicateBaseFolder = masterDBHelper.getTableAbsoluteRootFolerPath() +
                            Globals.WD360_USER_RESULTS_ROOT_FOLDER + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_BASE_FOLDER + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_DUPLICATE_FOLDER + File.separator;
                    FileUtils.cleanDirectory(new File(duplicateBaseFolder));
                    String[] results = new String[Globals.RESULT_SIZE];
                    String duplicateOptions = query[1];
                    List<String> duplicates = masterDBHelper.getAllDuplicateImages();
                    String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date());
                    String folderName = duplicateBaseFolder + timeStamp + File.separator;
                    File destination = Utility.createFolder(folderName);
                    if (null != destination) {
                        int idx = 0;
                        for (String duplicate : duplicates) {
                            if (duplicate.contains("yml")) {
                                continue;
                            }
                            if (duplicateOptions.contains("Near")) {
                                /*
                                 * TODO
                                 */
                            } else {
                                FileUtils.copyFile(new File(duplicate), new File(destination.getAbsoluteFile() + File.separator + Utility.getFileNameOnly(duplicate)));
                                results[idx++] = new File(destination.getAbsoluteFile() + File.separator + Utility.getFileNameOnly(duplicate)).getAbsolutePath();
                                if (idx >= Globals.RESULT_SIZE)
                                    break;
                            }
                        }
                    }
                    MESSAGE = destination.getAbsolutePath();
                    return JSONUtils.formResponseValues("200",
                            username,
                            cmd_opcode,
                            "",
                            "",
                            "0",
                            MESSAGE,
                            results);
                } else if (query[0].equalsIgnoreCase("dogs")) {
                    String dogBaseFolder = masterDBHelper.getTableAbsoluteRootFolerPath() +
                            Globals.WD360_USER_RESULTS_ROOT_FOLDER + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_BASE_FOLDER + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_DOG_FOLDER + File.separator;
                    FileUtils.cleanDirectory(new File(dogBaseFolder));
                    String[] results = new String[Globals.RESULT_SIZE];
                    List<String> dogImages = masterDBHelper.getAllDogPics();
                    String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date());
                    String folderName = dogBaseFolder + timeStamp + File.separator;
                    File destination = Utility.createFolder(folderName);
                    if (null != destination) {
                        int idx = 0;
                        for (String dogImage : dogImages) {
                            FileUtils.copyFile(new File(dogImage), new File(destination.getAbsoluteFile() + File.separator + Utility.getFileNameOnly(dogImage)), true);
                            results[idx++] = new File(destination.getAbsoluteFile() + File.separator + Utility.getFileNameOnly(dogImage)).getAbsolutePath();
                            if (idx >= Globals.RESULT_SIZE)
                                break;
                        }
                    }
                    MESSAGE = destination.getAbsolutePath();
                    return JSONUtils.formResponseValues("200",
                            username,
                            cmd_opcode,
                            "",
                            "",
                            "0",
                            MESSAGE,
                            results);
                } else if (query[0].equalsIgnoreCase("cats")) {
                    String catBaseFolder = masterDBHelper.getTableAbsoluteRootFolerPath() +
                            Globals.WD360_USER_RESULTS_ROOT_FOLDER + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_BASE_FOLDER + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_CAT_FOLDER + File.separator;
                    FileUtils.cleanDirectory(new File(catBaseFolder));
                    String[] results = new String[Globals.RESULT_SIZE];
                    List<String> catImages = masterDBHelper.getAllCatPics();
                    String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date());
                    String folderName =  catBaseFolder + timeStamp + File.separator;
                    File destination = Utility.createFolder(folderName);
                    if (null != destination) {
                        int idx = 0;
                        for (String catImage : catImages) {
                            FileUtils.copyFile(new File(catImage), new File(destination.getAbsoluteFile() + File.separator + Utility.getFileNameOnly(catImage)), true);
                            results[idx++] = new File(destination.getAbsoluteFile() + File.separator + Utility.getFileNameOnly(catImage)).getAbsolutePath();
                            if (idx >= Globals.RESULT_SIZE)
                                break;
                        }
                    }
                    MESSAGE = destination.getAbsolutePath();
                    return JSONUtils.formResponseValues("200",
                            username,
                            cmd_opcode,
                            "",
                            "",
                            "0",
                            MESSAGE,
                            results);
                } else if (query[0].equalsIgnoreCase("orientation")) {
                    String orientationBaseFolder = masterDBHelper.getTableAbsoluteRootFolerPath() +
                            Globals.WD360_USER_RESULTS_ROOT_FOLDER + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_BASE_FOLDER + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_ORIENTATION_FOLDER + File.separator;
                    FileUtils.cleanDirectory(new File(orientationBaseFolder));
                    String[] results = new String[Globals.RESULT_SIZE];
                    List<String> orientationImages = null;
                    String type = query[1];
                    if (type.equalsIgnoreCase("landscape")) {
                        orientationImages = masterDBHelper.getAllOrientationImages(true);
                    } else if (type.equalsIgnoreCase("portrait")) {
                        orientationImages = masterDBHelper.getAllOrientationImages(false);
                    } else {
                        return JSONUtils.formResponseValues("500",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "Incorrect query parameters for orientation, can be eithe one of this two - 'landscape' or 'portrait'",
                                Globals.emptyResponse);
                    }
                    String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date());
                    String folderName = orientationBaseFolder + timeStamp + File.separator;
                    File destination = Utility.createFolder(folderName);
                    if (null != destination) {
                        int idx = 0;
                        for (String orientationImage : orientationImages) {
                            FileUtils.copyFile(new File(orientationImage), new File(destination.getAbsoluteFile() + File.separator + Utility.getFileNameOnly(orientationImage)), true);
                            results[idx++] = new File(destination.getAbsoluteFile() + File.separator + Utility.getFileNameOnly(orientationImage)).getAbsolutePath();
                            if (idx >= Globals.RESULT_SIZE)
                                break;
                        }
                    }
                    MESSAGE = destination.getAbsolutePath();
                    return JSONUtils.formResponseValues("200",
                            username,
                            cmd_opcode,
                            "",
                            "",
                            "0",
                            MESSAGE,
                            results);
                } else if (query[0].equalsIgnoreCase("portrait")) {
                    String portraitBaseFolder = masterDBHelper.getTableAbsoluteRootFolerPath() +
                            Globals.WD360_USER_RESULTS_ROOT_FOLDER + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_BASE_FOLDER + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_PORTRAIT_FOLDER + File.separator;
                    FileUtils.cleanDirectory(new File(portraitBaseFolder));
                    String[] results = new String[Globals.RESULT_SIZE];
                    List<String> portraits = masterDBHelper.getAllPortraitPics();
                    String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date());
                    String folderName = portraitBaseFolder + timeStamp + File.separator;
                    File destination = Utility.createFolder(folderName);
                    if (null != destination) {
                        int idx = 0;
                        for (String portaitImage : portraits) {
                            FileUtils.copyFile(new File(portaitImage), new File(destination.getAbsoluteFile() + File.separator + Utility.getFileNameOnly(portaitImage)), true);
                            results[idx++] = new File(destination.getAbsoluteFile() + File.separator + Utility.getFileNameOnly(portaitImage)).getAbsolutePath();
                            if (idx >= Globals.RESULT_SIZE)
                                break;
                        }
                    }
                    MESSAGE = destination.getAbsolutePath();
                    return JSONUtils.formResponseValues("200",
                            username,
                            cmd_opcode,
                            "",
                            "",
                            "0",
                            MESSAGE,
                            results);
                } else if (query[0].equalsIgnoreCase("dimension")) {
                    String dimensionBaseFolder = masterDBHelper.getTableAbsoluteRootFolerPath() +
                            Globals.WD360_USER_RESULTS_ROOT_FOLDER + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_BASE_FOLDER + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_DIMENSION_FOLDER + File.separator;
                    FileUtils.cleanDirectory(new File(dimensionBaseFolder));
                    String[] results = new String[Globals.RESULT_SIZE];
                    List<String> dimImages = null;
                    query[1] = URLDecoder.decode(query[1], "UTF-8");
                    String[] items = query[1].split(":");
                    if (items.length == 2) {
                        dimImages = masterDBHelper.getAllDimensions(Integer.parseInt(items[0]), Integer.parseInt(items[1]));
                    } else if (items.length == 4) {
                        dimImages = masterDBHelper.getAllDimensions(Integer.parseInt(items[0]), Integer.parseInt(items[1]), Integer.parseInt(items[2]), Integer.parseInt(items[3]));
                    } else {
                        return JSONUtils.formResponseValues("500",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "Incorrect query parameters for dimension, can be eithe one of this two - 'minArea:maxArea' or 'minHeight:minWidth:maxHeight:maxWidth'",
                                Globals.emptyResponse);
                    }
                    String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date());
                    String folderName = dimensionBaseFolder + timeStamp + File.separator;
                    File destination = Utility.createFolder(folderName);
                    if (null != destination) {
                        int idx = 0;
                        for (String dimImage : dimImages) {
                            FileUtils.copyFile(new File(dimImage), new File(destination.getAbsoluteFile() + File.separator + Utility.getFileNameOnly(dimImage)), true);
                            results[idx++] = new File(destination.getAbsoluteFile() + File.separator + Utility.getFileNameOnly(dimImage)).getAbsolutePath();
                            if (idx >= Globals.RESULT_SIZE)
                                break;
                        }
                    }
                    MESSAGE = destination.getAbsolutePath();
                    return JSONUtils.formResponseValues("200",
                            username,
                            cmd_opcode,
                            "",
                            "",
                            "0",
                            MESSAGE,
                            results);
                } else if (query[0].equalsIgnoreCase("personnames")) {
                    String personnamesBaseFolder = masterDBHelper.getTableAbsoluteRootFolerPath() +
                            Globals.WD360_USER_RESULTS_ROOT_FOLDER + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_BASE_FOLDER + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_FACE_FOLDER + File.separator;
                    FileUtils.cleanDirectory(new File(personnamesBaseFolder));
                    String[] results = new String[Globals.RESULT_SIZE];
                    query[1] = URLDecoder.decode(query[1], "UTF-8");
                    String[] names = query[1].split(":");
                    List<String> images = masterDBHelper.getAllPicsOfTheseLabels(names);
                    String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date());
                    String folderName = personnamesBaseFolder + timeStamp + File.separator;
                    File destination = Utility.createFolder(folderName);
                    if (null != destination) {
                        int idx = 0;
                        for (String image : images) {
                            FileUtils.copyFile(new File(image), new File(destination.getAbsoluteFile() + File.separator + Utility.getFileNameOnly(image)), true);
                            results[idx++] = new File(destination.getAbsoluteFile() + File.separator + Utility.getFileNameOnly(image)).getAbsolutePath();
                            if (idx >= Globals.RESULT_SIZE)
                                break;
                        }
                    }
                    MESSAGE = destination.getAbsolutePath();
                    return JSONUtils.formResponseValues("200",
                            username,
                            cmd_opcode,
                            "",
                            "",
                            "0",
                            MESSAGE,
                            results);
                } else {
                    return JSONUtils.formResponseValues("500",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "Incorrect query parameter, can be either one of these - group, duplicate, cats, dogs, orientation, portrait, dimension, personnames",
                            Globals.emptyResponse);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return JSONUtils.formResponseValues("500",
                    "",
                    "",
                    "",
                    "",
                    "",
                    e.getMessage(),
                    Globals.emptyResponse);
        }
    }
}

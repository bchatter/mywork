package com.wdc.www.wd360;

import android.provider.BaseColumns;

/**
 * Created by Biswapratap Chatterjee on 7/3/2017.
 */

public final class MasterDBContract {
    private MasterDBContract() {}

    /* Inner class that defines the table contents */
    public static class MasterDBBaseColumns implements BaseColumns {
        public static final String COLUMN_NAME_FILE_ABSOLUTE_PATH = "absolutepath";
        public static final String COLUMN_NAME_DOC_INDEXER_OBJECT = "docIndexer";
        public static final String COLUMN_NAME_DOC_INDEXER_PATH = "docIndexerPath";
        public static final String COLUMN_NAME_TESS_BASE_API_OBJECT = "tessBaseApi";
        public static final String COLUMN_NAME_TESS_BASE_API_PATH = "tessBaseApiPath";
        public static final String COLUMN_NAME_GEOGRAPHY_LATITUDE = "latitude";
        public static final String COLUMN_NAME_GEOGRAPHY_LONGITIDE = "longitude";
        public static final String COLUMN_IMAGE_FACES = "isfaces";
        public static final String COLUMN_IMAGE_HAS_DOG = "isdog";
        public static final String COLUMN_IMAGE_HAS_CAT = "iscat";
        public static final String COLUMN_IMAGE_IS_A_GROUP_IMAGE = "isgroup";
        public static final String COLUMN_IMAGE_HEIGHT = "height";
        public static final String COLUMN_IMAGE_WIDTH = "width";
        public static final String COLUMN_IMAGE_AREA = "area";
        public static final String COLUMN_IMAGE_HASH_VALUE = "hashvalue";
        public static final String COLUMN_DOC_INDEXED = "isDocIndexed";
        public static final String COLUMN_IMAGE_FACE_TAGGED_AGE = "imageFaceTaggedAge";
    }
}
package com.wdc.www.wd360;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.wdc.www.wd360.MasterDBContract.MasterDBBaseColumns.*;

public class MasterDBHelper extends SQLiteOpenHelper {

    private static String TAG = MasterDBHelper.class.getSimpleName();
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "WD360Master.db";
    private String m_sql_create_entries = "";
    private String m_username = "";
    private SQLiteDatabase m_db = null;

    public MasterDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    synchronized public void setUserName(String username) {
        m_username = username;
    }

    synchronized private SQLiteDatabase getDBHandle() {
        return m_db;
    }

    synchronized private void openDB() {
        try {
            m_db = this.getWritableDatabase();
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    synchronized private void closeDB() {
        try {
            if (m_db.isOpen())
                m_db.close();
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    synchronized public void deleteTable() {
        try {
            openDB();
            getDBHandle().execSQL("DROP TABLE IF EXISTS " + m_username);
            closeDB();
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    @Nullable
    synchronized public List<String> getAllTableNames()
    {
        List<String> ret = null;
        try {
            openDB();
            ret = new ArrayList<String>();
            Cursor c = getDBHandle().rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name!='android_metadata' AND name!='databases' order by name", null);

            if (c.moveToFirst()) {
                while (!c.isAfterLast()) {
                    ret.add(c.getString(c.getColumnIndex("name")));
                    c.moveToNext();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public List<String> getAllDuplicateImages() {
        List<String> ret = new ArrayList<String>();
        try {
            openDB();
            String q = "SELECT " + COLUMN_NAME_FILE_ABSOLUTE_PATH +
                    ", COUNT(*) count FROM " + m_username + " GROUP BY " + COLUMN_IMAGE_HASH_VALUE + " HAVING count > 1";
            Cursor cursor = getDBHandle().rawQuery(q, null);
            // Iterate through cursor
            if(cursor.moveToFirst()) {
                do {
                    String absPath = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_FILE_ABSOLUTE_PATH));
                    ret.add(absPath);
                } while(cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public List<String> getAllCatPics() {
        List<String> ret = new ArrayList<String>();
        try {
            openDB();
            Cursor cursor = getDBHandle().rawQuery("select " + COLUMN_NAME_FILE_ABSOLUTE_PATH +
                    " from " + m_username + " where " + COLUMN_IMAGE_HAS_CAT + " > 0 AND " +
                    COLUMN_IMAGE_FACES + " = 'NA'", null);
            if (cursor.moveToFirst()) {
                do {
                    String absPath = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_FILE_ABSOLUTE_PATH));
                    ret.add(absPath);
                }
                while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public List<String> getAllDogPics() {
        List<String> ret = new ArrayList<String>();
        try {
            openDB();
            Cursor cursor = getDBHandle().rawQuery("select " + COLUMN_NAME_FILE_ABSOLUTE_PATH +
                    " from " + m_username + " where " + COLUMN_IMAGE_HAS_DOG + " > 0", null);
            if (cursor.moveToFirst()) {
                do {
                    String absPath = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_FILE_ABSOLUTE_PATH));
                    ret.add(absPath);
                }
                while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public Map<String, Integer> getAllGroupPics() {
        Map<String, Integer> ret = new HashMap<String, Integer>();
        try {
            openDB();
            Cursor cursor = getDBHandle().rawQuery("select " +
                    COLUMN_IMAGE_IS_A_GROUP_IMAGE + ", " + COLUMN_NAME_FILE_ABSOLUTE_PATH +
                    " from " + m_username + " where " + COLUMN_IMAGE_IS_A_GROUP_IMAGE + " > 2", null);
            if (cursor.moveToFirst()) {
                do {
                    String absPath = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_FILE_ABSOLUTE_PATH));
                    Integer faceCount = cursor.getInt(cursor.getColumnIndex(COLUMN_IMAGE_IS_A_GROUP_IMAGE));
                    ret.put(absPath, faceCount);
                }
                while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public List<String> getAllPicsOfTheseLabels(String[] labels) {
        List<String> ret = new ArrayList<String>();
        try {
            openDB();
            Cursor cursor = getDBHandle().rawQuery("select " +
                    COLUMN_IMAGE_FACES + ", " + COLUMN_NAME_FILE_ABSOLUTE_PATH +
                    " from " + m_username, null);
            if (cursor.moveToFirst()) {
                do {
                    String absPath = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_FILE_ABSOLUTE_PATH));
                    String faces = cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE_FACES));
                    for (int i = 0; i < labels.length; i++) {
                        if (faces.contains(labels[i])) {
                            ret.add(absPath);
                            break;
                        }
                    }
                }
                while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public List<String> getAllPortraitPics() {
        List<String> ret = new ArrayList<String>();
        try {
            openDB();
            Cursor cursor = getDBHandle().rawQuery("select " + COLUMN_NAME_FILE_ABSOLUTE_PATH +
                    " from " + m_username + " where " + COLUMN_IMAGE_IS_A_GROUP_IMAGE + " == 1", null);
            if (cursor.moveToFirst()) {
                do {
                    String absPath = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_FILE_ABSOLUTE_PATH));
                    ret.add(absPath);
                }
                while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public List<String> getAllOrientationImages(boolean flag) {
        List<String> ret = new ArrayList<String>();
        try {
            openDB();
            Cursor cursor = getDBHandle().rawQuery("SELECT " + COLUMN_NAME_FILE_ABSOLUTE_PATH + ", " + COLUMN_IMAGE_HEIGHT + ", " + COLUMN_IMAGE_WIDTH +
                    " FROM " + m_username, null);
            if (cursor.moveToFirst()) {
                do {
                    String absPath = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_FILE_ABSOLUTE_PATH));
                    int height = cursor.getInt(cursor.getColumnIndex(COLUMN_IMAGE_HEIGHT));
                    int width = cursor.getInt(cursor.getColumnIndex(COLUMN_IMAGE_WIDTH));
                    if (flag == true) {
                        if (width > height) {
                            ret.add(absPath);
                        }
                    } else {
                        if (width < height) {
                            ret.add(absPath);
                        }
                    }
                }
                while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public List<String> getAllDimensions(int minHeight, int minWidth, int maxHeight, int maxWidth) {
        List<String> ret = new ArrayList<String>();
        try {
            openDB();
            Cursor cursor = getDBHandle().rawQuery("SELECT " + COLUMN_NAME_FILE_ABSOLUTE_PATH +
                    " FROM " + m_username + " WHERE " +
                    COLUMN_IMAGE_HEIGHT + " >= " + minHeight + " AND " + COLUMN_IMAGE_WIDTH + " >= " + minWidth + " AND " +
                    COLUMN_IMAGE_HEIGHT + " <= " + maxHeight + " AND " + COLUMN_IMAGE_WIDTH + " <= " + maxWidth, null);
            if (cursor.moveToFirst()) {
                do {
                    String absPath = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_FILE_ABSOLUTE_PATH));
                    ret.add(absPath);
                }
                while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public List<String> getAllDimensions(int minArea, int maxArea) {
        List<String> ret = new ArrayList<String>();
        try {
            openDB();
            Cursor cursor = getDBHandle().rawQuery("SELECT " + COLUMN_NAME_FILE_ABSOLUTE_PATH +
                    " FROM " + m_username + " WHERE " +
                    COLUMN_IMAGE_AREA + " >= " + minArea + " AND " + COLUMN_IMAGE_AREA + " <= " + maxArea, null);
            if (cursor.moveToFirst()) {
                do {
                    String absPath = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_FILE_ABSOLUTE_PATH));
                    ret.add(absPath);
                }
                while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public Boolean isFileKnownToDB(String fileAbsPath) {
        Boolean ret = false;
        try {
            openDB();
            String q = "select * from " + m_username + " where " + COLUMN_NAME_FILE_ABSOLUTE_PATH + " = '" + fileAbsPath + "'";
            Cursor cursor = getDBHandle().rawQuery(q, null);
            int count = 0;
            if (cursor.moveToFirst()) {
                do {
                    int isIndexed = cursor.getInt(cursor.getColumnIndex(COLUMN_DOC_INDEXED));
                    if (isIndexed == 0) {
                        break;
                    }
                    count = count + 1;
                }
                while (cursor.moveToNext());
            }
            if (count > 0) {
                ret = true;
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public Boolean isFileAlreadyDocIndexed(String fileAbsPath) {
        Boolean ret = false;
        try {
            openDB();
            String q = "select " +
                    COLUMN_DOC_INDEXED +
                    " from " + m_username +
                    " where " + COLUMN_NAME_FILE_ABSOLUTE_PATH +
                    " = '" + fileAbsPath + "'";
            Cursor cursor = getDBHandle().rawQuery(q, null);
            int count = 0;
            if (cursor.moveToFirst()) {
                do {
                    int isIndexed = cursor.getInt(cursor.getColumnIndex(COLUMN_DOC_INDEXED));
                    if (isIndexed == 0) {
                        break;
                    }
                    count = count + 1;
                }
                while (cursor.moveToNext());
            }
            if (count > 0) {
                ret = true;
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public Boolean doesImageContainAnyFace(String fileAbsPath) {
        Boolean ret = false;
        try {
            openDB();
            String q = "select " +
                    COLUMN_IMAGE_IS_A_GROUP_IMAGE +
                    " from " + m_username +
                    " where " + COLUMN_NAME_FILE_ABSOLUTE_PATH +
                    " = '" + fileAbsPath + "'";
            Cursor cursor = getDBHandle().rawQuery(q, null);
            int count = 0;
            if (cursor.moveToFirst()) {
                do {
                    int numOfFaces = cursor.getInt(cursor.getColumnIndex(COLUMN_IMAGE_IS_A_GROUP_IMAGE));
                    if (numOfFaces <= 0) {
                        break;
                    }
                    count = count + 1;
                }
                while (cursor.moveToNext());
            }
            if (count > 0) {
                ret = true;
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public int getImageFileFaceTaggedCurrentAge(String fileAbsPath) {
        int age = 0;
        try {
            openDB();
            String q = "select " +
                    COLUMN_IMAGE_FACE_TAGGED_AGE +
                    " from " + m_username +
                    " where " + COLUMN_NAME_FILE_ABSOLUTE_PATH +
                    " = '" + fileAbsPath + "'";
            Cursor cursor = getDBHandle().rawQuery(q, null);
            if (cursor.moveToFirst()) {
                do {
                    age = cursor.getInt(cursor.getColumnIndex(COLUMN_IMAGE_FACE_TAGGED_AGE));
                }
                while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return age;
    }

    synchronized public Boolean isFileConstantsAlreadyTagged(String fileAbsPath) {
        Boolean ret = false;
        try {
            openDB();
            String q = "select " +
                    COLUMN_IMAGE_AREA +
                    " from " + m_username +
                    " where " + COLUMN_NAME_FILE_ABSOLUTE_PATH +
                    " = '" + fileAbsPath + "'";
            Cursor cursor = getDBHandle().rawQuery(q, null);
            int count = 0;
            if (cursor.moveToFirst()) {
                do {
                    int area = cursor.getInt(cursor.getColumnIndex(COLUMN_IMAGE_AREA));
                    if (area == 0) {
                        break;
                    }
                    count = count + 1;
                }
                while (cursor.moveToNext());
            }
            if (count > 0) {
                ret = true;
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public Boolean createTableForUser() {
        Boolean ret = false;
        try {
            openDB();
            m_sql_create_entries = "CREATE TABLE " + m_username + " (" +
                    COLUMN_NAME_FILE_ABSOLUTE_PATH + " TEXT," +
                    COLUMN_NAME_DOC_INDEXER_OBJECT + " BLOB," +
                    COLUMN_NAME_DOC_INDEXER_PATH + " TEXT," +
                    COLUMN_NAME_TESS_BASE_API_OBJECT + " BLOB," +
                    COLUMN_NAME_TESS_BASE_API_PATH + " TEXT," +
                    COLUMN_NAME_GEOGRAPHY_LATITUDE + " REAL," +
                    COLUMN_NAME_GEOGRAPHY_LONGITIDE + " REAL," +
                    COLUMN_IMAGE_FACES + " TEXT," +
                    COLUMN_IMAGE_HAS_DOG + " INTEGER," +
                    COLUMN_IMAGE_HAS_CAT + " INTEGER," +
                    COLUMN_IMAGE_IS_A_GROUP_IMAGE + " INTEGER," +
                    COLUMN_IMAGE_HEIGHT + " INTEGER," +
                    COLUMN_IMAGE_WIDTH + " INTEGER," +
                    COLUMN_IMAGE_AREA + " INTEGER," +
                    COLUMN_IMAGE_HASH_VALUE + " TEXT," +
                    COLUMN_DOC_INDEXED + " INTEGER," +
                    COLUMN_IMAGE_FACE_TAGGED_AGE + " INTEGER," +
                    " PRIMARY KEY(" + COLUMN_NAME_FILE_ABSOLUTE_PATH + "))";
            getDBHandle().execSQL(m_sql_create_entries);
            ret = true;
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    @Nullable
    synchronized public WD360TessBaseApi getTessBaseApi() {
        WD360TessBaseApi ret = null;
        try {
            openDB();
            Cursor cursor = getDBHandle().rawQuery("select " +
                    COLUMN_NAME_TESS_BASE_API_OBJECT +
                    " from " + m_username, null);
            if (cursor.moveToFirst()) {
                byte[] tessApi = cursor.getBlob(cursor.getColumnIndex(COLUMN_NAME_TESS_BASE_API_OBJECT));
                ret =  (WD360TessBaseApi)WD360Serializer.deserialize(tessApi);
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    @Nullable
    synchronized public WD360Indexer getDocIndexer() {
        WD360Indexer ret = null;
        try {
            openDB();
            Cursor cursor = getDBHandle().rawQuery("select " +
                    COLUMN_NAME_DOC_INDEXER_OBJECT +
                    " from " + m_username, null);
            if (cursor.moveToFirst()) {
                byte[] docidx = cursor.getBlob(cursor.getColumnIndex(COLUMN_NAME_DOC_INDEXER_OBJECT));
                ret =  (WD360Indexer)WD360Serializer.deserialize(docidx);
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    @Nullable
    synchronized public String getTableAbsoluteRootFolerPath() {
        String ret = null;
        try {
            openDB();
            Cursor cursor = getDBHandle().rawQuery("select " +
                    COLUMN_NAME_FILE_ABSOLUTE_PATH +
                    " from " + m_username, null);
            if (cursor.moveToFirst()) {
                ret =  cursor.getString(cursor.getColumnIndex(COLUMN_NAME_FILE_ABSOLUTE_PATH));
            }
        } catch (Exception e) {
            Log.i(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            if (e.getMessage().contains("no such table")) {
                if (true == createTableForUser()) {
                    ret = "CREATED";
                }
            }
        }
        closeDB();
        return ret;
    }

    synchronized public Boolean CheckIfTableAlreadyInDBorNot(String dbfield,
                                                             String fieldValue) {
        Boolean ret = false;
        try {
            openDB();
            String Query = "Select * from " + m_username + " where " + dbfield + " = '" + fieldValue + "'";
            Cursor cursor = getDBHandle().rawQuery(Query, null);
            if (cursor.getCount() == 1)
                ret = true;
            cursor.close();
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public int ItemsDocIndexedUntilNow() {
        int count = 0;
        try {
            openDB();
            String Query = "Select " + COLUMN_DOC_INDEXED + " from " + m_username;
            Cursor cursor = getDBHandle().rawQuery(Query, null);
            if (cursor.moveToFirst()) {
                do {
                    int isDocIndexed = cursor.getInt(cursor.getColumnIndex(COLUMN_DOC_INDEXED));
                    if (isDocIndexed == 1) {
                        count = count + 1;
                    }
                } while (cursor.moveToNext()) ;
            }
        } catch (Exception e) {
            count = 0;
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return count;
    }

    synchronized public int ItemsImageConstantIndexedUntilNow() {
        int count = 0;
        try {
            openDB();
            String Query = "Select " + COLUMN_IMAGE_AREA + " from " + m_username;
            Cursor cursor = getDBHandle().rawQuery(Query, null);
            if (cursor.moveToFirst()) {
                do {
                    int area = cursor.getInt(cursor.getColumnIndex(COLUMN_IMAGE_AREA));
                    if (area > 1) {
                        count = count + 1;
                    }
                } while (cursor.moveToNext()) ;
            }
        } catch (Exception e) {
            count = 0;
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return count;
    }

    synchronized public int ItemsImageFaceTaggedUntilNow() {
        int count = 0;
        try {
            openDB();
            String Query = "Select " + COLUMN_IMAGE_FACE_TAGGED_AGE + " from " + m_username;
            Cursor cursor = getDBHandle().rawQuery(Query, null);
            if (cursor.moveToFirst()) {
                do {
                    int age = cursor.getInt(cursor.getColumnIndex(COLUMN_IMAGE_FACE_TAGGED_AGE));
                    if (age > 0) {
                        count = count + 1;
                    }
                } while (cursor.moveToNext()) ;
            }
        } catch (Exception e) {
            count = 0;
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return count;
    }

    synchronized public void deleteRow(String absolutepath)
    {
        try {
            openDB();
            String q = "DELETE FROM " + m_username + " WHERE " + COLUMN_NAME_FILE_ABSOLUTE_PATH + " = '" + absolutepath + "'";
            getDBHandle().execSQL(q);
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
    }

    synchronized public Boolean incrementFaceTagAge(String absolutepath) {
        Boolean ret = false;
        try {
            int currentAge = getImageFileFaceTaggedCurrentAge(absolutepath);
            openDB();
            int newAge = currentAge + 1;
            String query = "UPDATE " + m_username + " SET " +
                    COLUMN_IMAGE_FACE_TAGGED_AGE + " = " + newAge +
                    " WHERE " + COLUMN_NAME_FILE_ABSOLUTE_PATH + " = '" + absolutepath + "'";
            getDBHandle().execSQL(query);
            ret = true;
        } catch (Exception e) {
            ret = false;
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public String getPredictedFacesFromDB(String absolutepath) {
        String ret = "";
        try {
            openDB();
            String Query = "select " + COLUMN_IMAGE_FACES + " from " +
                    m_username + " WHERE " + COLUMN_NAME_FILE_ABSOLUTE_PATH + " = '" + absolutepath + "'";
            Cursor cursor = getDBHandle().rawQuery(Query, null);
            if (cursor.moveToFirst()) {
                do {
                    ret = cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE_FACES));
                    if (false == ret.isEmpty()) {
                        break;
                    }
                } while (cursor.moveToNext()) ;
            }
        } catch (Exception e) {
            ret = "";
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public Boolean updateFace(String absolutepath, String new_faces) {
        Boolean ret = false;
        try {
            openDB();
            String query = "UPDATE " + m_username + " SET " +
                           COLUMN_IMAGE_FACES + " = '" + new_faces +
                           "' WHERE " + COLUMN_NAME_FILE_ABSOLUTE_PATH + " = '" + absolutepath + "'";
            getDBHandle().execSQL(query);
            ret = true;
        } catch (Exception e) {
            ret = false;
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public Boolean updateImageConstantValues(String absolutepath, /* This can not be updated */
                                                          double new_latitude,
                                                          double new_longitude,
                                                          int new_dog,
                                                          int new_cat,
                                                          int new_group,
                                                          int new_height,
                                                          int new_width,
                                                          int new_area,
                                                          String new_hashvalue) {
        Boolean ret = false;
        try {
            openDB();
            String query = "UPDATE " + m_username + " SET " +
                           COLUMN_NAME_GEOGRAPHY_LATITUDE + " = " + new_latitude + ", " +
                           COLUMN_NAME_GEOGRAPHY_LONGITIDE + " = " + new_longitude + ", " +
                           COLUMN_IMAGE_HAS_DOG + " = " + new_dog + ", " +
                           COLUMN_IMAGE_HAS_CAT + " = " + new_cat + ", " +
                           COLUMN_IMAGE_IS_A_GROUP_IMAGE + " = " + new_group + ", " +
                           COLUMN_IMAGE_HEIGHT + " = " + new_height + ", " +
                           COLUMN_IMAGE_WIDTH + " = " + new_width + ", " +
                           COLUMN_IMAGE_AREA + " = " + new_area + ", " +
                           COLUMN_IMAGE_HASH_VALUE + " = '" + new_hashvalue + "' " +
                           "WHERE " + COLUMN_NAME_FILE_ABSOLUTE_PATH + " = '" + absolutepath + "'";
            getDBHandle().execSQL(query);
            ret = true;
        } catch (Exception e) {
            ret = false;
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public Boolean insertRowIfNotExist(String absolutepath,
                                                    Object docIndexer,
                                                    String docIndexerPath,
                                                    Object tessBaseApi,
                                                    double latitude,
                                                    double longitude,
                                                    String faces,
                                                    int dog,
                                                    int cat,
                                                    int group,
                                                    int height,
                                                    int width,
                                                    int area,
                                                    String hashvalue,
                                                    int isDocIndexed,
                                                    int age) {
        Boolean ret = false;
        try {
            Boolean rslt = CheckIfTableAlreadyInDBorNot(COLUMN_NAME_FILE_ABSOLUTE_PATH, absolutepath);
            if (!rslt) {
                openDB();
                ContentValues values = new ContentValues();
                values.put(COLUMN_NAME_FILE_ABSOLUTE_PATH, absolutepath);
                if (null == docIndexer) {
                    values.putNull(COLUMN_NAME_DOC_INDEXER_OBJECT);
                } else {
                    byte[] docIdx = WD360Serializer.serialize(docIndexer);
                    values.put(COLUMN_NAME_DOC_INDEXER_OBJECT, docIdx);
                }
                if (null == tessBaseApi) {
                    values.putNull(COLUMN_NAME_TESS_BASE_API_OBJECT);
                } else {
                    byte[] tessApi = WD360Serializer.serialize(tessBaseApi);
                    values.put(COLUMN_NAME_TESS_BASE_API_OBJECT, tessApi);
                }
                if (null == docIndexerPath)
                    values.putNull(COLUMN_NAME_DOC_INDEXER_PATH);
                else
                    values.put(COLUMN_NAME_DOC_INDEXER_PATH, docIndexerPath);
                values.put(COLUMN_NAME_GEOGRAPHY_LATITUDE, latitude);
                values.put(COLUMN_NAME_GEOGRAPHY_LONGITIDE, longitude);
                if (null == faces)
                    values.putNull(COLUMN_IMAGE_FACES);
                else
                    values.put(COLUMN_IMAGE_FACES, faces);
                values.put(COLUMN_IMAGE_HAS_DOG, dog);
                values.put(COLUMN_IMAGE_HAS_CAT, cat);
                values.put(COLUMN_IMAGE_IS_A_GROUP_IMAGE, group);
                values.put(COLUMN_IMAGE_HEIGHT, height);
                values.put(COLUMN_IMAGE_WIDTH, width);
                values.put(COLUMN_IMAGE_AREA, area);
                if (null == hashvalue)
                    values.putNull(COLUMN_IMAGE_HASH_VALUE);
                else
                    values.put(COLUMN_IMAGE_HASH_VALUE, hashvalue);
                values.put(COLUMN_DOC_INDEXED, isDocIndexed);
                values.put(COLUMN_IMAGE_FACE_TAGGED_AGE, age);

                getDBHandle().insert(m_username, null, values);
                ret = true;
            } else {
                ret = true;
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
package com.wdc.www.wd360;



/**
 * Created by Biswapratap Chatterjee on 7/11/2017.
 */

public class ExecuteGetJobPercentageCommand {
    private static String TAG = ExecuteGetJobPercentageCommand.class.getSimpleName();

    public static String executeGetJobPercentage(String[] keyValues, String cmd_opcode) {
        try {
            String ret = "";
            String username = "";
            ret = JSONUtils.commandSpecificChecks(keyValues, cmd_opcode, 2);
            if (ret.contains("ASSERT"))
                return ret;
            else {
                username = Globals.myCloudUserId;
                String[] jobid = keyValues[2].split("=");
                if (!jobid[0].equalsIgnoreCase("jobid"))
                    return JSONUtils.formResponseValues("400",
                            "",
                            cmd_opcode,
                            "",
                            "",
                            "",
                            "Username key name is wrong, use 'jobid' for Job ID",
                            Globals.emptyResponse);
                String match = username + "_" + jobid[1];
                int percent = Globals.user_job_map.get(match).getPercent();
                String results [] = {};
                if (percent == 100)
                    results = Globals.user_job_map.get(match).getResults();
                return JSONUtils.formResponseValues("202",
                        username,
                        cmd_opcode,
                        String.valueOf(jobid),
                        "InProgress",
                        "percent",
                        percent + "% completed for Job ID : " + jobid[1],
                        results);

            }
        } catch (Exception e) {
Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return JSONUtils.formResponseValues("500",
                    "",
                    "",
                    "",
                    "",
                    "",
                    e.getMessage(),
                    Globals.emptyResponse);
        }
    }
}

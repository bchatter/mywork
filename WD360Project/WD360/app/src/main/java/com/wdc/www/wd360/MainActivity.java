package com.wdc.www.wd360;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent startup_intent = new Intent(this, StartupService.class);
        startup_intent.putExtra("MyCloudId", "bpc"); // dummy mycloud user id
        this.startService(startup_intent);
    }
}

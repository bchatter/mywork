package com.wdc.www.wd360;



import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Map;

/**
 * Created by Biswapratap Chatterjee on 7/11/2017.
 */

public class ExecuteImageTrainUploadCommand {
    private static String TAG = ExecuteImageTrainUploadCommand.class.getSimpleName();

    private static String checkIfPersonFaceAlreadyInTrainedList(WD360FaceDetection tmpWDFD, String[] faces) {
        String ret = "";
        try {
            Map<Integer, String> m_trainedData = tmpWDFD.getTrainedData();
            for (String face : m_trainedData.values()) {
                if (0 == face.compareTo(faces[0])) {
                    ret = faces[0];
                    break;
                }
                if (0 == face.compareTo(faces[1])) {
                    ret = faces[1];
                    break;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            ret = "";
        }
        return ret;
    }

    private static int getLabel(WD360FaceDetection tmpWDFD) {
        int ret = -1;
        try {
            Map<Integer, String> m_trainedData = tmpWDFD.getTrainedData();
            if (null == m_trainedData) {
                return 1;
            }
            int last_index = m_trainedData.values().size();
            if (last_index == 0) {
                return 1;
            }
            int latest_label = (Integer) m_trainedData.keySet().toArray()[last_index - 1];
            ret = latest_label + 1;
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            ret = -1;
        }
        return ret;
    }

    private static void appendEntryToFaceTrainCSVFile(String csvFilePath,
                                                      String filepath,
                                                      Integer label) {
        PrintWriter out = null;
        try {
            out = new PrintWriter(new FileOutputStream(new File(csvFilePath), true));
            out.append(filepath + ";" + label + Utility.newline);
            out.close();
        } catch (FileNotFoundException e) {
            if (null != out)
                out.close();
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    public static String executeImageTraining(Map<String, String> files,
                                              Map<String, String> filenames) {
        try {
            String[] split = ((String) filenames.values().toArray()[0]).split("_");
            if (split.length != 2) {
                Log.e(TAG, "Training set should be in this format <facename>_<index>.<extension>.jpg");
                return JSONUtils.formResponseValues("500",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "Training set should be in this format <facename>_<index>.<extension>.jpg",
                        Globals.emptyResponse);
            }
            MasterDBHelper masterDBHelper = new MasterDBHelper(Globals.gContext);
            masterDBHelper.setUserName(Globals.myCloudUserId);
            String absPath = masterDBHelper.getTableAbsoluteRootFolerPath();
            if (null == absPath) {
                Log.e(TAG, Globals.myCloudUserId + " is not a registered user");
                return JSONUtils.formResponseValues("500",
                        "",
                        "",
                        "",
                        "",
                        "",
                        Globals.myCloudUserId + " is not a registered user",
                        Globals.emptyResponse);
            }

            ServerBusy.setServerStatus(true);

            File userFaceTrainingFolder = Utility.createFolder(
                    absPath + Globals.USER_FACE_TRAINING_FOLDER + File.separator);
            if (null == userFaceTrainingFolder) {
                Log.e(TAG, "Failed to create user face training folder for " + Globals.myCloudUserId);
                return JSONUtils.formResponseValues("500",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "Failed to create user face training folder for " + Globals.myCloudUserId,
                        Globals.emptyResponse);
            }

            String[] faces = new String[2];
            Boolean update = false;

            if (filenames.size() > 0) {
                int count = 0;
                for (String value : filenames.values()) {
                    String temp = value.split("_")[0];
                    if (count == 0) {
                        faces[0] = temp;
                        count += 1;
                    } else if ((count == 1) && (0 != faces[0].compareTo(temp))) {
                        faces[count] = temp;
                        count += 1;
                    }
                }

                if (count != 2) {
                    Log.e(TAG, "Training set should have only two unique person names, found " + count);
                    return JSONUtils.formResponseValues("500",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "Training set should have two unique person names, found " + count,
                            Globals.emptyResponse);
                }

                WD360FaceDetection tmpWDFD = Utility.getTempWDFD(absPath, true);
                String ret = checkIfPersonFaceAlreadyInTrainedList(tmpWDFD, faces);

                if (!ret.isEmpty()) {
                    Log.e(TAG, "One of the faces in the training set [" + ret + "] is already trained, send new training set with two un-trained unique faces");
                    return JSONUtils.formResponseValues("500",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "One of the faces in the training set [" + ret + "] is already trained, send new training set with two un-trained unique faces",
                            Globals.emptyResponse);
                }

                String prefix = "";
                int lexicalDist_0_1 = faces[0].compareTo(faces[1]);
                int lexicalDist_1_0 = faces[1].compareTo(faces[0]);

                if (lexicalDist_0_1 < lexicalDist_1_0) {
                    prefix = faces[0] + "_" + faces[1];
                } else {
                    prefix = faces[1] + "_" + faces[0];
                }

                String csvfileUrl = prefix + ".csv";
                if (new File(csvfileUrl).exists()) {
                    Log.e(TAG, "WD360 is already trained for : " + csvfileUrl);
                    return JSONUtils.formResponseValues("500",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "WD360 is already trained for : " + csvfileUrl,
                            Globals.emptyResponse);
                }

                File yml = new File(absPath +
                        Globals.WD360_FACE_RECOGNIZER_MODEL_FOLDER + File.separator +
                        Globals.WD360_FACE_RECOGNIZER_MODEL_YML_FILE_NAME);

                if (null != yml) {
                    if (yml.exists()) {
                        update = true;
                    }

                    Boolean face2 = false;
                    File csvFile = Utility.getWritableFile(userFaceTrainingFolder, csvfileUrl);

                    for (int i = 0; i < filenames.size(); i++) {
                        String key1 = (String)filenames.keySet().toArray()[i];
                        if ((null != key1) && (key1.length() <= 0))
                            continue;
                        String value1 = (String)filenames.values().toArray()[i];
                        if ((null != value1) && (value1.length() <= 0))
                            continue;
                        if (value1.contains(faces[0])) {
                            face2 = false;
                        }
                        if (value1.contains(faces[1])) {
                            face2 = true;
                        }
                        String value2 = files.get(key1);
                        if ((null != value2) && (value2.length() <= 0))
                            continue;
                        String newFN = value1;
                        if ((null != newFN) && (newFN.length() <= 0))
                            continue;
                        File thisFaceTrainingFlder = Utility.createFolder(userFaceTrainingFolder.getAbsolutePath() + File.separator + prefix + File.separator);
                        if (null == thisFaceTrainingFlder)
                            continue;
                        File f = Utility.getWritableFile(thisFaceTrainingFlder, newFN);
                        if (null == f)
                            continue;
                        if (false == Utility.writeContentToFile(f, new FileInputStream(value2)))
                            continue;
                        int label = -1;
                        if (face2) {
                            label = getLabel(tmpWDFD) + 1;
                        } else {
                            label = getLabel(tmpWDFD);
                        }
                        appendEntryToFaceTrainCSVFile(csvFile.getAbsolutePath(), f.getAbsolutePath(), label);
                    }

                    tmpWDFD.release();

                    if (update == true) {
                        WD360FaceDetection wdfd = new WD360FaceDetection(
                                Globals.FACE_RECOGNIZER_TYPE_LBPH,
                                absPath + Globals.WD360_NATIVE_LOG_FOLDER + File.separator + "WD360.log",
                                absPath + Globals.WD360_ETC_FOLDER + File.separator + Globals.WD360_FACE_DETECTOR_XML_FILE_NAME,
                                absPath + Globals.WD360_ETC_FOLDER + File.separator + Globals.WD360_FACE_DETECTOR_DOG_XML_FILE_NAME,
                                absPath + Globals.WD360_ETC_FOLDER + File.separator + Globals.WD360_FACE_DETECTOR_CAT_XML_FILE_NAME,
                                absPath + Globals.WD360_FACE_RECOGNIZER_MODEL_FOLDER,
                                1
                        );
                        wdfd.update(csvFile.getAbsolutePath());
                        wdfd.release();
                    } else {
                        WD360FaceDetection wdfd = new WD360FaceDetection(
                                Globals.FACE_RECOGNIZER_TYPE_LBPH,
                                absPath + Globals.WD360_NATIVE_LOG_FOLDER + File.separator + "WD360.log",
                                absPath + Globals.WD360_ETC_FOLDER + File.separator + Globals.WD360_FACE_DETECTOR_XML_FILE_NAME,
                                absPath + Globals.WD360_ETC_FOLDER + File.separator + Globals.WD360_FACE_DETECTOR_DOG_XML_FILE_NAME,
                                absPath + Globals.WD360_ETC_FOLDER + File.separator + Globals.WD360_FACE_DETECTOR_CAT_XML_FILE_NAME,
                                absPath + Globals.WD360_FACE_RECOGNIZER_MODEL_FOLDER,
                                0
                        );
                        wdfd.train(csvFile.getAbsolutePath());
                        wdfd.release();
                    }
                } else {
                    ServerBusy.setServerStatus(false);
                    Log.e(TAG, "Failed to create user face training csv file for " + Globals.myCloudUserId);
                    return JSONUtils.formResponseValues("500",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "Failed to create user face training csv file for " + Globals.myCloudUserId,
                            Globals.emptyResponse);
                }
            } else {
                ServerBusy.setServerStatus(false);
                Log.e(TAG, "empty set of files names provided");
                return JSONUtils.formResponseValues("500",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "empty set of files names provided",
                        Globals.emptyResponse);
            }

            FileUtils.deleteQuietly(Globals.gContext.getCacheDir());
            WD360FaceDetection w = Utility.getTempWDFD(absPath, true);
            int tf = w.getTrainedData().size();
            APP_CONTEXT_DATA.incrementFaceTrainingAge();
            ServerBusy.setServerStatus(false);
            return JSONUtils.formResponseValues("200",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "Faces trained successfully",
                    Globals.emptyResponse);
        } catch (Exception e) {
            ServerBusy.setServerStatus(false);
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return JSONUtils.formResponseValues("500",
                    "",
                    "",
                    "",
                    "",
                    "",
                    e.getMessage(),
                    Globals.emptyResponse);
        }
    }
}
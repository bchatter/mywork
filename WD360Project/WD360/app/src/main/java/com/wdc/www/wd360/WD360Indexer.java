package com.wdc.www.wd360;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by Biswapratap Chatterjee on 7/18/2017.
 */

public class WD360Indexer extends Indexer implements Serializable {

    public WD360Indexer() {

    }

    public WD360Indexer(String indexDir) throws IOException {
        super(indexDir);
    }

    public void setIndexDir(String indexDir) throws IOException {
        setIndexDirectory(indexDir);
    }
}

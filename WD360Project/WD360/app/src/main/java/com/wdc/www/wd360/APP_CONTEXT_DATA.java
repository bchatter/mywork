package com.wdc.www.wd360;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;

/**
 * Created by Biswapratap Chatterjee on 8/16/2017.
 */

public class APP_CONTEXT_DATA {
    static int m_faceTrainingAge = 0;
    private static String TAG = APP_CONTEXT_DATA.class.getSimpleName();

    synchronized public static int getFaceTrainingAge() {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(Globals.WD360_FACE_TRAINING_AGE_INFO));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            String data = sb.toString();
            m_faceTrainingAge = Integer.parseInt(data);
        } catch (Exception e) {
            Log.i(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            PrintWriter out = null;
            try {
                out = new PrintWriter(Globals.WD360_FACE_TRAINING_AGE_INFO);
            } catch (Exception e1) {
                Log.e(TAG, Utility.getExceptionDetailedInfo(e1, TAG) + " : " + e1.getMessage());
            }
            m_faceTrainingAge = 1;
            out.println(m_faceTrainingAge);
            out.close();
            if (null != br)
                try {
                    br.close();
                } catch (Exception e2) {
                    Log.e(TAG, Utility.getExceptionDetailedInfo(e2, TAG) + " : " + e2.getMessage());
                }
        }

        if (null != br)
            try {
                br.close();
            } catch (Exception e3) {
                Log.e(TAG, Utility.getExceptionDetailedInfo(e3, TAG) + " : " + e3.getMessage());
            }

        return m_faceTrainingAge;
    }

    synchronized public static void incrementFaceTrainingAge() {
        try {
            m_faceTrainingAge = m_faceTrainingAge + 1;
            String faceTrainingAgeInfo = Globals.WD360_FACE_TRAINING_AGE_INFO;
            PrintWriter out = new PrintWriter(faceTrainingAgeInfo);
            out.println(String.valueOf(m_faceTrainingAge));
            out.close();
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }
}

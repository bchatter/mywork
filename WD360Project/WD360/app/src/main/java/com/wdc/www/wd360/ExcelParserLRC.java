package com.wdc.www.wd360;

import org.apache.poi.hssf.extractor.ExcelExtractor;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.concurrent.Callable;

/**
 * Created by Biswapratap Chatterjee on 6/28/2017.
 */

public class ExcelParserLRC implements Callable<IndexItem> {
    private final File f;

    public ExcelParserLRC(File file) {
        this.f = file;
    }

    public IndexItem call() {
        IndexItem idxItm = new IndexItem((long)f.getName().hashCode(), f.getAbsolutePath(), "");
        try {
            InputStream inp = new FileInputStream(f);
            HSSFWorkbook wb = new HSSFWorkbook(new POIFSFileSystem(inp));
            ExcelExtractor extractor = new ExcelExtractor(wb);
            extractor.setFormulasNotResults(true);
            extractor.setIncludeSheetNames(false);
            String content = extractor.getText();
            idxItm.setContent(content);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return idxItm;
    }
}
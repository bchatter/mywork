package com.wdc.www.wd360;

/**
 * Created by Biswapratap Chatterjee on 7/5/2017.
 */

public class Job {
    private int m_jobid;
    private WD360PercentMaker m_percentmaker;

    Job(WD360PercentMaker percentmaker, int jobid) {
        m_percentmaker = percentmaker;
        m_jobid = jobid;
    }

    int getPercent() {
        return m_percentmaker.getPercent();
    }

    String [] getResults() { return m_percentmaker.getResults(); }

    int getJobId() {
        return m_jobid;
    }
}

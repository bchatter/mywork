package com.wdc.www.wd360;

import org.apache.poi.hdgf.extractor.VisioTextExtractor;

import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.Callable;

/**
 * Created by Biswapratap Chatterjee on 7/3/2017.
 */

public class VisioParserLRC  implements Callable<IndexItem> {
    private final File f;

    public VisioParserLRC(File file) {
        this.f = file;
    }

    public IndexItem call() {
        IndexItem idxItm = new IndexItem((long)f.getName().hashCode(), f.getAbsolutePath(), "");
        try {
            VisioTextExtractor vte = new VisioTextExtractor(new FileInputStream(f));
            String content = vte.getText();
            idxItm.setContent(content);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return idxItm;
    }
}
package com.wdc.www.wd360;

import android.os.FileObserver;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by Biswapratap Chatterjee on 7/14/2017.
 */

public class RecursiveFileObserver extends FileObserver {
    public final String TAG = RecursiveFileObserver.class.getSimpleName();
    public static int MASK = MODIFY | CLOSE_WRITE
            | MOVED_FROM | MOVED_TO | DELETE | CREATE
            | DELETE_SELF | MOVE_SELF;

    List<SingleFileObserver> mObservers;
    String mPath;
    int mMask;


    public RecursiveFileObserver(String path) {
        super(path, MASK);
        mPath = path;
        mMask = MASK;
    }

    @Override
    public void startWatching() {
        if (mObservers != null) return;
        mObservers = new ArrayList<SingleFileObserver>();
        Stack<String> stack = new Stack<String>();
        stack.push(mPath);

        while (stack.size() != 0) {
            String parent = stack.pop();
            if (parent.contains(Globals.myCloudUserId)) {
                mObservers.add(new SingleFileObserver(parent, mMask));
            }
            File path = new File(parent);
            File[] files = path.listFiles();
            if (files == null) continue;
            for (int i = 0; i < files.length; ++i) {
                if (files[i].isDirectory() &&
                    (false == files[i].getAbsolutePath().contains(Globals.TESS_DATA_FOLDER)) &&
                    (false == files[i].getAbsolutePath().contains(Globals.DOC_INDEXER_FOLDER)) &&
                    (false == files[i].getAbsolutePath().contains(Globals.WD360_ETC_FOLDER)) &&
                    (false == files[i].getAbsolutePath().contains(Globals.WD360_NATIVE_LOG_FOLDER)) &&
                    (false == files[i].getAbsolutePath().contains(Globals.WD360_PRELOADED_FACE_TRAINING_FOLDER)) &&
                    (false == files[i].getAbsolutePath().contains(Globals.WD360_FACE_RECOGNIZER_MODEL_FOLDER)) &&
                    (false == files[i].getAbsolutePath().contains(Globals.USER_FACE_TRAINING_FOLDER)) &&
                    (false == files[i].getAbsolutePath().contains(Globals.WD360_USER_RESULTS_ROOT_FOLDER)) &&
                    (false == files[i].getAbsolutePath().contains(Globals.WD360_IGNORE_CODE_CACHE_FOLDER)) &&
                    (false == files[i].getAbsolutePath().contains(Globals.WD360_IGNORE_DATABASES_FOLDER)) &&
                    (false == files[i].getAbsolutePath().contains(Globals.WD360_IGNORE_LIB_FOLDER)) &&
                    (false == files[i].getAbsolutePath().contains("Android")) &&
                    (false == files[i].getAbsolutePath().contains("Alarms")) &&
                    (false == files[i].getAbsolutePath().contains("DCIM")) &&
                    (false == files[i].getAbsolutePath().contains("Download")) &&
                    (false == files[i].getAbsolutePath().contains("Movies")) &&
                    (false == files[i].getAbsolutePath().contains("Music")) &&
                    (false == files[i].getAbsolutePath().contains("Notifications")) &&
                    (false == files[i].getAbsolutePath().contains("Pictures")) &&
                    (false == files[i].getAbsolutePath().contains("Podcasts")) &&
                    (false == files[i].getAbsolutePath().contains("Ringtones")) &&
                    (false == files[i].getAbsolutePath().equals(".")) &&
                    (false == files[i].getAbsolutePath().equals(".."))) {
                    stack.push(files[i].getAbsolutePath());
                }
            }
        }

        for (int i = 0; i < mObservers.size(); i++) {
            mObservers.get(i).startWatching();
            Log.d(TAG, "Started watching # folder : " + mObservers.get(i).getPath());
        }
    }

    @Override
    public void stopWatching() {
        if (mObservers == null) return;

        for (int i = 0; i < mObservers.size(); ++i)
            mObservers.get(i).stopWatching();

        mObservers.clear();
        mObservers = null;
    }

    private void removeWatching(String file) {
        for (FileObserver fo : mObservers) {
            if (0 == ((SingleFileObserver)fo).getPath().compareTo(file)) {
                fo.stopWatching();
                Log.d(TAG, "Started watching folder : " + file);
                mObservers.remove(fo);
                File path = new File(file);
                File[] files = path.listFiles();
                if (files == null) return;
                for (int i = 0; i < files.length; ++i) {
                    if (files[i].isDirectory() &&
                            (false == files[i].getName().equals(".")) &&
                            (false == files[i].getName().equals(".."))) {
                        removeWatching(files[i].getPath());
                    }
                }
            }
        }
    }
/*
    private void addWatching(String file) {
        SingleFileObserver sfo = new SingleFileObserver(file, mMask);
        mObservers.add(sfo);
        sfo.startWatching();
        Log.d(TAG, "Started watching folder : " + file);
        File path = new File(file);
        File[] files = path.listFiles();
        if (files == null) return;
        for (int i = 0; i < files.length; ++i) {
            if (files[i].isDirectory() &&
                    !files[i].getName().equals(".") &&
                    !files[i].getName().equals("..")) {
                addWatching(files[i].getPath());
            }
        }
    }

    synchronized public void observerPutItem(String un,
                                             String path,
                                             WD360Indexer indexer,
                                             WD360TessBaseApi tessapi) {
        try {
            Globals.OBSERVER_MINER_QUEUE.put(new QueueItem(un, path, indexer, tessapi));
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }
*/
    private void processEvent(int event, String file) {
        String userName = "";
        int beginIndex = file.indexOf(Globals.ROOT_FOLDER_NAME) + Globals.ROOT_FOLDER_NAME.length();
        int endIndex = file.indexOf('/', beginIndex);
        if (-1 == endIndex) {
            userName = file.substring(beginIndex);
        } else {
            userName = file.substring(beginIndex, endIndex);
        }
        String item = "";
        if (event == FileObserver.CREATE && !file.equals(".probe")) {
            /*
            if (new File(file).isDirectory()) {
                addWatching(file);
                return;
            } else {
                Log.d(TAG, "File created [" + file + "]");
                MasterDBHelper mDBHlpr = new MasterDBHelper(Globals.gContext);
                mDBHlpr.setUserName(userName);
                String absUserRootPath = mDBHlpr.getTableAbsoluteRootFolerPath();
                WD360TessBaseApi tessBaseAPI = mDBHlpr.getTessBaseApi();
                WD360Indexer indexer = mDBHlpr.getDocIndexer();
                tessBaseAPI.init(absUserRootPath, Globals.TRAINING_LANGUAGE);
                try {
                    indexer.setIndexDir(absUserRootPath + Globals.DOC_INDEXER_FOLDER);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                observerPutItem(userName, file, indexer, tessBaseAPI);
            }
            */
        } else if ((event == FileObserver.DELETE || event == FileObserver.DELETE_SELF) && !file.equals(".probe")) {
            MasterDBHelper mDBHelper = new MasterDBHelper(Globals.gContext);
            mDBHelper.setUserName(userName);
            if
             (new File(file).isDirectory()) {
                removeWatching(file);
                return;
            } else {
                Log.d(TAG, "File deleted [" + file + "]");
                mDBHelper.deleteRow(file);
            }
        } else if (event == FileObserver.MODIFY && !file.equals(".probe")) {
            /*
            if (new File(file).isDirectory()) {
                addWatching(file);
                return;
            } else {
                Log.d(TAG, "File modified [" + file + "]");
                MasterDBHelper mDBHlpr = new MasterDBHelper(Globals.gContext);
                mDBHlpr.setUserName(userName);
                String absUserRootPath = mDBHlpr.getTableAbsoluteRootFolerPath();
                WD360TessBaseApi tessBaseAPI = mDBHlpr.getTessBaseApi();
                WD360Indexer indexer = mDBHlpr.getDocIndexer();
                tessBaseAPI.init(absUserRootPath, Globals.TRAINING_LANGUAGE);
                try {
                    indexer.setIndexDir(absUserRootPath + Globals.DOC_INDEXER_FOLDER);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                observerPutItem(userName, file, indexer, tessBaseAPI);
            }
            */
        } else if (!file.equals(".probe")) {
            // TODO
        }
    }

    @Override
    public void onEvent(int event, String file) {
        processEvent(event, file);
    }

    private class SingleFileObserver extends FileObserver {
        private String mPath;

        public SingleFileObserver(String path, int mask) {
            super(path, mask);
            mPath = path;
        }

        String getPath() {
            return mPath;
        }

        @Override
        public void onEvent(int event, String path) {
            event &= ALL_EVENTS;
            String newPath = mPath + "/" + path;

            if (false == newPath.contains(Globals.myCloudUserId)) {
                Log.d(TAG, "Ignoring events on [" + newPath + "]");
                return;
            }

            if ((newPath.contains(Globals.DOC_INDEXER_FOLDER)) ||
                (newPath.contains(Globals.TESS_DATA_FOLDER)) ||
                (newPath.contains(Globals.WD360_ETC_FOLDER)) ||
                (newPath.contains(Globals.WD360_NATIVE_LOG_FOLDER)) ||
                (newPath.contains(Globals.WD360_PRELOADED_FACE_TRAINING_FOLDER)) ||
                (newPath.contains(Globals.WD360_USER_RESULTS_ROOT_FOLDER)) ||
                (newPath.contains(Globals.USER_FACE_TRAINING_FOLDER)) ||
                (newPath.contains(Globals.WD360_IGNORE_CODE_CACHE_FOLDER)) &&
                (newPath.contains(Globals.WD360_IGNORE_DATABASES_FOLDER)) &&
                (newPath.contains(Globals.WD360_IGNORE_LIB_FOLDER)) &&
                (newPath.contains(Globals.WD360_FACE_RECOGNIZER_MODEL_FOLDER))) {
                Log.d(TAG, "Ignoring events on [" + newPath + "]");
                return;
            }
            RecursiveFileObserver.this.onEvent(event, newPath);
        }
    }
}

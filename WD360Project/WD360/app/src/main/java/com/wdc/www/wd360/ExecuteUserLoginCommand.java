package com.wdc.www.wd360;

import android.support.annotation.Nullable;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Biswapratap Chatterjee on 7/11/2017.
 */

public class ExecuteUserLoginCommand {
    private static String TAG = ExecuteUserLoginCommand.class.getSimpleName();
    private static List<File> folders = new ArrayList<File>();
    private static File docIndexerDir = null;
    private static File dataDir = null;
    private static File tessLangTrainedDataFile = null;

    private static void appendEntryToFaceTrainCSVFile(String csvFilePath,
                                                      String filepath,
                                                      Integer label) {
        PrintWriter out = null;
        try {
            out = new PrintWriter(new FileOutputStream(new File(csvFilePath), true));
            out.append(filepath + ";" + label + Utility.newline);
            out.close();
        } catch (FileNotFoundException e) {
            if (null != out)
                out.close();
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    public static void putSampleDataSet(String userdir) {
        try {
            String[] FILES = Globals.gContext.getAssets().list(Globals.ASSET_SAMPLE_DATASET_SET);
            File sampleDataSetFolder = Utility.createFolder(userdir + File.separator + Globals.ASSET_SAMPLE_DATASET_SET);
            if (null != sampleDataSetFolder) {
                folders.add(sampleDataSetFolder);
            }
            for (int i = 0; i < FILES.length; i++) {
                File FF = Utility.putFileFromAsset(Globals.ASSET_SAMPLE_DATASET_SET,
                        FILES[i], sampleDataSetFolder, "");
            }
        } catch (Exception e) {
            Log.e(TAG, "Rolling Back!");
            for (File folder : folders) {
                if (null != folder) {
                    folder.delete();
                }
            }
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    public static Boolean createPreLoadedFaceTrainingInfrastructure(String userdir) {
        Boolean ret = true;
        try {
            Map<String, Integer> csvData = new HashMap<String, Integer>();
            File yml = new File(userdir + File.separator + Globals.WD360_FACE_RECOGNIZER_MODEL_FOLDER +
                    File.separator + Globals.WD360_FACE_RECOGNIZER_MODEL_YML_FILE_NAME);

            String[] FOLDERS = Globals.gContext.getAssets().list(Globals.ASSET_PRELOADED_FACE_TRAINING_SET);
            if (FOLDERS.length == 2) {
                File preLoadedFaceTrainingFolder = Utility.createFolder(userdir + File.separator + Globals.WD360_PRELOADED_FACE_TRAINING_FOLDER);
                if (null != preLoadedFaceTrainingFolder) {
                    folders.add(preLoadedFaceTrainingFolder);
                }
                for (int i = 0; i < FOLDERS.length; i++) {
                    File preLoadedFaceTrainingSubFolder = Utility.createFolder(
                            preLoadedFaceTrainingFolder.getAbsolutePath() + File.separator + FOLDERS[i]);
                    if (null != preLoadedFaceTrainingSubFolder) {
                        folders.add(preLoadedFaceTrainingSubFolder);
                    }
                    String[] files = Globals.gContext.getAssets().list(Globals.ASSET_PRELOADED_FACE_TRAINING_SET + File.separator + FOLDERS[i]);
                    for (int j = 0; j < files.length; j++) {
                        String f = FOLDERS[i] + "_" + (j + 1) + "." + files[j].substring(files[j].lastIndexOf('.') + 1);
                        File FF = Utility.putFileFromAsset(Globals.ASSET_PRELOADED_FACE_TRAINING_SET + File.separator + FOLDERS[i], files[j], preLoadedFaceTrainingSubFolder, f);
                        csvData.put(FF.getAbsolutePath(), (i + 1));
                    }
                }

                String prefix = "";
                int lexicalDist_0_1 = FOLDERS[0].compareTo(FOLDERS[1]);
                int lexicalDist_1_0 = FOLDERS[1].compareTo(FOLDERS[0]);

                if (lexicalDist_0_1 < lexicalDist_1_0) {
                    prefix = FOLDERS[0] + "_" + FOLDERS[1];
                } else {
                    prefix = FOLDERS[1] + "_" + FOLDERS[0];
                }

                String csvfileUrl = prefix + ".csv";
                File userFaceTrainingFolder = Utility.createFolder(
                        userdir + File.separator + Globals.USER_FACE_TRAINING_FOLDER + File.separator);
                File csvFile = Utility.getWritableFile(userFaceTrainingFolder, csvfileUrl);

                for (int i = 0; i < csvData.size(); i++) {
                    String key = (String)csvData.keySet().toArray()[i];
                    Integer value = (Integer)csvData.values().toArray()[i];
                    appendEntryToFaceTrainCSVFile(csvFile.getAbsolutePath(), key, value);
                }

                WD360FaceDetection wdfd = new WD360FaceDetection(
                        Globals.FACE_RECOGNIZER_TYPE_LBPH,
                        userdir + File.separator + Globals.WD360_NATIVE_LOG_FOLDER + File.separator + "WD360.log",
                        userdir + File.separator + Globals.WD360_ETC_FOLDER + File.separator + Globals.WD360_FACE_DETECTOR_XML_FILE_NAME,
                        userdir + File.separator + Globals.WD360_ETC_FOLDER + File.separator + Globals.WD360_FACE_DETECTOR_DOG_XML_FILE_NAME,
                        userdir + File.separator + Globals.WD360_ETC_FOLDER + File.separator + Globals.WD360_FACE_DETECTOR_CAT_XML_FILE_NAME,
                        userdir + File.separator + Globals.WD360_FACE_RECOGNIZER_MODEL_FOLDER,
                        0
                );

                wdfd.train(csvFile.getAbsolutePath());
                wdfd.release();
                Utility.getTempWDFD(userdir + File.separator, true).release();
            } else {
                Log.e(TAG, "Number of PreLoaded Face Training Folders can only be : 2");
                ret = false;
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            ret = false;
        }

        if (false == ret) {
            Log.e(TAG, "Rolling Back!");
            for (File folder : folders) {
                if (null != folder) {
                    folder.delete();
                }
            }
        }

        return ret;
    }

    private static Boolean createSearchResultsInfrastructure(String userdir) {
        Boolean ret = true;
        try {
            File userResultsRootFolder = Utility.createFolder(
                    userdir + Globals.WD360_USER_RESULTS_ROOT_FOLDER);
            if (null == userResultsRootFolder) {
                ret = false;
            } else {
                folders.add(userResultsRootFolder);
            }
            File userDocSearchBaseFolder = Utility.createFolder(
                    userResultsRootFolder.getAbsolutePath() + File.separator +
                            Globals.WD360_USER_DOC_SEARCH_BASE_FOLDER);
            if (null == userDocSearchBaseFolder) {
                ret = false;
            } else {
                folders.add(userDocSearchBaseFolder);
            }
            File userImageSearchBaseFolder = Utility.createFolder(
                    userResultsRootFolder.getAbsolutePath() + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_BASE_FOLDER);
            if (null == userImageSearchBaseFolder) {
                ret = false;
            } else {
                folders.add(userImageSearchBaseFolder);
            }
            File userImageSearchGroupFolder = Utility.createFolder(
                    userImageSearchBaseFolder.getAbsolutePath() + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_GROUP_FOLDER);
            if (null == userImageSearchGroupFolder) {
                ret = false;
            } else {
                folders.add(userImageSearchGroupFolder);
            }
            File userImageSearchDuplicateFolder = Utility.createFolder(
                    userImageSearchBaseFolder.getAbsolutePath() + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_DUPLICATE_FOLDER);
            if (null == userImageSearchDuplicateFolder) {
                ret = false;
            } else {
                folders.add(userImageSearchDuplicateFolder);
            }
            File userImageSearchCatFolder = Utility.createFolder(
                    userImageSearchBaseFolder.getAbsolutePath() + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_CAT_FOLDER);
            if (null == userImageSearchCatFolder) {
                ret = false;
            } else {
                folders.add(userImageSearchCatFolder);
            }
            File userImageSearchDogFolder = Utility.createFolder(
                    userImageSearchBaseFolder.getAbsolutePath() + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_DOG_FOLDER);
            if (null == userImageSearchDogFolder) {
                ret = false;
            } else {
                folders.add(userImageSearchDogFolder);
            }
            File userImageSearchOrientationFolder = Utility.createFolder(
                    userImageSearchBaseFolder.getAbsolutePath() + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_ORIENTATION_FOLDER);
            if (null == userImageSearchOrientationFolder) {
                ret = false;
            } else {
                folders.add(userImageSearchOrientationFolder);
            }
            File userImageSearchPortraitFolder = Utility.createFolder(
                    userImageSearchBaseFolder.getAbsolutePath() + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_PORTRAIT_FOLDER);
            if (null == userImageSearchPortraitFolder) {
                ret = false;
            } else {
                folders.add(userImageSearchPortraitFolder);
            }
            File userImageSearchDimensionFolder = Utility.createFolder(
                    userImageSearchBaseFolder.getAbsolutePath() + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_DIMENSION_FOLDER);
            if (null == userImageSearchDimensionFolder) {
                ret = false;
            } else {
                folders.add(userImageSearchDimensionFolder);
            }
            File userImageSearchFaceFolder = Utility.createFolder(
                    userImageSearchBaseFolder.getAbsolutePath() + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_FACE_FOLDER);
            if (null == userImageSearchFaceFolder) {
                ret = false;
            } else {
                folders.add(userImageSearchFaceFolder);
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            ret = false;
        }

        if (false == ret) {
            Log.e(TAG, "Rolling Back!");
            for (File folder : folders) {
                if (null != folder) {
                    folder.delete();
                }
            }
        }

        return ret;
    }

    private static void createImageToTextTrainingInfraStructure(String userdir) {
        try {
            dataDir = Utility.createFolder(userdir + Globals.TESS_DATA_FOLDER);
            if (null == dataDir) {
                return;
            }
            Utility.putFileFromAsset(Globals.ASSET_TESS_ENGLISH_TRAINED_DATA_FOLDER,
                    Globals.ENGLISH_TRAINED_FILE, dataDir, "");
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    @Nullable
    synchronized private static WD360Indexer createDocIndexer(String userdir) throws IOException {
        docIndexerDir = Utility.createFolder(userdir + Globals.DOC_INDEXER_FOLDER);
        if (null == docIndexerDir) {
            return null;
        }
        WD360Indexer indexer = new WD360Indexer();
        return indexer;
    }

    synchronized private static void deleteDocIndexer() throws IOException {
        if (null != docIndexerDir)
            docIndexerDir.delete();
    }

    @Nullable
    synchronized private static WD360TessBaseApi createTessBaseApi(String userdir) {
        try {
            tessLangTrainedDataFile = new File(userdir + Globals.TESS_DATA_FOLDER
                    + File.separator + Globals.ENGLISH_TRAINED_FILE);
            if (!tessLangTrainedDataFile.exists()) {
                createImageToTextTrainingInfraStructure(userdir);
            }
            WD360TessBaseApi tessBaseAPI = new WD360TessBaseApi();
            return tessBaseAPI;
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return null;
        }
    }

    synchronized private static void deleteTessBaseApi() throws IOException {
        if (null != tessLangTrainedDataFile)
            tessLangTrainedDataFile.delete();
        if (null != dataDir)
            dataDir.delete();
    }

    public static String executeUserLogin(String[] keyValues, String cmd_opcode) {
        try {
            String ret = "";
            String username = "";
            if (keyValues.length != 3) {
                return JSONUtils.formResponseValues("400",
                        "",
                        cmd_opcode,
                        "",
                        "",
                        "",
                        "Requires at least " + 3 + " key value pairs",
                        Globals.emptyResponse);
            } else {
                String[] usrname = keyValues[1].split("=");
                if (!usrname[0].equalsIgnoreCase("username")) {
                    return JSONUtils.formResponseValues("400",
                            "",
                            cmd_opcode,
                            "",
                            "",
                            "",
                            "Username key name is wrong, use 'username' for User Name",
                            Globals.emptyResponse);
                } else {
                    username = usrname[1];
                }
            }

            String[] passwd = keyValues[2].split("=");
            if (!passwd[0].equalsIgnoreCase("passwd"))
                return JSONUtils.formResponseValues("400",
                        "",
                        cmd_opcode,
                        "",
                        "",
                        "",
                        "Username key name is wrong, use 'passwd' for Password",
                        Globals.emptyResponse);

            MasterDBHelper mDbHelper = new MasterDBHelper(Globals.gContext);
            mDbHelper.setUserName(username);
            String usrRootAbsPath = mDbHelper.getTableAbsoluteRootFolerPath();

            if ((null != usrRootAbsPath) && (0 == usrRootAbsPath.compareTo("CREATED"))) {
                usrRootAbsPath = Globals.ROOT_FOLDER_NAME + username + File.separator;
                File fol = Utility.createFolder(usrRootAbsPath);
                if (null == fol) {
                    mDbHelper.deleteRow(usrRootAbsPath);
                    return JSONUtils.formResponseValues("500",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "Failed to create home folder for user : " +
                                    username + ", rolled back",
                            Globals.emptyResponse);
                }
                if (false == createSearchResultsInfrastructure(usrRootAbsPath)) {
                    mDbHelper.deleteRow(usrRootAbsPath);
                    if (null != fol)
                        fol.delete();
                    return JSONUtils.formResponseValues("500",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "Failed to create Search results infrastructure for user : " +
                                    username + ", rolled back",
                            Globals.emptyResponse);
                }

                WD360Indexer docIndexer = createDocIndexer(usrRootAbsPath);
                if (null == docIndexer) {
                    mDbHelper.deleteRow(usrRootAbsPath);
                    for (File folder : folders) {
                        if (null != folder) {
                            folder.delete();
                        }
                    }
                    if (null != fol)
                        fol.delete();
                    return JSONUtils.formResponseValues("500",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "Failed to create document indexer for user : " +
                                    username + ", rolled back",
                            Globals.emptyResponse);
                }
                WD360TessBaseApi tessApi = createTessBaseApi(usrRootAbsPath);
                if (null == tessApi) {
                    mDbHelper.deleteRow(usrRootAbsPath);
                    for (File folder : folders) {
                        if (null != folder) {
                            folder.delete();
                        }
                    }
                    if (null != fol)
                        fol.delete();
                    deleteDocIndexer();
                    return JSONUtils.formResponseValues("500",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "Failed to create document indexer for user : " +
                                    username + ", rolled back",
                            Globals.emptyResponse);
                }
                if (!mDbHelper.insertRowIfNotExist(usrRootAbsPath,
                        docIndexer,
                        docIndexerDir.getAbsolutePath(),
                        tessApi,
                        0.0,
                        0.0,
                        "NA",
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        "NA",
                        0,
                        0)) {
                    mDbHelper.deleteRow(usrRootAbsPath);
                    for (File folder : folders) {
                        if (null != folder) {
                            folder.delete();
                        }
                    }
                    deleteDocIndexer();
                    deleteTessBaseApi();
                    if (null != fol)
                        fol.delete();
                    return JSONUtils.formResponseValues("500",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "Failed make entry into db for user, rolled back",
                            Globals.emptyResponse);
                } else {
                    File wd360etc = Utility.createFolder(usrRootAbsPath +
                            Globals.WD360_ETC_FOLDER + File.separator);
                    File wd360frm = Utility.createFolder(usrRootAbsPath +
                            Globals.WD360_FACE_RECOGNIZER_MODEL_FOLDER + File.separator);
                    File wd360nativeLogs = Utility.createFolder(usrRootAbsPath +
                            Globals.WD360_NATIVE_LOG_FOLDER + File.separator);

                    if ((null == wd360etc) || (null == wd360frm) || (null == wd360nativeLogs)) {
                        mDbHelper.deleteRow(usrRootAbsPath);
                        for (File folder : folders) {
                            if (null != folder) {
                                folder.delete();
                            }
                        }
                        deleteDocIndexer();
                        deleteTessBaseApi();
                        if (null != wd360etc)
                            wd360etc.delete();
                        if (null != wd360frm)
                            wd360frm.delete();
                        if (null != wd360nativeLogs)
                            wd360nativeLogs.delete();
                        if (null != fol)
                            fol.delete();
                        return JSONUtils.formResponseValues("500",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "Failed to create face detector related files, rolled back",
                                Globals.emptyResponse);
                    } else {
                        File xmlFile = Utility.putFileFromAsset(Globals.WD360_ETC_FOLDER,
                                Globals.WD360_FACE_DETECTOR_XML_FILE_NAME, wd360etc, "");
                        File dogXmlFile = Utility.putFileFromAsset(Globals.WD360_ETC_FOLDER,
                                Globals.WD360_FACE_DETECTOR_DOG_XML_FILE_NAME, wd360etc, "");
                        File catXmlFile = Utility.putFileFromAsset(Globals.WD360_ETC_FOLDER,
                                Globals.WD360_FACE_DETECTOR_CAT_XML_FILE_NAME, wd360etc, "");

                        if ((null == xmlFile) || (null == dogXmlFile) || (null == catXmlFile)) {
                            mDbHelper.deleteRow(usrRootAbsPath);
                            for (File folder : folders) {
                                if (null != folder) {
                                    folder.delete();
                                }
                            }
                            deleteDocIndexer();
                            deleteTessBaseApi();

                            if (null != xmlFile)
                                xmlFile.delete();
                            if (null != dogXmlFile)
                                dogXmlFile.delete();
                            if (null != catXmlFile)
                                catXmlFile.delete();

                            if (null != fol)
                                fol.delete();
                            if (null != wd360etc)
                                wd360etc.delete();
                            if (null != wd360frm)
                                wd360frm.delete();
                            if (null != wd360nativeLogs)
                                wd360nativeLogs.delete();
                            if (null != fol)
                                fol.delete();
                            return JSONUtils.formResponseValues("500",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "Failed to create wd 360 etc folder",
                                    Globals.emptyResponse);
                        }
                    }

                    return JSONUtils.formResponseValues("200",
                            username,
                            cmd_opcode,
                            "",
                            "",
                            "",
                            username +
                                    " logged in successfully, now you can push your files into ROOT/"
                                    + username + " folder.",
                            Globals.emptyResponse);
                }
            } else {
                if ((null != usrRootAbsPath) && (0 != usrRootAbsPath.compareTo("CREATED"))) {
                    return JSONUtils.formResponseValues("200",
                            username,
                            cmd_opcode,
                            "",
                            "",
                            "",
                            username +
                                    " already logged in, now you can push your files into ROOT/"
                                    + username + " folder.",
                            Globals.emptyResponse);
                } else {
                    return JSONUtils.formResponseValues("500",
                            username,
                            cmd_opcode,
                            "",
                            "",
                            "",
                            "Failed to create entry in DB for user : " + username,
                            Globals.emptyResponse);
                }
            }
        } catch (Exception e) {
            Log.d(TAG, "executeUserLogin() : " + e.getMessage());
            return JSONUtils.formResponseValues("500",
                    "",
                    "",
                    "",
                    "",
                    "",
                    e.getMessage(),
                    Globals.emptyResponse);

        }
    }
}

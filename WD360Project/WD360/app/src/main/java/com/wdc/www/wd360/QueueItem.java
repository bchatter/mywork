package com.wdc.www.wd360;

/**
 * Created by Biswapratap Chatterjee on 7/18/2017.
 */

public class QueueItem {
    public String m_userName;
    public String m_absFilePath;
    public WD360Indexer m_indexer;
    public WD360TessBaseApi m_tessApi;

    public QueueItem(String userName, String absFilePath, WD360Indexer indexer, WD360TessBaseApi tessApi) {
        m_userName = userName;
        m_absFilePath = absFilePath;
        m_indexer = indexer;
        m_tessApi = tessApi;
    }
}

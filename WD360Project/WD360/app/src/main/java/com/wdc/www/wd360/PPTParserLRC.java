package com.wdc.www.wd360;

import org.apache.poi.hslf.extractor.PowerPointExtractor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.concurrent.Callable;

/**
 * Created by Biswapratap Chatterjee on 6/28/2017.
 */

public class PPTParserLRC implements Callable<IndexItem> {
    private final File f;

    public PPTParserLRC(File file) {
        this.f = file;
    }

    public IndexItem call() {
        IndexItem idxItm = new IndexItem((long)f.getName().hashCode(), f.getAbsolutePath(), "");
        try {
            InputStream inp = new FileInputStream(f);
            PowerPointExtractor ppe = new PowerPointExtractor(new POIFSFileSystem(inp));
            String content = ppe.getText(true,true);
            idxItm.setContent(content);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return idxItm;
    }
}
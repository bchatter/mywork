package com.wdc.www.wd360;

/**
 * Created by Biswapratap Chatterjee on 6/27/2017.
 */

import android.util.Log;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Searcher {
    private IndexSearcher searcher;
    private QueryParser contentQueryParser;
    private static String TAG = Searcher.class.getSimpleName();

    public Searcher(String userName) {
        try {
            MasterDBHelper mDbHelper = new MasterDBHelper(Globals.gContext);
            mDbHelper.setUserName(userName);
            String usrRootAbsPath = mDbHelper.getTableAbsoluteRootFolerPath();
            if (null == usrRootAbsPath) {
                return;
            }
            String docIndexerFolder = usrRootAbsPath + "docIndexer";
            searcher = new IndexSearcher(IndexReader.open(FSDirectory.open(new File(docIndexerFolder))));
            StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_41);
            contentQueryParser = new QueryParser(Version.LUCENE_41, IndexItem.CONTENT, analyzer);
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    public List<String> findByContent(String queryString, int numOfResults) throws ParseException, IOException {
        List<String> queryResults = new ArrayList<>();
        Query query = contentQueryParser.parse(queryString);
        TopDocs topDocs = searcher.search(query, numOfResults);
        ScoreDoc[] scoredocs = topDocs.scoreDocs;
        for (int i = 0; i < scoredocs.length; i++) {
            queryResults.add(searcher.doc(scoredocs[i].doc).get(IndexItem.TITLE));
        }
        return queryResults;
    }

    public void close() throws IOException {

    }
}
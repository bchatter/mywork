package com.wdc.www.wd360;



import java.io.File;

/**
 * Created by Biswapratap Chatterjee on 7/14/2017.
 */

public class ObserverMinerThread implements Runnable {
    private static String TAG = ObserverMinerThread.class.getSimpleName();

    @Override
    public void run() {
        File rootPath = new File(Globals.ROOT_FOLDER_NAME);
        RecursiveFileObserver observer = null;
        observer = new RecursiveFileObserver(rootPath.getAbsolutePath());
        observer.startWatching();

        try {
            new BlockedQueueBasedMiner().mine();
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + e.getMessage());
        }
    }
}

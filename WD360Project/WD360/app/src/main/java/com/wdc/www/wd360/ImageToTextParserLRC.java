package com.wdc.www.wd360;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;


import java.io.File;
import java.util.concurrent.Callable;

/**
 * Created by Biswapratap Chatterjee on 6/29/2017.
 */

public class ImageToTextParserLRC implements Callable<IndexItem> {
    private Boolean discard;
    private final File file;
    private final WD360TessBaseApi baseApi;
    private static String TAG = ImageToTextParserLRC.class.getSimpleName();

    public ImageToTextParserLRC(File f, WD360TessBaseApi b) {
        this.file = f;
        this.baseApi = b;
        long MAX_ALLOWED_SIZE = 100000;
        long size = f.length();
        //Log.i(TAG, f.getAbsolutePath() + " size = " + size + " bytes, MAX_ALLOWED_SIZE = " + MAX_ALLOWED_SIZE + " bytes");
        if (size > MAX_ALLOWED_SIZE) {
            this.discard = true;
        } else {
            this.discard = false;
        }
        //Log.i(TAG, f.getAbsolutePath() + " discarded = " + this.discard);
    }

    public IndexItem call() {
        IndexItem idxItm = new IndexItem((long)file.getName().hashCode(), file.getAbsolutePath(), "");
        if (discard) {
            return idxItm;
        }
        try {
            Bitmap image = BitmapFactory.decodeFile(file.getAbsolutePath());
            baseApi.setImage(image);
            String content = baseApi.getUTF8Text();
            idxItm.setContent(content);
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        return idxItm;
    }
}

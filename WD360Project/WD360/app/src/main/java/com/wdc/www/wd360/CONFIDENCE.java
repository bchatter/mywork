package com.wdc.www.wd360;

/**
 * Created by Biswapratap Chatterjee on 7/10/2017.
 */

public class CONFIDENCE {
    public double m_confidence;
    public CONFIDENCE(double c) {
        m_confidence = c;
    }
    double getConfidence() {
        return m_confidence;
    }
};
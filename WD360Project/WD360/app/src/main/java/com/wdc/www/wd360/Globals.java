package com.wdc.www.wd360;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Biswapratap Chatterjee on 5/12/2017.
 */

public class Globals {
    static final int MINING_BURST_SIZE = 199;
    static final int RESULT_SIZE = 60;
    static final int JAVA_GC_CLEANUP_TIME = 1000;
    //static final int NEXT_MININING_ATTEMPT_TIME = 900000;
    static final int NEXT_MININING_ATTEMPT_TIME = 300000;
    static boolean LOG = true;
    static String myCloudUserId = "";
    static Context gContext;
    static int EXTRACT_TEXT_TIMEOUT = 60;
    static String ROOT_FOLDER_NAME = "";
    static final String TRAINING_LANGUAGE = "eng";
    static final String TRAINED_DATA_FILE_EXTENSION = ".traineddata";
    static final String ENGLISH_TRAINED_FILE = TRAINING_LANGUAGE + TRAINED_DATA_FILE_EXTENSION;
    static final String USER_FACE_TRAINING_FOLDER = "UserFaceTrainingFolder";
    static final String FACE_RECOGNIZER_TYPE_FISHER = "fisher";
    static final String FACE_RECOGNIZER_TYPE_LBPH = "lbph";
    static final String DOC_INDEXER_FOLDER = "docIndexer";
    static final String TESS_DATA_FOLDER = "tessdata";
    static final String ASSET_TESS_ENGLISH_TRAINED_DATA_FOLDER = "Tesseract_English_Trained_Data";
    static final String ASSET_PRELOADED_FACE_TRAINING_SET = "PreLoadedFaceTrainingSet";
    static final String ASSET_SAMPLE_DATASET_SET = "SAMPLE_DATASET";
    static Map<String, Job> user_job_map = new HashMap<String, Job>();
    static String emptyResponse[] = {};
    static LinkedBlockingQueue<QueueItem> OBSERVER_MINER_QUEUE = new LinkedBlockingQueue<QueueItem>();
    static LinkedBlockingQueue<QueueItem> PERIODIC_MINER_QUEUE = new LinkedBlockingQueue<QueueItem>();
    static final String WD360_ETC_FOLDER = "WD360ETC";
    static final String WD360_NATIVE_LOG_FOLDER = "WD360NativeLogs";
    static final String WD360_FACE_RECOGNIZER_MODEL_FOLDER = "WD360FRM";
    static final String WD360_FACE_RECOGNIZER_MODEL_YML_FILE_NAME = "WD360.yml";
    static final String WD360_FACE_DETECTOR_XML_FILE_NAME = "haarcascade_frontalface_alt.xml";
    static final String WD360_FACE_DETECTOR_DOG_XML_FILE_NAME = "haarcascade_frontaldogface.xml";
    static final String WD360_FACE_DETECTOR_CAT_XML_FILE_NAME = "haarcascade_frontalcatface.xml";
    static final String WD360_PRELOADED_FACE_TRAINING_FOLDER = "WD360PreLoadedFaceTraining";
    static final String WD360_USER_RESULTS_ROOT_FOLDER = "WD360Results";
    static final String WD360_USER_DOC_SEARCH_BASE_FOLDER = "Doc";
    static final String WD360_USER_IMAGE_SEARCH_BASE_FOLDER = "Image";
    static final String WD360_USER_IMAGE_SEARCH_GROUP_FOLDER = "Group";
    static final String WD360_USER_IMAGE_SEARCH_DUPLICATE_FOLDER = "Duplicate";
    static final String WD360_USER_IMAGE_SEARCH_PORTRAIT_FOLDER = "Portrait";
    static final String WD360_USER_IMAGE_SEARCH_DIMENSION_FOLDER = "Dimension";
    static final String WD360_USER_IMAGE_SEARCH_CAT_FOLDER = "Cat";
    static final String WD360_USER_IMAGE_SEARCH_DOG_FOLDER = "Dog";
    static final String WD360_USER_IMAGE_SEARCH_ORIENTATION_FOLDER = "Orientation";
    static final String WD360_USER_IMAGE_SEARCH_FACE_FOLDER = "Face";
    static final String WD360_IGNORE_CODE_CACHE_FOLDER = "code_cache";
    static final String WD360_IGNORE_DATABASES_FOLDER = "databases";
    static final String WD360_IGNORE_LIB_FOLDER = "lib";
    static String WD360_FACE_TRAINING_AGE_INFO = "";
}

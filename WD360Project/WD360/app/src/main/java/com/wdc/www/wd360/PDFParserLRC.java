package com.wdc.www.wd360;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;

import java.io.File;
import java.util.concurrent.Callable;

/**
 * Created by Biswapratap Chatterjee on 6/28/2017.
 */

public class PDFParserLRC implements Callable<IndexItem> {
    private final File f;

    public PDFParserLRC(File file) {
        this.f = file;
    }

    public IndexItem call() {
        IndexItem idxItm = new IndexItem((long)f.getName().hashCode(), f.getAbsolutePath(), "");
        try {
            String content = "";
            PdfReader reader = new PdfReader(f.getAbsolutePath());
            int num = reader.getNumberOfPages();
            for (int i=1; i<=num; i++) {
                if (!reader.isTampered() && !reader.isEncrypted()) {
                    try {
                        content = content + PdfTextExtractor.getTextFromPage(reader, i);
                    } catch (Exception e) {
                        e.printStackTrace();
                        continue;
                    }
                }
            }
            idxItm.setContent(content);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return idxItm;
    }
}
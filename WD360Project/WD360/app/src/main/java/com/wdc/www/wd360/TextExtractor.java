package com.wdc.www.wd360;



import org.jsoup.Jsoup;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by Biswapratap Chatterjee on 7/4/2017.
 */

public class TextExtractor {
    public static final String TAG = TextExtractor.class.getSimpleName();

    private static IndexItem doc_html_text_index(File file, String type) {
        IndexItem idxItm = new IndexItem((long)file.getName().hashCode(), file.getAbsolutePath(), "");
        try {
            String content = "";
            InputStream is = new FileInputStream(file);
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String line = buf.readLine();
            StringBuilder sb = new StringBuilder();
            while (line != null) {
                sb.append(line).append("\n");
                line = buf.readLine();
            }
            buf.close();
            if (type.equalsIgnoreCase("text")) {
                content = sb.toString();
                idxItm.setContent(content);
            } else {
                String html = sb.toString();
                content = Jsoup.parse(html).text();
                idxItm.setContent(content);
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        return idxItm;
    }

    public static int getIndexItem(File file, WD360TessBaseApi baseApi, IndexItem[] idxItem) {
/*
        if (Utility.isTypeFile(file, "pdf") ||
                Utility.isTypeFile(file, "ppt") ||
                Utility.isTypeFile(file, "presentation") ||
                Utility.isTypeFile(file, "excel") ||
                Utility.isTypeFile(file, "word") ||
                Utility.isTypeFile(file, "rtf") ||
                Utility.isTypeFile(file, "ppt") ||
                Utility.isTypeFile(file, "presentation") ||
                Utility.isTypeFile(file, "visio") ||
                Utility.isTypeFile(file, "outlook") ||
                Utility.isTypeFile(file, "msg") ||
                Utility.isTypeFile(file, "image")) {

            Callable<IndexItem> task = null;
            ExecutorService executor = Executors.newCachedThreadPool();

            if (Utility.isTypeFile(file, "pdf")) {
                task = new PDFParserLRC(file);
            }
            else if (Utility.isTypeFile(file, "ppt") || Utility.isTypeFile(file, "presentation")) {
                task = new PPTParserLRC(file);
            }
            else if (Utility.isTypeFile(file, "excel")) {
                task = new ExcelParserLRC(file);
            }
            else if (Utility.isTypeFile(file, "word") || Utility.isTypeFile(file, "rtf")) {
                task = new DocxParserLRC(file);
            }
            else if (Utility.isTypeFile(file, "visio")) {
                task = new VisioParserLRC(file);
            }
            else if (Utility.isTypeFile(file, "outlook") || Utility.isTypeFile(file, "msg")) {
                task = new OutlookParserLRC(file);
            }
            else if (Utility.isTypeFile(file, "image"))
                task = new ImageToTextParserLRC(file, baseApi);

            Future<IndexItem> future = executor.submit(task);
            try {
                idxItem[0] = future.get(Globals.EXTRACT_TEXT_TIMEOUT, TimeUnit.SECONDS);
                content[0] = idxItem[0].getContent();
                return 1;
            } catch (TimeoutException ex) {
                return 0;
            } catch (InterruptedException e) {
                return 0;
            } catch (ExecutionException e) {
                return 0;
            } catch (Exception e) {
                return 0;
            }
*/
        Callable<IndexItem> task = null;
        ExecutorService executor = Executors.newCachedThreadPool();
        if (Utility.isTypeFile(file, "image")) {
            task = new ImageToTextParserLRC(file, baseApi);
            Future<IndexItem> future = executor.submit(task);
            try {
                idxItem[0] = future.get(Globals.EXTRACT_TEXT_TIMEOUT, TimeUnit.SECONDS);
                return 1;
            } catch (TimeoutException ex) {
                return 0;
            } catch (InterruptedException e) {
                return 0;
            } catch (ExecutionException e) {
                return 0;
            } catch (Exception e) {
                return 0;
            }
        } else if (Utility.isTypeFile(file, "html") || Utility.isTypeFile(file, "text")) {
            if (Utility.isTypeFile(file, "html")) {
                idxItem[0] = doc_html_text_index(file, "html");
                 return 1;
            } else if (Utility.isTypeFile(file, "text")) {
                idxItem[0] = doc_html_text_index(file, "text");
                return 1;
            }
        } else {
            return -1;
        }
        return -1;
    }
}

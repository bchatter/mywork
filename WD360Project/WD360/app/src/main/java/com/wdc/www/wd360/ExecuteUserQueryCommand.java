package com.wdc.www.wd360;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Biswapratap Chatterjee on 7/11/2017.
 */

public class ExecuteUserQueryCommand {
    private static String TAG = ExecuteUserQueryCommand.class.getSimpleName();
    private static String[] results = new String[Globals.RESULT_SIZE];

    public static String executeUserQuery(String[] keyValues, String cmd_opcode) {
        try {
            String ret = "";
            String username = "";
            ret = JSONUtils.commandSpecificChecks(keyValues, cmd_opcode,2);
            if (ret.contains("ASSERT"))
                return ret;
            else {
                username = Globals.myCloudUserId;
                String[] query = keyValues[1].split("=");
                if (!query[0].equalsIgnoreCase("query"))
                    return JSONUtils.formResponseValues("40",
                            "",
                            cmd_opcode,
                            "",
                            "",
                            "",
                            "Username key name is wrong, use 'query' for the Query String",
                            Globals.emptyResponse);

                int numofResults = 30;
                List<String> docresults = null;
                Searcher searcher = new Searcher(username);
                docresults = searcher.findByContent(query[1], numofResults);

                MasterDBHelper masterDBHelper = new MasterDBHelper(Globals.gContext);
                masterDBHelper.setUserName(username);

                String docBaseFolder = masterDBHelper.getTableAbsoluteRootFolerPath() +
                        Globals.WD360_USER_RESULTS_ROOT_FOLDER + File.separator +
                        Globals.WD360_USER_DOC_SEARCH_BASE_FOLDER + File.separator;
                FileUtils.cleanDirectory(new File(docBaseFolder));

                String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date());
                String folderName = docBaseFolder + timeStamp + File.separator;
                File destination = Utility.createFolder(folderName);
                if (null != destination) {
                    int idx = 0;
                    for (String docresult : docresults) {
                        FileUtils.copyFile(new File(docresult), new File(destination.getAbsoluteFile() + File.separator + Utility.getFileNameOnly(docresult)), true);
                        results[idx++] = new File(destination.getAbsoluteFile() + File.separator + Utility.getFileNameOnly(docresult)).getAbsolutePath();
                        if (idx >= Globals.RESULT_SIZE)
                            break;
                    }
                }

                return JSONUtils.formResponseValues("200",
                        username,
                        cmd_opcode,
                        "",
                        "",
                        "0",
                        destination.getAbsolutePath(),
                        results);
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return JSONUtils.formResponseValues("500",
                    "",
                    "",
                    "",
                    "",
                    "",
                    e.getMessage(),
                    Globals.emptyResponse);
        }
    }
}

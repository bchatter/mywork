package com.wdc.www.wd360;

import android.content.Context;


import com.wdc.nassdk.MyCloudUIServer;

import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD;

import static fi.iki.elonen.NanoHTTPD.Response.Status.OK;

/**
 * Created by Biswapratap Chatterjee on 5/12/2017.
 */

public class WD360AppUI extends MyCloudUIServer {
    private static String TAG = WD360AppUI.class.getSimpleName();

    public WD360AppUI(Context context) {
        super(context);
    }

    private String processPostRequest(String[] keyValues) {
        String ret = "";
        String[] co = new String[1];
        co[0] = "";
        String cmd_opcode = "";

        ret = JSONUtils.initialChecks(keyValues, co);
        if (ret.contains("ASSERT"))
            return ret;

        cmd_opcode = co[0];

        Log.d(TAG, "processPostRequest() cmd_opcode = " + cmd_opcode);

        if (cmd_opcode.equalsIgnoreCase("doc_search")) {
            return ExecuteUserQueryCommand.executeUserQuery(keyValues, cmd_opcode);
        }
        else if (cmd_opcode.equalsIgnoreCase("image_search")) {
            return ExecuteUserImageQueryCommands.executeUserImageQuery(keyValues, cmd_opcode);
        }
        else if (cmd_opcode.equalsIgnoreCase("trained_faces")) {
            Log.d(TAG, "processPostRequest() Calling ExecuteUserImageQueryCommands.executeUserTrainedImagesQuery()");
            return ExecuteUserImageQueryCommands.executeUserTrainedImagesQuery(keyValues, cmd_opcode);
        }
        else if (cmd_opcode.equalsIgnoreCase("log_on")) {
            return Log.turnOnLogging();
        }
        else if (cmd_opcode.equalsIgnoreCase("log_off")) {
            return Log.turnOffLogging();
        }
        else {
            return JSONUtils.formResponseValues("500",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "Unknown Command",
                    Globals.emptyResponse);
        }
    }

    private Response processGetRequest(IHTTPSession session) {
        try {
            String mimeType = "";
            String uri = session.getUri();

            Log.d(TAG, "[INIT] WD360AppUI:processGetRequest() uri : " + uri);

            if (uri.contains("wd360")){
                String filepath = uri;
                String filename = filepath.substring(filepath.lastIndexOf('/') + 1, filepath.length());
                InputStream isr = new FileInputStream(uri);
                mimeType = URLConnection.guessContentTypeFromName(filename);
                if (mimeType == null || mimeType.length() == 0) {
                    mimeType = URLConnection.guessContentTypeFromStream(isr);
                    if (mimeType == null || mimeType.length() == 0) {
                        mimeType = "";
                    }
                }
                return newChunkedResponse(OK, mimeType, isr);
            }

            if (uri.equals("/")) {
                uri = "/index.html";
            }

            String filepath = "website" + uri;
            String filename = filepath.substring(filepath.lastIndexOf('/') + 1, filepath.length());
            InputStream isr = Globals.gContext.getAssets().open(filepath);
            mimeType = URLConnection.guessContentTypeFromName(filename);
            if (mimeType == null || mimeType.length() == 0) {
                mimeType = URLConnection.guessContentTypeFromStream(isr);
                if (mimeType == null || mimeType.length() == 0) {
                    mimeType = "";
                }
            }
            return newChunkedResponse(OK, mimeType, isr);
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return newFixedLengthResponse("<h1> Web page not found</h1>");
        }
    }

    @Override
    public Response get(IHTTPSession session) {
        Log.d(TAG, "[INIT} WD360AppUI:get() Entering");
        Response response = processGetRequest(session);
        Log.d(TAG, "[INIT} WD360AppUI:get() Exiting");
        return response;
    }

    private NanoHTTPD.Response.IStatus getHttpStatus(String response) {
        try {
            JSONObject json = new JSONObject(response);
            String httpstatus = (String) json.get("httpstatus");
            if (httpstatus.equalsIgnoreCase("200"))
                return NanoHTTPD.Response.Status.OK;
            else if (httpstatus.equalsIgnoreCase("202"))
                return NanoHTTPD.Response.Status.ACCEPTED;
            else if (httpstatus.equalsIgnoreCase("400"))
                return NanoHTTPD.Response.Status.BAD_REQUEST;
            else if (httpstatus.equalsIgnoreCase("401"))
                return NanoHTTPD.Response.Status.UNAUTHORIZED;
            else
                return NanoHTTPD.Response.Status.INTERNAL_ERROR;
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return NanoHTTPD.Response.Status.INTERNAL_ERROR;
        }
    }

    @Override
    public Response post(IHTTPSession session) {
        Log.d(TAG, "[INIT} WD360AppUI:post() Entering");
        try{
            Map<String, String> files = new HashMap<String, String>();
            Map<String, String> filenames = new HashMap<String, String>();
            session.parseBody(files);
            String postBody = session.getQueryParameterString();
            Log.d(TAG, postBody);
            filenames = session.getParms();
            String[] keyValuestemp = postBody.split("\n");
            for (int i = 0; i < keyValuestemp.length; i++) {
                keyValuestemp[i] = java.net.URLDecoder.decode(keyValuestemp[i], "UTF-8");
                keyValuestemp[i] = keyValuestemp[i].trim();
                Log.d(TAG, keyValuestemp[i]);
            }

            String[] keyValues = keyValuestemp[1].split("&");

            String[] co = new String[1];
            co[0] = "";
            String cmd_opcode = "";
            JSONUtils.initialChecks(keyValues, co);
            cmd_opcode = co[0];
            Log.d(TAG, "post() cmd_opcode = " + cmd_opcode);

            if (false == cmd_opcode.equalsIgnoreCase("trained_faces")) {
                if (true == ServerBusy.getServerStatus()) {
                    Log.d(TAG, "[INIT} WD360AppUI:post() Exiting");
                    return newFixedLengthResponse(Response.Status.FORBIDDEN, "application/json", "");
                }
            }

            if (filenames.size() > 0) {
                if (filenames.containsKey("access_token")) {
                    filenames.remove("access_token");
                }
            }

            if ((files.size() > 0) &&
                (filenames.size() > 0) &&
                (filenames.size() == files.size()) &&
                (false == cmd_opcode.equalsIgnoreCase("doc_search")) &&
                (false == cmd_opcode.equalsIgnoreCase("image_search")) &&
                (false == cmd_opcode.equalsIgnoreCase("trained_faces"))) {
                String res = ExecuteImageTrainUploadCommand.executeImageTraining(files, filenames);
                return newFixedLengthResponse(getHttpStatus(res), "application/json", res);
            }

            if (keyValues.length > 0) {
                String response = processPostRequest(keyValues);
                return newFixedLengthResponse(getHttpStatus(response), "application/json", response);
            }
            else {
                Log.d(TAG, "[INIT} WD360AppUI:post() Exiting");
                return newFixedLengthResponse("<h1>" + "No POST parameters received" + "</h1>");
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            Log.d(TAG, "[INIT} WD360AppUI:post() Exiting");
            return newFixedLengthResponse("<h1>" + e.getMessage() + "</h1>");
        }
    }
}
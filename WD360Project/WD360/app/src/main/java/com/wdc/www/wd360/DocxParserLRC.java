package com.wdc.www.wd360;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;

import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.Callable;

/**
 * Created by Biswapratap Chatterjee on 6/28/2017.
 */

public class DocxParserLRC implements Callable<IndexItem> {
    private final File f;

    public DocxParserLRC(File file) {
        this.f = file;
    }

    public IndexItem call() {
        IndexItem idxItm = new IndexItem((long)f.getName().hashCode(), f.getAbsolutePath(), "");
        try {
            HWPFDocument document = new HWPFDocument(new FileInputStream(f.getAbsolutePath()));
            WordExtractor wordExtractor = new WordExtractor(document);
            String content = wordExtractor.getText();
            idxItm.setContent(content);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return idxItm;
    }
}
package com.wdc.www.wd360;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.IOException;

public abstract class WD360BaseStartupService extends Service {
    private static final String TAG = "WD360BaseStartupService";
    WD360CloudUIServer mUiServer;

    public abstract WD360CloudUIServer createMyCloudUIServer();

    public WD360BaseStartupService() {
    }

    @Nullable
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        this.startUiServer();
    }

    private void startUiServer() {
        if(this.mUiServer == null) {
            (new Thread(new Runnable() {
                public void run() {
                    WD360BaseStartupService.this.mUiServer = WD360BaseStartupService.this.createMyCloudUIServer();

                    try {
                        WD360BaseStartupService.this.mUiServer.start();
                    } catch (IOException var2) {
                        Log.e("BaseStartupService", "Ui Server start failed with " + var2.getMessage());
                        var2.printStackTrace();
                    }

                }
            })).start();
        }

    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void stopUiServer(@NonNull Context context) {
        Log.e("BaseStartupService", "stopUiServer ");
        this.mUiServer.stop();
    }
}

package com.wdc.www.wd360;

import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.Serializable;

/**
 * Created by Biswapratap Chatterjee on 7/18/2017.
 */

public class WD360TessBaseApi extends TessBaseAPI implements Serializable {

    public WD360TessBaseApi() {
        super();
    }
}

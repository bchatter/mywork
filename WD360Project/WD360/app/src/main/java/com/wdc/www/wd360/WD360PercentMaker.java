package com.wdc.www.wd360;

/**
 * Created by 17328 on 7/5/2017.
 */

public abstract interface WD360PercentMaker {
    int getPercent();
    String [] getResults();
}

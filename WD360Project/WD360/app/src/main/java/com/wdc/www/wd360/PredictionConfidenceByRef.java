package com.wdc.www.wd360;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Biswapratap Chatterjee on 7/9/2017.
 */

public class PredictionConfidenceByRef {
    public Map<PREDICTION, CONFIDENCE> PCByRef = new HashMap<PREDICTION, CONFIDENCE>();

    public void pushItem(int p, double c) {
        PCByRef.put(new PREDICTION(p), new CONFIDENCE(c));
    }
}

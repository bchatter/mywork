package com.wdc.www.wd360;



import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by Biswapratap Chatterjee on 7/18/2017.
 */

public class WD360Serializer {
    private static String TAG = WD360Serializer.class.getSimpleName();

    synchronized public static byte[] serialize(Object obj) {
        byte[] byteArr = {};
        try {
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            ObjectOutputStream o = new ObjectOutputStream(b);
            o.writeObject(obj);
            byteArr = b.toByteArray();
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        return byteArr;
    }

    synchronized public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
        Object obj = new Object();
        try {
            ByteArrayInputStream b = new ByteArrayInputStream(bytes);
            ObjectInputStream o = new ObjectInputStream(b);
            obj = o.readObject();
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        return obj;
    }
}
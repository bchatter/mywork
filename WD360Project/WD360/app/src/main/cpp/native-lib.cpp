#include "opencv2/core/utility.hpp"
#include "opencv2/face.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/objdetect.hpp"

#include "jni.h"
#include <iostream>
#include <fstream>
#include <dirent.h>
#include <sys/stat.h>

using namespace cv;
using namespace std;

CvHaarClassifierCascade *cascade_f;
CvHaarClassifierCascade *cascade_e;
CvHaarClassifierCascade *cascade_n;
CvHaarClassifierCascade *cascade_m;

#define MAX_NUM_OF_UNIQUE_FACES 50

typedef struct _FRONTAL_FACE_FEATURES {
    int face_x1;
    int face_y1;
    int face_x2;
    int face_y2;
    double face_area;
} FRONTAL_FACE_FEATURES;

typedef struct _FRONTAL_EYE_FEATURES {
    int eye_x1;
    int eye_y1;
    int eye_x2;
    int eye_y2;
    double eye_corner_to_corner_distance;
} FRONTAL_EYE_FEATURES;

typedef struct _FRONTAL_NOSE_FEATURES {
    int nose_x1;
    int nose_y1;
    int nose_x2;
    int nose_y2;
    double nose_corner_to_corner_distance;
} FRONTAL_NOSE_FEATURES;

typedef struct _FRONTAL_MOUTH_FEATURES {
    int mouth_x1;
    int mouth_y1;
    int mouth_x2;
    int mouth_y2;
    double mouth_corner_to_corner_distance;
    int mouth_x1c;
    int mouth_y1c;
    int mouth_x2c;
    int mouth_y2c;
    double mouth_top_to_bottom_distance;
} FRONTAL_MOUTH_FEATURES;

typedef struct _FRONTAL_FACIAL_FEATURES {
    IplImage *img;
    FRONTAL_FACE_FEATURES face_features[1];
    FRONTAL_EYE_FEATURES eye_features[2];
    FRONTAL_NOSE_FEATURES nose_features[1];
    FRONTAL_MOUTH_FEATURES mouth_features[1];
} FRONTAL_FACIAL_FEATURES;

FRONTAL_FACIAL_FEATURES unique_faces[MAX_NUM_OF_UNIQUE_FACES];
int index_unique_faces = 0;
CvMemStorage   *storage;

string logpath = "";
Size global_min;
bool logon = false;

void LOG(string msg) {
    if (true == logon) {
        ofstream m_mycout(logpath.c_str(), ios_base::out | ios_base::app);
        m_mycout << "[INFO] " << msg << endl;
        m_mycout.close();
    }
}

void CLEAR_OLD_LOG() {
    ofstream m_mycout(logpath.c_str(), ios_base::out | ios_base::trunc);
    m_mycout.close();
}

class WD360FaceDetection {

private:
    string m_haarcascade_frontalface_xmlPath;
    string m_haarcascade_eye_xmlPath;
    string m_haarcascade_mcs_nose_xmlPath;
    string m_haarcascade_mcs_mouth_xmlPath;
    string m_catXmlPath;
    string m_dogXmlPath;
    string m_modelSavePath;
    int m_load;
    int m_faceRecognizer_loaded;
    Ptr<cv::face::FaceRecognizer> m_model;

    void detectFaceFeatures(IplImage *img, CvRect *face, FRONTAL_FACIAL_FEATURES &feature)
    {
        int i;
        bool hasEyes = false;
        bool hasNose = false;
        bool hasMouth = false;
        cvRectangle(img,
                    cvPoint(face->x, face->y),
                    cvPoint(face->x + face->width, face->y + face->height),
                    CV_RGB(255, 0, 0), 1, 8, 0);

        // Set the Region of Interest
        CvRect eyeROI = cvRect(face->x, face->y + (face->height/5.5), face->width, face->height/3.0);
        CvRect noseROI = cvRect(face->x, face->y + (face->height/2.5), face->width, face->height/3.0);
        CvRect mouthROI = cvRect(face->x, face->y + (face->height/1.5), face->width, face->height/2.5);
        CvRect *r;

        // detect eyes
        cvSetImageROI(img, eyeROI);
        CvSeq* eyes = cvHaarDetectObjects(
                img, cascade_e, storage,
                1.15, 3, 0, cvSize(25, 15));
        cvResetImageROI(img);

        // detect nose
        cvSetImageROI(img, noseROI);
        CvSeq* noses = cvHaarDetectObjects(
                img, cascade_n, storage,
                1.1, 3, 0, cvSize(25, 15));
        cvResetImageROI(img);

        // detect Mouth
        cvSetImageROI(img, mouthROI);
        CvSeq* mouths = cvHaarDetectObjects(
                img, cascade_m, storage,
                1.1, 3, 0, Size(30, 30));
        cvResetImageROI(img);

        for( i = 0; i < 2; i++ ) {
            r = (CvRect*)cvGetSeqElem( eyes, i );
            int x1 = r->x + eyeROI.x;
            int y1 = r->y + eyeROI.y;
            int x2 = x1 + r->width;
            int y2 = y1 + r->height;

            feature.eye_features[i].eye_x1 = x1;
            feature.eye_features[i].eye_y1 = y1;
            feature.eye_features[i].eye_x2 = x2;
            feature.eye_features[i].eye_y2 = y2;
            feature.eye_features[i].eye_corner_to_corner_distance = cv::norm(Point2f(x1, y1) - Point2f(x2, y2));
        }

        // draw a rectangle for each nose found
        for( i = 0; i < 1; i++ ) {
            r = (CvRect*)cvGetSeqElem( noses, i );
            int x1 = r->x + noseROI.x;
            int y1 = r->y + noseROI.y;
            int x2 = x1 + r->width;
            int y2 = y1 + r->height;
            int xc = (x1 + x2)/2;
            int yc = (y1 + y2)/2;

            feature.nose_features[i].nose_x1 = x1;
            feature.nose_features[i].nose_y1 = y1;
            feature.nose_features[i].nose_x2 = x2;
            feature.nose_features[i].nose_y2 = y2;
            feature.nose_features[i].nose_corner_to_corner_distance = cv::norm(Point2f(x1, y1) - Point2f(x2, y2));
        }

        // draw a rectangle for each mouth found
        for( i = 0; i < 1; i++ ) {
            int margin_left = 10;
            int margin_right = 0;
            r = (CvRect*)cvGetSeqElem( mouths, i );
            int x1 = r->x + mouthROI.x;
            int y1 = r->y + mouthROI.y;
            int x2 = x1 + r->width;
            int y2 = y1 + r->height;
            int x1c = x1 + margin_left;
            int y1c = (y1 + y2)/2;
            int x2c = x2 - margin_right;
            int y2c = (y1 + y2)/2 - 5;

            feature.mouth_features[i].mouth_x1 = x1;
            feature.mouth_features[i].mouth_y1 = y1;
            feature.mouth_features[i].mouth_x2 = x2;
            feature.mouth_features[i].mouth_y2 = y2;
            feature.mouth_features[i].mouth_corner_to_corner_distance = cv::norm(Point2f(x1, y1) - Point2f(x2, y2));
            feature.mouth_features[i].mouth_x1c = x1c;
            feature.mouth_features[i].mouth_y1c = y1c;
            feature.mouth_features[i].mouth_x2c = x2c;
            feature.mouth_features[i].mouth_y2c = y2c;
            feature.mouth_features[i].mouth_top_to_bottom_distance = cv::norm(Point2f(x1, y1) - Point2f(x2, y2));
        }
    }

    void read_csv(const string& csvfile, vector<Mat>& resizedImages, vector<int>& labels, char separator = ';') {
        LOG("Entering read_csv()");
        std::ifstream file(csvfile.c_str(), ifstream::in);
        if (!file) {
            LOG("No valid input file was given, please check the given filename.");
        }

        vector<Mat> faces;
        string line, path, classlabel;
        try {
            while (getline(file, line)) {
                stringstream liness(line);
                getline(liness, path, separator);
                getline(liness, classlabel);
                if (!path.empty() && !classlabel.empty()) {
                    Mat m = imread(path);
                    if ( m.empty() ) {
                        stringstream ss3;
                        ss3 << "read_csv() " << path << " could not be read!";
                        LOG(ss3.str());
                        continue;
                    }
                    int posSlash = path.find_last_of("/");
                    string onlyFileName = path.substr(posSlash + 1);
                    int posUnderscore = onlyFileName.find_first_of("_");
                    string personName = onlyFileName.substr(0, posUnderscore);
                    stringstream ss44;
                    ss44 << "read_casv() Found." << endl
                         << "path         = " << path << endl
                         << "onlyFileName = " << onlyFileName << endl
                         << "personName   = " << personName << endl
                         << "label        = " << atoi(classlabel.c_str());
                    LOG(ss44.str());
                    m_model->setLabelInfo(atoi(classlabel.c_str()), personName.c_str());
                    faces.clear();
                    detect_faces(m, faces);
                    for (int i = 0; i < faces.size(); i++) {
                        cv::resize(faces[i], faces[i], global_min);
                        resizedImages.push_back(faces[i]);
                        labels.push_back(atoi(classlabel.c_str()));
                    }
                }
            }

            stringstream ss3;
            ss3 << "read_csv() : total number of faces detected : " << resizedImages.size();
            LOG(ss3.str());
        } catch (cv::Exception e) {
            stringstream ss;
            ss << "read_csv() exception occured : " << e.msg;
            LOG(ss.str());
        }
        LOG("Exiting read_csv()");
    }

    void to_grayscale(Mat img, Mat &gray) {
        try {
            Mat tempgray;
            cv::cvtColor(img, tempgray, COLOR_RGB2GRAY);
            equalizeHist(tempgray, gray);
        } catch (cv::Exception e) {
            stringstream ss;
            ss << "to_grayscale() exception occured : " << e.msg;
            LOG(ss.str());
        }
    }

    bool is_new_unique_face(FRONTAL_FACIAL_FEATURES features) {
        /*
         * TODO
         */
        return true;
    }

    void detect_faces(Mat img, vector<Mat> &mats) {
        try {
            CascadeClassifier cascade = CascadeClassifier(m_haarcascade_frontalface_xmlPath.c_str());
            if (cascade.empty()) {
                LOG("detect_faces() : Could not obtain the cascade classified");
            }
            Mat gray;
            to_grayscale(img, gray);
            vector<Rect> rects;
            cascade.detectMultiScale(gray, rects);
            for (int i = 0; i < rects.size(); i++) {
                Mat tempmats[1];
                tempmats[0] = gray(rects[i]).clone();
                cv::resize(tempmats[0], tempmats[0], global_min);
                mats.push_back(tempmats[0]);
            }
            rects.clear();
        } catch (cv::Exception e) {
            stringstream ss;
            ss << "detect_faces() exception occured : " << e.msg;
            LOG(ss.str());
        }
    }

public:
    WD360FaceDetection(string type, string xmlPath,
                       string dogXmlPath, string catXmlPath,
                       string modelSavePath, int load) {
        LOG("Entering WD360FaceDetection() constructor ...");
        int exists = 0;
        m_faceRecognizer_loaded = 1;
        m_haarcascade_frontalface_xmlPath = xmlPath;
        m_dogXmlPath = dogXmlPath;
        m_catXmlPath = catXmlPath;
        m_modelSavePath = modelSavePath;
        m_load = load;
        ifstream ifile(m_modelSavePath.c_str());
        if (ifile) {
            exists = 1;
            stringstream ss;
            ss << "WD360FaceDetection() [SUCCESS] " << m_modelSavePath << " : file exists";
            LOG(ss.str());
        } else {
            exists = 0;
            stringstream ss;
            ss << "WD360FaceDetection() [ERROR] " << m_modelSavePath << " : file doesn't exist";
            LOG(ss.str());
        }
        try {
            if ((1 == load) && (1 == exists)) {
                if (!type.compare("fisher")) {
                    Ptr<cv::face::FisherFaceRecognizer> tempmodel = cv::face::FisherFaceRecognizer::load<cv::face::FisherFaceRecognizer>(modelSavePath.c_str());
                    m_model = tempmodel;
                    LOG("WD360FaceDetection() Loaded FisherFaceRecognizer model successfully");
                } else if (!type.compare("eigen")) {
                    Ptr<cv::face::EigenFaceRecognizer> tempmodel = cv::face::EigenFaceRecognizer::load<cv::face::EigenFaceRecognizer>(modelSavePath.c_str());
                    m_model = tempmodel;
                    LOG("WD360FaceDetection() Loaded EigenFaceRecognizer model successfully");
                } else if (!type.compare("lbph")) {
                    Ptr<cv::face::LBPHFaceRecognizer> tempmodel = cv::face::LBPHFaceRecognizer::load<cv::face::LBPHFaceRecognizer>(modelSavePath.c_str());
                    m_model = tempmodel;
                    LOG("WD360FaceDetection() Loaded LBPHFaceRecognizer model successfully");
                } else {
                    LOG("Error training faces. Reason: Unknown FaceRecognizer");
                }
            } else if (0 == load) {
                if (!type.compare("fisher")) {
                    m_model = cv::face::FisherFaceRecognizer::create();
                    LOG("WD360FaceDetection() created FisherFaceRecognizer");
                } else if (!type.compare("eigen")) {
                    m_model = cv::face::EigenFaceRecognizer::create();
                    LOG("WD360FaceDetection() created EigenFaceRecognizer");
                } else if (!type.compare("lbph")) {
                    m_model = cv::face::LBPHFaceRecognizer::create();
                    LOG("WD360FaceDetection() created LBPHFaceRecognizer");
                } else {
                    m_faceRecognizer_loaded = 0;
                    LOG("Error training faces. Reason: Unknown FaceRecognizer");
                }
            } else {
                m_faceRecognizer_loaded = 0;
                LOG("Error training faces. Reason: Invalid load value, can be either 0 or 1 only or yml file doesn't exist");
            }
        } catch (cv::Exception e) {
            m_faceRecognizer_loaded = 0;
            stringstream ss;
            ss << "WD360FaceDetection() exception occured : " << e.msg;
            LOG(ss.str());
        }
        LOG("Exiting WD360FaceDetection() constructor ...");
    }

    ~WD360FaceDetection() {
        LOG("Entering WD360FaceDetection() destructor ...");
        LOG("Exiting WD360FaceDetection() destructor ...");
    }

    void get_unique_faces(string imagePath) {
        IplImage *img;
        const char *file1 = m_haarcascade_frontalface_xmlPath.c_str();
        const char *file2 = m_haarcascade_eye_xmlPath.c_str();
        const char *file3 = m_haarcascade_mcs_nose_xmlPath.c_str();
        const char *file4 = m_haarcascade_mcs_mouth_xmlPath.c_str();

        cascade_f = (CvHaarClassifierCascade*)cvLoad(file1, 0, 0, 0);
        cascade_e = (CvHaarClassifierCascade*)cvLoad(file2, 0, 0, 0);
        cascade_n = (CvHaarClassifierCascade*)cvLoad(file3, 0, 0, 0);
        cascade_m = (CvHaarClassifierCascade*)cvLoad(file4, 0, 0, 0);

        // setup memory storage, needed by the object detector
        storage = cvCreateMemStorage(0);

        String imgFPath(imagePath.c_str());
        Mat sample = imread(imgFPath);
        vector<Mat> faces;
        detect_faces(sample, faces);

        for (int i = 0; i < faces.size(); i++) {
            cv::resize(faces[i], faces[i], global_min);
            if (faces[i].size() != global_min) {
                LOG("predict() failed to resize an image");
                continue;
            }

            FRONTAL_FACIAL_FEATURES feature;
            cv::Rect rect = cv::Rect(0, 0, faces[i].size().width, faces[i].size().height);
            feature.face_features[i].face_x1 = rect.x;
            feature.face_features[i].face_y1 = rect.y;
            feature.face_features[i].face_x2 = rect.x + rect.height;
            feature.face_features[i].face_y2 = rect.y + rect.width;

            //detectFaceFeatures(img, &rect, feature);

            if(true == is_new_unique_face(feature)) {
                unique_faces[index_unique_faces] = feature;
                index_unique_faces++;
            }
        }
        cvReleaseImage(&img);
    }

    void train(string csvfile) {
        stringstream ss;
        ss << "Entering train() with csvfile : " << csvfile;
        LOG(ss.str());
        vector<Mat> images;
        vector<int> labels;

        if (m_faceRecognizer_loaded == 0) {
            LOG("No faceRecognizer was loaded");
            LOG("Exiting WD360FaceDetection() destructor ...");
            return;
        }

        try {
            read_csv(csvfile, images, labels);
        } catch (cv::Exception& e) {
            stringstream ss;
            ss << "train() Error opening file " << csvfile << ". Reason: " << e.msg;
            LOG(ss.str());
        }

        if (images.size() <= 1) {
            stringstream ss;
            ss << "train() This demo needs at least 2 images to work. Please add more images to your data set!" << csvfile;
            LOG(ss.str());
        }

        try {
            m_model->train(images, labels);
            m_model->save(m_modelSavePath.c_str());
        } catch (cv::Exception& e) {
            stringstream ss;
            ss << "Error training faces. Reason: " << e.msg;
            LOG(ss.str());
        }
        stringstream ss1;
        ss1 << "Exiting train() with csvfile : " << csvfile;
        LOG(ss1.str());
    }

    void update(string newcsvfile) {
        stringstream ss;
        ss << "Entering update() with csvfile : " << newcsvfile;
        LOG(ss.str());
        vector<Mat> newimages;
        vector<int> newlabels;
        if (m_faceRecognizer_loaded == 0) {
            LOG("No faceRecognizer was loaded");
            LOG("Exiting WD360FaceDetection() destructor ...");
            return;
        }

        try {
            read_csv(newcsvfile, newimages, newlabels);
        } catch (cv::Exception& e) {
            stringstream ss;
            ss << "update() Error opening file " << newcsvfile << ". Reason: " << e.msg;
            LOG(ss.str());
        }

        if (newimages.size() <= 1) {
            stringstream ss;
            ss << "update() This demo needs at least 2 images to work. Please add more images to your data set!" << newcsvfile;
            LOG(ss.str());
        }

        try {
            m_model->update(newimages, newlabels);
            m_model->save(m_modelSavePath.c_str());
        } catch (cv::Exception& e) {
            stringstream ss;
            ss << "Error updating() faces. Reason: " << e.msg;
            LOG(ss.str());
        }
        stringstream ss1;
        ss1 << "Exiting update() with csvfile : " << newcsvfile;
        LOG(ss1.str());
    }

    int sortbydatetime(const struct dirent **a, const struct dirent **b)
    {
        int rval;
        struct stat sbuf2, sbuf1;
        char path1[PATH_MAX], path2[PATH_MAX];

        rval = stat(path1, &sbuf1);
        if (rval) {
            stringstream ss;
            ss << "sortbydatetime() : stat() error" << rval;
            LOG(ss.str());
            return 0;
        }
        rval = stat(path2, &sbuf2);
        if (rval) {
            stringstream ss;
            ss << "sortbydatetime() : stat() error" << rval;
            LOG(ss.str());
            return 0;
        }

        return sbuf1.st_mtime < sbuf2.st_mtime;
    }

    void getAllTrainedFacesAndTheirLabelsUntilNow(string &result, string frmFolder, string type) {
        LOG("Entering getAllTrainedFacesAndTheirLabelsUntilNow()");
        result = "";

        stringstream ymlFileName;
        ymlFileName << frmFolder << "/WD360.yml";
        stringstream ss1;
        ss1 << "getAllTrainedFacesAndTheirLabelsUntilNow() called with = " << ymlFileName.str().c_str();
        LOG(ss1.str());
        std::ifstream infile(ymlFileName.str().c_str());
        if (!infile.good()) {
            stringstream ss;
            ss << "getAllTrainedFacesAndTheirLabelsUntilNow() Reason: " << ymlFileName.str() << " file not found";
            LOG(ss.str());
            LOG("Exiting getAllTrainedFacesAndTheirLabelsUntilNow()");
            return;
        }
        Ptr<cv::face::FaceRecognizer> temp_model;
        if (!type.compare("fisher")) {
            temp_model = cv::face::FisherFaceRecognizer::load<cv::face::FisherFaceRecognizer>(
                    ymlFileName.str().c_str());
        } else if (!type.compare("eigen")) {
            temp_model = cv::face::EigenFaceRecognizer::load<cv::face::EigenFaceRecognizer>(
                    ymlFileName.str().c_str());
        } else if (!type.compare("lbph")) {
            temp_model = cv::face::LBPHFaceRecognizer::load<cv::face::LBPHFaceRecognizer>(
                    ymlFileName.str().c_str());
        } else {
            LOG("Error training faces. Reason: Unknown FaceRecognizer");
            LOG("Exiting getAllTrainedFacesAndTheirLabelsUntilNow()");
            return;
        }
        for (int i = 1; i < 0XFFFFFFFF; i++) {
            String face = temp_model->getLabelInfo(i);
            if (face.length() == 0) {
                break;
            }
            if (1 == i){
                stringstream ss1;
                ss1 << face << "_" << i;
                result.append(ss1.str());
            } else {
                stringstream ss2;
                ss2 << ":" << face << "_" << i;
                result.append(ss2.str());
            }
        }

        LOG("Exiting getAllTrainedFacesAndTheirLabelsUntilNow()");
    }

    /*
     * type = 0, Human.
     * type = 1, Dog,
     * type = 2, Cat,
     */
    int howManyFaces(string imagefilepath, int type) {
        LOG("Entering howManyFaces() ...");
        int numOfFaces = 0;
        Mat sample;
        try {
            CascadeClassifier cascade;
            String imgFPath(imagefilepath.c_str());
            sample = imread(imgFPath);
            if (0 == type) {
                cascade = CascadeClassifier(m_haarcascade_frontalface_xmlPath.c_str());
            } else if (1 == type) {
                cascade = CascadeClassifier(m_dogXmlPath.c_str());
            } else if (2 == type) {
                cascade = CascadeClassifier(m_catXmlPath.c_str());
            }
            if (cascade.empty()) {
                LOG("howManyFaces() : Could not obtain the cascade classified");
            }
            Mat gray;
            to_grayscale(sample, gray);
            vector<Rect> rects;
            stringstream ss;
            ss << "howManyFaces() detectMultiScale() on file : " << imagefilepath;
            LOG(ss.str());
            cascade.detectMultiScale(gray, rects);
            numOfFaces = rects.size();
            rects.clear();
        } catch (cv::Exception e) {
            stringstream ss;
            ss << "Error howManyFaces() faces. Reason: " << e.msg;
            LOG(ss.str());
        }
        LOG("Exiting howManyFaces() ...");
        return numOfFaces;
    }

    void getDimension(string imagefilepath, string &underscore_delimite_height_width_area) {
        LOG("Entering getDimension() ...");
        try {
            String imgFPath(imagefilepath.c_str());
            Mat sample = imread(imgFPath);
            Size size = sample.size();
            stringstream ssret;
            ssret << size.height << "_" << size.width << "_" << size.area();
            underscore_delimite_height_width_area.assign(ssret.str());
        } catch (cv::Exception e) {
            stringstream ss;
            ss << "Error getDimension() faces. Reason: " << e.msg;
            LOG(ss.str());
        }
        LOG("Exiting getDimension() ...");
    }

    void predict(string imagefilepath, string &predictedfaces) {
        LOG("Entering predict() ...");
        if (m_faceRecognizer_loaded == 0) {
            LOG("No faceRecognizer was loaded");
            LOG("Exiting WD360FaceDetection() destructor ...");
            return;
        }
        Mat sample;
        try {
            String imgFPath(imagefilepath.c_str());
            sample = imread(imgFPath);
            vector<Mat> faces;
            detect_faces(sample, faces);

            for (int i = 0; i < faces.size(); i++) {
                cv::resize(faces[i], faces[i], global_min);
                if (faces[i].size() != global_min) {
                    LOG("predict() failed to resize an image");
                    continue;
                }

                int predictedLabel = -1;
                double confidence = 0.0;
                m_model->predict(faces[i], predictedLabel, confidence);
                String name = m_model->getLabelInfo(predictedLabel);
                stringstream ss2;
                ss2 << "nativePredict() : " << imagefilepath << " detected face [" << i << "] : Name = " << name << ", Confidence = " << confidence;
                LOG(ss2.str());
                if (confidence == 0.0) {
                    predictedfaces.append(name);
                    predictedfaces.append("_");
                }
            }
        } catch (cv::Exception e) {
            stringstream ss;
            ss << "Error predict() faces. Reason: " << e.msg;
            LOG(ss.str());
        }
        LOG("Exiting predict() ...");
    }
};

string jstring2string(JNIEnv *env, jstring jStr) {
    string ret = "";
    if (!jStr) {
        LOG("Exiting jstring2string() : Reason jni string is null...");
        return "";
    }

    try {
        vector<char> charsCode;
        const jchar *chars = env->GetStringChars(jStr, NULL);
        jsize len = env->GetStringLength(jStr);
        jsize i;
        for( i=0;i<len; i++){
            int code = (int)chars[i];
            charsCode.push_back(code);
        }
        env->ReleaseStringChars(jStr, chars);
        ret = string(charsCode.begin(), charsCode.end());
    } catch (exception e) {
        stringstream ss;
        ss << "Error jstring2string() Reason: " << e.what();
        LOG(ss.str());
    }
    return ret;
}

extern "C"
JNIEXPORT jlong
JNICALL
Java_com_wdc_www_wd360_WD360FaceDetection_nativeCreateObject
        (JNIEnv *p_jenv, jobject p_jthis, jstring p_jtype,
         jstring p_jlogpath, jstring p_jxmlpath,
         jstring p_jdogxmlpath, jstring p_jcatxmlpath,
         jstring p_jmodelsavepath, jint p_jload) {
    logpath = jstring2string(p_jenv, p_jlogpath);
    CLEAR_OLD_LOG();
    LOG("Entering nativeCreateObject() ...");
    jlong result = 0;
    try {
        string stdType = jstring2string(p_jenv, p_jtype);
        string xmlPath = jstring2string(p_jenv, p_jxmlpath);
        string dogXmlPath = jstring2string(p_jenv, p_jdogxmlpath);
        string catXmlPath = jstring2string(p_jenv, p_jcatxmlpath);
        string modelSavePath = jstring2string(p_jenv, p_jmodelsavepath);
        int load = p_jload;

        global_min.height = 100;
        global_min.width = 100;

        stringstream ss1;
        ss1 << "nativeCreateObject() : called with : "
            << endl << "Type            = " << stdType
            << endl << "Log Path        = " << logpath
            << endl << "XML Path        = " << xmlPath
            << endl << "Dog XML Path    = " << dogXmlPath
            << endl << "Cat XML Path    = " << catXmlPath
            << endl << "Model Save Path = " << modelSavePath
            << endl << "Load            = " << load;
        LOG(ss1.str());
        result = (jlong)new WD360FaceDetection(stdType, xmlPath, dogXmlPath,
                                               catXmlPath, modelSavePath, load);
    } catch (cv::Exception& e) {
        stringstream ss;
        ss << "Error nativeCreateObject(). Reason: " << e.msg;
        LOG(ss.str());
    }

    LOG("Exiting nativeCreateObject() ...");
    return result;
}

extern "C"
JNIEXPORT void
JNICALL
Java_com_wdc_www_wd360_WD360FaceDetection_nativeDestroyObject
        (JNIEnv *p_jenv, jobject p_jthis, jlong p_native_ptr) {
    LOG("Entering nativeDestroyObject() ...");
    try {
        if (p_native_ptr != 0) {
            delete (WD360FaceDetection*)p_native_ptr;
        }
    } catch (cv::Exception& e) {
        stringstream ss;
        ss << "Error nativeDestroyObject(). Reason: " << e.msg;
        LOG(ss.str());
    }

    LOG("Exiting nativeDestroyObject() ...");
}

extern "C"
JNIEXPORT void
JNICALL
Java_com_wdc_www_wd360_WD360FaceDetection_nativeLogOn
        (JNIEnv *p_jenv, jobject p_jthis, jlong p_native_ptr) {
    LOG("Entering nativeLogOn() ...");
    logon = true;
    LOG("Entering nativeLogOff() ...");
}

extern "C"
JNIEXPORT void
JNICALL
Java_com_wdc_www_wd360_WD360FaceDetection_nativeLogOff
        (JNIEnv *p_jenv, jobject p_jthis, jlong p_native_ptr) {
    LOG("Entering nativeLogOn() ...");
    logon = false;
    LOG("Entering nativeLogOff() ...");
}

extern "C"
JNIEXPORT jstring
JNICALL
Java_com_wdc_www_wd360_WD360FaceDetection_nativeGetAllTrainedFacesAndTheirLabelsUntilNow
        (JNIEnv *p_jenv, jobject p_jthis, jlong p_native_ptr, jstring p_jfrm_folder, jstring p_jtype) {
    LOG("Entering nativeGetAllTrainedFacesAndTheirLabelsUntilNow() ...");
    jstring result = (jstring)"";
    try {
        if (p_native_ptr != 0) {
            string stdfrmfolder = jstring2string(p_jenv, p_jfrm_folder);
            string stdType = jstring2string(p_jenv, p_jtype);
            stringstream ss1;
            ss1 << "nativeGetAllTrainedFacesAndTheirLabelsUntilNow() : called with : "
                << endl << "FaceRecognizer Model Folder Path = " << stdfrmfolder;
            LOG(ss1.str());
            string stdResult = "";
            ((WD360FaceDetection*)p_native_ptr)->getAllTrainedFacesAndTheirLabelsUntilNow(stdResult, stdfrmfolder, stdType);
            char bytes[1024];
            memset(bytes, 0, sizeof(bytes));
            for (int i = 0; i < stdResult.size(); i++) {
                bytes[i] = (char)stdResult[i];
            }
            result = p_jenv->NewStringUTF(bytes);
        }
    } catch (cv::Exception& e) {
        stringstream ss;
        ss << "Error nativeGetAllTrainedFacesAndTheirLabelsUntilNow(). Reason: " << e.msg;
        LOG(ss.str());
        result = (jstring)"";
    }

    LOG("Exiting nativeGetAllTrainedFacesAndTheirLabelsUntilNow() ...");
    return result;
}

extern "C"
JNIEXPORT void
JNICALL
Java_com_wdc_www_wd360_WD360FaceDetection_nativeTrain
        (JNIEnv *p_jenv, jobject p_jthis, jlong p_native_ptr, jstring p_jcsvfile) {
    LOG("Entering nativeTrain() ...");
    try {
        if(p_native_ptr != 0) {
            string stdcsvfile = jstring2string(p_jenv, p_jcsvfile);
            stringstream ss1;
            ss1 << "nativeTrain() : called with : "
                << endl << "CSV File Path = " << stdcsvfile;
            LOG(ss1.str());
            ((WD360FaceDetection*)p_native_ptr)->train(stdcsvfile);
        } else {
            LOG("nativeTrain() : Null face detection handle");
        }
    } catch (cv::Exception& e) {
        stringstream ss;
        ss << "Error nativeTrain(). Reason: " << e.msg;
        LOG(ss.str());
    }
    LOG("Exiting nativeTrain() ...");
}

extern "C"
JNIEXPORT void
JNICALL
Java_com_wdc_www_wd360_WD360FaceDetection_nativeUpdate
        (JNIEnv *p_jenv, jobject p_jthis, jlong p_native_ptr, jstring p_jnewcsvfile) {
    LOG("Entering nativeUpdate() ...");
    try {
        if(p_native_ptr != 0) {
            string newstdcsvfile = jstring2string(p_jenv, p_jnewcsvfile);
            stringstream ss1;
            ss1 << "nativeUpdate() : called with : "
                << endl << "New CSV File Path = " << newstdcsvfile;
            LOG(ss1.str());
            ((WD360FaceDetection*)p_native_ptr)->update(newstdcsvfile);
        } else {
            LOG("nativeUpdate() : Null face detection handle");
        }
    } catch (cv::Exception& e) {
        stringstream ss;
        ss << "Error nativeUpdate(). Reason: " << e.msg;
        LOG(ss.str());
    }
    LOG("Entering nativeUpdate() ...");
}

extern "C"
JNIEXPORT jstring
JNICALL
Java_com_wdc_www_wd360_WD360FaceDetection_nativePredict
        (JNIEnv *p_jenv, jobject p_jthis, jlong p_native_ptr, jstring p_jimagefilepath) {
    LOG("Entering nativePredict() ...");
    jstring result = (jstring)"";
    try {
        if(p_native_ptr != 0) {
            string stdimagefilepath = jstring2string(p_jenv, p_jimagefilepath);
            stringstream ss1;
            ss1 << "nativePredict() : called with : "
                << endl << "Image file path = " << stdimagefilepath;
            LOG(ss1.str());
            string predictedfaces;
            ((WD360FaceDetection*)p_native_ptr)->predict(stdimagefilepath, predictedfaces);
            char bytes[1024];
            memset(bytes, 0, sizeof(bytes));
            for (int i = 0; i < predictedfaces.size(); i++) {
                bytes[i] = (char)predictedfaces[i];
            }
            result = p_jenv->NewStringUTF(bytes);
        } else {
            LOG("nativePredict() : Null face detection handle");
        }
    } catch (cv::Exception& e) {
        stringstream ss;
        ss << "Error nativePredict(). Reason: " << e.msg;
        LOG(ss.str());
        result = (jstring)"";
    }
    LOG("Exiting nativePredict() ...");
    return result;
}

extern "C"
JNIEXPORT jint
JNICALL
Java_com_wdc_www_wd360_WD360FaceDetection_nativeHowManyFaces
        (JNIEnv *p_jenv, jobject p_jthis, jlong p_native_ptr,
         jstring p_jimagefilepath, jint p_jtype) {
    jint ret = 0;
    LOG("Entering nativeHowManyFaces() ...");
    try {
        if(p_native_ptr != 0) {
            int type = p_jtype;
            string stdimagefilepath = jstring2string(p_jenv, p_jimagefilepath);
            stringstream ss1;
            ss1 << "nativeHowManyFaces() : called with : "
                << endl << "Image file path = " << stdimagefilepath
                << endl << "Type            = " << type;
            LOG(ss1.str());
            ret = (jint)((WD360FaceDetection*)p_native_ptr)->howManyFaces(stdimagefilepath, type);
        } else {
            LOG("nativeHowManyFaces() : Null face detection handle");
        }
    } catch (cv::Exception& e) {
        ret = 0;
        stringstream ss;
        ss << "Error nativeHowManyFaces(). Reason: " << e.msg;
        LOG(ss.str());
    }
    LOG("Exiting nativeHowManyFaces() ...");
    return ret;
}

extern "C"
JNIEXPORT jstring
JNICALL
Java_com_wdc_www_wd360_WD360FaceDetection_nativeGetDimension
        (JNIEnv *p_jenv, jobject p_jthis, jlong p_native_ptr, jstring p_jimagefilepath) {
    LOG("Entering nativeGetDimension() ...");
    jstring result = (jstring)"";
    try {
        if (p_native_ptr != 0) {
            string stdimagefilepath = jstring2string(p_jenv, p_jimagefilepath);
            stringstream ss1;
            ss1 << "nativeGetDimension() : called with : "
                << endl << "Image File Path = " << stdimagefilepath;
            LOG(ss1.str());
            string stdResult = "";
            ((WD360FaceDetection*)p_native_ptr)->getDimension(stdimagefilepath, stdResult);
            char bytes[1024];
            memset(bytes, 0, sizeof(bytes));
            for (int i = 0; i < stdResult.size(); i++) {
                bytes[i] = (char)stdResult[i];
            }
            result = p_jenv->NewStringUTF(bytes);
        }
    } catch (cv::Exception& e) {
        stringstream ss;
        ss << "Error nativeGetDimension(). Reason: " << e.msg;
        LOG(ss.str());
    }

    LOG("Exiting nativeGetDimension() ...");
    return result;
}

package com.wdc.www.wd360;

/**
 * Created by Biswapratap Chatterjee on 8/11/2017.
 */

public class Log {
    public static String turnOnLogging() {
        Globals.LOG = true;
        return JSONUtils.formResponseValues("200",
                "",
                "",
                "",
                "",
                "",
                "",
                Globals.emptyResponse);
    }

    public static String turnOffLogging() {
        Globals.LOG = false;
        return JSONUtils.formResponseValues("200",
                "",
                "",
                "",
                "",
                "",
                "",
                Globals.emptyResponse);
    }

    public static void i(String tag, String string) {
        if (Globals.LOG) android.util.Log.i(tag, string);
    }

    public static void e(String tag, String string) {
        if (Globals.LOG) android.util.Log.e(tag, string);
    }

    public static void d(String tag, String string) {
        if (Globals.LOG) android.util.Log.d(tag, string);
    }

    public static void v(String tag, String string) {
        if (Globals.LOG) android.util.Log.v(tag, string);
    }

    public static void w(String tag, String string) {
        if (Globals.LOG) android.util.Log.w(tag, string);
    }
}
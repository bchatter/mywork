package com.wdc.www.wd360;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Biswapratap Chatterjee on 7/11/2017.
 */

public class JSONUtils {
    private static final String TAG = JSONUtils.class.getSimpleName();

    public static String jsonResponseCreator(String [] values,
                                             String [] result) {
        try {
            JSONArray allDataArray = new JSONArray();
            JSONObject json = new JSONObject();
            json.put("httpstatus", values[0]);
            json.put("username", values[1]);
            json.put("cmd", values[2]);
            json.put("jobid", values[3]);
            json.put("jobstate", values[4]);
            json.put("jobpercent", values[5]);
            json.put("errormessage", values[6]);
            if (!(result.length == 0)) {
                for(int index = 0; index < result.length; index++) {
                    JSONObject eachData = new JSONObject();
                    try {
                        eachData.put("resultvalue", result[index]);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    allDataArray.put(eachData);
                }
            }
            json.put("result", allDataArray);
            return json.toString();
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return "ASSERT : " + e.getMessage();
        }
    }

    public static String formResponseValues(String httpstatus,
                                            String username,
                                            String cmd,
                                            String jobid,
                                            String jobstate,
                                            String jobpercent,
                                            String errormessage,
                                            String [] result) {
        try {
            String [] respVals = new String[7];
            respVals[0] = httpstatus;
            respVals[1] = username;
            respVals[2] = cmd;
            respVals[3] = jobid;
            respVals[4] = jobstate;
            respVals[5] = jobpercent;
            respVals[6] = errormessage;
            return jsonResponseCreator(respVals, result);
        } catch (Exception e) {
            Log.d(TAG, "ASSERT : formResponseValues() : " + e.getMessage());
            return "ASSERT : " + e.getMessage();
        }
    }

    public static String initialChecks(String [] keyValues,
                                       String [] cmd_opcode) {
        try {
            if (keyValues.length == 0)
                return formResponseValues("400",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "No Parameters found",
                        Globals.emptyResponse);
            else {
                String[] command = keyValues[0].split("=");
                if (command.length != 2)
                    return formResponseValues("400",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "Invalid command parameter structure, are you missing a =",
                            Globals.emptyResponse);
                else {
                    if (!command[0].equalsIgnoreCase("cmd"))
                        return formResponseValues("400",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "Invalid Command param name, it must be 'cmd'",
                                Globals.emptyResponse);
                    else {
                        cmd_opcode[0] = command[1];
                        return "SUCCESS";
                    }
                }
            }
        } catch (Exception e) {
            Log.d(TAG, "ASSERT : initialChecks() : " + e.getMessage());
            return "ASSERT : " + e.getMessage();
        }
    }

    public static String commandSpecificChecks(String[] keyValues,
                                               String cmd_opcode,
                                               int expectedKeyLength) {
        try {
            if (keyValues.length != expectedKeyLength)
                return formResponseValues("400",
                        "",
                        cmd_opcode,
                        "",
                        "",
                        "",
                        "Requires at least " + expectedKeyLength + " key value pairs",
                        Globals.emptyResponse);
            else {
                return "SUCCESS";
            }
        } catch (Exception e) {
            Log.d(TAG, "ASSERT : commandSpecificChecks() : " + e.getMessage());
            return "ASSERT : " + e.getMessage();
        }
    }
}

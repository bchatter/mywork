package com.wdc.www.wd360;

import android.provider.BaseColumns;

/**
 * Created by Biswapratap Chatterjee on 7/3/2017.
 */

public final class MasterDBContract {
    private MasterDBContract() {}

    /* Inner class that defines the table contents */
    public static class MasterDBBaseColumns implements BaseColumns {
        public static final String COLUMN_NAME_FILE_ABSOLUTE_PATH = "absolutepath";
        public static final String COLUMN_IMAGE_IS_A_GROUP_IMAGE = "isgroup";
        public static final String COLUMN_IMAGE_NEAR_DUPLICATE_HASH_VALUE = "nearduplicatehashvalue";
        public static final String COLUMN_DOC_INDEXED = "isDocIndexed";
    }
}
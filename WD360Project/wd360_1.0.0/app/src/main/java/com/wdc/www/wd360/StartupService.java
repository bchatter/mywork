package com.wdc.www.wd360;

import android.app.Service;
import android.content.Intent;
import android.os.FileObserver;

import com.wdc.nassdk.BaseStartupService;
import com.wdc.nassdk.MyCloudUIServer;

import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;

/**
 * Created by Biswapratap Chatterjee on 5/12/2017.
 */

public class StartupService extends BaseStartupService {
    private static String TAG = StartupService.class.getSimpleName();
    public static FileObserver observer;

    @Override
    public MyCloudUIServer createMyCloudUIServer() {
        return new WD360AppUI(getApplicationContext());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "[INIT] StartupService:onStartCommand() Entering");
        Globals.myCloudUserId = intent.getStringExtra("MyCloudId");
        Log.d(TAG, "[INIT] StartupService:onStartCommand() Globals.myCloudUserId = " + Globals.myCloudUserId);
        flags = Service.START_STICKY;

        if (false == StartupServiceStatus.getServiceStatus()) {
            StartupServiceStatus.setServiceStatus(true);
            Globals.gContext = getApplicationContext();
            FileUtils.deleteQuietly(Globals.gContext.getCacheDir());

            try {
                Globals.ROOT_FOLDER_NAME = MyCloudUIServer.getRootFolder(Globals.gContext, "");
                if (Globals.ROOT_FOLDER_NAME.endsWith(File.separator)) {
                    Globals.ROOT_FOLDER_NAME = MyCloudUIServer.getRootFolder(Globals.gContext, "");
                } else {
                    Globals.ROOT_FOLDER_NAME = MyCloudUIServer.getRootFolder(Globals.gContext, "") + File.separator;
                }

                String splits[] = Globals.ROOT_FOLDER_NAME.split(File.separator);
                Globals.ROOT_FOLDER_NAME = "";
                for (int i = 0; i < splits.length; i++) {
                    Globals.ROOT_FOLDER_NAME = Globals.ROOT_FOLDER_NAME + splits[i] + File.separator;
                }

                if ((null == Globals.gContext) || (Globals.myCloudUserId.isEmpty())) {
                    Log.e(TAG, "******************* Either Globals.gContext is NULL or Globals.myCloudUserId is empty");
                    Log.d(TAG, "[INIT} StartupService:onStartCommand() Exiting");
                    super.onStartCommand(intent, flags, startId);
                }

                String response = "200";
                File userRootFolder = new File(Globals.ROOT_FOLDER_NAME + Globals.myCloudUserId);
                if ((null == userRootFolder) || (false == userRootFolder.exists())) {
                    String[] keyValues = new String[3];
                    keyValues[1] = "username=" + Globals.myCloudUserId;
                    keyValues[0] = "cmd=user_login";
                    keyValues[2] = "passwd=1234";
                    String cmd_opcode = "user_login";
                    response = ExecuteUserLoginCommand.executeUserLogin(keyValues, cmd_opcode);
                } else {
                    File resultsFolder = new File(Globals.ROOT_FOLDER_NAME + Globals.WD360_USER_RESULTS_ROOT_FOLDER);
                    if ((null == resultsFolder) || (false == resultsFolder.exists())) {
                        String[] keyValues = new String[3];
                        keyValues[1] = "username=" + Globals.myCloudUserId;
                        keyValues[0] = "cmd=user_login";
                        keyValues[2] = "passwd=1234";
                        String cmd_opcode = "user_login";
                        response = ExecuteUserLoginCommand.executeUserLogin(keyValues, cmd_opcode);
                    }
                }

                if (response.contains("200")) {
                    Thread periodicThread = new Thread(new PeriodicMinerThread());
                    periodicThread.start();

                    Thread observerThread = new Thread(new ObserverMinerThread());
                    observerThread.start();
                } else {
                    Log.e(TAG, "Failed to start Server");
                }
            } catch (Exception e) {
                Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            }
        } else {
            Log.d(TAG, "[INIT] StartupService:onStartCommand() ALREADY STARTED");
        }
        Log.d(TAG, "[INIT] StartupService:onStartCommand() Exiting");
        return super.onStartCommand(intent, flags, startId);
    }
}

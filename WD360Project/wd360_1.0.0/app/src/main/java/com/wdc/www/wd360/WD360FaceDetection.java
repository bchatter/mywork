package com.wdc.www.wd360;

/**
 * Created by Biswapratap Chatterjee on 7/21/2017.
 */

public class WD360FaceDetection {
    private String m_logpath;
    private String m_xmlpath;
    private static String TAG = WD360FaceDetection.class.getSimpleName();

    static {
        System.loadLibrary("native-lib");
    }

    public WD360FaceDetection(String logpath,
                              String xmlpath) {
        m_logpath = logpath;
        m_xmlpath = xmlpath;
        mNativeObj = nativeCreateObject(
                m_logpath,
                m_xmlpath);
    }

    /*
     * type = 0, Human.
     */
    public int howManyFaces(String imagefilepath, int type) {
        int ret = 0;
        try {
            ret = nativeHowManyFaces(mNativeObj, imagefilepath, type);
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        return ret;
    }

    public void release() {
        try {
            nativeDestroyObject(mNativeObj);
            mNativeObj = 0;
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    private static long mNativeObj = 0;
    private native long nativeCreateObject(String logpath, String xmlpath);
    private native void nativeDestroyObject(long thiz);
    private native int nativeHowManyFaces(long thiz, String imagefilepath, int type);
}

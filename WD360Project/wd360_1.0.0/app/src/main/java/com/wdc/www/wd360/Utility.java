package com.wdc.www.wd360;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.webkit.MimeTypeMap;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by Biswapratap Chatterjee on 7/5/2017.
 */

public class Utility {
    private static final String TAG = Utility.class.getSimpleName();
    public static final String newline = System.lineSeparator();
    private static WD360FaceDetection temp_wdfd;

    public static WD360FaceDetection getTempWDFD(String userAbsRootFolder, Boolean force) {
        if (false == force) {
            if (null != temp_wdfd)
                return temp_wdfd;
        }
        temp_wdfd = new WD360FaceDetection(
                userAbsRootFolder + Globals.WD360_NATIVE_LOG_FOLDER + File.separator + "WD360.log",
                "NA");
        return temp_wdfd;
    }

    public static void getCurrentGroupResults() {
        try {
            String username = Globals.myCloudUserId;
            MasterDBHelper masterDBHelper = new MasterDBHelper(Globals.gContext);
            masterDBHelper.setUserName(username);
            String groupBaseFolder = Globals.ROOT_FOLDER_NAME + File.separator +
                    Globals.WD360_USER_RESULTS_ROOT_FOLDER + File.separator +
                    Globals.WD360_USER_IMAGE_SEARCH_BASE_FOLDER + File.separator +
                    Globals.WD360_USER_IMAGE_SEARCH_GROUP_FOLDER + File.separator;

            FileUtils.cleanDirectory(new File(groupBaseFolder));
            String[] group_results = new String[Globals.RESULT_SIZE];
            Map<String, Integer> groups = masterDBHelper.getAllGroupPics();
            String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date());
            String folderName = groupBaseFolder + timeStamp + File.separator;
            File destination = Utility.createFolder(folderName);
            if (null != destination) {
                int idx = 0;
                for (Map.Entry<String, Integer> pair : groups.entrySet()) {
                    String srcFile = pair.getKey();
                    File src = new File(srcFile);
                    if(null != src && src.exists() && src.isFile()) {
                        String thumbNailStr = Utility.getThumbNailForImage(srcFile, destination.getAbsolutePath());
                        group_results[idx++] = thumbNailStr + ":" + srcFile;
                    } else {
                        masterDBHelper.deleteRow(srcFile);
                    }
                }
            }
            String MESSAGE = destination.getAbsolutePath();
            Globals.group_result = JSONUtils.formResponseValues("200",
                    username,
                    "image_query",
                    "",
                    "",
                    "0",
                    MESSAGE,
                    group_results);
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    public static void getCurrentExtactDuplicateResults() {
        try {
            String username = Globals.myCloudUserId;
            MasterDBHelper masterDBHelper = new MasterDBHelper(Globals.gContext);
            masterDBHelper.setUserName(username);
            String duplicateBaseFolder = Globals.ROOT_FOLDER_NAME + File.separator +
                    Globals.WD360_USER_RESULTS_ROOT_FOLDER + File.separator +
                    Globals.WD360_USER_IMAGE_SEARCH_BASE_FOLDER + File.separator +
                    Globals.WD360_USER_IMAGE_SEARCH_DUPLICATE_FOLDER + File.separator;
            FileUtils.cleanDirectory(new File(duplicateBaseFolder));
            String[] duplicate_results = new String[Globals.RESULT_SIZE];
            Map<ExactDuplicateFile, List<ExactDuplicateFile>> duplicates = masterDBHelper.getAllDuplicateImages();
            String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date());
            String folderName = duplicateBaseFolder + timeStamp + File.separator;
            File destination = Utility.createFolder(folderName);
            if (null != destination) {
                int idx = 0;
                for (Map.Entry<ExactDuplicateFile, List<ExactDuplicateFile>> pair : duplicates.entrySet()) {
                    String predicateEntriesForThisSubject = "";
                    String thumbNailSubjectStr = "";
                    File src1 = new File(pair.getKey().fileAbsolutePath);
                    if(null != src1 && src1.exists() && src1.isFile()) {
                        thumbNailSubjectStr = Utility.getThumbNailForImage(pair.getKey().fileAbsolutePath, destination.getAbsolutePath());
                    } else {
                        masterDBHelper.deleteRow(pair.getKey().fileAbsolutePath);
                    }
                    for (ExactDuplicateFile ef : pair.getValue()) {
                        String thumbNailPredicateStr = "";
                        File src2 = new File(ef.fileAbsolutePath);
                        if(null != src2 && src2.exists() && src2.isFile()) {
                            thumbNailPredicateStr = Utility.getThumbNailForImage(ef.fileAbsolutePath, destination.getAbsolutePath());
                        } else {
                            masterDBHelper.deleteRow(ef.fileAbsolutePath);
                        }
                        predicateEntriesForThisSubject = predicateEntriesForThisSubject + "#" +
                                thumbNailPredicateStr + ":" +
                                ef.fileAbsolutePath + ":" +
                                ef.fileName + ":" +
                                ef.lastModifiedDataTime;
                    }
                    duplicate_results[idx++] = thumbNailSubjectStr + ":" + pair.getKey().fileAbsolutePath + ":" +
                            pair.getKey().fileName + ":" +
                            pair.getKey().lastModifiedDataTime +
                            predicateEntriesForThisSubject;
                }
            }
            String MESSAGE = destination.getAbsolutePath();
            Globals.exact_duplicate_result = JSONUtils.formResponseValues("200",
                    username,
                    "image_query",
                    "",
                    "",
                    "0",
                    MESSAGE,
                    duplicate_results);
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    public static void getCurrentNearDuplicateResults() {
        try {
            String username = Globals.myCloudUserId;
            MasterDBHelper masterDBHelper = new MasterDBHelper(Globals.gContext);
            masterDBHelper.setUserName(username);
            String duplicateBaseFolder = Globals.ROOT_FOLDER_NAME + File.separator +
                    Globals.WD360_USER_RESULTS_ROOT_FOLDER + File.separator +
                    Globals.WD360_USER_IMAGE_SEARCH_BASE_FOLDER + File.separator +
                    Globals.WD360_USER_IMAGE_SEARCH_NEAR_DUPLICATE_FOLDER + File.separator;
            FileUtils.cleanDirectory(new File(duplicateBaseFolder));
            String[] nearduplicate_results = new String[Globals.RESULT_SIZE];
            Map<NearDuplicateFile, List<NearDuplicateFile>> nearduplicates = masterDBHelper.getAllNearDuplicateImages();
            String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date());
            String folderName = duplicateBaseFolder + timeStamp + File.separator;
            File destination = Utility.createFolder(folderName);
            if (null != destination) {
                int idx = 0;
                for (Map.Entry<NearDuplicateFile, List<NearDuplicateFile>> pair : nearduplicates.entrySet()) {
                    String predicateEntriesForThisSubject = "";
                    String thumbNailSubjectStr = "";
                    File src1 = new File(pair.getKey().fileAbsolutePath);
                    if(null != src1 && src1.exists() && src1.isFile()) {
                        thumbNailSubjectStr = Utility.getThumbNailForImage(pair.getKey().fileAbsolutePath, destination.getAbsolutePath());
                    } else {
                        masterDBHelper.deleteRow(pair.getKey().fileAbsolutePath);
                    }
                    //Log.d(TAG, "ExecuteUserImageQueryCommands::executeUserImageQuery(nearduplicate) Subject ThumbNail Str : " + thumbNailSubjectStr);
                    for (NearDuplicateFile nf : pair.getValue()) {
                        String thumbNailPredicateStr = "";
                        File src2 = new File(nf.fileAbsolutePath);
                        if(null != src2 && src2.exists() && src2.isFile()) {
                            thumbNailPredicateStr = Utility.getThumbNailForImage(nf.fileAbsolutePath, destination.getAbsolutePath());
                        } else {
                            masterDBHelper.deleteRow(nf.fileAbsolutePath);
                        }
                        //Log.d(TAG, "ExecuteUserImageQueryCommands::executeUserImageQuery(nearduplicate) Predicate ThumbNail Str : " + thumbNailSubjectStr);
                        predicateEntriesForThisSubject = predicateEntriesForThisSubject + "#" +
                                thumbNailPredicateStr + ":" +
                                nf.fileAbsolutePath + ":" +
                                nf.fileName + ":" +
                                nf.lastModifiedDataTime + ":" +
                                nf.dimension + ":" +
                                nf.size + ":" +
                                nf.density;
                    }
                    nearduplicate_results[idx++] = thumbNailSubjectStr + ":" + pair.getKey().fileAbsolutePath + ":" +
                            pair.getKey().fileName + ":" +
                            pair.getKey().lastModifiedDataTime + ":" +
                            pair.getKey().dimension + ":" +
                            pair.getKey().size + ":" +
                            pair.getKey().density +
                            predicateEntriesForThisSubject;
                }
            }
            String MESSAGE = destination.getAbsolutePath();
            Globals.near_duplicate_result = JSONUtils.formResponseValues("200",
                    username,
                    "image_query",
                    "",
                    "",
                    "0",
                    MESSAGE,
                    nearduplicate_results);
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    public static String convertTimeWithTimeZome(long time){
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("UTC"));
        cal.setTimeInMillis(time);
        return (cal.get(Calendar.YEAR) + " " + (cal.get(Calendar.MONTH) + 1) + " "
                + cal.get(Calendar.DAY_OF_MONTH) + " " + cal.get(Calendar.HOUR_OF_DAY) + ":"
                + cal.get(Calendar.MINUTE));

    }

    public static int hamming_distance(String s1, String s2) {
        if (s1 != null && s2 != null) {
            if (s1.length() == s2.length() && s1.length() != 0 && s2.length() != 0) {
                int counter = 0;
                for (int k = 0; k < s1.length(); k++) {
                    if (s1.charAt(k) != s2.charAt(k)) {
                        counter++;
                    }
                }
                return counter;
            } else {
                //Log.d(TAG, "Length of strings not equal: s1 = " + s1.length() + " and s2 = " + s2.length() + " or smaller then 0");
                return -1;
            }
        }
        return -1;
    }

    public static String calculateMD5(File updateFile) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            Log.e("calculateMD5", "Exception while getting Digest" + e.getMessage());
            return null;
        }

        InputStream is;
        try {
            is = new FileInputStream(updateFile);
        } catch (FileNotFoundException e) {
            Log.e("calculateMD5", "Exception while getting FileInputStream" + e.getMessage());
            return null;
        }

        byte[] buffer = new byte[8192];
        int read;
        try {
            while ((read = is.read(buffer)) > 0) {
                digest.update(buffer, 0, read);
            }
            byte[] md5sum = digest.digest();
            BigInteger bigInt = new BigInteger(1, md5sum);
            String output = bigInt.toString(16);
            // Fill to 32 chars
            output = String.format("%32s", output).replace(' ', '0');
            return output;
        } catch (IOException e) {
            throw new RuntimeException("Unable to process file for MD5", e);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                Log.e("calculateMD5", "Exception on closing MD5 input stream" + e.getMessage());
            }
        }
    }

    public static String getExceptionDetailedInfo(Exception e, String TAG) {
        String msg = "";
        try {
            for (int i = 0; i < e.getStackTrace().length; i++) {
                if (e.getStackTrace()[i].getClassName().contains(TAG)) {
                    msg = e.getStackTrace()[i].getFileName() + " : " +
                            e.getStackTrace()[i].getMethodName() + " : " +
                            e.getStackTrace()[i].getLineNumber();
                }
            }
        } catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        }
        return msg;
    }

    public static String getThumbNailForImage(String srcFile, String destination) {
        String thumbNailStr = "";
        try {
            Bitmap myBitmap = BitmapFactory.decodeFile(srcFile);
            double height = myBitmap.getHeight();
            double width = myBitmap.getWidth();
            double new_height = 200;
            double new_width = (width / height) * new_height;
            if (new_width > 180) {
                new_width = 180;
                new_height = (height / width) * new_width;
            }
            int h = (int)Math.floor(new_height);
            int w = (int)Math.floor(new_width);
            Bitmap thumbNail = Utility.resizeImage(myBitmap, h, w);
            thumbNailStr = destination + File.separator + Utility.getFileNameOnly(srcFile);
            OutputStream out = new FileOutputStream(thumbNailStr);
            String ext = Utility.getFileExtension(new File(srcFile));
            if ((null != ext) && ((ext.equalsIgnoreCase("jpg") || (ext.equalsIgnoreCase("jpeg"))))) {
                thumbNail.compress(Bitmap.CompressFormat.JPEG, 100, out);
            } else  if ((null != ext) && (ext.equalsIgnoreCase("png"))) {
                thumbNail.compress(Bitmap.CompressFormat.PNG, 100, out);
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        return thumbNailStr;
    }

    public static boolean safeDeleteAFile(String fileAbsPath, String username) {
        boolean ret = true;
        try {
            MasterDBHelper masterDBHelper = new MasterDBHelper(Globals.gContext);
            masterDBHelper.setUserName(username);
            File f = new File(fileAbsPath);
            if (f.exists() && f.isFile()) {
                boolean dbDel = false;
                boolean del = f.delete();
                if (true == del) {
                    dbDel = masterDBHelper.deleteRow(fileAbsPath);
                    if (false == dbDel) {
                        Utility.getWritableFile(new File(f.getParent()), f.getName());
                    }
                }
                if (false == del || false == dbDel) {
                    ret = false;
                }
            } else {
                ret = false;
            }
        } catch (Exception e) {
            ret = false;
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        return ret;
    }

    public static Bitmap resizeImage(Bitmap bm, int newHeight, int newWidth) {
        Bitmap resizedBitmap = null;
        try {
            resizedBitmap = Bitmap.createScaledBitmap(bm, newWidth, newHeight, false);
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        return resizedBitmap;
    }

    public static String getFileExtension(File file) {
        String ext = "";
        try {
            String fileName = file.getName();
            int i = fileName.lastIndexOf('.');
            if (i > 0) {
                ext = fileName.substring(i+1);
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            ext = "";
        }
        return ext;
    }

    public static String getFileExtensionString(String fileAbsPath) {
        String ext = "";
        try {
            int i = fileAbsPath.lastIndexOf('.');
            if (i > 0) {
                ext = fileAbsPath.substring(i+1);
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            ext = "";
        }
        return ext;
    }

    @Nullable
    private static String getTheMimeType(Uri uri) {
        try {
            String mimeType = "";
            if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
                ContentResolver cr = Globals.gContext.getContentResolver();
                if (cr == null) {
                    Log.d(TAG, "Utility.java : getTheMimeType() : getContentResolver() : returned null for file " + uri.toString());
                    return null;
                }
                mimeType = cr.getType(uri);
            } else {
                String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri.toString());
                if (null == fileExtension) {
                    Log.d(TAG, "Utility.java : getTheMimeType() : MimeTypeMap.getFileExtensionFromUrl() : returned null for file " + uri.toString());
                    return null;
                }
                mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase());
                if (null == mimeType) {
                    Log.d(TAG, "Utility.java : getTheMimeType() : MimeTypeMap.getSingleton().getMimeTypeFromExtension() : returned null for file " + uri.toString());
                    return null;
                }
            }
            return mimeType;
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return null;
        }
    }

    public static Boolean isTypeFile(File f, String type) {
        try {
            String retType = "";
            String extension = "";
            if (f.isDirectory()) {
                Log.d(TAG, "Utility.java : isTypeFile() : is a directory " + f.getAbsolutePath());
                return false;
            }
            retType = getTheMimeType(Uri.parse(f.toURI().toString()));
            if (null == retType) {
                Log.d(TAG, "Utility.java : isTypeFile() : returned null for file " + f.getAbsolutePath());
                Log.d(TAG, "Utility.java : isTypeFile() : trying with file extension");
                extension = getFileExtension(f);
                if (type.contains("image")) {
                    if ((extension.equalsIgnoreCase("jpg")) ||
                        (extension.equalsIgnoreCase("jpeg")) ||
                        (extension.equalsIgnoreCase("bmp")) ||
                        (extension.equalsIgnoreCase("png")) ||
                        (extension.equalsIgnoreCase("gif"))) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }

            if (retType.contains(type)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return false;
        }
    }

    @Nullable
    synchronized public static File putFileFromAsset(String dirifany, String fileUrl, File folder, String newFileName) {
        try {
            if (newFileName.isEmpty()) {
                newFileName = fileUrl;
            }
            File file = getWritableFile(folder, newFileName);
            if (null == file) {
                Log.e(TAG, "Utility.java : putFileFromAsset() : getWritableFile() : returned null for folder " + dirifany + " and file " + newFileName);
                return null;
            }
            InputStream is = Globals.gContext.getAssets().open(dirifany + File.separator + fileUrl);
            if (null == is) {
                Log.e(TAG, "Utility.java : putFileFromAsset() : Globals.gContext.getAssets().open() : returned null for folder " + dirifany + " and file " + fileUrl);
                return null;
            }
            if (!writeContentToFile(file, is)) {
                Log.e(TAG, "Utility.java : putFileFromAsset() : writeContentToFile() : returned false for folder " + dirifany + " and file " + fileUrl);
                return null;
            }
            return file;
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return null;
        }
    }

    @Nullable
    synchronized public static File putFile(InputStream is, String fileUrl, File folder) {
        try {
            File file = getWritableFile(folder, fileUrl);
            if (null == file) {
                Log.e(TAG, "Utility.java : putFile() : getWritableFile() : returned null for folder " + folder + " and file " + fileUrl);
                return null;
            }
            if (!writeContentToFile(file, is)) {
                Log.e(TAG, "Utility.java : putFile() : writeContentToFile() : returned false for folder " + folder + " and file " + fileUrl);
                return null;
            }
            return file;
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return null;
        }
    }

    @Nullable
    synchronized public static File getWritableFile(File folder, String fileName) {
        try {
            File imageFile = new File(folder.getAbsolutePath(), fileName);
            if (null == imageFile) {
                Log.e(TAG, "Utility.java : getWritableFile() : new File() : returned null for folder " + folder + " and file " + fileName);
                return null;
            }
            if (!imageFile.exists()) {
                imageFile.createNewFile();
            }
            return imageFile;
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return null;
        }
    }

    synchronized public static Boolean writeContentToFile(File destFile, InputStream is) {
        try {
            FileOutputStream fos = new FileOutputStream(destFile);
            if (null == fos) {
                Log.e(TAG, "Utility.java : writeContentToFile() : new FileOutputStream() : returned null for file " + destFile);
                return false;
            }
            int read = 0;
            byte[] buffer = new byte[32768];
            if (null == buffer) {
                Log.e(TAG, "Utility.java : writeContentToFile() : new byte[32768] : returned null for file " + destFile);
                return false;
            }
            while ((read = is.read(buffer)) > 0) {
                fos.write(buffer, 0, read);
            }
            fos.flush();
            fos.close();
            return true;
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return false;
        }
    }

    @Nullable
    synchronized public static File createFolder(String folder_name) {
        try {
            File newFolder = null;
            newFolder = new File(folder_name);
            if (null == newFolder) {
                Log.e(TAG, "Utility.java : createFolder() : new File() : returned null for folder " + folder_name);
                return null;
            }
            if (!newFolder.exists()) {
                newFolder.mkdir();
            }
            return newFolder;
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return null;
        }
    }

    @Nullable
    synchronized public static String getFileNameOnly(String url) {
        try {
            File f = new File(url);
            return f.getName();
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        return null;
    }

    @Nullable
    synchronized public static List<File> listfiles(String username, String directoryName) {
        File directory = new File(directoryName);
        List<File> resultList = new ArrayList<File>();
        File[] fList = directory.listFiles();
        resultList.addAll(Arrays.asList(fList));
        for (File file : fList) {
            if (file.isFile()) {
            } else if (file.isDirectory()) {
                resultList.remove(file);
                if ((false == file.getAbsolutePath().contains(Globals.WD360_ETC_FOLDER)) &&
                    (false == file.getAbsolutePath().contains(Globals.WD360_NATIVE_LOG_FOLDER)) &&
                    (false == file.getAbsolutePath().contains(Globals.WD360_USER_RESULTS_ROOT_FOLDER))) {
                    resultList.addAll(listfiles(username, file.getAbsolutePath()));
                }
            }
        }
        return resultList;
    }

    @Nullable
    synchronized public static List<File> listMineablefiles(String username, String directoryName) {
        File directory = new File(directoryName);
        List<File> resultList = new ArrayList<File>();
        File[] fList = directory.listFiles();
        resultList.addAll(Arrays.asList(fList));
        boolean imageYes = false;
        boolean docYes = false;
        for (File file : fList) {
            imageYes = false;
            docYes = false;
            if (file.isFile()) {
                MasterDBHelper mdDHelper = new MasterDBHelper(Globals.gContext);
                mdDHelper.setUserName(username);

                String fileName = file.getName();
                String extension = fileName.substring(fileName.lastIndexOf('.') + 1);

                if ((false == extension.equalsIgnoreCase("jpg")) &&
                    (false == extension.equalsIgnoreCase("jpeg")) &&
                    (false == extension.equalsIgnoreCase("png"))) {
                    resultList.remove(file);
                }

                if (false == mdDHelper.isFileKnownToDB(file.getAbsolutePath())) {
                    imageYes = true;
                    docYes = true;
                }

                if ((imageYes == false) && (docYes == false)) {
                    resultList.remove(file);
                }
            } else if (file.isDirectory()) {
                resultList.remove(file);
                resultList.addAll(listMineablefiles(username, file.getAbsolutePath()));
            }
        }
        return resultList;
    }
}

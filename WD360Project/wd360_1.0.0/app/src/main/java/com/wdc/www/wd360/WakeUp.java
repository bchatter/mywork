package com.wdc.www.wd360;

import java.util.concurrent.TimeUnit;

/**
 * Created by 17328 on 8/29/2017.
 */

public class WakeUp {
    private static String TAG = PeriodicMinerThread.class.getSimpleName();
    void WAIT() {
        try {
            Globals.FILESYSTEM_EVENT_BURST_QUEUE.take();
            while (true) {
                if (null == Globals.FILESYSTEM_EVENT_BURST_QUEUE.poll(5, TimeUnit.SECONDS)) {
                    break;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    void NOTIFY(String path) {
        try {
            Globals.FILESYSTEM_EVENT_BURST_QUEUE.put(path);
        } catch (InterruptedException e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }
}

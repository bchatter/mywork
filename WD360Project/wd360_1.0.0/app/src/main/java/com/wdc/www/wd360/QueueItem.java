package com.wdc.www.wd360;

/**
 * Created by Biswapratap Chatterjee on 7/18/2017.
 */

public class QueueItem {
    public String m_userName;
    public String m_absFilePath;

    public QueueItem(String userName, String absFilePath) {
        m_userName = userName;
        m_absFilePath = absFilePath;
    }
}

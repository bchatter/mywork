package com.wdc.www.wd360;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;


import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.wdc.www.wd360.MasterDBContract.MasterDBBaseColumns.*;

public class MasterDBHelper extends SQLiteOpenHelper {

    private static String TAG = MasterDBHelper.class.getSimpleName();
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "WD360Master.db";
    private String m_sql_create_entries = "";
    private String m_username = "";
    private SQLiteDatabase m_db = null;

    public MasterDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    synchronized public void setUserName(String username) {
        m_username = username;
    }

    synchronized private SQLiteDatabase getDBHandle() {
        return m_db;
    }

    synchronized private void openDB() {
        try {
            m_db = this.getWritableDatabase();
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    synchronized private void closeDB() {
        try {
            if (m_db.isOpen())
                m_db.close();
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    synchronized public void deleteTable() {
        try {
            openDB();
            getDBHandle().execSQL("DROP TABLE IF EXISTS '" + m_username + "'");
            closeDB();
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    @Nullable
    synchronized public List<String> getAllTableNames()
    {
        List<String> ret = null;
        try {
            openDB();
            ret = new ArrayList<String>();
            Cursor c = getDBHandle().rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name!='android_metadata' AND name!='databases' order by name", null);

            if (c.moveToFirst()) {
                while (!c.isAfterLast()) {
                    ret.add(c.getString(c.getColumnIndex("name")));
                    c.moveToNext();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public Map<NearDuplicateFile, List<NearDuplicateFile>> getAllNearDuplicateImages() {
        Map<String, Integer> avoidRedundancy = new HashMap<String, Integer>();
        Map<NearDuplicateFile, List<NearDuplicateFile>> ret = new HashMap<NearDuplicateFile, List<NearDuplicateFile>>();
        Map<String, String> temp = new HashMap<String, String>();
        try {
            openDB();
            String q = "SELECT " + COLUMN_NAME_FILE_ABSOLUTE_PATH + ", " + COLUMN_IMAGE_NEAR_DUPLICATE_HASH_VALUE +
                    " FROM '" + m_username + "'";
            Cursor cursor = getDBHandle().rawQuery(q, null);
            if(cursor.moveToFirst()) {
                do {
                    String absPath = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_FILE_ABSOLUTE_PATH));
                    String nearDupHashVal = cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE_NEAR_DUPLICATE_HASH_VALUE));
                    temp.put(absPath, nearDupHashVal);
                } while(cursor.moveToNext());
            }

            for (Map.Entry<String, String> pair1 : temp.entrySet()) {
                String AB2 = "";
                List<NearDuplicateFile> nearDuplicates = new ArrayList<NearDuplicateFile>();
                String AB1 = pair1.getKey();
                File ab1 = new File(AB1);
                NearDuplicateFile subjectFile = new NearDuplicateFile();
                if (ab1.exists()) {
                    if (ab1.isFile()) {
                        subjectFile.fileName = ab1.getName();
                        subjectFile.fileAbsolutePath = AB1;
                        subjectFile.lastModifiedDataTime = Utility.convertTimeWithTimeZome(ab1.lastModified());
                        Bitmap myBitmap = BitmapFactory.decodeFile(AB1);
                        subjectFile.dimension = myBitmap.getHeight() + " X " + myBitmap.getWidth();
                        subjectFile.size = Integer.toString(myBitmap.getByteCount());
                        subjectFile.density = Integer.toString(myBitmap.getDensity());
                    }
                } else {
                    deleteRow(AB1);
                }
                for (Map.Entry<String, String> pair2 : temp.entrySet()) {
                    AB2 = pair2.getKey();
                    int ham_dist = Utility.hamming_distance(pair1.getValue(), pair2.getValue());
                    if (ham_dist == 0 || ham_dist == -1) {
                        continue;
                    }
                    if ((ham_dist > 0) && (ham_dist < 10 )) {
                        File ab2 = new File(AB2);
                        if (ab2.exists()) {
                            if (ab2.isFile()) {
                                NearDuplicateFile nearDuplicateFile = new NearDuplicateFile();
                                nearDuplicateFile.fileName = ab2.getName();
                                nearDuplicateFile.fileAbsolutePath = AB2;
                                nearDuplicateFile.lastModifiedDataTime = Utility.convertTimeWithTimeZome(ab2.lastModified());
                                Bitmap myBitmap = BitmapFactory.decodeFile(AB2);
                                nearDuplicateFile.dimension = myBitmap.getHeight() + " X " + myBitmap.getWidth();
                                nearDuplicateFile.size = Integer.toString(myBitmap.getByteCount());
                                nearDuplicateFile.density = Integer.toString(myBitmap.getDensity());
                                nearDuplicates.add(nearDuplicateFile);
                            }
                        } else {
                            deleteRow(AB1);
                        }
                    }
                }
                if ((nearDuplicates.size() > 0) && (false == ret.containsKey(subjectFile))) {
                    boolean allowed = true;
                    if (true == avoidRedundancy.containsKey(subjectFile.fileAbsolutePath)) {
                        allowed = false;
                    }
                    if (allowed) {
                        for (NearDuplicateFile f : nearDuplicates) {
                            if (true == avoidRedundancy.containsKey(f.fileAbsolutePath)) {
                                allowed = false;
                                break;
                            }
                        }
                    }
                    if (allowed) {
                        avoidRedundancy.put(subjectFile.fileAbsolutePath, 0);
                        for (NearDuplicateFile f : nearDuplicates) {
                            avoidRedundancy.put(f.fileAbsolutePath, 0);
                        }
                        ret.put(subjectFile, nearDuplicates);
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public Map<ExactDuplicateFile, List<ExactDuplicateFile>> getAllDuplicateImages() {
        Map<String, Integer> avoidRedundancy = new HashMap<String, Integer>();
        Map<ExactDuplicateFile, List<ExactDuplicateFile>> ret = new HashMap<ExactDuplicateFile, List<ExactDuplicateFile>>();
        Map<String, String> temp = new HashMap<String, String>();
        try {
            openDB();
            String q = "SELECT " + COLUMN_NAME_FILE_ABSOLUTE_PATH + ", " + COLUMN_IMAGE_NEAR_DUPLICATE_HASH_VALUE +
                    " FROM '" + m_username + "'";
            Cursor cursor = getDBHandle().rawQuery(q, null);
            if(cursor.moveToFirst()) {
                do {
                    String absPath = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_FILE_ABSOLUTE_PATH));
                    String nearDupHashVal = cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE_NEAR_DUPLICATE_HASH_VALUE));
                    temp.put(absPath, nearDupHashVal);
                } while(cursor.moveToNext());
            }

            for (Map.Entry<String, String> pair1 : temp.entrySet()) {
                String AB2 = "";
                List<ExactDuplicateFile> exactDuplicates = new ArrayList<ExactDuplicateFile>();
                String AB1 = pair1.getKey();
                File ab1 = new File(AB1);
                ExactDuplicateFile subjectFile = new ExactDuplicateFile();
                if (ab1.exists()) {
                    if (ab1.isFile()) {
                        subjectFile.fileName = ab1.getName();
                        subjectFile.fileAbsolutePath = AB1;
                        subjectFile.lastModifiedDataTime = Utility.convertTimeWithTimeZome(ab1.lastModified());
                    }
                } else {
                    deleteRow(AB1);
                }
                for (Map.Entry<String, String> pair2 : temp.entrySet()) {
                    AB2 = pair2.getKey();
                    int ham_dist = Utility.hamming_distance(pair1.getValue(), pair2.getValue());
                    if (ham_dist == -1) {
                        continue;
                    }
                    if ((ham_dist == 0) && (false == AB1.equals(AB2))) {
                        File ab2 = new File(AB2);
                        if (ab2.exists()) {
                            if (ab2.isFile()) {
                                ExactDuplicateFile exactFile = new ExactDuplicateFile();
                                exactFile.fileName = ab2.getName();
                                exactFile.fileAbsolutePath = AB2;
                                exactFile.lastModifiedDataTime = Utility.convertTimeWithTimeZome(ab2.lastModified());
                                exactDuplicates.add(exactFile);
                            }
                        } else {
                            deleteRow(AB2);
                        }
                    }
                }
                if ((exactDuplicates.size() > 0) && (false == ret.containsKey(subjectFile))) {
                    boolean allowed = true;
                    if (true == avoidRedundancy.containsKey(subjectFile.fileAbsolutePath)) {
                        allowed = false;
                    }
                    if (allowed) {
                        for (ExactDuplicateFile f : exactDuplicates) {
                            if (true == avoidRedundancy.containsKey(f.fileAbsolutePath)) {
                                allowed = false;
                                break;
                            }
                        }
                    }
                    if (allowed) {
                        avoidRedundancy.put(subjectFile.fileAbsolutePath, 0);
                        for (ExactDuplicateFile f : exactDuplicates) {
                            avoidRedundancy.put(f.fileAbsolutePath, 0);
                        }
                        ret.put(subjectFile, exactDuplicates);
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public Map<String, Integer> getAllGroupPics() {
        Map<String, Integer> ret = new HashMap<String, Integer>();
        try {
            openDB();
            Cursor cursor = getDBHandle().rawQuery("select " +
                    COLUMN_IMAGE_IS_A_GROUP_IMAGE + ", " + COLUMN_NAME_FILE_ABSOLUTE_PATH +
                    " from '" + m_username + "' where " + COLUMN_IMAGE_IS_A_GROUP_IMAGE + " > 2", null);
            if (cursor.moveToFirst()) {
                do {
                    String absPath = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_FILE_ABSOLUTE_PATH));
                    Integer faceCount = cursor.getInt(cursor.getColumnIndex(COLUMN_IMAGE_IS_A_GROUP_IMAGE));
                    ret.put(absPath, faceCount);
                }
                while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public Boolean isFileKnownToDB(String fileAbsPath) {
        Boolean ret = false;
        try {
            openDB();
            String q = "select * from '" + m_username + "' where " + COLUMN_NAME_FILE_ABSOLUTE_PATH + " = '" + fileAbsPath + "'";
            Cursor cursor = getDBHandle().rawQuery(q, null);
            int count = 0;
            if (cursor.moveToFirst()) {
                do {
                    int isIndexed = cursor.getInt(cursor.getColumnIndex(COLUMN_DOC_INDEXED));
                    if (isIndexed == 0) {
                        break;
                    }
                    count = count + 1;
                }
                while (cursor.moveToNext());
            }
            if (count > 0) {
                ret = true;
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public Boolean isFileAlreadyDocIndexed(String fileAbsPath) {
        Boolean ret = false;
        try {
            openDB();
            String q = "select " +
                    COLUMN_DOC_INDEXED +
                    " from '" + m_username +
                    "' where " + COLUMN_NAME_FILE_ABSOLUTE_PATH +
                    " = '" + fileAbsPath + "'";
            Cursor cursor = getDBHandle().rawQuery(q, null);
            int count = 0;
            if (cursor.moveToFirst()) {
                do {
                    int isIndexed = cursor.getInt(cursor.getColumnIndex(COLUMN_DOC_INDEXED));
                    if (isIndexed == 0) {
                        break;
                    }
                    count = count + 1;
                }
                while (cursor.moveToNext());
            }
            if (count > 0) {
                ret = true;
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public Boolean doesImageContainAnyFace(String fileAbsPath) {
        Boolean ret = false;
        try {
            openDB();
            String q = "select " +
                    COLUMN_IMAGE_IS_A_GROUP_IMAGE +
                    " from '" + m_username +
                    "' where " + COLUMN_NAME_FILE_ABSOLUTE_PATH +
                    " = '" + fileAbsPath + "'";
            Cursor cursor = getDBHandle().rawQuery(q, null);
            int count = 0;
            if (cursor.moveToFirst()) {
                do {
                    int numOfFaces = cursor.getInt(cursor.getColumnIndex(COLUMN_IMAGE_IS_A_GROUP_IMAGE));
                    if (numOfFaces <= 0) {
                        break;
                    }
                    count = count + 1;
                }
                while (cursor.moveToNext());
            }
            if (count > 0) {
                ret = true;
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public Boolean createTableForUser() {
        Boolean ret = false;
        try {
            openDB();
            m_sql_create_entries = "CREATE TABLE '" + m_username + "' (" +
                    COLUMN_NAME_FILE_ABSOLUTE_PATH + " TEXT," +
                    COLUMN_IMAGE_IS_A_GROUP_IMAGE + " INTEGER," +
                    COLUMN_IMAGE_NEAR_DUPLICATE_HASH_VALUE + " TEXT," +
                    COLUMN_DOC_INDEXED + " INTEGER," +
                    " PRIMARY KEY(" + COLUMN_NAME_FILE_ABSOLUTE_PATH + "))";
            getDBHandle().execSQL(m_sql_create_entries);
            ret = true;
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    @Nullable
    synchronized public String getTableAbsoluteRootFolerPath() {
        String ret = null;
        try {
            openDB();
            Cursor cursor = getDBHandle().rawQuery("select " +
                    COLUMN_NAME_FILE_ABSOLUTE_PATH +
                    " from '" + m_username + "'", null);
            if (cursor.moveToFirst()) {
                ret =  cursor.getString(cursor.getColumnIndex(COLUMN_NAME_FILE_ABSOLUTE_PATH));
            }
        } catch (Exception e) {
            Log.i(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            if (e.getMessage().contains("no such table")) {
                if (true == createTableForUser()) {
                    ret = "CREATED";
                }
            }
        }
        closeDB();
        return ret;
    }

    synchronized public Boolean CheckIfTableAlreadyInDBorNot(String dbfield,
                                                             String fieldValue) {
        Boolean ret = false;
        try {
            openDB();
            String Query = "Select * from '" + m_username + "' where " + dbfield + " = '" + fieldValue + "'";
            Cursor cursor = getDBHandle().rawQuery(Query, null);
            if (cursor.getCount() == 1)
                ret = true;
            cursor.close();
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public int ItemsDocIndexedUntilNow() {
        int count = 0;
        try {
            openDB();
            String Query = "Select " + COLUMN_DOC_INDEXED + " from '" + m_username + "'";
            Cursor cursor = getDBHandle().rawQuery(Query, null);
            if (cursor.moveToFirst()) {
                do {
                    int isDocIndexed = cursor.getInt(cursor.getColumnIndex(COLUMN_DOC_INDEXED));
                    if (isDocIndexed == 1) {
                        count = count + 1;
                    }
                } while (cursor.moveToNext()) ;
            }
        } catch (Exception e) {
            count = 0;
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return count;
    }

    synchronized public boolean deleteRow(String absolutepath)
    {
        boolean ret = true;
        try {
            openDB();
            String q = "DELETE FROM '" + m_username + "' WHERE " + COLUMN_NAME_FILE_ABSOLUTE_PATH + " = '" + absolutepath + "'";
            getDBHandle().execSQL(q);
            String verify_query = "SELECT * FROM '" + m_username + "' WHERE " + COLUMN_NAME_FILE_ABSOLUTE_PATH + " = '" + absolutepath + "'";
            Cursor cursor = getDBHandle().rawQuery(verify_query, null);
            int count = 0;
            if (cursor.moveToFirst()) {
                do {
                    count = count + 1;
                } while (cursor.moveToNext()) ;
            }
            if (count > 0) {
                ret = false;
            }
        } catch (Exception e) {
            ret = false;
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public Boolean updateImageConstantValues(String absolutepath, /* This can not be updated */
                                                          int new_group,
                                                          String new_nearduplicatehashvalue) {
        Boolean ret = false;
        try {
            openDB();
            String query = "UPDATE '" + m_username + "' SET " +
                           COLUMN_IMAGE_IS_A_GROUP_IMAGE + " = " + new_group + ", " +
                           COLUMN_IMAGE_NEAR_DUPLICATE_HASH_VALUE + " = '" + new_nearduplicatehashvalue + "' " +
                           "WHERE " + COLUMN_NAME_FILE_ABSOLUTE_PATH + " = '" + absolutepath + "'";
            getDBHandle().execSQL(query);
            ret = true;
        } catch (Exception e) {
            ret = false;
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    synchronized public Boolean insertRowIfNotExist(String absolutepath,
                                                    int group,
                                                    String nearduplicatehashvalue,
                                                    int isDocIndexed) {
        Boolean ret = false;
        try {
            Boolean rslt = CheckIfTableAlreadyInDBorNot(COLUMN_NAME_FILE_ABSOLUTE_PATH, absolutepath);
            if (!rslt) {
                openDB();

                String ROW1 = "INSERT INTO '" + m_username + "' ("
                        + COLUMN_NAME_FILE_ABSOLUTE_PATH + ", "
                        + COLUMN_IMAGE_IS_A_GROUP_IMAGE + ", "
                        + COLUMN_IMAGE_NEAR_DUPLICATE_HASH_VALUE + ", "
                        + COLUMN_DOC_INDEXED +
                        ") Values ("
                        + "'" + absolutepath + "',"
                        + group + ", "
                        + "'" + nearduplicatehashvalue + "', "
                        + isDocIndexed + ")";
                getDBHandle().execSQL(ROW1);
                ret = true;
            } else {
                ret = true;
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
        closeDB();
        return ret;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
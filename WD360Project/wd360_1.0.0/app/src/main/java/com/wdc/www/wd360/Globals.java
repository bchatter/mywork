package com.wdc.www.wd360;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Biswapratap Chatterjee on 5/12/2017.
 */

public class Globals {
    static final int MINING_BURST_SIZE = 199;
    static final int RESULT_SIZE = 1000;
    static final int JAVA_GC_CLEANUP_TIME = 1000;
    //static final int NEXT_MININING_ATTEMPT_TIME = 900000;
    static final int NEXT_MININING_ATTEMPT_TIME = 300000;
    static boolean LOG = true;
    static String myCloudUserId = "";
    static Context gContext;
    static String ROOT_FOLDER_NAME = "";
    static String emptyResponse[] = {};
    static LinkedBlockingQueue<String> FILESYSTEM_EVENT_BURST_QUEUE = new LinkedBlockingQueue<String>();
    static LinkedBlockingQueue<String> FILEOBSERVER_HOLD_QUEUE = new LinkedBlockingQueue<String>();
    static LinkedBlockingQueue<QueueItem> PERIODIC_MINER_QUEUE = new LinkedBlockingQueue<QueueItem>();
    static final String WD360_ETC_FOLDER = "WD360ETC";
    static final String WD360_NATIVE_LOG_FOLDER = "WD360NativeLogs";
    static final String WD360_FACE_DETECTOR_XML_FILE_NAME = "haarcascade_frontalface_alt.xml";
    static final String WD360_USER_RESULTS_ROOT_FOLDER = "WD360Results";
    static final String WD360_USER_IMAGE_SEARCH_BASE_FOLDER = "Image";
    static final String WD360_USER_IMAGE_SEARCH_GROUP_FOLDER = "Group";
    static final String WD360_USER_IMAGE_SEARCH_DUPLICATE_FOLDER = "Duplicate";
    static final String WD360_USER_IMAGE_SEARCH_NEAR_DUPLICATE_FOLDER = "NearDuplicate";
    static final String WD360_IGNORE_CODE_CACHE_FOLDER = "code_cache";
    static final String WD360_IGNORE_DATABASES_FOLDER = "databases";
    static final String WD360_IGNORE_LIB_FOLDER = "lib";
    static String ABSOLUTE_USER_IMAGE_SEARCH_GROUP_FOLDER_PATH = "";
    static String ABSOLUTE_USER_IMAGE_SEARCH_DUPLICATE_FOLDER_PATH = "";
    static String ABSOLUTE_USER_IMAGE_SEARCH_NEAR_DUPLICATE_FOLDER_PATH = "";
    static WakeUp wakeup = new WakeUp();
    static String group_result = JSONUtils.formResponseValues("200",
            "",
            "",
            "",
            "",
            "",
            "",
            Globals.emptyResponse);
    static String exact_duplicate_result = JSONUtils.formResponseValues("200",
            "",
            "",
            "",
            "",
            "",
            "",
            Globals.emptyResponse);
    static String near_duplicate_result = JSONUtils.formResponseValues("200",
            "",
            "",
            "",
            "",
            "",
            "",
            Globals.emptyResponse);
}

package com.wdc.www.wd360;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;

import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * Created by Biswapratap Chatterjee on 6/24/2017.
 */

class BlockedQueueBasedMiner {

    private static String TAG = BlockedQueueBasedMiner.class.getSimpleName();

    synchronized private void runindexer(String userName,
                                         String fileAbsolutePath) {
        try {
            int group = 0;

            MasterDBHelper mdDHelper = new MasterDBHelper(Globals.gContext);
            mdDHelper.setUserName(userName);
            String userAbsFolder = mdDHelper.getTableAbsoluteRootFolerPath();

            File file = new File(fileAbsolutePath);
            if ((null != file) && (file.exists())) {
                mdDHelper.insertRowIfNotExist(fileAbsolutePath,
                        group,
                        "NA",
                        1);

                WD360FaceDetection wdfd = new WD360FaceDetection(
                        Globals.ROOT_FOLDER_NAME + File.separator + Globals.WD360_NATIVE_LOG_FOLDER + File.separator + "WD360.log",
                        Globals.ROOT_FOLDER_NAME + File.separator + Globals.WD360_ETC_FOLDER + File.separator + Globals.WD360_FACE_DETECTOR_XML_FILE_NAME
                );

                Bitmap myBitmap = BitmapFactory.decodeFile(fileAbsolutePath);
                ImagePHash imgPHash = new ImagePHash();
                String nearDuplicateHashHexValue = imgPHash.calculatePHash(myBitmap);
                group = wdfd.howManyFaces(fileAbsolutePath, 0);
                Log.d(TAG, fileAbsolutePath + " : found = " + group + " faces");

                mdDHelper.updateImageConstantValues(fileAbsolutePath,
                        group,
                        nearDuplicateHashHexValue);
                wdfd.release();
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    synchronized public QueueItem periodicGetItem() throws Exception {
        return Globals.PERIODIC_MINER_QUEUE.poll(5, TimeUnit.SECONDS);
    }

    void mineWithTimeout() throws Exception {
        String un = "";
        while (true) {
            int count = 0;
            QueueItem item = null;
            item = periodicGetItem();
            if (null == item) {
                break;
            }
            try {
                un = item.m_userName;
                runindexer(item.m_userName,
                           item.m_absFilePath);
                count++;
                if (count >= Globals.MINING_BURST_SIZE) {
                    Thread.sleep(Globals.JAVA_GC_CLEANUP_TIME);
                    count = 0;
                }
            } catch (Exception e) {
                Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
                continue;
            }
        }
    }
}

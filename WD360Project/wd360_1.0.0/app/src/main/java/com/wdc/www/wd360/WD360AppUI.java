package com.wdc.www.wd360;

import android.content.Context;


import com.wdc.nassdk.MyCloudUIServer;
import com.wdc.nassdk.Utils;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD;

import static fi.iki.elonen.NanoHTTPD.Response.Status.OK;

/**
 * Created by Biswapratap Chatterjee on 5/12/2017.
 */

public class WD360AppUI extends MyCloudUIServer {
    private static String TAG = WD360AppUI.class.getSimpleName();

    public WD360AppUI(Context context) {
        super(context);
    }

    private String deleteFiles(String[] keyValues) {
        String username = Globals.myCloudUserId;
        try {
            String[] query = keyValues[1].split("=");
            String filesStr = query[1];
            filesStr = java.net.URLDecoder.decode(filesStr, "UTF-8");
            String[] files = filesStr.split(":");
            String[] failed_files = new String[files.length];

            for (int i = 0; i < files.length; i++) {
                if (false == Utility.safeDeleteAFile(files[i], username)) {
                    failed_files[i] = files[i];
                }
            }

            Thread.sleep(6000);

            while (ServerBusy.getServerStatus() == true) {
                Thread.sleep(4000);
            }

            return JSONUtils.formResponseValues("200",
                    username,
                    "delete_files",
                    "",
                    "",
                    "0",
                    "Deleted Files",
                    failed_files);
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return JSONUtils.formResponseValues("500",
                    username,
                    "delete_files",
                    "",
                    "",
                    "0",
                    e.getMessage(),
                    Globals.emptyResponse);
        }
    }

    private String processPostRequest(String[] keyValues) {
        String ret = "";
        String[] co = new String[1];
        co[0] = "";
        String cmd_opcode = "";

        ret = JSONUtils.initialChecks(keyValues, co);
        if (ret.contains("ASSERT"))
            return ret;

        cmd_opcode = co[0];

        Log.d(TAG, "processPostRequest() cmd_opcode = " + cmd_opcode);

        if (cmd_opcode.equalsIgnoreCase("image_search")) {
            return ExecuteUserImageQueryCommands.executeUserImageQuery(keyValues, cmd_opcode);
        }
        else if (cmd_opcode.equalsIgnoreCase("delete_files")) {
            return deleteFiles(keyValues);
        }
        else if (cmd_opcode.equalsIgnoreCase("log_on")) {
            return Log.turnOnLogging();
        }
        else if (cmd_opcode.equalsIgnoreCase("log_off")) {
            return Log.turnOffLogging();
        }
        else {
            return JSONUtils.formResponseValues("500",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "Unknown Command",
                    Globals.emptyResponse);
        }
    }

    private Response processGetRequest(IHTTPSession session) {
        try {
            String mimeType = "";
            String uri = session.getUri();

            Log.d(TAG, "[INIT] WD360AppUI:processGetRequest() uri : " + uri);

            String pathSelector = "";
            if(Utils.isNotWdNas()) {
                pathSelector = "storage";
            } else {
                pathSelector = "wd360";
            }

            if (uri.contains(pathSelector)){
                String filepath = uri;
                String filename = filepath.substring(filepath.lastIndexOf('/') + 1, filepath.length());
                InputStream isr = new FileInputStream(uri);
                mimeType = URLConnection.guessContentTypeFromName(filename);
                if (mimeType == null || mimeType.length() == 0) {
                    mimeType = URLConnection.guessContentTypeFromStream(isr);
                    if (mimeType == null || mimeType.length() == 0) {
                        mimeType = "";
                    }
                }
                return newChunkedResponse(OK, mimeType, isr);
            }

            if (uri.equals("/")) {
                uri = "/index.html";
            }

            String filepath = "website" + uri;
            String filename = filepath.substring(filepath.lastIndexOf('/') + 1, filepath.length());
            InputStream isr = Globals.gContext.getAssets().open(filepath);
            mimeType = URLConnection.guessContentTypeFromName(filename);
            if (mimeType == null || mimeType.length() == 0) {
                mimeType = URLConnection.guessContentTypeFromStream(isr);
                if (mimeType == null || mimeType.length() == 0) {
                    mimeType = "";
                }
            }
            return newChunkedResponse(OK, mimeType, isr);
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return newFixedLengthResponse("<h1> Web page not found</h1>");
        }
    }

    @Override
    public Response get(IHTTPSession session) {
        Log.d(TAG, "[INIT} WD360AppUI:get() Entering");
        Response response = processGetRequest(session);
        Log.d(TAG, "[INIT} WD360AppUI:get() Exiting");
        return response;
    }

    private NanoHTTPD.Response.IStatus getHttpStatus(String response) {
        try {
            JSONObject json = new JSONObject(response);
            String httpstatus = (String) json.get("httpstatus");
            if (httpstatus.equalsIgnoreCase("200"))
                return NanoHTTPD.Response.Status.OK;
            else if (httpstatus.equalsIgnoreCase("202"))
                return NanoHTTPD.Response.Status.ACCEPTED;
            else if (httpstatus.equalsIgnoreCase("400"))
                return NanoHTTPD.Response.Status.BAD_REQUEST;
            else if (httpstatus.equalsIgnoreCase("401"))
                return NanoHTTPD.Response.Status.UNAUTHORIZED;
            else
                return NanoHTTPD.Response.Status.INTERNAL_ERROR;
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return NanoHTTPD.Response.Status.INTERNAL_ERROR;
        }
    }

    @Override
    public Response post(IHTTPSession session) {
        Log.d(TAG, "[INIT} WD360AppUI:post() Entering");
        try{
            Map<String, String> files = new HashMap<String, String>();
            Map<String, String> filenames = new HashMap<String, String>();
            session.parseBody(files);
            String postBody = session.getQueryParameterString();
            Log.d(TAG, postBody);
            filenames = session.getParms();
            String[] keyValuestemp = postBody.split("\n");
            for (int i = 0; i < keyValuestemp.length; i++) {
                keyValuestemp[i] = java.net.URLDecoder.decode(keyValuestemp[i], "UTF-8");
                keyValuestemp[i] = keyValuestemp[i].trim();
                Log.d(TAG, keyValuestemp[i]);
            }

            String[] keyValues = null;
            if (Utils.isNotWdNas()) {
                keyValues = keyValuestemp[0].split("&");
            } else {
                keyValues = keyValuestemp[1].split("&");
            }

            String[] co = new String[1];
            co[0] = "";
            String cmd_opcode = "";
            JSONUtils.initialChecks(keyValues, co);
            cmd_opcode = co[0];
            Log.d(TAG, "post() cmd_opcode = " + cmd_opcode);

            if (true == ServerBusy.getServerStatus()) {
                Log.d(TAG, "[INIT} WD360AppUI:post() Exiting");
                return newFixedLengthResponse(Response.Status.FORBIDDEN, "application/json", "");
            }

            if (filenames.size() > 0) {
                if (filenames.containsKey("access_token")) {
                    filenames.remove("access_token");
                }
            }

            if (keyValues.length > 0) {
                String response = processPostRequest(keyValues);
                return newFixedLengthResponse(getHttpStatus(response), "application/json", response);
            }
            else {
                Log.d(TAG, "[INIT} WD360AppUI:post() Exiting");
                return newFixedLengthResponse("<h1>" + "No POST parameters received" + "</h1>");
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            Log.d(TAG, "[INIT} WD360AppUI:post() Exiting");
            return newFixedLengthResponse("<h1>" + e.getMessage() + "</h1>");
        }
    }
}
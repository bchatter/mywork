package com.wdc.www.wd360;

/**
 * Created by Biswapratap Chatterjee on 8/29/2017.
 */

public class NearDuplicateFile {
    public String fileName;
    public String fileAbsolutePath;
    public String lastModifiedDataTime;
    public String dimension;
    public String size;
    public String density;
}

package com.wdc.www.wd360;

/**
 * Created by 17328 on 8/14/2017.
 */

public class ServerBusy {
    private static boolean m_server_busy = false;

    synchronized public static boolean getServerStatus() {
        return m_server_busy;
    }

    synchronized public static void setServerStatus(boolean val) {
        m_server_busy = val;
    }
}

package com.wdc.www.wd360;



import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * Created by Biswapratap Chatterjee on 7/14/2017.
 */

public class ObserverMinerThread implements Runnable {
    private static String TAG = ObserverMinerThread.class.getSimpleName();

    @Override
    public void run() {
        File rootPath = new File(Globals.ROOT_FOLDER_NAME);
        RecursiveFileObserver observer = null;
        observer = new RecursiveFileObserver(rootPath.getAbsolutePath());
        observer.startWatching();
        try {
            do {
                MasterDBHelper mDBHlpr = new MasterDBHelper(Globals.gContext);
                mDBHlpr.setUserName(Globals.myCloudUserId);
                int items_doc_indexed_until_now = mDBHlpr.ItemsDocIndexedUntilNow();
                if (items_doc_indexed_until_now == 0) {
                    Globals.FILEOBSERVER_HOLD_QUEUE.poll(300, TimeUnit.SECONDS);
                } else {
                    Globals.FILEOBSERVER_HOLD_QUEUE.poll(1800, TimeUnit.SECONDS);
                }
                Globals.wakeup.NOTIFY("dummy");
            } while (true);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

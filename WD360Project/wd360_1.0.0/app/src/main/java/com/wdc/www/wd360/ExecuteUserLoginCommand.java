package com.wdc.www.wd360;

import android.support.annotation.Nullable;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.dom.DOMLocator;

/**
 * Created by Biswapratap Chatterjee on 7/11/2017.
 */

public class ExecuteUserLoginCommand {
    private static String TAG = ExecuteUserLoginCommand.class.getSimpleName();
    private static List<File> folders = new ArrayList<File>();

    private static Boolean createSearchResultsInfrastructure(String userdir) {
        Boolean ret = true;
        try {
            File userResultsRootFolder = Utility.createFolder(
                    Globals.ROOT_FOLDER_NAME + File.separator + Globals.WD360_USER_RESULTS_ROOT_FOLDER);
            if (null == userResultsRootFolder) {
                ret = false;
            } else {
                folders.add(userResultsRootFolder);
            }
            File userImageSearchBaseFolder = Utility.createFolder(
                    userResultsRootFolder.getAbsolutePath() + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_BASE_FOLDER);
            if (null == userImageSearchBaseFolder) {
                ret = false;
            } else {
                folders.add(userImageSearchBaseFolder);
            }
            File userImageSearchGroupFolder = Utility.createFolder(
                    userImageSearchBaseFolder.getAbsolutePath() + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_GROUP_FOLDER);
            if (null == userImageSearchGroupFolder) {
                ret = false;
            } else {
                folders.add(userImageSearchGroupFolder);
                Globals.ABSOLUTE_USER_IMAGE_SEARCH_GROUP_FOLDER_PATH = userImageSearchGroupFolder.getAbsolutePath();
            }
            File userImageSearchDuplicateFolder = Utility.createFolder(
                    userImageSearchBaseFolder.getAbsolutePath() + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_DUPLICATE_FOLDER);
            if (null == userImageSearchDuplicateFolder) {
                ret = false;
            } else {
                folders.add(userImageSearchDuplicateFolder);
                Globals.ABSOLUTE_USER_IMAGE_SEARCH_DUPLICATE_FOLDER_PATH = userImageSearchDuplicateFolder.getAbsolutePath();
            }
            File userImageSearchNearDuplicateFolder = Utility.createFolder(
                    userImageSearchBaseFolder.getAbsolutePath() + File.separator +
                            Globals.WD360_USER_IMAGE_SEARCH_NEAR_DUPLICATE_FOLDER);
            if (null == userImageSearchNearDuplicateFolder) {
                ret = false;
            } else {
                folders.add(userImageSearchNearDuplicateFolder);
                Globals.ABSOLUTE_USER_IMAGE_SEARCH_NEAR_DUPLICATE_FOLDER_PATH = userImageSearchNearDuplicateFolder.getAbsolutePath();
            }

        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            ret = false;
        }

        if (false == ret) {
            Log.e(TAG, "Rolling Back!");
            for (File folder : folders) {
                if (null != folder) {
                    folder.delete();
                }
            }
        }

        return ret;
    }

    public static String executeUserLogin(String[] keyValues, String cmd_opcode) {
        try {
            String ret = "";
            String username = "";
            if (keyValues.length != 3) {
                return JSONUtils.formResponseValues("400",
                        "",
                        cmd_opcode,
                        "",
                        "",
                        "",
                        "Requires at least " + 3 + " key value pairs",
                        Globals.emptyResponse);
            } else {
                String[] usrname = keyValues[1].split("=");
                if (!usrname[0].equalsIgnoreCase("username")) {
                    return JSONUtils.formResponseValues("400",
                            "",
                            cmd_opcode,
                            "",
                            "",
                            "",
                            "Username key name is wrong, use 'username' for User Name",
                            Globals.emptyResponse);
                } else {
                    username = usrname[1];
                }
            }

            String[] passwd = keyValues[2].split("=");
            if (!passwd[0].equalsIgnoreCase("passwd"))
                return JSONUtils.formResponseValues("400",
                        "",
                        cmd_opcode,
                        "",
                        "",
                        "",
                        "Username key name is wrong, use 'passwd' for Password",
                        Globals.emptyResponse);

            MasterDBHelper mDbHelper = new MasterDBHelper(Globals.gContext);
            mDbHelper.setUserName(username);
            String usrRootAbsPath = mDbHelper.getTableAbsoluteRootFolerPath();

            if ((null != usrRootAbsPath) && (0 == usrRootAbsPath.compareTo("CREATED"))) {
                usrRootAbsPath = Globals.ROOT_FOLDER_NAME + Globals.myCloudUserId + File.separator;
                File fol = Utility.createFolder(usrRootAbsPath);
                if (null == fol) {
                    mDbHelper.deleteRow(usrRootAbsPath);
                    return JSONUtils.formResponseValues("500",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "Failed to create home folder for user : " +
                                    username + ", rolled back",
                            Globals.emptyResponse);
                }
                if (false == createSearchResultsInfrastructure(usrRootAbsPath)) {
                    mDbHelper.deleteRow(usrRootAbsPath);
                    if (null != fol)
                        fol.delete();
                    return JSONUtils.formResponseValues("500",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "Failed to create Search results infrastructure for user : " +
                                    username + ", rolled back",
                            Globals.emptyResponse);
                }

                if (!mDbHelper.insertRowIfNotExist(usrRootAbsPath,
                        0,
                        "NA",
                        0)) {
                    mDbHelper.deleteRow(usrRootAbsPath);
                    for (File folder : folders) {
                        if (null != folder) {
                            folder.delete();
                        }
                    }
                    if (null != fol)
                        fol.delete();
                    return JSONUtils.formResponseValues("500",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "Failed make entry into db for user, rolled back",
                            Globals.emptyResponse);
                } else {
                    File wd360etc = Utility.createFolder(Globals.ROOT_FOLDER_NAME + File.separator +
                            Globals.WD360_ETC_FOLDER + File.separator);
                    File wd360nativeLogs = Utility.createFolder(Globals.ROOT_FOLDER_NAME + File.separator +
                            Globals.WD360_NATIVE_LOG_FOLDER + File.separator);

                    if ((null == wd360etc) || (null == wd360nativeLogs)) {
                        mDbHelper.deleteRow(usrRootAbsPath);
                        for (File folder : folders) {
                            if (null != folder) {
                                folder.delete();
                            }
                        }
                        if (null != wd360etc)
                            wd360etc.delete();
                        if (null != wd360nativeLogs)
                            wd360nativeLogs.delete();
                        if (null != fol)
                            fol.delete();
                        return JSONUtils.formResponseValues("500",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "Failed to create face detector related files, rolled back",
                                Globals.emptyResponse);
                    } else {
                        File xmlFile = Utility.putFileFromAsset(Globals.WD360_ETC_FOLDER,
                                Globals.WD360_FACE_DETECTOR_XML_FILE_NAME, wd360etc, "");

                        if (null == xmlFile) {
                            mDbHelper.deleteRow(usrRootAbsPath);
                            for (File folder : folders) {
                                if (null != folder) {
                                    folder.delete();
                                }
                            }
                            if (null != xmlFile)
                                xmlFile.delete();
                            if (null != fol)
                                fol.delete();
                            if (null != wd360etc)
                                wd360etc.delete();
                            if (null != wd360nativeLogs)
                                wd360nativeLogs.delete();
                            if (null != fol)
                                fol.delete();
                            return JSONUtils.formResponseValues("500",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "Failed to create wd 360 etc folder",
                                    Globals.emptyResponse);
                        }
                    }

                    return JSONUtils.formResponseValues("200",
                            username,
                            cmd_opcode,
                            "",
                            "",
                            "",
                            username +
                            " logged in successfully, now you can push your files into ROOT/"
                            + username + " folder.",
                            Globals.emptyResponse);
                }
            } else {
                if ((null != usrRootAbsPath) && (0 != usrRootAbsPath.compareTo("CREATED"))) {
                    return JSONUtils.formResponseValues("200",
                            username,
                            cmd_opcode,
                            "",
                            "",
                            "",
                            username +
                            " already logged in, now you can push your files into ROOT/"
                            + username + " folder.",
                            Globals.emptyResponse);
                } else {
                    return JSONUtils.formResponseValues("500",
                            username,
                            cmd_opcode,
                            "",
                            "",
                            "",
                            "Failed to create entry in DB for user : " + username,
                            Globals.emptyResponse);
                }
            }
        } catch (Exception e) {
            Log.d(TAG, "executeUserLogin() : " + e.getMessage());
            return JSONUtils.formResponseValues("500",
                    "",
                    "",
                    "",
                    "",
                    "",
                    e.getMessage(),
                    Globals.emptyResponse);
        }
    }
}

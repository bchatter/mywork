package com.wdc.www.wd360;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Biswapratap Chatterjee on 7/14/2017.
 */

public class PeriodicMinerThread implements Runnable {
    private static String TAG = PeriodicMinerThread.class.getSimpleName();

    synchronized public void periodicPutItem(String un,
                                             String path) {
        try {
            Globals.PERIODIC_MINER_QUEUE.put(new QueueItem(un, path));
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
        }
    }

    @Override
    public void run() {
        int iteration = 1;
        MasterDBHelper mDBHlpr = new MasterDBHelper(Globals.gContext);
        do {
            try {
                ServerBusy.setServerStatus(true);
                /*
                 * SERVER BUSY STATUS ON HERE
                 */
                    List<String> existingUserNames = mDBHlpr.getAllTableNames();
                    int items_doc_indexed_until_now = 0;
                    if (null != existingUserNames) {
                        for (String oneExistingUser : existingUserNames) {
                            mDBHlpr.setUserName(oneExistingUser);
                            String absUserRootPath = mDBHlpr.getTableAbsoluteRootFolerPath();
                            if (null != absUserRootPath) {
                                List<File> files = Utility.listMineablefiles(oneExistingUser, absUserRootPath);
                                Log.d(TAG, "###################### ITEMS TO BE MINED NOW FOR USER [ " + oneExistingUser + " ] IN IREATION [ " + iteration + " ] = " + files.size());
                                for (File file : files) {
                                    periodicPutItem(oneExistingUser, file.getAbsolutePath());
                                }
                                if (Globals.PERIODIC_MINER_QUEUE.size() > 0) {
                                    new BlockedQueueBasedMiner().mineWithTimeout();
                                }
                            }
                            items_doc_indexed_until_now = mDBHlpr.ItemsDocIndexedUntilNow();
                            Log.d(TAG, "###################### ITEMS DOC INDEXED FOR USER [ " + oneExistingUser + " ] IN IREATION [ " + iteration + " ] = " + items_doc_indexed_until_now);
                        }
                    }
                    if (0 < items_doc_indexed_until_now) {
                        Utility.getCurrentGroupResults();
                        Log.d(TAG, "Completed getting results for Group Image Search");
                        Utility.getCurrentExtactDuplicateResults();
                        Log.d(TAG, "Completed getting results for Exact Duplicate Image Search");
                        Utility.getCurrentNearDuplicateResults();
                        Log.d(TAG, "Completed getting results for Near Duplicate Image Search");
                    }
                    iteration++;
                 /*
                 * SERVER BUSY STATUS OFF HERE
                 */
                ServerBusy.setServerStatus(false);
                Globals.wakeup.WAIT();
            } catch (Exception e) {
                Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            }
        } while (true);
    }
}

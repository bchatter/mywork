package com.wdc.www.wd360;

import android.os.FileObserver;

import com.wdc.nassdk.Utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Stack;

/**
 * Created by Biswapratap Chatterjee on 7/14/2017.
 */

public class RecursiveFileObserver extends FileObserver {
    public final String TAG = RecursiveFileObserver.class.getSimpleName();
    public static int MASK = MODIFY | CLOSE_WRITE
            | MOVED_FROM | MOVED_TO | DELETE | CREATE
            | DELETE_SELF | MOVE_SELF;

    List<SingleFileObserver> mObservers;
    String mPath;
    int mMask;


    public RecursiveFileObserver(String path) {
        super(path, MASK);
        mPath = path;
        mMask = MASK;
    }

    @Override
    public void startWatching() {
        if (mObservers != null) return;
        mObservers = new ArrayList<SingleFileObserver>();
        Stack<String> stack = new Stack<String>();
        stack.push(mPath);

        while (stack.size() != 0) {
            String parent = stack.pop();
            if (parent.contains(Globals.myCloudUserId)) {
                mObservers.add(new SingleFileObserver(parent, mMask));
            }
            File path = new File(parent);
            File[] files = path.listFiles();
            if (files == null) continue;
            for (int i = 0; i < files.length; ++i) {
                if (files[i].isDirectory() &&
                    (false == files[i].getAbsolutePath().contains(Globals.WD360_ETC_FOLDER)) &&
                    (false == files[i].getAbsolutePath().contains(Globals.WD360_NATIVE_LOG_FOLDER)) &&
                    (false == files[i].getAbsolutePath().contains(Globals.WD360_USER_RESULTS_ROOT_FOLDER)) &&
                    (false == files[i].getAbsolutePath().contains(Globals.WD360_IGNORE_CODE_CACHE_FOLDER)) &&
                    (false == files[i].getAbsolutePath().contains(Globals.WD360_IGNORE_DATABASES_FOLDER)) &&
                    (false == files[i].getAbsolutePath().contains(Globals.WD360_IGNORE_LIB_FOLDER)) &&
                    (false == files[i].getAbsolutePath().contains("Android")) &&
                    (false == files[i].getAbsolutePath().contains("Alarms")) &&
                    (false == files[i].getAbsolutePath().contains("DCIM")) &&
                    (false == files[i].getAbsolutePath().contains("Download")) &&
                    (false == files[i].getAbsolutePath().contains("Movies")) &&
                    (false == files[i].getAbsolutePath().contains("Music")) &&
                    (false == files[i].getAbsolutePath().contains("Notifications")) &&
                    (false == files[i].getAbsolutePath().contains("Pictures")) &&
                    (false == files[i].getAbsolutePath().contains("Podcasts")) &&
                    (false == files[i].getAbsolutePath().contains("Ringtones")) &&
                    (false == files[i].getAbsolutePath().equals(".")) &&
                    (false == files[i].getAbsolutePath().equals(".."))) {
                    stack.push(files[i].getAbsolutePath());
                }
            }
        }

        for (int i = 0; i < mObservers.size(); i++) {
            mObservers.get(i).startWatching();
            Log.d(TAG, "Started watching # folder : " + mObservers.get(i).getPath());
        }
    }

    private void removeWatching(String file) {
        for (FileObserver fo : mObservers) {
            if (0 == ((SingleFileObserver)fo).getPath().compareTo(file)) {
                fo.stopWatching();
                Log.d(TAG, "Stopped watching folder : " + file);
                mObservers.remove(fo);
                File path = new File(file);
                File[] files = path.listFiles();
                if (files == null) return;
                for (int i = 0; i < files.length; ++i) {
                    if (files[i].isDirectory() &&
                       (false == files[i].getName().equals(".")) &&
                       (false == files[i].getName().equals(".."))) {
                        removeWatching(files[i].getPath());
                    }
                }
            }
        }
    }

    @Override
    public void stopWatching() {
        if (mObservers == null) return;

        for (int i = 0; i < mObservers.size(); ++i)
            mObservers.get(i).stopWatching();

        mObservers.clear();
        mObservers = null;
    }

    @Override
    public void onEvent(int event, String file) {
        /*
         * Do Nothing
         */
    }

    private class SingleFileObserver extends FileObserver {
        private String mPath;

        public SingleFileObserver(String path, int mask) {
            super(path, mask);
            mPath = path;
        }

        String getPath() {
            return mPath;
        }

        @Override
        public void onEvent(int event, String path) {
            event &= ALL_EVENTS;
            String newPath = mPath + "/" + path;

            if (event == DELETE) {
                String userName = "";
                int beginIndex = newPath.indexOf(Globals.ROOT_FOLDER_NAME) + Globals.ROOT_FOLDER_NAME.length();
                int endIndex = newPath.indexOf('/', beginIndex);
                if (-1 == endIndex) {
                    userName = newPath.substring(beginIndex);
                } else {
                    userName = newPath.substring(beginIndex, endIndex);
                }
                MasterDBHelper mDBHelper = new MasterDBHelper(Globals.gContext);
                mDBHelper.setUserName(userName);
                File f = new File(newPath);
                if (f.exists() && f.isFile()) {
                    if (true == mDBHelper.isFileKnownToDB(newPath)) {
                        if (true == mDBHelper.deleteRow(newPath)) {
                            Log.d(TAG, "Removed [" + newPath + "] from DB");
                        } else {
                            Utility.getWritableFile(new File(f.getParent()), f.getName());
                        }
                    } else {
                        Log.d(TAG, "False delete events on [" + newPath + "]");
                    }
                } else if (f.exists() && f.isDirectory()) {
                    removeWatching(newPath);
                    Log.d(TAG, "Recursively stopped watching [" + newPath + "]");
                }
            }

            if (false == newPath.contains(Globals.myCloudUserId)) {
                Log.d(TAG, "Ignoring events on [" + newPath + "]");
                return;
            }
            Globals.wakeup.NOTIFY(newPath);
        }
    }
}

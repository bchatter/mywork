package com.wdc.www.wd360;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent startup_intent = new Intent(this, StartupService.class);
        startup_intent.putExtra("MyCloudId", "auth0|59a11db2ff5c385f4c0b6bb7");
        this.startService(startup_intent);
    }
}

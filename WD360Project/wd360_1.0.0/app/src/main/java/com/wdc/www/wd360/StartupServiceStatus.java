package com.wdc.www.wd360;

/**
 * Created by Biswapratap Chatterjee on 8/17/2017.
 */

public class StartupServiceStatus {
    private static boolean m_serviceStatus = false;

    synchronized public static void setServiceStatus(boolean serviceStatus) {
        m_serviceStatus = serviceStatus;
    }

    synchronized public static boolean getServiceStatus() {
        return m_serviceStatus;
    }
}

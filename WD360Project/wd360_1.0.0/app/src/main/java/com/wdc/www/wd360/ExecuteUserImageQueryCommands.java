package com.wdc.www.wd360;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.wdc.nassdk.Utils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Biswapratap Chatterjee on 7/11/2017.
 */

public class ExecuteUserImageQueryCommands {
    private static String TAG = ExecuteUserImageQueryCommands.class.getSimpleName();
    private static String MESSAGE = "Failed to query";

    public static String executeUserImageQuery(String[] keyValues, String cmd_opcode) {
        try {
            String ret = "";
            String username = "";
            ret = JSONUtils.commandSpecificChecks(keyValues, cmd_opcode, 2);
            if (ret.contains("ASSERT"))
                return ret;
            else {
                username = Globals.myCloudUserId;
                String[] query = null;
                query = keyValues[1].split("=");

                MasterDBHelper masterDBHelper = new MasterDBHelper(Globals.gContext);
                masterDBHelper.setUserName(username);

                if (query[0].equalsIgnoreCase("group")) {
                    return Globals.group_result;
                } else if (query[0].equalsIgnoreCase("duplicate")) {
                    return Globals.exact_duplicate_result;
                } else if (query[0].equalsIgnoreCase("nearduplicate")) {
                    return Globals.near_duplicate_result;
                } else {
                    return JSONUtils.formResponseValues("500",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "Incorrect query parameter, can be either one of these - group, duplicate",
                            Globals.emptyResponse);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, Utility.getExceptionDetailedInfo(e, TAG) + " : " + e.getMessage());
            return JSONUtils.formResponseValues("500",
                    "",
                    "",
                    "",
                    "",
                    "",
                    e.getMessage(),
                    Globals.emptyResponse);
        }
    }
}

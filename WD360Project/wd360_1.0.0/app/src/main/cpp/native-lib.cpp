#include "opencv2/core/utility.hpp"
#include "opencv2/face.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/objdetect.hpp"

#include "jni.h"
#include <fstream>

using namespace cv;
using namespace std;

string logpath = "";
Size global_min;
bool logon = false;

void LOG(string msg) {
    if (true == logon) {
        ofstream m_mycout(logpath.c_str(), ios_base::out | ios_base::app);
        m_mycout << "[INFO] " << msg << endl;
        m_mycout.close();
    }
}

void CLEAR_OLD_LOG() {
    ofstream m_mycout(logpath.c_str(), ios_base::out | ios_base::trunc);
    m_mycout.close();
}

class WD360FaceDetection {

private:
    string m_haarcascade_frontalface_xmlPath;

    void to_grayscale(Mat img, Mat &gray) {
        try {
            Mat tempgray;
            cv::cvtColor(img, tempgray, COLOR_RGB2GRAY);
            equalizeHist(tempgray, gray);
        } catch (cv::Exception e) {
            stringstream ss;
            ss << "to_grayscale() exception occured : " << e.msg;
            LOG(ss.str());
        }
    }

public:
    WD360FaceDetection(string xmlPath) {
        LOG("Entering WD360FaceDetection() constructor ...");
        m_haarcascade_frontalface_xmlPath = xmlPath;
        LOG("Exiting WD360FaceDetection() constructor ...");
    }

    ~WD360FaceDetection() {
        LOG("Entering WD360FaceDetection() destructor ...");
        LOG("Exiting WD360FaceDetection() destructor ...");
    }

    /*
     * type = 0, Human.
     */
    int howManyFaces(string imagefilepath, int type) {
        LOG("Entering howManyFaces() ...");
        int numOfFaces = 0;
        Mat sample;
        try {
            CascadeClassifier cascade;
            String imgFPath(imagefilepath.c_str());
            sample = imread(imgFPath);
            if (0 == type) {
                cascade = CascadeClassifier(m_haarcascade_frontalface_xmlPath.c_str());
            } else {
                LOG("howManyFaces() : Incorrect type value");
            }
            if (cascade.empty()) {
                LOG("howManyFaces() : Could not obtain the cascade classified");
            }
            Mat gray;
            to_grayscale(sample, gray);
            vector<Rect> rects;
            stringstream ss;
            ss << "howManyFaces() detectMultiScale() on file : " << imagefilepath;
            LOG(ss.str());
            cascade.detectMultiScale(gray, rects);
            numOfFaces = rects.size();
            rects.clear();
        } catch (cv::Exception e) {
            stringstream ss;
            ss << "Error howManyFaces() faces. Reason: " << e.msg;
            LOG(ss.str());
        }
        LOG("Exiting howManyFaces() ...");
        return numOfFaces;
    }
};

string jstring2string(JNIEnv *env, jstring jStr) {
    string ret = "";
    if (!jStr) {
        LOG("Exiting jstring2string() : Reason jni string is null...");
        return "";
    }

    try {
        vector<char> charsCode;
        const jchar *chars = env->GetStringChars(jStr, NULL);
        jsize len = env->GetStringLength(jStr);
        jsize i;
        for( i=0;i<len; i++){
            int code = (int)chars[i];
            charsCode.push_back(code);
        }
        env->ReleaseStringChars(jStr, chars);
        ret = string(charsCode.begin(), charsCode.end());
    } catch (exception e) {
        stringstream ss;
        ss << "Error jstring2string() Reason: " << e.what();
        LOG(ss.str());
    }
    return ret;
}

extern "C"
JNIEXPORT jlong
JNICALL
Java_com_wdc_www_wd360_WD360FaceDetection_nativeCreateObject
        (JNIEnv *p_jenv, jobject p_jthis, jstring p_jlogpath, jstring p_jxmlpath) {
    logpath = jstring2string(p_jenv, p_jlogpath);
    CLEAR_OLD_LOG();
    LOG("Entering nativeCreateObject() ...");
    jlong result = 0;
    try {
        string xmlPath = jstring2string(p_jenv, p_jxmlpath);

        global_min.height = 100;
        global_min.width = 100;

        stringstream ss1;
        ss1 << "nativeCreateObject() : called with : "
            << endl << "Log Path        = " << logpath
            << endl << "XML Path        = " << xmlPath;
        LOG(ss1.str());
        result = (jlong)new WD360FaceDetection(xmlPath);
    } catch (cv::Exception& e) {
        stringstream ss;
        ss << "Error nativeCreateObject(). Reason: " << e.msg;
        LOG(ss.str());
    }

    LOG("Exiting nativeCreateObject() ...");
    return result;
}

extern "C"
JNIEXPORT void
JNICALL
Java_com_wdc_www_wd360_WD360FaceDetection_nativeDestroyObject
        (JNIEnv *p_jenv, jobject p_jthis, jlong p_native_ptr) {
    LOG("Entering nativeDestroyObject() ...");
    try {
        if (p_native_ptr != 0) {
            delete (WD360FaceDetection*)p_native_ptr;
        }
    } catch (cv::Exception& e) {
        stringstream ss;
        ss << "Error nativeDestroyObject(). Reason: " << e.msg;
        LOG(ss.str());
    }

    LOG("Exiting nativeDestroyObject() ...");
}

extern "C"
JNIEXPORT void
JNICALL
Java_com_wdc_www_wd360_WD360FaceDetection_nativeLogOn
        (JNIEnv *p_jenv, jobject p_jthis, jlong p_native_ptr) {
    LOG("Entering nativeLogOn() ...");
    logon = true;
    LOG("Entering nativeLogOff() ...");
}

extern "C"
JNIEXPORT void
JNICALL
Java_com_wdc_www_wd360_WD360FaceDetection_nativeLogOff
        (JNIEnv *p_jenv, jobject p_jthis, jlong p_native_ptr) {
    LOG("Entering nativeLogOn() ...");
    logon = false;
    LOG("Entering nativeLogOff() ...");
}

extern "C"
JNIEXPORT jint
JNICALL
Java_com_wdc_www_wd360_WD360FaceDetection_nativeHowManyFaces
        (JNIEnv *p_jenv, jobject p_jthis, jlong p_native_ptr,
         jstring p_jimagefilepath, jint p_jtype) {
    jint ret = 0;
    LOG("Entering nativeHowManyFaces() ...");
    try {
        if(p_native_ptr != 0) {
            int type = p_jtype;
            string stdimagefilepath = jstring2string(p_jenv, p_jimagefilepath);
            stringstream ss1;
            ss1 << "nativeHowManyFaces() : called with : "
                << endl << "Image file path = " << stdimagefilepath
                << endl << "Type            = " << type;
            LOG(ss1.str());
            ret = (jint)((WD360FaceDetection*)p_native_ptr)->howManyFaces(stdimagefilepath, type);
        } else {
            LOG("nativeHowManyFaces() : Null face detection handle");
        }
    } catch (cv::Exception& e) {
        ret = 0;
        stringstream ss;
        ss << "Error nativeHowManyFaces(). Reason: " << e.msg;
        LOG(ss.str());
    }
    LOG("Exiting nativeHowManyFaces() ...");
    return ret;
}

All images in this directory (c) Richard Szeliski and Microsoft
Corporation.

These images may only be used for the purpose of participating in the
ICCV'2005 Computer Vision Contest and may not be otherwise displayed,
posted, or redistributed.

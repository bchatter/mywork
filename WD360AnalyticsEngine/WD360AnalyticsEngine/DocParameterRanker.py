import os
from pandas.io.json import json_normalize
from operator import itemgetter
import pandas as pd
from WD360AnalyticsEngine.WDUtils import *

def docKeywordTextRanking(query_meta_data_dict, docCollection):

    try:
        r = []
        temp_results = []
        queryDF = (json_normalize(query_meta_data_dict, ['keywords']))
        renamedQueryDF = queryDF.rename(columns={'relevance': 'query_relevance', 'text': 'query_text'})
        for c in docCollection:
            try:
                if len(c['response']) == 0:
                    continue
                collectionDF = (json_normalize(c, ['response', 'keywords'], ['filepath']))
                sub_collectionDF = collectionDF[['relevance', 'text']]
                masterDF = pd.concat([renamedQueryDF, sub_collectionDF], axis=1)
                r = []
                r.append(masterDF.apply(lambda row: getSimilarityScore(row['query_text'], masterDF), axis=1))
                dummy_x_axis = r[0]
                new_dummy_x_axis = [(a, 0) for a, b in dummy_x_axis]
                shift, val = getAreaBetweenTwoGraphs(r[0], new_dummy_x_axis)
                temp_results.append([os.path.basename(collectionDF['filepath'][0]), shift, val])
            except Exception as ins:
                print "Exception inside loop docKeywordTextRanking : " + str(ins)
                continue
        x = sorted(temp_results, key=itemgetter(1,2))
        results = x[0:5]
        return results
    except Exception as inst:
        print "Exception docKeywordTextRanking : " + str(inst)
        return tuple([])

def docEntitiesRanking(query_meta_data_dict, docCollection):
    try:
        r = []
        temp_results = []
        queryDF = (json_normalize(query_meta_data_dict, ['entities']))
        queryDF['rowIndex'] = queryDF.apply(rowIndex, axis=1)
        queryDF['typelist'] = queryDF.apply(lambda row: flattenTypes(row['disambiguation'], row['rowIndex'], queryDF), axis=1)
        queryDF = queryDF[['rowIndex', 'relevance', 'text', 'typelist', 'count']]
        renamedQueryDF = queryDF.rename(columns={'relevance': 'query_relevance', 'text': 'query_text', 'typelist':'query_typelist', 'count': 'query_count'})
        for c in docCollection:
            try:
                if len(c['response']) == 0:
                    continue
                collectionDF = (json_normalize(c, ['response', 'entities'], ['filepath']))
                collectionDF['rowIndex'] = collectionDF.apply(rowIndex, axis=1)
                collectionDF['typelist'] = collectionDF.apply(lambda row: flattenTypes(row['disambiguation'], row['rowIndex'], collectionDF), axis=1)
                collectionDF_subset = collectionDF[['rowIndex', 'relevance', 'text', 'typelist', 'count']]
                masterDF = pd.concat([renamedQueryDF, collectionDF_subset], axis=1)
                r = []
                r.append(masterDF.apply(lambda row: getEntitySimilarityScore(row['query_text'], row['query_typelist'], masterDF), axis=1))
                dummy_x_axis = r[0]
                #print dummy_x_axis
                new_dummy_x_axis = [(a, 0) for a, b in dummy_x_axis]
                shift, val = getAreaBetweenTwoGraphs(r[0], new_dummy_x_axis)
                temp_results.append([os.path.basename(collectionDF['filepath'][0]), shift, val])
            except Exception as ins:
                print "Exception inside loop docEntitiesRanking : " + str(ins)
                continue
        x = sorted(temp_results, key=itemgetter(1, 2))
        results = x[0:5]
        return results
    except Exception as inst:
        print "Exception docEntitiesRanking : " + str(inst)
        return tuple([])

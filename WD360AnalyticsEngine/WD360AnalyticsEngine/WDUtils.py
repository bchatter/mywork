import heapq
import re

from pandas import Index
from shapely.geometry import Polygon
import time
import numpy as np
import pandas as pd
from textblob import TextBlob
from fuzzywuzzy import fuzz
import hashlib

def getAreaBetweenTwoGraphs(x_y_curve1, x_y_curve2):

    polygon_points = []

    for xyvalue in x_y_curve1:
        polygon_points.append([xyvalue[0], xyvalue[1]])

    for xyvalue in x_y_curve2[::-1]:
        polygon_points.append([xyvalue[0], xyvalue[
            1]])

    for xyvalue in x_y_curve1[0:1]:
        polygon_points.append(
            [xyvalue[0], xyvalue[1]])

    polygon = Polygon(polygon_points)
    scientific_area = polygon.area
    scientific_area_str = str(scientific_area)
    pos = scientific_area_str.find("e")
    if pos != -1:
        sign = scientific_area_str[pos + 1]
        sign_val = scientific_area_str[(pos + 2):]
        shift = 0
        x = 0.0
        if sign == "-":
            shift = 0
            multiply_factor = float("1E+" + sign_val)
            x = polygon.area * multiply_factor
        else:
            if sign == "+":
                shift = 1
                multiply_factor = float("1E-" + sign_val)
                x = polygon.area * multiply_factor
        return (shift, x)
    else:
        return (0, scientific_area)

def pause():
    time.sleep(2)

def rowIndex(row):
    return row.name


def flattenTypes(disambiguation, index, df):
    val = []
    try:
        if not pd.isnull(disambiguation):
            val.append(df.iloc[index]['type'])
            [val.append(x) for x in disambiguation.get('subtype')]
        else:
            val.append(df.iloc[index]['type'])
        return val
    except Exception as inst:
        print "Exception flattenTypes : " + str(inst)
        return val

def getEntitySimilarityScore(query_text, query_typelist, masterDF):
    try:
        if pd.isnull(query_text) and pd.isnull(query_typelist):
            return tuple([])
        v = 0
        matched_text = ""
        query_index = np.where(masterDF['query_text'] == query_text)[0][0]
        query_typelist_str = ' '.join(chunk for chunk in query_typelist)

        for text, typelist in zip(masterDF['text'], masterDF['typelist']):
            if pd.isnull(text) and pd.isnull(typelist):
                continue
            typelist_str = ' '.join(chunk for chunk in typelist)
            vnew_text = fuzz.token_sort_ratio(query_text, text)
            vnew_type = fuzz.token_sort_ratio(query_typelist_str, typelist_str)
            fvalue = np.mean([vnew_text, vnew_type])
            if fvalue > v:
                v = fvalue
                matched_text = text

        matched_index = np.where(masterDF['text'] == matched_text)[0][0]
        matched_relevance = masterDF.iloc[matched_index]['relevance']
        matched_count = masterDF.iloc[matched_index]['count']
        query_relevance = masterDF.iloc[query_index]['query_relevance']
        query_count = masterDF.iloc[query_index]['query_count']
        score = float(abs((float(matched_relevance) * float(matched_count)) - (float(query_relevance) * float(query_count))))
        return tuple([int(hashlib.md5(query_text).hexdigest(), 16), score])
    except Exception as inst:
        print "Exception getEntitySimilarityScore : " + str(inst)
        return tuple([])

def getSimilarityScore(query_text, masterDF):

    try:
        v = 0
        matched_text = ""
        query_index = np.where(masterDF['query_text'] == query_text)[0][0]

        for text in masterDF['text']:
            vnew = fuzz.token_sort_ratio(query_text, text)
            if vnew > v:
                v = vnew
                matched_text = text

        matched_index = np.where(masterDF['text'] == matched_text)[0][0]
        matched_relevance = masterDF.iloc[matched_index]['relevance']
        query_relevance = masterDF.iloc[query_index]['query_relevance']
        score = float(abs(float(matched_relevance) - float(query_relevance)))
        return tuple([int(hashlib.md5(query_text).hexdigest(), 16), score])
    except Exception as inst:
        print "Exception getSimilarityScore : " + str(inst)
        return tuple([])

'''
def label_mode(g, df):
    str = ""
    max_score = 0
    for idx, val in enumerate(g):
        value = df['score'][idx]
        if value > max_score:
            max_score = val
            str = val
    return str
'''

def get_total_valuation_per_label(g, df):
    total_duration = 0.0
    try:
        for i in g.index:
            total_duration = float(total_duration) + \
                             (float(df['end'][i]) -
                              float(df['start'][i]))
    except Exception as inst:
        print(inst)
    total_valuation = total_duration * g.size
    return total_valuation

def getLabelIntValue(g, df):
    retval = 0
    try:
        for i in g.index:
            if df['label'][i] == 'Verse':
                return 1
            if df['label'][i] == 'Chorus':
                return 2
            if df['label'][i] == 'Unlabeled':
                return 3
    except Exception as inst:
        print(inst)
    return retval

def clean_label(label):
    return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", label).split())

'''
def get_overall_sentiment_score(g, df):
    real_score = 0
    try:
        real_score_list = []
        for score, label in df[['score','label']].itertuples(index=False):
            label_analysis = TextBlob(clean_label(label))
            label_sentiment_polarity =  label_analysis.sentiment.polarity
            real_score_list.append(float(score) * float(label_sentiment_polarity))
        real_score = np.mean(real_score_list)
    except Exception as inst:
        print(inst)
    return real_score
'''

def get_top_two_sentiment_score(g, df):
    real_score = 0
    try:
        real_score_list = []
        glist = g.tolist()
        sort_result = heapq.nlargest(2, glist)

        if len(sort_result) == 0:
            return 0
        elif len(sort_result) == 1:
            top1_index = Index(g).get_loc(sort_result[0])
            if isinstance(top1_index, np.ndarray):
                pos = 0
                for idx in top1_index:
                    if idx == True:
                        top1_index = pos
                        break
                    pos = pos + 1
            elif isinstance(top1_index, slice):
                top1_index = glist[0]
            top1_label = df.iloc[top1_index]['label']
            top1_clean_label = clean_label(top1_label)
            top1_sentiment_polarity = TextBlob(top1_clean_label).sentiment.polarity
            real_score_list.append(float(sort_result[0]) * float(top1_sentiment_polarity))
            real_score = np.mean(real_score_list)
        elif len(sort_result) == 2:
            top1_index = Index(g).get_loc(sort_result[0])
            if isinstance(top1_index, np.ndarray):
                pos = 0
                for idx in top1_index:
                    if idx == True:
                        top1_index = pos
                        break
                    pos = pos + 1
            elif isinstance(top1_index, slice):
                top1_index = glist[0]
            top2_index = Index(g).get_loc(sort_result[1])
            if isinstance(top2_index, np.ndarray):
                pos = 0
                for idx in top2_index:
                    if idx == True:
                        top2_index = pos
                        break
                    pos = pos + 1
            elif isinstance(top2_index, slice):
                top2_index = glist[1]

            top1_label = df.iloc[top1_index]['label']
            top2_label = df.iloc[top2_index]['label']

            top1_clean_label = clean_label(top1_label)
            top2_clean_label = clean_label(top2_label)

            top1_sentiment_polarity = TextBlob(top1_clean_label).sentiment.polarity
            top2_sentiment_polarity = TextBlob(top2_clean_label).sentiment.polarity

            real_score_list.append(float(sort_result[0]) * float(top1_sentiment_polarity))
            real_score_list.append(float(sort_result[1]) * float(top2_sentiment_polarity))

            real_score = np.mean(real_score_list)
    except Exception as inst:
        print "Exception get_top_two_sentiment_score : " + str(inst)
    return real_score

'''
def mood_duration(g, df):
    for idx, val in enumerate(g):
        diff = df['timeline.mood.end'][idx] - df['timeline.mood.start'][idx]
        break
    return diff
'''

def audioMoodScoreRanking(queryAudioMood_ScoreDF, audioMood_ScoreDF, filepath):
    try:
        yT = [tuple(x) for x in queryAudioMood_ScoreDF.to_records(index=False)]
        ySize = len(yT)
        xT = [tuple(x) for x in audioMood_ScoreDF.to_records(index=False)]
        xSize = len(xT)
        if ySize > xSize:
            yT = xT[0:xSize]
        else:
            xT = yT[0:ySize]
        shift, val = getAreaBetweenTwoGraphs(xT, yT)
        return [filepath, shift, val]
    except Exception as inst:
        print "Exception audioMoodScoreRanking : " + str(inst)
        return [filepath, 0, 0]

'''
def audioMoodLabelRanking(queryAudioMood_LabelDF, audioMood_LabelDF, filepath):
    yT = [tuple(x) for x in queryAudioMood_LabelDF.to_records(index=False)]
    ySize = len(yT)
    xT = [tuple(x) for x in audioMood_LabelDF.to_records(index=False)]
    xSize = len(xT)
    if ySize > xSize:
        yT = xT[0:xSize]
    else:
        xT = yT[0:ySize]
    nzeros, val = getAreaBetweenTwoGraphs(xT, yT)
    return (filepath, area)
'''
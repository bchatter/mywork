from __future__ import unicode_literals
import urllib2
import uuid
from threading import Thread
from flask import jsonify, json
from flask import Flask, request
from werkzeug.utils import secure_filename
from bs4 import BeautifulSoup
import youtube_dl
from wikipedia import wikipedia
from WD360AnalyticsEngine import *
from WD360AnalyticsEngine.CoreBasedThreadPool import *
from WD360AnalyticsEngine.MetaDataExtractor import getDBCollectionsCount

app = Flask(__name__)

app.config['AUDIO_UPLOAD_FOLDER'] = AUDIO_UPLOAD_FOLDER
app.config['DOC_UPLOAD_FOLDER'] = DOC_UPLOAD_FOLDER
app.config['IMAGE_UPLOAD_FOLDER'] = IMAGE_UPLOAD_FOLDER

processing_mapping = { }
audio_mapping = { }
doc_mapping = { }
image_mapping = { }

def get_sec(time_str):
    h, m, s = time_str.split(':')
    return int(h) * 3600 + int(m) * 60 + int(s)

def convert_youtube_url_into_webm_file_and_get_metadata_string(youtube_url):

    ydl_opts = {
        'format': 'bestaudio/best',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
        }],
    }
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        try:
            ydl.download([youtube_url])
        except Exception:
            pass

    try:
        for filename in os.listdir("."):
            file_extension = os.path.splitext(filename)[1]
            if file_extension == ".webm" or file_extension == ".m4a":
                file_info = os.stat(filename)
                if file_info.st_size >= 10000000:
                    os.remove(filename)
                    return []
                audio_search_meta_data = saveAudioFileAnalyzedData(filename, 1)
                if audio_search_meta_data == 1 or len(audio_search_meta_data['features']) == 0:
                    return []
                else:
                    return contextualSearchEngineForAudioQuery(audio_search_meta_data)
    except Exception as inst:
        print "Exception convert_youtube_url_into_webm_file_and_get_metadata_string : " + str(inst)
        return []

def convert_query_into_youtube_url(querystr):

    try:
        index1 = 0
        index2 = 0
        selected_youtube_url = []
        index_result = 0
        playtime_array = []
        videourl_array = []
        query = urllib.quote(querystr)
        url = "https://www.youtube.com/results?search_query=" + query
        response = urllib2.urlopen(url)
        html = response.read()
        soup = BeautifulSoup(html, "html.parser")
        for playtime in soup.findAll(attrs={'class': 'video-time'}):
            ptime = str(playtime.text)
            if ptime.count(":") == 1:
                ptime = "0:" + ptime
            else:
                if ptime.count(":") == 0:
                    ptime = "0:0:" + ptime
            playtime_array.insert(index1, ptime)
            index1 = index1 + 1
        for vid in soup.findAll(attrs={'class': 'yt-uix-tile-link'}):
            videourl = 'https://www.youtube.com' + vid['href']
            videourl_array.insert(index2, videourl)
            index2 = index2 + 1

        for pt in playtime_array:
            if get_sec(pt) < 700:
                selected_youtube_url.append(videourl_array.pop(index_result))
            else:
                index_result = index_result + 1

        for url in selected_youtube_url:
            ret = convert_youtube_url_into_webm_file_and_get_metadata_string(url)
            if len(ret) != 0:
                return ret
            else:
                continue
            return []
    except Exception as inst:
        print "Exception convert_query_into_youtube_url : " + str(inst)
        return []

def convert_query_into_google_doc_url_and_get_metadata_string(querystr):

    try:
        rslt = wikipedia.search(querystr)
        for r in rslt:
            ny = wikipedia.page(r)
            metadata = saveDocTextFileAnalyzedData(ny.content, 1, 1)
            if len(metadata) != 0:
                ret = contextualSearchEngineForDocQuery(metadata)
                if len(metadata) != 0:
                    return ret
                else:
                    continue
            else:
                continue
        return []
    except Exception as inst:
        print "Exception convert_query_into_google_doc_url_and_get_metadata_string : " + str(inst)
        return []

def get_soup(url,header):
    return BeautifulSoup(urllib2.urlopen(urllib2.Request(url,headers=header)),'html.parser')

def convert_query_into_google_image_url_and_get_metadata_string(querystr):

    query = querystr
    image_type="ActiOn"
    query= query.split()
    query='+'.join(query)
    url="https://www.google.co.in/search?q="+query+"&source=lnms&tbm=isch"

    DIR="Pictures"
    header={'User-Agent':"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36"
    }
    soup = get_soup(url,header)

    count = 0
    ActualImages=[]
    for a in soup.find_all("div",{"class":"rg_meta"}):
        link , Type =json.loads(a.text)["ou"]  ,json.loads(a.text)["ity"]
        ActualImages.append((link,Type))
        count = count + 1
        if count >= 5:
            break

    if not os.path.exists(DIR):
                os.mkdir(DIR)
    DIR = os.path.join(DIR, query.split()[0])

    if not os.path.exists(DIR):
                os.mkdir(DIR)

    for i , (img , Type) in enumerate( ActualImages):
        try:
            req = urllib2.Request(img, headers={'User-Agent' : header})
            raw_img = urllib2.urlopen(req).read()

            cntr = len([i for i in os.listdir(DIR) if image_type in i]) + 1
            imagepath = ""
            if len(Type)==0:
                imagepath = os.path.join(DIR , image_type + "_"+ str(cntr)+".jpg")
                f = open(imagepath, 'wb')
            else:
                imagepath = os.path.join(DIR , image_type + "_"+ str(cntr)+"."+Type)
                f = open(imagepath, 'wb')
            f.write(raw_img)
            f.close()
            metadata = saveImageFileAnalyzedData(imagepath, int(1))
            if len(metadata) != 0:
                ret = contextualSearchEngineForImageQuery(metadata)
                if len(metadata) != 0:
                    return ret
                else:
                    continue
            else:
                continue
        except Exception as e:
            print "could not load : " + img
            print e
    return []

def audioResultCallback(results):
    if results is not None:
        [searchResult.append(x) for x in results]
    g_audioSearchDone.set()

def docResultCallback(results):
    if results is not None:
        [searchResult.append(x) for x in results]
    g_docSearchDone.set()

def imageResultCallback(results):
    if results is not None:
        [searchResult.append(x) for x in results]
    g_imageSearchDone.set()

@app.route('/WD360AE/query', methods=['POST'])
def serverUserQuery():

    done_str = request.form.get('upload_done')
    if done_str == '1':
        g_uploadDoneEvent.set()
        return jsonify({'info_message': 'All files uploaded successfully'})

    if (g_uploadDoneEvent.is_set()) and (g_databasefilledEvent.is_set()):

        if 'file' in request.files:
            file = request.files['file']
            if file.filename == '':
                return jsonify({'error_message': 'No audio selected file'})
            else:
                securefName = secure_filename(file.filename)
                file.save(os.path.join("search", securefName))
                filepath = "search" + "/" + securefName
                if os.path.isfile(filepath):
                    file_info = os.stat(filepath)
                    if file_info.st_size >= 10000000:
                        os.remove(filepath)
                        return jsonify({'file_name': securefName,
                                        'error_message': 'Currently not supported for file size greater than 10 MB'})
                else:
                    return jsonify({'file_name': securefName,
                             'error_message': 'Unable to search for this input'})

                mime = mimetypes.MimeTypes()
                url = urllib.pathname2url(filepath)
                mime_type = mime.guess_type(url)

                if (mime_type[0].find("audio")) != -1 or (mime_type[0].find("video")) != -1:
                    audio_search_meta_data = saveAudioFileAnalyzedData(filepath, 1)
                    os.remove(filepath)
                    if len(audio_search_meta_data) == 0:
                        return jsonify({'file_name': securefName,
                                        'error_message': 'Unable to search for this audio input'})
                    else:
                        audio_hyperlink_list = contextualSearchEngineForAudioQuery(audio_search_meta_data)
                        return jsonify({'message': 'Querry Successfull', 'result': audio_hyperlink_list})

                elif (mime_type[0].find("html") != -1 or
                      mime_type[0].find("text") != -1 or
                      mime_type[0].find("pdf") != -1 or
                      mime_type[0].find("document") != -1 or
                      mime_type[0].find("msword") != -1 or
                      mime_type[0].find("presentation") != -1):
                    doc_search_meta_data = saveDocTextFileAnalyzedData(filepath, 1)
                    os.remove(filepath)
                    if len(doc_search_meta_data) == 0:
                        return jsonify({'file_name': securefName,
                                        'error_message': 'Unable to search for this doc input'})
                    else:
                        doc_hyperlink_list = contextualSearchEngineForDocQuery(doc_search_meta_data)
                        return jsonify({'message': 'Querry Successfull', 'result': doc_hyperlink_list})

                elif (mime_type[0].find("image")) != -1:
                    image_search_meta_data = saveImageFileAnalyzedData(filepath, 1)
                    os.remove(filepath)
                    if len(image_search_meta_data) == 0:
                        return jsonify({'file_name': securefName,
                                        'error_message': 'Unable to search for this image input'})
                    else:
                        image_hyperlink_list = contextualSearchEngineForImageQuery(image_search_meta_data)
                        return jsonify({'message': 'Querry Successfull', 'result': image_hyperlink_list})
                else:

                    return jsonify({'file_name': securefName,
                                    'error_message': 'Unknown type of input file'})
        else:
            querystr = request.form.get('query')
            p.apply_async(convert_query_into_youtube_url, (querystr,), callback=audioResultCallback)
            p.apply_async(convert_query_into_google_doc_url_and_get_metadata_string, (querystr,), callback=docResultCallback)
            p.apply_async(convert_query_into_google_image_url_and_get_metadata_string, (querystr,), callback=imageResultCallback)

        if g_audioSearchDone.wait(timeout=300) and g_docSearchDone.wait(timeout=300) and g_imageSearchDone.wait(timeout=300):
            return jsonify({'message': 'Querry Successfull', 'result':searchResult})
    else:
        return jsonify({'message': 'Please Wait! Data Minining in progress ...'})


@app.route('/WD360AE/audio/upload', methods=['POST'])
def uploadAudioFile():

    if 'file' not in request.files:
        return jsonify({'error_message':'No audio file part', 'upload_status':1})
    file = request.files['file']

    if file.filename == '':
        return jsonify({'error_message':'No audio selected file', 'upload_status':1})

    securefName = secure_filename(file.filename)
    file.save(os.path.join(app.config['AUDIO_UPLOAD_FOLDER'], securefName))
    if os.path.isfile(AUDIO_UPLOAD_FOLDER + "/" + securefName):
        file_info = os.stat(AUDIO_UPLOAD_FOLDER + "/" + securefName)
        if file_info.st_size >= 10000000:
            os.remove(AUDIO_UPLOAD_FOLDER + "/" + securefName)
            return jsonify({'file_name': securefName, 'error_message':'Currently not supported for file size greater than 10 MB', 'upload_status': 1})
    fid = uuid.uuid4()
    audio_mapping[fid] = securefName

    return jsonify({'file_name':securefName, 'file_id':fid, 'upload_status':0})


@app.route('/WD360AE/image/upload', methods=['POST'])
def uploadImageFile():

    if 'file' not in request.files:
        return jsonify({'error_message':'No image file part', 'upload_status':1})
    file = request.files['file']

    if file.filename == '':
        return jsonify({'error_message':'No image selected file', 'upload_status':1})

    securefName = secure_filename(file.filename)
    file.save(os.path.join(app.config['IMAGE_UPLOAD_FOLDER'], securefName))
    fid = uuid.uuid4();
    image_mapping[fid] = securefName;

    return jsonify({'file_name':securefName, 'file_id':fid, 'upload_status':0})


@app.route('/WD360AE/doc/upload', methods=['POST'])
def uploadDocumentFile():

    if 'file' not in request.files:
        return jsonify({'error_message':'No document file part', 'upload_status':1})
    file = request.files['file']

    if file.filename == '':
        return jsonify({'error_message':'No document selected file', 'upload_status':1})

    securefName = secure_filename(file.filename)
    file.save(os.path.join(app.config['DOC_UPLOAD_FOLDER'], securefName))
    fid = uuid.uuid4();
    doc_mapping[fid] = securefName;

    return jsonify({'file_name':securefName, 'file_id':fid, 'upload_status':0})

def pool_init():
    print "Starting : " + str(multiprocessing.current_process().name) + " pid : " + str(os.getpid())

def main():

    global p
    p = multiprocessing.Pool(initializer=pool_init, processes=g_poolAvailableProcessCount)
    thread = Thread(target=execute, args=(p, 0))
    thread.start()
    dbcountThread = Thread(target=getDBCollectionsCount)
    dbcountThread.start()
    app.run("localhost", 5000)
    p.close()
    p.join()

if __name__ == '__main__':
   main()
   print "Exiting Main Thread"


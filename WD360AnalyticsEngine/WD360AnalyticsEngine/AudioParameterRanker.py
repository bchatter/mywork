import os

from pandas.io.json import json_normalize
from operator import itemgetter
from WD360AnalyticsEngine.WDUtils import *


def audioMoodRanking(query_meta_data_dict, audiCollection):
    results = []
    mood_score_rslts = []
    try:
        dfq = (json_normalize(query_meta_data_dict, ['features', 'timeline', 'mood', 'values'], [['features', 'timeline', 'mood', 'end']]))
        f = {'score': [lambda g : get_top_two_sentiment_score(g, dfq)]}
        queryAudioMoodDF = dfq.groupby(['features.timeline.mood.end']).agg(f).reset_index()
        queryAudioMood_RealScoreDF = queryAudioMoodDF[['score', 'features.timeline.mood.end']]
        for c in audiCollection:
            try:
                df = (json_normalize(c, ['features', 'timeline', 'mood', 'values'], ['filepath',
                                                                                    ['features', 'timeline', 'mood','end']]))
                f = {'score': [lambda g: get_top_two_sentiment_score(g, df)]}
                audioMoodDF = df.groupby(['features.timeline.mood.end', 'filepath']).agg(f).reset_index()
                audioMood_RealScoreDF = audioMoodDF[['score', 'features.timeline.mood.end']]
                mood_score_rslts.append(audioMoodScoreRanking(queryAudioMood_RealScoreDF, audioMood_RealScoreDF, os.path.basename(audioMoodDF['filepath'][0])))
            except Exception as inst:
                print "Exception audioMoodRanking inside loop : " + str(inst)
                continue
        x = sorted(mood_score_rslts, key=itemgetter(1, 2))
        results = x[0:5]
    except Exception as inst:
        print "Exception audioMoodRanking : " + str(inst)
    #print "Mood Ranking : " + str(results) + "\n"
    return results



def audioBpmRanking(query_meta_data_dict, audiCollection):
    results_bpm = []
    results_duration = []
    bpm_score_rslts = []
    duration_score_rslts = []
    try:
        dfq = (json_normalize(query_meta_data_dict, ['features', 'meta'], [['features', 'meta', 'bpm'], ['features', 'meta', 'duration']]))
        for c in audiCollection:
            try:
                df = (json_normalize(c, ['features', 'meta'], ['filepath', ['features', 'meta', 'bpm'], ['features', 'meta', 'duration']]))
                bpm_score_rslts.append(tuple([os.path.basename(df['filepath'][0]), (abs(df['features.meta.bpm'][0] - dfq['features.meta.bpm'][0]))]))
                duration_score_rslts.append(tuple([os.path.basename(df['filepath'][0]), (abs(df['features.meta.duration'][0] - dfq['features.meta.duration'][0]))]))
            except Exception as inst:
                print "Exception audioBpmRanking inside loop : " + str(inst)
                continue
        x = sorted(bpm_score_rslts, key=itemgetter(1))
        y = sorted(duration_score_rslts, key=itemgetter(1))
        results_bpm = x[0:5]
        results_duration = y[0:5]
        #print "BPM Ranking : " + str(results_bpm) + "\n"
        #print "Duration Ranking : " + str(results_duration) + "\n"
        return (results_bpm, results_duration)
    except Exception as inst:
        print "Exception audioBpmRanking : " + str(inst)
        return (results_bpm, results_duration)


def audioMetaInfoRanking(query_meta_data_dict, audiCollection):
    artist_results = []
    artist_temp_results = []
    composer_results = []
    composer_temp_results = []
    try:
        queryAudioMetaInfoDF = (json_normalize(query_meta_data_dict, ['composer'], ['artist', 'composer']))
        query_artist = clean_label(queryAudioMetaInfoDF.iloc[0]['artist'])
        query_composer = clean_label(queryAudioMetaInfoDF.iloc[0]['composer'])
        for c in audiCollection:
            try:
                audioMetaInfoDF = (json_normalize(c, ['composer'], ['artist', 'composer', 'filepath']))
                artist = clean_label(audioMetaInfoDF.iloc[0]['artist'])
                composer = clean_label(audioMetaInfoDF.iloc[0]['composer'])
                artist_val = fuzz.token_sort_ratio(query_artist, artist)
                composer_val = fuzz.token_sort_ratio(query_composer, composer)
                artist_temp_results.append([os.path.basename(audioMetaInfoDF['filepath'][0]), artist_val])
                composer_temp_results.append([os.path.basename(audioMetaInfoDF['filepath'][0]), composer_val])
            except Exception as inst:
                print "Exception audioBeatRanking inside loop : " + str(inst)
                continue
        x = sorted(artist_temp_results, key=itemgetter(1))
        y = sorted(composer_temp_results, key=itemgetter(1))
        artist_results = x[0:5]
        composer_results = y[0:5]
        #print "Artist Ranking : " + str(artist_results) + "\n"
        #print "Composer Ranking : " + str(composer_results) + "\n"
        return (artist_results, composer_results)
    except Exception as inst:
        print "Exception audioMetaInfoRanking : " + str(inst)
        return (artist_results, composer_results)

def audioBeatRanking(query_meta_data_dict, audiCollection):
    results = []
    temp_results = []
    try:
        queryAudioBeatDF = (json_normalize(query_meta_data_dict, ['features', 'timeline', 'beat']))
        yT = [tuple(x) for x in queryAudioBeatDF.to_records(index=False)]
        ySize = len(yT)
        for c in audiCollection:
            try:
                audioBeatDF = (json_normalize(c, ['features', 'timeline', 'beat'], ['filepath']))
                subsetDF = audioBeatDF[['time', 'strength']]
                xT = [tuple(x) for x in subsetDF.to_records(index=False)]
                xSize = len(xT)
                if ySize > xSize:
                    yT = yT[0:xSize]
                else:
                    xT = xT[0:ySize]
                shift, val = getAreaBetweenTwoGraphs(xT, yT)
                temp_results.append([os.path.basename(audioBeatDF['filepath'][0]), shift, val])
            except Exception as inst:
                print "Exception audioBeatRanking inside loop : " + str(inst)
                continue
        x = sorted(temp_results, key=itemgetter(1, 2))
        results = x[0:5]
    except Exception as inst:
        print "Exception audioBeatRanking : " + str(inst)
    #print "Beat Ranking : " + str(results) + "\n"
    return results



def audioSegmentRanking(query_meta_data_dict, audiCollection):
    results = []
    temp_results = []
    try:
        queryAudioSegmentDF = (json_normalize(query_meta_data_dict, ['features', 'timeline', 'segment']))
        f = {'start': [lambda g: get_total_valuation_per_label(g, queryAudioSegmentDF)],
                                'id' : [lambda g: getLabelIntValue(g, queryAudioSegmentDF)] }
        newQuerySegmentDF = queryAudioSegmentDF.groupby(['label']).agg(f).reset_index()
        verse_valuation_df = newQuerySegmentDF[['id', 'start']]
        yT = [tuple(x) for x in verse_valuation_df.to_records(index=False)]
        ySize = len(yT)
        for c in audiCollection:
            try:
                df = (json_normalize(c, ['features', 'timeline', 'segment'], ['filepath']))
                f = {'start': [lambda g: get_total_valuation_per_label(g, df)],
                     'id': [lambda g: getLabelIntValue(g, df)]}
                newdf = df.groupby(['label']).agg(f).reset_index()
                verse_val_df = newdf[['id', 'start']]
                xT = [tuple(x) for x in verse_val_df.to_records(index=False)]
                xSize = len(xT)
                if ySize > xSize:
                    yT = yT[0:xSize]
                else:
                    xT = xT[0:ySize]
                shift, val = getAreaBetweenTwoGraphs(xT, yT)
                temp_results.append([os.path.basename(df['filepath'][0]), shift, val])
            except Exception as inst:
                print "Exception audioSegmentRanking inside loop : " + str(inst)
                continue
        x = sorted(temp_results, key=itemgetter(1, 2))
        results = x[0:5]
    except Exception as inst:
        print "Exception audioSegmentRanking : " + str(inst)
    #print "Segment Ranking : " + str(results) + "\n"
    return results
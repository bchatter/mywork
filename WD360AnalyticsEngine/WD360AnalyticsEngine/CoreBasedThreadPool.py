import json
import mimetypes
import urllib
import urlparse

import cv2
import docx2txt
import requests
import watson_developer_cloud
import watson_developer_cloud.natural_language_understanding.features.v1 as \
    features
from PyPDF2 import PdfFileReader
from mutagen.easyid3 import EasyID3
from pptx import Presentation
from pymongo import MongoClient

from AudioParameterRanker import *
from WD360AnalyticsEngine.DocParameterRanker import *
from WD360AnalyticsEngine.pyimagesearch.searcher import Searcher
from pyimagesearch.colordescriptor import ColorDescriptor


def readPDFFile(file_url):

    PDF = PdfFileReader(file(file_url, 'rb'))
    pdfText = []
    if PDF.isEncrypted:
        '''Not Supported'''
        return ""
    for page in PDF.pages:
        pdfText.append(page.extractText())
    text = '\n'.join(chunk for chunk in pdfText if chunk)
    return text

def readPresentationFile(file_url):

    prs = Presentation(file_url)
    text_runs = []

    for slide in prs.slides:
        for shape in slide.shapes:
            if not shape.has_textframe:
                continue
            for paragraph in shape.textframe.paragraphs:
                for run in paragraph.runs:
                    text_runs.append(run.text)

    text = '\n'.join(chunk for chunk in text_runs if chunk)
    return text

def saveDocTextFileAnalyzedData(filepath, is_text, flag):

    new_text = ""
    if is_text == 0:
        mime = mimetypes.MimeTypes()
        if urlparse.urlparse(filepath).scheme.find('http') != -1:
            url = filepath
        else:
            url = urllib.pathname2url(filepath)

        mime_type = mime.guess_type(url)
        if (mime_type is not None and mime_type[0] is not None):
            print mime_type[0]
            if (mime_type[0].find("html") == -1 and
                mime_type[0].find("text") == -1 and
                mime_type[0].find("pdf") == -1 and
                mime_type[0].find("document") == -1 and
                mime_type[0].find("msword") == -1 and
                mime_type[0].find("presentation") == -1):
                return {}
            else:
                if mime_type[0].find("html") != -1:
                    '''
                    Extract readable text from the url here
                    '''
                    try:
                        html = urllib.urlopen(filepath).read()
                        from bs4 import BeautifulSoup
                        soup = BeautifulSoup(html)

                        for script in soup(["script", "style"]):
                            script.extract()

                        temp_text = soup.get_text()
                        lines = (line.strip() for line in temp_text.splitlines())
                        chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
                        text = '\n'.join(chunk for chunk in chunks if chunk)

                        pHTMLtext = (text.encode('utf-8'))
                        new_text = pHTMLtext.encode('ascii', 'ignore').decode('ascii')
                    except Exception as inst:
                        print "pHTMLtext.decode : " + str(inst)
                elif mime_type[0].find("text") != -1:
                    file = open(filepath, "r")
                    ptext = file.read()
                    try:
                        new_text = ptext.encode('ascii', 'ignore').decode('ascii')
                    except Exception as inst:
                        print "ptext.decode : " + str(inst)
                elif mime_type[0].find("pdf") != -1:
                    try:
                        new_text = readPDFFile(filepath).encode('ascii', 'ignore').decode('ascii')
                    except Exception as inst:
                        print "readPDFFile : " + str(inst)
                elif mime_type[0].find("msword") != -1:
                    try:
                        new_text = docx2txt.process(filepath).encode('ascii', 'ignore').decode('ascii')
                    except Exception as inst:
                        print "docx2txt : " + str(inst)
                elif mime_type[0].find("presentation") == -1:
                    try:
                        new_text = readPresentationFile(filepath).encode('ascii', 'ignore').decode('ascii')
                    except Exception as inst:
                        print "readPresentationFile : " + str(inst)
        else:
            '''check mime type first in unknown check externsion'''
            if(url.lower().endswith(('.pdf'))):
                new_text = readPDFFile(url).encode('ascii', 'ignore').decode('ascii')
            if (url.lower().endswith(('.docx'))):
                new_text = docx2txt.process(url).encode('ascii', 'ignore').decode('ascii')
            if(url.lower().endswith(('.pptx'))):
                new_text = readPresentationFile(url).encode('ascii', 'ignore').decode('ascii')
    else:
        new_text = filepath.encode('ascii', 'ignore').decode('ascii')

    response = {}

    try:
        natural_language_understanding = watson_developer_cloud.NaturalLanguageUnderstandingV1(
            version='2017-02-27',
            username='6a07b447-7248-4816-af41-a5d290ca5d17',
            password='MxLJUn38Evrx')

        response = natural_language_understanding.analyze(
            text=new_text,
            features=[features.Entities(), features.Keywords()])
    except Exception as inst:
        print "natural_language_understanding.analyze : " + str(inst)

    if flag == 1:
        return response
    else:
        text_new_meta_data_dict = {'filepath': filepath, 'response': response}
        client = MongoClient('localhost', 27017)
        db = client.WD360DB
        TextCollection = db.TextCollection
        aid = TextCollection.insert_one(text_new_meta_data_dict).inserted_id
        print "inserted id : " + str(aid) + "\n"
        client.close()

    return {}

def saveImageFileAnalyzedData(i, f):

    try:
        cd = ColorDescriptor((8, 12, 3))
        image = cv2.imread(i)
        features = cd.describe(image)
        features = [str(x) for x in features]
        pic_meta_data_dict = { 'features' : features, 'filepath' : i }

        if f == 1:
            return pic_meta_data_dict

        client = MongoClient('localhost', 27017)
        db = client.WD360DB
        ImageCollection = db.ImageCollection
        aid = ImageCollection.insert_one(pic_meta_data_dict).inserted_id

        print "[" + str(os.getpid()) + "] Successfully inserted : " + i + " image metadata into db with id : " + str(aid) + "\n"
        return 0
    except Exception as inst:
        print inst
        return 1

def saveAudioFileAnalyzedData(i, f):

    try:
        try:
            audio = EasyID3(i)

            title = audio['title']
            artist = audio['artist']
            album = audio['album']
            composer = audio['composer']
        except Exception as inst:
            title = "Unknown"
            artist = "Unknown"
            album = "Unknown"
            composer = "Unknown"
            print "Exception saveAudioFileAnalyzedData EasyID3 : " + str(inst)

        client = MongoClient('localhost', 27017)
        db = client.WD360DB
        AudioCollection = db.AudioCollection

        gracenote_post_url = 'http://devapi.gracenote.com/timeline/api/1.0/audio/extract/'
        gracenote_get_url = 'http://devapi.gracenote.com/timeline/api/1.0/audio/features/'

        print "post started"
        rpost = requests.post(gracenote_post_url, files={
            "audio_file": open(i, 'rb')
        })

        if (rpost.status_code == 200) or (rpost.status_code == 202):
            os.remove(i)
            print "post ended"
            rpostJson = json.loads(rpost.content)
            audio_id = rpostJson['audio_id']
            print "get started, audio id : " + str(audio_id)
            rget = requests.get(gracenote_get_url + audio_id)
            if (rget.status_code == 200) or (rget.status_code == 202):
                print "get ended"
                rgetJson = json.loads(rget.content)
                job_status = rgetJson['job_status']

                retry = 1
                while "0" == job_status and retry <= 4:
                    rget = requests.get(gracenote_get_url + audio_id)
                    rgetJson = json.loads(rget.content)
                    job_status = rgetJson['job_status']
                    retry = retry + 1

                if "0" == job_status and retry > 4:
                    audio_new_meta_data_dict = {'filepath': i, 'title': title, 'artist': artist, 'album': album,
                                                'composer': composer, 'features': {}}
                    if f == 1:
                        client.close()
                        return audio_new_meta_data_dict
                    else:
                        aid = AudioCollection.insert_one(audio_new_meta_data_dict).inserted_id
                        client.close()
                    return 0

                if "2" == job_status:
                    audio_new_meta_data_dict = {'filepath': i, 'title': title, 'artist': artist, 'album': album,
                                                'composer': composer, 'features': {}}
                    if f == 1:
                        client.close()
                        return audio_new_meta_data_dict
                    else:
                        aid = AudioCollection.insert_one(audio_new_meta_data_dict).inserted_id
                        client.close()
                    return 0

                if "1" == job_status:
                    print "Got Metadata"
                    audio_meta_data_dict = json.loads(rgetJson['features'])
                    audio_new_meta_data_dict = {'filepath': i, 'title': title, 'artist': artist, 'album': album,
                                                'composer': composer, 'features': audio_meta_data_dict}
                    if f == 1:
                        client.close()
                        return audio_new_meta_data_dict
                    else:
                        aid = AudioCollection.insert_one(audio_new_meta_data_dict).inserted_id
                        client.close()
                    return 0
            else:
                audio_new_meta_data_dict = {'filepath': i, 'title': title, 'artist': artist, 'album': album,
                                            'composer': composer, 'features': {}}
                if f == 1:
                    client.close()
                    return audio_new_meta_data_dict
                else:
                    aid = AudioCollection.insert_one(audio_new_meta_data_dict).inserted_id
                    client.close()
                    return 0
        else:
            os.remove(i)
            audio_new_meta_data_dict = {'filepath': i, 'title': title, 'artist': artist, 'album': album,
                                        'composer': composer, 'features': {}}
            if f == 1:
                client.close()
                return audio_new_meta_data_dict
            else:
                aid = AudioCollection.insert_one(audio_new_meta_data_dict).inserted_id
                client.close()
                return 0
    except Exception as inst:
        print inst
        return 1

def saveFileAnalyzedData(i, f):

    if i.find("audio") != -1:
        saveAudioFileAnalyzedData(i, f)
    elif i.find("doc") != -1:
        saveDocTextFileAnalyzedData(i, f)
    elif i.find("image") != -1:
        saveImageFileAnalyzedData(i, f)
    else:
        '''error'''

def contextualSearchEngineForImageQuery(query_meta_data_dict):

    try:
        searcher = Searcher()
        final_result_list = searcher.search(query_meta_data_dict)
        return final_result_list
    except Exception as inst:
        print "Exception contextualSearchEngineForImageQuery : " + str(inst)
        return []

def contextualSearchEngineForAudioQuery(query_meta_data_dict):

    try:
        client = MongoClient('localhost', 27017)
        db = client.WD360DB
        audioCollectionHdl = db.AudioCollection
        audiCollection = audioCollectionHdl.find({})

        meta_artist_results, meta_composer_results = audioMetaInfoRanking(query_meta_data_dict, audiCollection)
        audiCollection.rewind()
        beat_results = audioBeatRanking(query_meta_data_dict, audiCollection)
        audiCollection.rewind()
        mood_results = audioMoodRanking(query_meta_data_dict, audiCollection)
        audiCollection.rewind()
        bpm_results, duration_results = audioBpmRanking(query_meta_data_dict, audiCollection)
        audiCollection.rewind()
        segment_results = audioSegmentRanking(query_meta_data_dict, audiCollection)
        audiCollection.rewind()

        final_result_list = []
        [final_result_list.append(x[0]) for x in meta_artist_results]
        [final_result_list.append(x[0]) for x in meta_composer_results]
        [final_result_list.append(x[0]) for x in beat_results]
        [final_result_list.append(x[0]) for x in mood_results]
        [final_result_list.append(x[0]) for x in bpm_results]
        [final_result_list.append(x[0]) for x in duration_results]
        [final_result_list.append(x[0]) for x in segment_results]
        result = list(set(final_result_list))

        print result
        return result
    except Exception as inst:
        print "Exception contextualSearchEngineForAudioQuery : " + str(inst)
        return []

def contextualSearchEngineForDocQuery(query_meta_data_dict):

    try:
        client = MongoClient('localhost', 27017)
        db = client.WD360DB
        TextCollection = db.TextCollection
        textCollection = TextCollection.find({})

        r1 = docKeywordTextRanking(query_meta_data_dict, textCollection)
        textCollection.rewind()
        r2 = docEntitiesRanking(query_meta_data_dict, textCollection)
        textCollection.rewind()

        final_result_list = []
        [final_result_list.append(x[0]) for x in r1]
        [final_result_list.append(x[0]) for x in r2]
        result = list(set(final_result_list))

        return result
    except Exception as inst:
        print "Exception contextSearchEngineForDocQuery : " + str(inst)
        return []

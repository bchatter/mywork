import os

import time
from pymongo import MongoClient

from WD360AnalyticsEngine.CoreBasedThreadPool import saveFileAnalyzedData
from WD360AnalyticsEngine import *
from WD360AnalyticsEngine.WDUtils import pause


def distribute(nitems, nprocs=None):
    if nprocs is None:
        nprocs = g_poolAvailableProcessCount
    nitems_per_proc = (nitems+nprocs-1)/nprocs
    return [(i, min(nitems, i+nitems_per_proc))
            for i in range(0, nitems, nitems_per_proc)]

def assignTaskInPool(i, imin, imax, f):
    [saveFileAnalyzedData(inp, f) for inp in i[imin:imax]]

def extractMetaData(p, f):

    while g_uploadDoneEvent.wait():
        tasks = []
        dir_list = [(IMAGE_UPLOAD_FOLDER + "/" + file) for file in os.listdir(IMAGE_UPLOAD_FOLDER)]
        dir_list.append([(DOC_UPLOAD_FOLDER + "/" + file) for file in os.listdir(DOC_UPLOAD_FOLDER)] )
        dir_list.append([(AUDIO_UPLOAD_FOLDER + "/" + file) for file in os.listdir(AUDIO_UPLOAD_FOLDER)] )

        for imin, imax in distribute(len(dir_list)):
            tasks.append(p.apply_async(assignTaskInPool, (dir_list, imin, imax, f, ), ))
        for t in tasks:
            t.wait()

def execute(p, f):
    extractMetaData(p, f)

def getDBCollectionsCount():

    while(True):
        client = MongoClient('localhost', 27017)
        db = client.WD360DB
        texts = db.TextCollection.count()
        images = db.ImageCollection.count()
        audios = db.AudioCollection.count()

        if texts > 20 and images > 70 and audios > 50:
            g_databasefilledEvent.set()
            client.close()
            break
        else:
            time.sleep(60)

import threading
import multiprocessing
import sharedmem
import numpy

global AUDIO_UPLOAD_FOLDER
global DOC_UPLOAD_FOLDER
global IMAGE_UPLOAD_FOLDER

AUDIO_UPLOAD_FOLDER = "uploads/audio"
DOC_UPLOAD_FOLDER = "uploads/doc"
IMAGE_UPLOAD_FOLDER = "uploads/image"

global AUDIO_METADATA
global IMAGE_METADATA
global DOC_METADATA
global AUDIO_CONTEXTUAL_SEARCH

AUDIO_METADATA = 1
IMAGE_METADATA = 2
DOC_METADATA = 3
AUDIO_CONTEXTUAL_SEARCH = 4

global searchResult
searchResult = []

global g_AssignTaskLock
g_AssignTaskLock = threading.Lock()

global g_audioSearchDone
g_audioSearchDone = threading.Event()
g_audioSearchDone.clear()

global g_docSearchDone
g_docSearchDone = threading.Event()
g_docSearchDone.clear()

global g_imageSearchDone
g_imageSearchDone = threading.Event()
g_imageSearchDone.clear()

global g_uploadDoneEvent
g_uploadDoneEvent = threading.Event()
g_uploadDoneEvent.clear()

global g_databasefilledEvent
g_databasefilledEvent = threading.Event()
g_databasefilledEvent.clear()

global g_poolAvailableProcessCount
g_poolAvailableProcessCount = multiprocessing.cpu_count()

import RestServer
from WD360AnalyticsEngine.MetaDataExtractor import execute









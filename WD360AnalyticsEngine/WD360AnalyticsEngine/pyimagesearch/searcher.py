# import the necessary packages
import numpy as np
from pymongo import MongoClient
from operator import itemgetter

class Searcher:
	def __init__(self):
		pass

	def search(self, queryFeatures, limit = 10):
		try:
			results = {}
			client = MongoClient('localhost', 27017)
			db = client.WD360DB
			ImageCollection = db.ImageCollection
			imageCollection = ImageCollection.find({})

			for c in imageCollection:
				try:
					features = [float(x) for x in c['features']]
					queryFeatures = [float(x) for x in queryFeatures]
					d = self.chi2_distance(features, queryFeatures)
					results[c['filepath']] = d
				except Exception as inst:
					continue

			new_results = sorted(results.items(), key=itemgetter(1))
		except Exception as inst:
			print "Exception Image Searcher search() : " + str(inst)

		return new_results[:limit]

	def chi2_distance(self, histA, histB, eps = 1e-10):
		d = 0.5 * np.sum([((a - b) ** 2) / (a + b + eps)
			for (a, b) in zip(histA, histB)])
		return d
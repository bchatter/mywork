import hashlib
import random
import pandas as pd
from Document import Document
from TextRepresentor import TextRepresentor, WORD2VEC_SIZE

class DocumentVault:

    def __init__(self):
        self.TR = TextRepresentor()

        document_items = []
        document_hash = {}
        self.document_count = 0

        self.df = pd.read_csv('sample_dataset.csv',
                         names=['Sentiment', 'Index', 'DateTime', 'Topic', 'Username', 'Text'])

        #self.df = pd.read_csv('black_white_dataset.csv',
        #                 names=['Sentiment', 'Index', 'DateTime', 'Topic', 'Username', 'Text'])

        self.classifications = set([int(x) for x in self.df['Sentiment'].tolist()])
        self.classifications = sorted(self.classifications)

        self.input_dim = WORD2VEC_SIZE
        self.output_dim = len(self.classifications)

        for index, row in self.df.iterrows():
            hash_value = index
            if hash_value not in document_hash:
                document_hash[hash_value] = len(document_items)
                document_items.append(Document(self.TR, row['Text'], int(row['Sentiment']), self.classifications))
                self.document_count = self.document_count + 1

        self.document_items = document_items  # List of document_items objects
        self.document_hash = document_hash  # Mapping from each hashvalue to its index in Document Vault

        random.shuffle(self.document_items)
        training_documents_len = int(0.8 * len(self.document_items))
        self.training_documents = self.document_items[ : training_documents_len]
        self.testing_documents = self.document_items[training_documents_len : ]

        print('Total documents for training: ' +  str(len(self.training_documents)))
        print('Total documents for testing: ' +  str(len(self.testing_documents)))
        print('Document Vault size: ' +  str(len(self.document_items)))


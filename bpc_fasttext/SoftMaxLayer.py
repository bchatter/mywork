from ActivationLayer import ActivationLayer
from LinearLayer import LinearLayer
import numpy as np
from scipy import special
from sklearn import metrics

class SoftMaxLayer(ActivationLayer):
    def __init__(self, n_in, n_node, W=None, b=None):
        super(SoftMaxLayer, self).__init__(n_in, n_node, W=None, b=None, act="SOFTMAX")
        self.predict = None
        self.truth = None
        return

    def forward(self, x):
        # hidden part first, computing softmax
        self.y = super(SoftMaxLayer, self).forward(x)
        pos = self.y.argmax()
        self.predict = np.array([0 for x in range(self.n_node)]).reshape(self.n_node, 1)
        self.predict[pos] = 1
        return self.predict

    def backward(self, in_truth):
        # minimizing the negative loglikelihood over the classes
        #
        # This is the negative log - likelihood of the true labels
        # given a probabilistic classifier’s predictions. It is defined as:
        #
        # -logP(yt | yp) = -(yt log(yp) + (1 - yt) log(1 - yp))
        # where yt = true label probabilities, denoted as self.truth here, and
        #       yp = probabilistic classifiers predictions, denoted as self.y here

        self.truth = np.array(in_truth).reshape(self.n_node, 1)
        unity = np.ones(shape=(self.n_node, 1))

        # Since log(0) gives divide by zero error, hence clipping
        # diff logs with a lower number like 1e-15

        eps = 1e-15
        diff_y = unity - self.y
        diff_y[diff_y == 0] = eps
        diff_truth = unity - self.truth
        diff_truth[diff_truth == 0] = eps
        node_grad = -special.xlogy(self.y, self.truth) - special.xlogy(diff_y, diff_truth)

        # backpropagating the error gradient to the linear layer now

        self.out_grad = LinearLayer.backward(self, node_grad)
        return self.out_grad

    def update(self, learning_rate):
        super(SoftMaxLayer, self).update(learning_rate)
        return

    def __str__(self):
        s = super(SoftMaxLayer, self).__str__()
        s += "\nPrediction:\n" + str(self.predict)
        s += "\nTruth:\n" + str(self.truth)
        return s
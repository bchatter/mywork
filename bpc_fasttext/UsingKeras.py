import numpy
import time
from keras import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import KFold, cross_val_score

from DocumentVault import DocumentVault
from TextRepresentor import WORD2VEC_SIZE

model = Sequential()
model.add(Dense(WORD2VEC_SIZE + 1, input_dim=WORD2VEC_SIZE, activation='linear'))
model.add(Dense(3, activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='sgd', metrics=['accuracy'])

def baseline_model():
    return model

def learnAndEvaluate(dV):
    seed = 7
    numpy.random.seed(seed)

    X = []
    Y = []

    for d in dV.document_items:
        i = d.text_representation
        X.append(i)
        o = d.raw_label
        Y.append(o)

    XX = numpy.array(X)
    YY = numpy.array(Y)

    try:
        estimator = KerasClassifier(build_fn=baseline_model,
                                    epochs=1,
                                    batch_size=32,
                                    verbose=0)
        kfold = KFold(n_splits=5,
                      shuffle=True,
                      random_state=seed)
        results = cross_val_score(estimator,
                                  XX,
                                  YY,
                                  cv=kfold)
        print("Baseline: %.2f%% (%.2f%%)" % (results.mean()*100, results.std()*100))
    except Exception as inst:
        print(inst)

if __name__ == "__main__":
    docVault = DocumentVault()
    start = time.time()
    learnAndEvaluate(docVault)
    print("Total Time Taken : " + str(time.time() - start))


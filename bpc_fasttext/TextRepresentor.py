import os
import pandas as pd
from gensim.models import Word2Vec

WORD2VEC_SIZE = 100

class TextRepresentor:

    def __init__(self):
        #if not os.path.exists('word2vec_model.bin'):
        if 1:
            if os.path.exists('word2vec_model.bin'):
                os.remove('word2vec_model.bin')
            sentence_corpus = []
            self.df = pd.read_csv('sample_dataset.csv',
                             names=['Sentiment', 'Index', 'DateTime', 'Topic', 'Username', 'Text'])
            #self.df = pd.read_csv('sample_dataset.csv',
            #                 names=['Sentiment', 'Index', 'DateTime', 'Topic', 'Username', 'Text'])
            for index, row in self.df.iterrows():
                text_split = str(row['Text']).split()
                sentence_corpus.append(text_split)

            self.model = Word2Vec(sentence_corpus, size=WORD2VEC_SIZE)
            self.model.wv.save_word2vec_format('word2vec_model.bin')
        else:
            self.model = Word2Vec.load('word2vec_model.bin')

    def getTextRepresentation(self, text):
        words = text.split()
        word_vector_sum = [0 for i in range(100)]
        num_of_words = 0
        pos = 0
        for w in words:
            try:
                word_vector_sum = self.model[w]
                pos = pos + 1
                num_of_words = num_of_words + 1
                break
            except:
                pos = pos + 1
                continue
        if num_of_words == 0:
            return word_vector_sum
        for w in words[pos:]:
            try:
                word_vector_sum = word_vector_sum + self.model[w]
                num_of_words = num_of_words + 1
            except:
                continue
        text_representation = word_vector_sum / num_of_words
        return text_representation


from Activation import Activation
from LinearLayer import LinearLayer

class ActivationLayer(LinearLayer):
    def __init__(self, n_in, n_node, W=None, b=None, act="LINEAR"):
        super(ActivationLayer, self).__init__(n_in, n_node, W=None, b=None)
        self.activation = Activation(act)
        return

    def forward(self, x):
        if x.size != self.n_in:
            raise ValueError("Incorrect data input", x.size, self.n_in)
        # linear part first
        signal = super(ActivationLayer, self).forward(x)
        # nonlinear
        self.y = self.activation.func(signal)
        return self.y

    def backward(self, in_grad):
        if in_grad.size != self.n_node:
            raise ValueError("Incorrect data input")
        # nonlinear
        self.in_grad = in_grad
        node_grad = self.activation.grad(self.y)
        node_grad = node_grad * in_grad
        # linear
        self.out_grad = super(ActivationLayer, self).backward(node_grad)
        return self.out_grad

    def update(self, learning_rate):
        super(ActivationLayer, self).update(learning_rate)
        return

    def __str__(self):
        s = super(ActivationLayer, self).__str__()
        s += str(self.activation)
        return s
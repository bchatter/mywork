import numpy as np

class HierarchicalHuffmanEncoder:

    def __init__(self):
        pass

    def encode_huffman(self, documentVault):
        # Build a Huffman tree
        document_vault_size = len(documentVault.document_items)
        factor = [t.raw_label for t in documentVault.document_items]
        count = factor  + [1e15] * (document_vault_size - 1)
        parent = [0] * (2 * document_vault_size - 2)
        binary = [0] * (2 * document_vault_size - 2)

        pos1 = document_vault_size - 1
        pos2 = document_vault_size

        for i in xrange(document_vault_size - 1):
            # Find min1
            if pos1 >= 0:
                if count[pos1] < count[pos2]:
                    min1 = pos1
                    pos1 -= 1
                else:
                    min1 = pos2
                    pos2 += 1
            else:
                min1 = pos2
                pos2 += 1

            # Find min2
            if pos1 >= 0:
                if count[pos1] < count[pos2]:
                    min2 = pos1
                    pos1 -= 1
                else:
                    min2 = pos2
                    pos2 += 1
            else:
                min2 = pos2
                pos2 += 1

            count[document_vault_size + i] = count[min1] + count[min2]
            parent[min1] = document_vault_size + i
            parent[min2] = document_vault_size + i
            binary[min2] = 1

        # Assign binary code and path pointers to each vocab word
        root_idx = 2 * document_vault_size - 2
        for i, token in enumerate(documentVault.document_items):
            path = []  # List of indices from the leaf to the root
            code = []  # Binary Huffman encoding from the leaf to the root

            node_idx = i
            while node_idx < root_idx:
                if node_idx >= document_vault_size: path.append(node_idx)
                code.append(binary[node_idx])
                node_idx = parent[node_idx]
            path.append(root_idx)

            # These are path and code from the root to the leaf
            token.path = [j - document_vault_size for j in path[::-1]]
            token.code = code[::-1]

import time
import numpy as np
from ActivationLayer import ActivationLayer
from DocumentVault import DocumentVault
from SoftMaxLayer import SoftMaxLayer

class FastTextModel:

    def __init__(self, learning_rate=0.1):
        self.docVault = DocumentVault()
        self.train_input = self.docVault.training_documents
        self.test_input = self.docVault.testing_documents
        self.n_in = self.docVault.input_dim
        self.n_node = self.n_in + 1
        self.n_class = self.docVault.output_dim
        self.hidden_layer1 = ActivationLayer(self.n_in, self.n_node)
        self.output_layer = SoftMaxLayer(self.n_node, self.n_class)
        self.learning_rate = learning_rate
        return

    def forward(self, x):
        y = self.hidden_layer1.forward(x)
        self.output_layer.forward(y)
        return

    def backward(self, label):
        out_grad = self.output_layer.backward(label)
        self.hidden_layer1.backward(out_grad)
        return

    def update(self):
        self.hidden_layer1.update(self.learning_rate)
        self.output_layer.update(self.learning_rate)
        return

    def train_1sample(self, x, label):
        self.forward(x)
        self.backward(label)
        self.update()
        return

    def train(self):
        for doc in self.train_input:
            X = np.array(doc.text_representation).reshape(self.n_in, 1)
            y = doc.label
            self.train_1sample(X, y)
        return

    def predict_1sample(self, x):
        y = self.hidden_layer1.forward(x)
        predict = self.output_layer.forward(y)
        return predict

    def test(self):
        correct = 0
        total = 0
        for doc in self.test_input:
            X = np.array(doc.text_representation).reshape(self.n_in, 1)
            y = np.array(doc.label).reshape(len(doc.label), 1)
            predict = self.predict_1sample(X)
            if np.array_equal(y, predict):
                correct += 1
            total += 1
        accuracy = correct * 100 / total
        return accuracy

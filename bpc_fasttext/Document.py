class Document:
    def __init__(self, TR, text, lbl, classifications):
        self.TR = TR
        self.text = text
        self.raw_label = lbl
        label_vector = []
        for c in classifications:
            if c == lbl:
                label_vector.append(1)
            else:
                label_vector.append(0)
        self.label = label_vector
        self.text_representation = self.__getTextReprentation(text)
        self.path = None  # Path (list of indices) from the root to the document (leaf)
        self.code = None  # Huffman encoding

    def __getTextReprentation(self, s):
        return self.TR.getTextRepresentation(s)

import time
from FastTextModel import FastTextModel

if __name__ == "__main__":
    fast_text = FastTextModel()
    start = time.time()
    fast_text.train()
    end = time.time()
    print("Total time taken : " + str(end - start))
    accuracy = fast_text.test()
    print("Accuracy : " + str(accuracy))

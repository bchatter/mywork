import numpy as np


class Activation:
    def relu(self, x):
        return np.maximum(x, 0)

    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def tanh(self, x):
        return np.tanh(x)

    def linear(self, x):
        return x

    def softmax(self, x):
        exps = np.exp(x - x.max())
        return exps / np.sum(exps)

    def g_relu(self, x):
        return 1 * (x > 0)

    def g_sigmoid(self, x):
        return (1 - x) * x

    def g_tanh(self, x):
        return 1 - x * x

    def g_linear(self, x):
        return 1 * (x == x)

    def g_softmax(self, x):
        dx_ds = np.diag(x) - np.dot(x, x.T)
        return dx_ds.sum(axis=0).reshape(-1, 1)

    def __init__(self, act):
        funcs = {
            "TANH": self.tanh,
            "SIGMOID": self.sigmoid,
            "RELU": self.relu,
            "LINEAR": self.linear,
            "SOFTMAX": self.softmax
        }

        grads = {
            "TANH": self.g_tanh,
            "SIGMOID": self.g_sigmoid,
            "RELU": self.g_relu,
            "LINEAR": self.g_linear,
            "SOFTMAX": self.g_softmax
        }
        self.act = act
        self.func = funcs[act]
        self.grad = grads[act]

        return

    def __str__(self):
        s = "\nActivation:" + self.act
        return s
import random
import numpy as np

class LinearLayer(object):
    def init_weights(self, num):
        w = []
        for i in range(num):
            w.append(random.uniform(-1, 1))
        return w

    def __init__(self, n_in, n_node, W=None, b=None):
        self.n_node = n_node
        self.n_in = n_in

        size = n_in * n_node
        if W == None:
            weights = self.init_weights(size)
            weights = np.array(weights).reshape(n_node, n_in)
            scale = np.sqrt(2. / size)
            self.W = weights * scale
        else:
            nu, ni = W.shape
            if nu != n_node or ni != n_in:
                raise ValueError("Incorrect weights input")
            else:
                self.W = W

        self.b = b if b != None else np.zeros(shape=(n_node, 1))

        # self.W = np.ones(shape=(n_node, n_in))
        # self.b = np.ones(shape=(n_node, 1))

        self.x = None
        self.signal = None
        self.y = None

        self.in_grad = None
        self.G = None
        self.g_b = None
        self.out_grad = None
        return

    def forward(self, x):
        self.x = x
        self.y = np.dot(self.W, x) + self.b
        self.signal = self.y
        return self.y

    def backward(self, node_grad):
        self.G = np.outer(node_grad, self.x)
        self.g_b = node_grad
        self.out_grad = np.dot(self.W.T, node_grad)
        return self.out_grad

    def update(self, learning_rate):
        self.W = self.W - self.G * learning_rate
        self.b = self.b - self.g_b * learning_rate
        return

    def __str__(self):
        s = "\nx is:\n" + str(self.x)
        s += "\nW is:\n" + str(self.W)
        s += "\nb is:\n" + str(self.b)
        s += "\ny is:\n" + str(self.y)
        s += "\nin_grad is:\n" + str(self.in_grad)
        s += "\nG is:\n" + str(self.G)
        s += "\ng_b is:\n" + str(self.g_b)
        s += "\nout_grad is:\n" + str(self.out_grad)
        return s
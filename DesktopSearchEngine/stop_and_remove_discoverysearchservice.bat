@echo off
set sd=%1

echo "Stopping Discovery Search Service"
CALL "%sd%\\Elastic-MasterEligibleNode\\bin\\elasticsearch-service.bat" stop DiscoverySearchService

echo "Removing Discovery Search Service"
CALL "%sd%\\Elastic-MasterEligibleNode\\bin\\elasticsearch-service.bat" remove DiscoverySearchService

CALL rmdir "%sd%\\DiscoverySearch"
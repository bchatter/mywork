//===================================================================================================================================================
//
//
//  Author : Biswapratap Chatterjee
//  Company: Western Digital
//  Product: Discovery Search Engine
//
//
//===================================================================================================================================================

var diskinfo = null;
var winattr = null;
const spawn = require('child_process').spawn;
if (process.platform === "win32") {
    diskinfo = require('diskinfo');
    winattr = require('winattr');
}    
const drivelist = require('drivelist');
const fsUtils = require("nodejs-fs-utils");
const firstBy = require('thenby');
const username = require('username');
const fs = require('fs');

//===================================================================================================================================================

watch_dir_list = "";
user_home_dir = "";
user_my_document_dir = "";
task_start_count = 0;
task_finish_count = 0;
volumes = [];

//===================================================================================================================================================

function setOS() {
	OS = process.platform;
    console.log("OS : " + OS);
}

function setCurrentUser() {
    UN = username.sync();
    console.log("UN : " + UN);
}

//===================================================================================================================================================

function getNumericalTime(t) {
    var T = String(t);
    var end =  T.indexOf("GMT") - 1;
    T = T.substr(0, end);
    var splits = T.split(' ');
    var date = parseInt(splits[2]);
    var month = 0;
    var monthstr = splits[1];

    if (monthstr.toLowerCase() === "jan") {
        month = 1;
    } else if (monthstr.toLowerCase() === "feb") {
        month = 2;
    } else if (monthstr.toLowerCase() === "mar") {
        month = 3;
    } else if (monthstr.toLowerCase() === "apr") {
        month = 4;
    } else if (monthstr.toLowerCase() === "may") {
        month = 5;
    } else if (monthstr.toLowerCase() === "jun") {
        month = 6;
    } else if (monthstr.toLowerCase() === "jul") {
        month = 7;
    } else if (monthstr.toLowerCase() === "aug") {
        month = 8;
    } else if (monthstr.toLowerCase() === "sep") {
        month = 9;
    } else if (monthstr.toLowerCase() === "oct") {
        month = 10;
    } else if (monthstr.toLowerCase() === "nov") {
        month = 11;
    } else if (monthstr.toLowerCase() === "dec") {
        month = 12;
    } else {
        month = 999;
    }

    var year = parseInt(splits[3]);
    var time = splits[4]
    var time_splits = time.split(':');
    var hour = parseInt(time_splits[0]);
    var min = parseInt(time_splits[1]);
    var sec = parseInt(time_splits[2]);

    var numericalTime = parseInt("" + year + month + date + hour + min + sec);

    return numericalTime;
}

//===================================================================================================================================================

function get_Level_1_DirectoryList(d) {
	var level_depth = 0;
	var level_1_dirs = [];
    fsUtils.walkSync(d, {
        skipErrors  : true,
        logErrors   : false
    }, function (err, path, stats, next, cache) {
        if (!err) {
            if (stats.isDirectory() && level_depth > 0) {
                level_1_dirs.push({path: path, stats: stats});
            } else {
                // Do not consider files here
            }
        } else {
            // Ignore Error
        }
        level_depth++;
        if (level_depth < 2){
            next();
        }
    });

    return level_1_dirs;
}

//===================================================================================================================================================

function setDirectoryInfo_Mac(p, l) {
    //console.log(JSON.stringify(p.stats, null, 2));

    directory_weight = 0;

    var dir_name = p.path.split('\\').pop().split('/').pop();             
    
    if (dir_name.startsWith(".")) {
        directory_weight = directory_weight + 9;
    } else if (dir_name.match(/^\d/)) {
        directory_weight = directory_weight + 8;
    } else if (dir_name.startsWith("_")) {
        directory_weight = directory_weight + 7;
    } else {
        directory_weight = directory_weight + 6;
    }

    //main.Log(p.path + " : " + directory_weight);

    var aTime = parseInt(p.stats.atimeMs);
    var mTime = parseInt(p.stats.mtimeMs);
    var cTime = parseInt(p.stats.ctimeMs);

    l.push({
        directoryName: dir_name, 
        directoryPath: p.path, 
        lastAccessTime: aTime, 
        lastModificationTime: mTime, 
        creationTime: cTime, 
        directortyWeight: directory_weight
    });
}

function setDirectoryInfo_Win32(p, l) {
    var attrs = winattr.getSync(p.path);

    directory_weight = 0;

    if (String(attrs.hidden) === "true") {
        directory_weight = directory_weight + 10;
    }

    if (String(attrs.system) === "true") {
        directory_weight = directory_weight + 15;
    }

    var dir_name = p.path.split('\\').pop().split('/').pop();             

    if (dir_name.startsWith(".")) {
        directory_weight = directory_weight + 9;
    } else if (dir_name.match(/^\d/)) {
        directory_weight = directory_weight + 8;
    } else if (dir_name.startsWith("_")) {
        directory_weight = directory_weight + 7;
    } else {
    	directory_weight = directory_weight + 6;
    }

    //main.Log(p.path + " : " + directory_weight);

    var aTime = getNumericalTime(p.stats.atime);
    var mTime = getNumericalTime(p.stats.mtime);
    var cTime = getNumericalTime(p.stats.ctime);

    l.push({
        directoryName: dir_name, 
        directoryPath: p.path, 
        lastAccessTime: aTime, 
        lastModificationTime: mTime, 
        creationTime: cTime, 
        directortyWeight: directory_weight
    });
}

//===================================================================================================================================================

function createLevelOneDIrectoryList(d, ignore) {
	var level_1_weighted_list = [];
    var level_1_dirs = get_Level_1_DirectoryList(d);

    for (var i = 0; i < level_1_dirs.length; i++) {
    	var dir_name = level_1_dirs[i].path.split('\\').pop().split('/').pop();

        if (dir_name.toLowerCase() === ignore) {
            continue;
        }

    	if (dir_name.toLowerCase().indexOf("doc") != -1) {
    		level_1_weighted_list.push({
		        directoryName: dir_name, 
		        directoryPath: level_1_dirs[i].path, 
		        lastAccessTime: 0, 
		        lastModificationTime: 99999999999999, 
		        creationTime: 0, 
		        directortyWeight: 5
		    })
    	} else if ((dir_name.toLowerCase().indexOf("music")) != -1 || 
    		       (dir_name.toLowerCase().indexOf("audio")) != -1 || 
    		       (dir_name.toLowerCase().indexOf("song") != -1)) {
    		level_1_weighted_list.push({
		        directoryName: dir_name, 
		        directoryPath: level_1_dirs[i].path, 
		        lastAccessTime: 0, 
		        lastModificationTime: 99999999999999, 
		        creationTime: 0, 
		        directortyWeight: 2
		    })
    	} else if ((dir_name.toLowerCase().indexOf("pic")) != -1 || 
    		       (dir_name.toLowerCase().indexOf("image")) != -1 || 
    		       (dir_name.toLowerCase().indexOf("img") != -1) || 
    		       (dir_name.toLowerCase().indexOf("photo") != -1)) {
    		level_1_weighted_list.push({
		        directoryName: dir_name, 
		        directoryPath: level_1_dirs[i].path, 
		        lastAccessTime: 0, 
		        lastModificationTime: 99999999999999, 
		        creationTime: 0, 
		        directortyWeight: 1
		    })
    	} else if ((dir_name.toLowerCase().indexOf("video")) != -1 || 
    		       (dir_name.toLowerCase().indexOf("media")) != -1 || 
    		       (dir_name.toLowerCase().indexOf("movie") != -1)) {
    		level_1_weighted_list.push({
		        directoryName: dir_name, 
		        directoryPath: level_1_dirs[i].path, 
		        lastAccessTime: 0, 
		        lastModificationTime: 99999999999999, 
		        creationTime: 0, 
		        directortyWeight: 3
		    })
    	} else if (dir_name.toLowerCase().indexOf("desktop") != -1) {
    		level_1_weighted_list.push({
		        directoryName: dir_name, 
		        directoryPath: level_1_dirs[i].path, 
		        lastAccessTime: 0, 
		        lastModificationTime: 99999999999999, 
		        creationTime: 0, 
		        directortyWeight: 4
		    })
    	} else if ((dir_name.toLowerCase() === ".git") || (dir_name.toLowerCase() === ".svn")) {
    		// Ignore
    	} else {
    		if (OS === "win32") { // For Windows
            	setDirectoryInfo_Win32(level_1_dirs[i], level_1_weighted_list);
        	} else { // For Mac
            	setDirectoryInfo_Mac(level_1_dirs[i], level_1_weighted_list);
        	}
    	}
    }

    level_1_weighted_list.sort(
		firstBy("directortyWeight", 1)
		.thenBy("lastModificationTime", -1)
	);

	var level_1_weighted_str = "";

	for (var i = 0; i < level_1_weighted_list.length; i++) {
        if (i == 0) {
            level_1_weighted_str = level_1_weighted_list[i].directoryPath;
        } else {
            level_1_weighted_str = level_1_weighted_str + ";" + level_1_weighted_list[i].directoryPath;
        }
	}

	return level_1_weighted_str;
}

//===================================================================================================================================================

function INDEX_MANAGER_STD_OUT(data) {
    console.log(`${data}`);
}

function INDEX_MANAGER_STD_ERR(data) {
    console.log(`${data}`);
}

function INDEX_MANAGER_ON_CLOSE(code) {
    console.log("Index Manager Stopped");
}

//===================================================================================================================================================

function indexManagementTaskCreator(d1, d2) {
    try {
        console.log("Spawning ==> [Index Manager Process]");
        var i = spawn('node', ['fileSystemObserver.js', d1, d2]);
        i.stdout.on('data', INDEX_MANAGER_STD_OUT);
        i.stderr.on('data', INDEX_MANAGER_STD_ERR);
        i.on('close', INDEX_MANAGER_ON_CLOSE);
    } catch (err) {
        console.log("Failure inside Index Manager Process : " + err);
    }
}

//===================================================================================================================================================

function INDEXER_STD_OUT(data) {
	console.log(`${data}`);
}

function INDEXER_STD_ERR(data) {
	console.log(`${data}`);
}

function INDEXER_ON_CLOSE(code) {
	console.log("An Indexing Task Completed!");
    task_finish_count++;
    if (task_finish_count == task_start_count) {
        console.log("All Indexing Tasks Completed. Calling Index Manager *********>>>>>");
        indexManagementTaskCreator(user_home_dir, user_my_document_dir);
    }
}

//===================================================================================================================================================

function createIndexingTasks() {
    for (var i = 0; i < volumes.length; i++) {
        if (volumes[i].dtype == 1) { // Sytem Volume
            var user_home_dir = "";
            var user_my_document_dir = "";

            if (volumes[i].mountName === "/") {
                user_home_dir = "/Users/" + UN;
                user_my_document_dir = "/Users/" + UN + "/Documents";
            } else {
                user_home_dir = volumes[i].mountName + "/Users/" + UN;
                user_my_document_dir = volumes[i].mountName + "/Users/" + UN + "/Documents";
            }

            try {
                var stats = fs.statSync(user_home_dir);
                var user_home_dir_level_1_str = createLevelOneDIrectoryList(user_home_dir, "documents");
                watch_dir_list = watch_dir_list + ";" + user_home_dir_level_1_str;
                console.log("Spawning for ===> User Home Directory");
                var i1 = spawn('node', ['wdCluster.js', user_home_dir_level_1_str, "home"]);
                i1.stdout.on('data', INDEXER_STD_OUT);
                i1.stderr.on('data', INDEXER_STD_ERR);
                i1.on('close', INDEXER_ON_CLOSE);
                task_start_count++;
            } catch(err) {
                //console.log("HOME EXCEPTION : " + err);
            }

            try {
                var stats = fs.statSync(user_home_dir);
                var user_my_document_dir_level_1_str = createLevelOneDIrectoryList(user_my_document_dir, "");
                watch_dir_list = watch_dir_list + ";" + user_my_document_dir_level_1_str;
                console.log("Spawning for ===> User My Documents Directory");
                var i2 = spawn('node', ['wdCluster.js', user_my_document_dir_level_1_str, "documents"]);
                i2.stdout.on('data', INDEXER_STD_OUT);
                i2.stderr.on('data', INDEXER_STD_ERR);
                i2.on('close', INDEXER_ON_CLOSE);
                task_start_count++
            } catch(err) {
                //console.log("MY DOCUMENTS EXCEPTION : " + err);
            }      
        } else { // Removable Volumes

        }
    }

    //indexManagementTaskCreator(watch_dir_list);
}

//===================================================================================================================================================

function getDrivesInfo_Mac(error, disks) {
    if (!error) {
        disks.forEach(function(entry) {
            var _dtype = 0;
            //console.log(JSON.stringify(entry, null, 2));
            var _dataSize = parseInt(entry.size);
            if (entry.system == true) {
                _dtype = 1;
            } else {
                _dtype = 2;
            }
            var mps = entry.mountpoints;
            for (var i = 0; i < mps.length; i++) {
                console.log("Mount Name : " + mps[i].path);
                volumes.push({
                    mountName: mps[i].path, 
                    dataSize: _dataSize, 
                    dtype: _dtype
                });
            }
        });

        createIndexingTasks();
    }
}

function getVolumeInfo_Mac() {
    drivelist.list(getDrivesInfo_Mac);
}

function getVolumeInfo_Win32() {
    diskinfo.getDrives(function(error, drives) {
    	if (!error) {
	        for (var i = 0; i < drives.length; i++) {
	            var _dtype = 0;
	            //main.Log(JSON.stringify(drives[i], null, 2));
	            if (drives[i].filesystem === "Local Fixed Disk") {
	                _dtype = 1;
	            } else {
	                _dtype = 2;
	            }
	            var _dataSize = parseInt(drives[i].used);
	            if (drives[i].blocks > 524288000) {
	                volumes.push({
	                    mountName: drives[i].mounted, 
	                    dataSize: _dataSize, 
	                    dtype: _dtype
	                });
	            }
	        }

	        createIndexingTasks();
    	}
    });
}

//===================================================================================================================================================

function indexingTaskCreator() {
    if (OS === "win32") {
        getVolumeInfo_Win32();
    } else {
        getVolumeInfo_Mac();
    }    
}

function taskCreator() {
    setOS();
    setCurrentUser();
    indexingTaskCreator();
}

//===================================================================================================================================================

console.log("[Task Creator] ==>");
taskCreator();

//===================================================================================================================================================

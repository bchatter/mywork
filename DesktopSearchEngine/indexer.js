//===================================================================================================================================================
//
//
//  Author : Biswapratap Chatterjee
//  Company: Western Digital
//  Product: Discovery Search Engine
//
//
//===================================================================================================================================================

const fsUtils = require("nodejs-fs-utils");
const spawn = require('child_process').spawn;
const username = require('username');
const fs = require('fs');
var elasticsearch = require('elasticsearch');

//===================================================================================================================================================

var prefix_indexName = "wdd";
var indextype = "fd";
var indexName = "";
var MAX = 28000;
var extension = {
    TEXT: 1,
    CPP_SOURCE_FILE: 2,
    JAVA_SOURCE_FILE: 3,
    PYTHON_SOURCE_FILE: 4,
    JAVASCRIPT: 5,
    HTML: 6,
    XML: 7,
    IMAGE: 8,
    RTF: 9,
    DOC: 10,
    PPT: 11,
    XLS: 12,
    PDF: 13,
    RSS: 14,


    JAR_FILE:16,
    LIB: 17,
    DLL: 18,
    DYLIB: 19,
    EXE: 20,
    APP: 21,
    BAT: 22,
    SH: 23,
    MSI: 24,
    PKG: 25,
    ISO: 26,
    ZIP: 27,

    UNKNOWN: 99
};

//===================================================================================================================================================

function setCurrentUserAndIndexName() {
    UN = username.sync()
    indexName = prefix_indexName + "_" + UN;
}

//===================================================================================================================================================

function buildAndExecuteIndex(dirpath) {
    var bodystr = "";

    try {
        fsUtils.walkSync(dirpath, {
        skipErrors  : true,
        logErrors   : false
        }, function (err, path, stats, next, cache) {
            if (!err) {
                var _name = path.split('\\').pop().split('/').pop();
                var _path = path.split("\\").join("/");
                var _size = 0;
                var _extstr = "";
                var _ext = 0;

                if (_name != ".git" || _name != ".svn")
                {
                    if (stats.isFile()) {
                        _size = fsUtils.fsizeSync(path);
                        _extstr = _name.split('.').pop();
                        _ext = 0;

                        if ((_extstr.toUpperCase().indexOf("ATOM") != -1) ||  (_extstr.toUpperCase().indexOf("RSS") != -1)) {
                            _ext = extension.RSS;
                        } else if (_extstr.toUpperCase().indexOf("PDF") != -1) {
                            _ext = extension.PDF;
                        } else if ((_extstr.toUpperCase().indexOf("XLS") != -1) ||  (_extstr.toUpperCase().indexOf("XLSX") != -1) ||  (_extstr.toUpperCase().indexOf("XLSB") != -1) ||  (_extstr.toUpperCase().indexOf("XLSM") != -1) ||  (_extstr.toUpperCase().indexOf("XLTX") != -1)) {
                            _ext = extension.XLS;
                        } else if ((_extstr.toUpperCase().indexOf("PPTX") != -1) ||  (_extstr.toUpperCase().indexOf("POTX") != -1)) {
                            _ext = extension.PPT;
                        } else if ((_extstr.toUpperCase().indexOf("DOC") != -1) ||  (_extstr.toUpperCase().indexOf("DOCX") != -1)) {
                            _ext = extension.DOC;
                        } else if (_extstr.toUpperCase().indexOf("RTF") != -1) {
                            _ext = extension.RTF;
                        } else if ((_extstr.toUpperCase().indexOf("PNG") != -1) ||  (_extstr.toUpperCase().indexOf("JPG") != -1) ||  (_extstr.toUpperCase().indexOf("JPEG") != -1) ||  (_extstr.toUpperCase().indexOf("GIF") != -1)) {
                            _ext = extension.IMAGE;
                        } else if (_extstr.toUpperCase().indexOf("XML") != -1) {
                            _ext = extension.XML;
                        } else if ((_extstr.toUpperCase().indexOf("HTML") != -1) ||  (_extstr.toUpperCase().indexOf("HTM") != -1)) {
                            _ext = extension.HTML;
                        } else if (_extstr.toUpperCase().indexOf("JS") != -1) {
                            _ext = extension.JAVASCRIPT;
                        } else if ((_extstr.toUpperCase().indexOf("CPP") != -1) ||  (_extstr.toUpperCase().indexOf("C") != -1) ||  (_extstr.toUpperCase().indexOf("H") != -1) ||  (_extstr.toUpperCase().indexOf("HPP") != -1)) {
                            _ext = extension.CPP_SOURCE_FILE;
                        } else if (_extstr.toUpperCase().indexOf("PY") != -1) {
                            _ext = extension.PYTHON_SOURCE_FILE
                        } else if (_extstr.toUpperCase().indexOf("JAVA") != -1) {
                            _ext = extension.JAVA_SOURCE_FILE;
                        } else if (_extstr.toUpperCase().indexOf("JAR") != -1) {
                            _ext = extension.JAR_FILE;
                        } else if (_extstr.toUpperCase().indexOf("DLL") != -1) {
                            _ext = extension.DLL;
                        } else if (_extstr.toUpperCase().indexOf("DYLIB") != -1) {
                            _ext = extension.DYLIB;
                        } else if (_extstr.toUpperCase().indexOf("EXE") != -1) {
                            _ext = extension.EXE;
                        } else if (_extstr.toUpperCase().indexOf("APP") != -1) {
                            _ext = extension.APP;
                        } else if (_extstr.toUpperCase().indexOf("BAT") != -1) {
                            _ext = extension.BAT;
                        } else if (_extstr.toUpperCase().indexOf("SH") != -1) {
                            _ext = extension.SH;
                        } else if ((_extstr.toUpperCase().indexOf("LIB") != -1) || (_extstr.toUpperCase().indexOf("A") != -1)) {
                            _ext = extension.LIB;
                        } else if (_extstr.toUpperCase().indexOf("MSI") != -1) {
                            _ext = extension.MSI;
                        } else if (_extstr.toUpperCase().indexOf("PKG") != -1) {
                            _ext = extension.PKG;
                        } else if ((_extstr.toUpperCase().indexOf("ISO") != -1) || (_extstr.toUpperCase().indexOf("DMG") != -1)) {
                            _ext = extension.ISO;
                        } else if ((_extstr.toUpperCase().indexOf("ZIP") != -1) || (_extstr.toUpperCase().indexOf("TAR") != -1) || (_extstr.toUpperCase().indexOf("GZ") != -1) || (_extstr.toUpperCase().indexOf("BZ") != -1)) {
                            _ext = extension.ZIP;
                        } else if ((_extstr.toUpperCase().indexOf("TEXT") != -1) || (_extstr.toUpperCase().indexOf("TXT") != -1)) {
                            _ext = extension.TEXT;
                        } else {
                            _ext = extension.UNKNOWN;
                        }

                        bodystr = bodystr + '{ "index" :  { "_index" : "' + indexName + '", "_type" : "' + indextype + '", "_id" : "' + fs.statSync(_path).ino + '" } },\n' + 
                                            '{ "fileabsolutepath" : "' + _path + '", "filename" : "' + _name + '", "type" : "File", "size" : "' + _size + '", "extension" : "' + _ext + '", "views" : 1 }\n';
                    } else {
                        bodystr = bodystr + '{ "index" :  { "_index" : "' + indexName + '", "_type" : "' + indextype + '", "_id" : "' + fs.statSync(_path).ino + '" } },\n' + 
                                            '{ "fileabsolutepath" : "' + _path + '", "filename" : "' + _name + '", "type" : "Folder", "size" : "NA", "extension" : "' + _ext + '", "views" : 1 }\n';
                    }

                    if (bodystr.length > MAX) {
                        try {
                            elasticclient.bulk({
                                body: [
                                    bodystr
                                ]
                            }, function (err, resp) {
                                if (err) {
                                    //console.log("[Indexer] bulk[1] index returned error : " + err);
                                } else {
                                    //console.log("[Indexer] bulk[1] index success");
                                }
                            });
                        } catch (err) {
                            console.log("[Indexer] bulk[1] exception : " + err);
                        }
                        
                        total_files_processed_until_now = 0;
                        bodystr = "";
                    }
                }
            }
            next();
        });
    } catch (err) {
        // Ignore
    }


    if (bodystr.length > 0) {
        try {
            elasticclient.bulk({
                body: [
                    bodystr
                ]
            }, function (err, resp) {
                if (err) {
                    //console.log("[Indexer] bulk[2] index returned error : " + err);
                    process.exit();
                } else {
                    //console.log("[Indexer] bulk[2] index success");
                    process.exit();
                }
            });
        } catch (err) {
            console.log("[Indexer] bulk[2] exception : " + err);
        }

        bodystr = "";
    }
}

//===================================================================================================================================================

var elasticclient = new elasticsearch.Client({ host: 'wdcluster:9200', log: 'error' });

elasticclient.ping({
    requestTimeout: 30000,
}, function (error) {
    if (error) {
        console.error('{WD Cluster} is down trying {WD Loopback Cluster}!');
        elasticclient = new elasticsearch.Client({ host: 'localhost:9200', log: 'error' });
        elasticclient.ping({
            requestTimeout: 30000,
        }, function (error) {
            if (error) {
                console.error('{WD Loopback Cluster} is also down. Exiting App!');
                process.exit();
            } else {
                console.log('Connected with {WD Loopback CLuster}!');
                setCurrentUserAndIndexName();
                buildAndExecuteIndex(process.argv[2]);
            }
        });
    } else {
        console.log('Connected with {WD CLuster}!');
        setCurrentUserAndIndexName();
        buildAndExecuteIndex(process.argv[2]);
    }
});

//===================================================================================================================================================
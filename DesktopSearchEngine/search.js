//===================================================================================================================================================
//
//
//  Author : Biswapratap Chatterjee
//  Company: Western Digital
//  Product: Discovery Search Engine
//
//
//===================================================================================================================================================

var gSearchShow = 1;
function searchBarShow() {
    if (gSearchShow) {
        $('#searchBar').hide();
        gSearchShow = 0;
    } else {
		$('#searchBar').show();
        gSearchShow = 1;
    }
    $("#rslt").hide();
}

function hideDivs() {
    //For Hiding Search items
    $("#rslt").is(":visible")
    {
        if(event.target.id == "rslt") {
            // Do Nothing
        } else if(event.target.id == "fname") {
            // Do Nothing
        } else {
            $("#rslt").hide();
        }
    }

    //For Hiding Popus
    $("#popup").is(":visible")
    {
        var devId = event.target.id;
        if(devId.indexOf("WDDevice_") != -1) {
            // Do Nothing
        } else {
            $("#popup").hide();
        }
    }
}

//===================================================================================================================================================

var os = require("os");
const fs = require('fs');
var path = require('path');
const remote2 = require('electron').remote
var elasticclient = remote2.getGlobal('sharedObj').elasticclient;
const spawn = require('child_process').spawn;
const shell = require('electron').shell;
const ipc2 = require('electron').ipcRenderer;
const username = require('username');
const mainProcess2 = remote2.require('./app');
var sizeOfImage = require('image-size');
var requestIp = require('request-ip');
//var jimp = require("jimp");

//===================================================================================================================================================

var indexName = "";
var indextype = "fd";
var prefix_indexName = "wdd";
var search_connection_cluster = 1;

var extension = {
    TEXT: 1,
    CPP_SOURCE_FILE: 2,
    JAVA_SOURCE_FILE: 3,
    PYTHON_SOURCE_FILE: 4,
    JAVASCRIPT: 5,
    HTML: 6,
    XML: 7,
    IMAGE: 8,
    RTF: 9,
    DOC: 10,
    PPT: 11,
    XLS: 12,
    PDF: 13,
    RSS: 14,


    JAR_FILE:16,
    LIB: 17,
    DLL: 18,
    DYLIB: 19,
    EXE: 20,
    APP: 21,
    BAT: 22,
    SH: 23,
    MSI: 24,
    PKG: 25,
    ISO: 26,
    ZIP: 27,

    UNKNOWN: 99
};

//===================================================================================================================================================

function setCurrentUserAndIndexName() {
    mainProcess2.test("Entering setCurrentUserAndIndexName()");
    UN = username.sync();
    HN = os.hostname();
    indexName = prefix_indexName + "_" + UN;
    mainProcess2.test("Index Name : " + indexName);
    mainProcess2.test("Index Type : " + indextype);
    mainProcess2.test("Host Name : " + HN);
    mainProcess2.test("Exiting setCurrentUserAndIndexName()");
}

//===================================================================================================================================================

function TASK_CREATOR_STD_OUT(data) {
    mainProcess2.test(`${data}`);
}

function TASK_CREATOR_ERR(data) {
    mainProcess2.test(`${data}`);
}

function TASK_CREATOR_CLOSE(code) {
    mainProcess2.test("[Renderer] All tasks completed!");
}

//===================================================================================================================================================

function initIndexing() {
    mainProcess2.test("[Renderer] Spawning Task Creator ===>");
    var i = spawn('node', ['taskCreator.js']);
    i.stdout.on('data', TASK_CREATOR_STD_OUT);
    i.stderr.on('data', TASK_CREATOR_ERR);
    i.on('close', TASK_CREATOR_CLOSE);
}

//===================================================================================================================================================

function init() {
    mainProcess2.test("Init");
    setCurrentUserAndIndexName();

    elasticclient.indices.exists({
            index: indexName,
            expandWildcards: 'all'
        }, function (error, exists) {
            if (exists === true) {
                mainProcess2.test('[INIT] wdd Index Already Exists');
                initIndexing();
            } else {
                mainProcess2.test('[INIT] creating user index');
                elasticclient.indices.create({
                    index : indexName,
                    body : {                   
                        "settings": {
                            "index.routing.allocation.enable" : "all",
                            "index.routing.allocation.include._ip": "10.65.173.48, 10.65.38.243",
                            "index": {
                                "number_of_shards" : 2,
                                "number_of_replicas" : 1
                            },
                            "analysis": {
                                "analyzer": {
                                    "autocomplete": {
                                        "tokenizer": "autocomplete",
                                        "filter": [
                                            "lowercase"
                                        ]
                                    },
                                    "autocomplete_search": {
                                        "tokenizer": "lowercase"
                                    }
                                },
                                "tokenizer": {
                                    "autocomplete": {
                                        "type": "edge_ngram",
                                        "min_gram": 1,
                                        "max_gram": 10,
                                        "token_chars": [
                                            "letter"
                                        ]
                                    }
                                }
                            }
                        }                    
                    }
                }, function (error, response) {
                    if (error) {
                        mainProcess2.test('[INIT] Create wdd Index with settings [Failed]');
                    } else {
                        mainProcess2.test('[INIT] Create wdd Index with settings [Succeeded]');
                        elasticclient.indices.putMapping({
                            index : indexName,
                            type : indextype,
                            body : {
                                "fd": {
                                    "properties": {
                                        "fileabsolutepath": {
                                            "type": "text"
                                        },
                                        "filename": {
                                            "type": "text",
                                            "analyzer": "autocomplete",
                                            "search_analyzer": "autocomplete_search"
                                        },
                                        "summary": {
                                            "type": "text",
                                            "analyzer": "autocomplete",
                                            "search_analyzer": "autocomplete_search"
                                        }
                                    }
                                }
                            }
                        }, function (error, response) {
                            if (error) {
                                mainProcess2.test('[INIT] Put Mappings Failed');
                            } else {
                                mainProcess2.test('[INIT] wdd index created');
                                initIndexing();
                            }
                        });
                    }
                });
            }
        });
}

//===================================================================================================================================================

function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Byte';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
};

//===================================================================================================================================================

function openFileLocation(obj) {
    var abspath = obj.id;
    mainProcess2.test("User selected : " + abspath);
    var did = fs.statSync(abspath).ino;
    mainProcess2.test("DID : " + did);
    elasticclient.update({
        index: indexName,
        type: indextype,
        id: did,
        body: {
            script: 'ctx._source.views += 1'
        }
    }, function (error, response) {
        if (error) {
            mainProcess2.test(error);
        } else {
            mainProcess2.test("Incremented view count by 1 for filename : " + abspath);
        }
    });

    var ret = shell.openItem(abspath);
}

//===================================================================================================================================================

function createThumbnail(p) {
    var dimensions = sizeOfImage(p);
    var h1 = dimensions.height;
    var w1 = dimensions.width;
    var h2 = 0;
    var w2 = 0;

    h2 = 70;
    w2 = (w1 / h1) * h2;
    if (w2 > 60) {
        w2 = 60;
        h2 = (h1 / w1) * w2;
    }

    var icon = '"' + p + '" width="' + w2 + '" height="' + h2 + '"';
    return icon;
}

function getHTML(f, p, e, s, t) {
    var htm = "";
    if (t === "Folder") {
        htm = '<a href="javascript:void(0)" style="display:block;padding-left:10px;border:1px outset" id="' + p + 
                '" onclick="openFileLocation(this);" title="Type: ' + t + 
                '\nLocation: ' + p + 
                '\nSize: "NA">' + 
                '<img border="1" style="padding-right:10px" alt="Unknown" src="assets/folder.ico" width="40" height="40">' + 
                f + '</a>';
    } else {
        var icon = "";
        var eEnum = parseInt(e);
        var size = bytesToSize(parseInt(s, 10));

        if (eEnum == extension.TEXT) {
            icon = "assets/text.png";
        } else if (eEnum == extension.CPP_SOURCE_FILE) {
            if (process.platform === "win32") {
                icon = "assets/msvc.png";
            } else {
                icon = "assets/Xcode.ico";
            }
        } else if (eEnum == extension.PYTHON_SOURCE_FILE) {
            icon = "assets/python.png";
        } else if (eEnum == extension.JAVA_SOURCE_FILE) {
            icon = "assets/java.png";
        } else if (eEnum == extension.JAVASCRIPT) {
            icon = "assets/javascript.ico";
        } else if (eEnum == extension.HTML) {
            icon = "assets/html.png";
        } else if (eEnum == extension.XML) {
            icon = "assets/xml.png";            
        } else if (eEnum == extension.IMAGE) {
            icon = createThumbnail(p);
            htm = '<a href="javascript:void(0)" style="display:block;padding-left:10px;border:1px outset" id="' + p + 
                '" onclick="openFileLocation(this);" title="Type: ' + t + 
                '\nLocation: ' + p + 
                '\nSize: "' + size + '">' + 
                '<img border="1" style="padding-right:10px" alt="Unknown" src=' + icon + '>' + 
                f + '</a>';
            return htm;
        } else if (eEnum == extension.RTF) {
            icon = "assets/rtf.ico";            
        } else if (eEnum == extension.DOC) {
            icon = "assets/doc.png";            
        } else if (eEnum == extension.PPT) {
            icon = "assets/ppt.png";            
        } else if (eEnum == extension.XLS) {
            icon = "assets/XLS.ico";            
        } else if (eEnum == extension.PDF) {
            icon = "assets/PDF.ico";            
        } else if (eEnum == extension.RSS) {
            icon = "assets/rss.png";



        } else if (eEnum == extension.JAR_FILE) {
            icon = "assets/jar.png";
        } else if (eEnum == extension.LIB) {
            icon = "assets/lib.ico";
        } else if (eEnum == extension.DLL) {
            icon = "assets/dll.jpg";
        } else if (eEnum == extension.DYLIB) {
            icon = "assets/dylib.ico";
        } else if (eEnum == extension.EXE) {
            icon = "assets/exe.png";
        } else if (eEnum == extension.APP) {
            icon = "assets/app.ico";
        } else if (eEnum == extension.BAT) {
            icon = "assets/bat.png";
        } else if (eEnum == extension.SH) {
            icon = "assets/sh.png";
        } else if (eEnum == extension.MSI) {
            icon = "assets/msi.ico";
        } else if (eEnum == extension.PKG) {
            icon = "assets/pkg.png";
        } else if (eEnum == extension.ISO) {
            icon = "assets/iso.png";
        } else if (eEnum == extension.ZIP) {
            icon = "assets/ZIP.png";
        } else {
            icon = "assets/unknown.png";
        }

        htm = '<a href="javascript:void(0)" style="display:block;padding-left:10px;border:1px outset" id="' + p + 
                '" onclick="openFileLocation(this);" title="Type: ' + t + 
                '\nLocation: ' + p + 
                '\nSize: "' + size + '">' + 
                '<img border="1" style="padding-right:10px" alt="Unknown" src=' + icon + ' width="40" height="40">' + 
                f + '</a>';            
    }

    return htm;    
}

//===================================================================================================================================================
function getsuggestions() {
    var term = document.getElementById("fname").value;
    mainProcess2.test("[SEARCH] term : " + term);

    if ((search_connection_cluster == 1) || (search_connection_cluster == 2)) {
        elasticclient.search({
            index: indexName,
            body: {
                "query": {
                    "function_score": {
                        "query": {
                            "multi_match": {
    							"query": term,
    							"type": "phrase",
                                "type": "phrase_prefix",
                                "fields": [ "filename" ]
                            }
                        },
                        "field_value_factor": {
                            "field": "views",
                            "modifier": "log1p",
                            "factor": 2 
                        }
                    }
                }
            }
        }, function (error, response) {
            if (error) {
                mainProcess2.test("[1] Search error happened");
                mainProcess2.test(JSON.stringify(error, null, 2));
                
                mainProcess2.createLocalhostClient();
                var elasticclient2 = remote2.getGlobal('sharedObj').elasticclient2;
                elasticclient2.search({
                    index: indexName,
                    body: {
                        "query": {
                            "function_score": {
                                "query": {
                                    "multi_match": {
                                        "query": term,
                                        "type": "phrase",
                                        "type": "phrase_prefix",
                                        "fields": [ "filename" ]
                                    }
                                },
                                "field_value_factor": {
                                    "field": "views",
                                    "modifier": "log1p",
                                    "factor": 2 
                                }
                            }
                        }
                    }
                }, function (error, response) {
                    if (error) {
                        mainProcess2.test("[2] Search error happened");
                        mainProcess2.test(JSON.stringify(error, null, 2));
                        search_connection_cluster = 2;
                    } else {
                        search_connection_cluster = 0;
                        mainProcess2.test("[SEARCH] Hits = " + response.hits.total);
                        var html = '';
                        var r = response;
                        var result_count = r.hits.total;
                        if (result_count < 5) {
                            // Do nothing
                        } else if (result_count > 10) {
                            result_count = 10;
                        }
                        var resltdiv = document.getElementById('rslt');
                        resltdiv.innerHTML = '';
                        if(result_count>0)
                        {
                            $("#rslt").show();
                        }
                        else{
                            $("#rslt").hide();
                        }
                        for (var i = 0; i < result_count; i++) {
                            var result_file_name = r.hits.hits[i]._source.filename;
                            var result_abs_path = r.hits.hits[i]._source.fileabsolutepath;
                            var result_ext = r.hits.hits[i]._source.extension;
                            var result_type = r.hits.hits[i]._source.type;
                            var result_size = r.hits.hits[i]._source.size;
                            html = html + getHTML(result_file_name, result_abs_path, result_ext, result_size, result_type);
                        }
                        resltdiv.innerHTML = html;                    
                    }
                });
            } else {
                search_connection_cluster = 1;
                mainProcess2.test("[SEARCH] Hits = " + response.hits.total);
                var html = '';
                var r = response;
                var result_count = r.hits.total;
                if (result_count < 5) {
                    // Do nothing
                } else if (result_count > 10) {
                    result_count = 10;
                }
                var resltdiv = document.getElementById('rslt');
                resltdiv.innerHTML = '';
                if(result_count>0)
                {
                    $("#rslt").show();
                }
                else{
                    $("#rslt").hide();
                }
                for (var i = 0; i < result_count; i++) {
                    var result_file_name = r.hits.hits[i]._source.filename;
                    var result_abs_path = r.hits.hits[i]._source.fileabsolutepath;
                    var result_ext = r.hits.hits[i]._source.extension;
                    var result_type = r.hits.hits[i]._source.type;
                    var result_size = r.hits.hits[i]._source.size;
                    html = html + getHTML(result_file_name, result_abs_path, result_ext, result_size, result_type);
                }
                resltdiv.innerHTML = html;
            }
        });
    } else if (search_connection_cluster == 0) {
        var elasticclient2 = remote2.getGlobal('sharedObj').elasticclient2;
        elasticclient2.search({
            index: indexName,
            body: {
                "query": {
                    "function_score": {
                        "query": {
                            "multi_match": {
                                "query": term,
                                "type": "phrase",
                                "type": "phrase_prefix",
                                "fields": [ "filename" ]
                            }
                        },
                        "field_value_factor": {
                            "field": "views",
                            "modifier": "log1p",
                            "factor": 2 
                        }
                    }
                }
            }
        }, function (error, response) {
            if (error) {
                mainProcess2.test("[2] Search error happened");
                mainProcess2.test(JSON.stringify(error, null, 2));
                search_connection_cluster = 2;
            } else {
                search_connection_cluster = 0;
                mainProcess2.test("[SEARCH] Hits = " + response.hits.total);
                var html = '';
                var r = response;
                var result_count = r.hits.total;
                if (result_count < 5) {
                    // Do nothing
                } else if (result_count > 10) {
                    result_count = 10;
                }
                var resltdiv = document.getElementById('rslt');
                resltdiv.innerHTML = '';
                if(result_count>0)
                {
                    $("#rslt").show();
                }
                else{
                    $("#rslt").hide();
                }
                for (var i = 0; i < result_count; i++) {
                    var result_file_name = r.hits.hits[i]._source.filename;
                    var result_abs_path = r.hits.hits[i]._source.fileabsolutepath;
                    var result_ext = r.hits.hits[i]._source.extension;
                    var result_type = r.hits.hits[i]._source.type;
                    var result_size = r.hits.hits[i]._source.size;
                    html = html + getHTML(result_file_name, result_abs_path, result_ext, result_size, result_type);
                }
                resltdiv.innerHTML = html;                    
            }
        });        
    }

    mainProcess2.test("[SEARCH] exiting ...")
}

//===================================================================================================================================================

ipc2.on('initsearch', (event, message) => 
{
    mainProcess2.test('initsearch event received : ' + message);
    init(); 
})

//===================================================================================================================================================

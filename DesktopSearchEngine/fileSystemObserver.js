//===================================================================================================================================================
//
//
//  Author : Biswapratap Chatterjee
//  Company: Western Digital
//  Product: Discovery Search Engine
//
//
//===================================================================================================================================================

const watch = require('node-watch');
const username = require('username');
const fs = require('fs');
const fsUtils = require("nodejs-fs-utils");
const spawn = require('child_process').spawn;
var elasticsearch = require('elasticsearch');

//===================================================================================================================================================

var prefix_indexName = "wdd";
var indextype = "fd";
var indexName = "";

//===================================================================================================================================================

function setCurrentUserAndIndexName() {
    UN = username.sync()
    indexName = prefix_indexName + "_" + UN;
}

//===================================================================================================================================================

function update(fileAbsPath) {
    console.log("Update Event Occured on : " + fileAbsPath);
    var fileName = fileAbsPath.split('\\').pop().split('/').pop();
    var filePath = fileAbsPath.split("\\").join("/");
    try {
        var uid = fs.statSync(fileAbsPath).ino;
        var fileSize = fsUtils.fsizeSync(fileAbsPath);
        elasticclient.update({
            index: indexName,
            type: indextype,
            id: uid,
            body: {
                script: 'ctx._source.views += 1',
                fileabsolutepath: filePath,
                filename: fileName,
                size: fileSize,
            }
        }, function (error, response) {
            if (error) {
                console.log("Failed to update an existing document : " + fileName);
            } else {
                console.log("Updated an existing document : " + fileName);
            }
        });
    } catch (e) {
        console.log('XPOST() Exception : ' + e);
    }
}

//===================================================================================================================================================

function create(fileAbsPath) {
    console.log("Create Event Occured on : " + fileAbsPath);
    var fileName = fileAbsPath.split('\\').pop().split('/').pop();
    var filePath = fileAbsPath.split("\\").join("/");
    try {
        var fileSize = fsUtils.fsizeSync(fileAbsPath);
        var _stats = fs.statSync(fileAbsPath);
        var uid = _stats.ino;
        var fileType = "";
        if (_stats.isDirectory()) {
          fileType = "Folder";
        } else {
          fileType = "File";
        }
        elasticclient.index({
            index: indexName,
            type: indextype,
            id: uid,
            refresh: true,
            body: {
                views: 10,
                fileabsolutepath: filePath,
                filename: fileName,
                type: fileType,
                size: fileSize,
            }
        }, function (error, response) {
            if (error) {
                console.log("Failed to create a new document : " + fileName);
            } else {
                console.log("Created a new document : " + fileName);
            }
        });
    } catch (e) {
        console.log('XPUT() Exception : ' + e);
    }
}

//===================================================================================================================================================

function remove(fileAbsPath) {
    console.log("Delete Event Occured on : " + fileAbsPath);
    var fileName = fileAbsPath.split('\\').pop().split('/').pop();
    try {
        var uid = fs.statSync(fileAbsPath).ino;
        elasticclient.delete({
            index: indexName,
            type: indextype,
            id: uid
        }, function (error, response) {
            if (error) {
                console.log("Failed to deleted an existing document : " + fileName);
            } else {
                console.log("Deleted an existing document : " + fileName);
            }
        });
    } catch (e) {
        console.log('XDELETE() Exception : ' + e);
    }
}

//===================================================================================================================================================

function watchDirectory(d)
{
    try {
        watch(d, { recursive: true }, function(event, fileAbsPath) {
            if(fileAbsPath) {
                var ffound = "false";
                if (event == 'update') {
                    var fileName = fileAbsPath.split('\\').pop().split('/').pop();
                    if ((fileAbsPath.toLowerCase().indexOf("feeder") != -1) || 
                        (fileAbsPath.toLowerCase().indexOf("log") != -1) || 
                        (fileAbsPath.toLowerCase().indexOf("git") != -1) || 
                        (fileAbsPath.toLowerCase().indexOf("svn") != -1) || 
                        (fileAbsPath.toLowerCase().indexOf("AppData".toLowerCase()) != -1) || 
                        (fileName.toLowerCase().indexOf(UN.toLowerCase()) != -1)) {
                        // console.log("Ignoring unwanted events ...");
                    } else {
                        try {
                            var uid = fs.statSync(fileAbsPath).ino;
                            elasticclient.get({
                                index: indexName,
                                type: indextype,
                                id: uid
                            }, function (error, response) {
                                if (error) {
                                    ffound = "false";
                                } else {
                                    console.log(JSON.stringify(response, null, 2));
                                    ffound = String(response.found);
                                }

                                if (ffound === "true") {
                                    // Updated
                                    update(fileAbsPath);

                                } else {
                                    // Created
                                    create(fileAbsPath);
                                }
                            });
                        } catch (e) {
                            console.log('XGET() Exception : ' + e);
                        }
                    }
                } else if (event == 'remove') {
                    // Delete
                    remove(fileAbsPath);
                }
            }
            else {
                console.log('fileAbsPath not provided')
            }
        });
    } catch (e) {
        console.log("Watch directory doesn't exist = " + d);
    }   
}

function watchDirectoryList(dirlist) {
    var watchList = dirlist.split(";");
    for (var i = 0; i < watchList.length; i++) {
        watchDirectory(watchList[i]);
    }
}

//===================================================================================================================================================

var elasticclient = new elasticsearch.Client({ host: 'wdcluster:9200', log: 'error' });
elasticclient.ping({
    requestTimeout: 30000,
}, function (error) {
    if (error) {
        console.error('WD Cluster is down trying localhost WD Cluster!');
        elasticclient = new elasticsearch.Client({ host: '127.0.0.1:9200', log: 'error' });
        elasticclient.ping({
            requestTimeout: 30000,
        }, function (error) {
            if (error) {
                console.error('WD localhost Cluster is also down. Exiting process!');
                process.exit();
            } else {
                console.log('Connected with localhost WD CLuster!');
                setCurrentUserAndIndexName();
                watchDirectoryList(process.argv[2]);
            }
        });
    } else {
        console.log('Connected with WD CLuster!');
        setCurrentUserAndIndexName();
        watchDirectoryList(process.argv[2]);
    }
});

//===================================================================================================================================================



function popUP(id)
{
    var devId=gDeviceIds[id];
    mainProcess.test(devId);
    var flist=getSupportedFeatures(devId);
    mainProcess.test(flist);
    var popMenu=getPopUpMenu(id,devId,flist);
    $('#popuplist').empty();
    $('#popuplist').append(popMenu);
    $('#popup').show();
}

function getSupportedFeatures(devId)
{
    var message=mainProcess.getDeviceFeatureList(devId);
    mainProcess.test(message)
    var ret=[],status=[];
    var features=[];
    var jsonObject=JSON.parse(message);

    //Security
    if(jsonObject['security']['supported'])
    {
        features.push('security');
        var tp=jsonObject['security']['state'];
        status.push(tp);
    }
    //Led
    if(jsonObject['led']['supported']){
        features.push('led');
        if(jsonObject['led']['state']){
            status.push('ON');
            gLEDstate=1;
            gLEDstr='ON'
        }
        else
        {
            status.push('OFF');
            gLEDstate=0;
            gLEDstr='OFF'
        }
    }
    //RAID
    if(jsonObject['raid']['supported']){ 
        features.push('raid');
        status.push('RAID-0')//Now hard coded.
    }

    //UAS
    if(jsonObject.hasOwnProperty('uas')){ 
        features.push('uas');
        if(jsonObject['uas']['state']){
            status.push('ON')
            gUASstate=1;
        }
        else
        { 
            status.push('OFF')
            gUASstate=0;
        }
    }


    ret.push(features);
    ret.push(status);  
    return ret;

}

function getPopUpMenu(index, sDevId,ret)
{
    var featurelist=ret[0];
    var state=ret[1];

    var str='';
    for(var i=0;i<featurelist.length;++i)
    {
        if(featurelist[i]=='security')
        {
            if(state[i]=='securityDisabled')
            {
                gSetPwd=1;
                str+='<li class="list-group-item" onclick="changeToSetPasswordPage(' + index + ');">' +
                    '<label width="30" height="10">Set Password</label>'+
                '</li>'
            }
            else
            {
                gSetPwd=0;
                str+='<li class="list-group-item" onclick="changeToEditPasswordPage(' + index + ');">' +
                    '<label width="30" height="10">Edit Password</label>'+
                '</li>'
            }
        }
        else if(featurelist[i]=='led')
        {
            var ledStr=''
            if(state[i]=='ON')
            {
                ledStr='<label width="30" height="10">LED Settings ON </label>'+
                    '<img class="img media-object pull-right" src="./assets/checkmark.png" width="15" height="15" alt="Image">'
            }
            else
            {
                ledStr='<label width="30" height="10">LED Settings (OFF) </label>';
            }

            str+='<li class="list-group-item"  onclick="changeToLEDPage(' + index + ');" width="30" height="20">' +
                    ledStr+
                '</li>';
        }
        else if(featurelist[i]=='raid')
        {
            str+='<li class="list-group-item" onclick="changeToRAIDPage(' + index + ');" width="30" height="20">' +
                    '<label width="30" height="30">RAID </label>'+
                '</li>'
        }
        else if(featurelist[i]=='uas')
        {
            var uasStr=''
            if(state[i]=='ON')
            {
                uasStr='<label width="30" height="10">UAS (ON)</label>'
            }
            else
            {
                uasStr='<label width="30" height="10">UAS (OFF)</label>';
            }

            str+='<li class="list-group-item"  onclick="changeToUASPage(' + index + ');" width="30" height="20">' +
            uasStr+
                '</li>';

        }

    }
return str;
}
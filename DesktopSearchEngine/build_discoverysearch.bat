set version=%1
CALL electron-packager . DiscoverySearch --platform=win32 --arch=x64 --app-version=%version% --electron-version=1.4.13 --icon="icon\Rafiqul-Hassan-Blogger-Search.ico"
CALL "C:\Program Files\7-Zip\7z.exe" x Elasticsearch_minimal.zip
CALL move Elasticsearch DiscoverySearch-win32-x64/.
CALL "C:\Program Files\7-Zip\7z.exe" x Installers.zip
CALL move Installers DiscoverySearch-win32-x64/.
CALL copy install_softwares.bat DiscoverySearch-win32-x64\install_softwares.bat
CALL copy install_and_start_discoverysearchservice.bat DiscoverySearch-win32-x64\install_and_start_discoverysearchservice.bat
CALL copy stop_and_remove_discoverysearchservice.bat DiscoverySearch-win32-x64\stop_and_remove_discoverysearchservice.bat
CALL "C:\Program Files (x86)\Inno Setup 5\compil32.exe" /cc innosetup.iss
CALL move Output\setup.exe Output\DiscoverySearch_%version%.exe
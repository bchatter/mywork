const { app, BrowserWindow, ipcMain } = require('electron');
const path = require ('path');
const fs = require('fs');
const os = require('os');
var ffi = require('ffi')
var ref = require('ref');
var elasticsearch = require('elasticsearch');
global.sharedObj = { elasticclient: null, elasticclient2: null };
global.sharedObj.elasticclient = new elasticsearch.Client({ host: 'wdcluster:9200', log: 'error' });

global.sharedObj.elasticclient.ping({
    requestTimeout: 30000,
}, function (error) {
    if (error) {
        console.error('{WD Cluster} is down. Trying {WD Loopback Cluster}!');
        global.sharedObj.elasticclient = new elasticsearch.Client({ host: 'localhost:9200', log: 'error' });
        global.sharedObj.elasticclient.ping({
            requestTimeout: 30000,
        }, function (error) {
            if (error) {
                console.error('{WD Loopback Cluster} is also down. Exiting App!');
                process.exit();
            } else {
                console.log('Connected with {WD Loopback CLuster}!');
            }
        });
    } else {
        console.log('Connected with {WD CLuster}!');
    }
});

var mainWindow = null
if (process.platform == "win32") {
    libPath = __dirname + '/WDDeviceSettings.dll';
} else if (process.platform == "darwin") {
    libPath = __dirname + '/WDDeviceSettings.dylib';
}
console.log(libPath);


var wdDeviceLibInterface = ffi.Library(libPath, 
{
    "getFeatureList"  : [ 'string', ['string'] ], 
    "setFeature"      : [ 'string', ['string', 'string'] ],
    "getFeature"      : [ 'string', ['string', 'string'] ],
    "sampleProgressResponseFunction" : [ 'int', [ 'string', 'pointer', 'pointer'] ],
    "releaseMemory" : [ 'void', [ 'string'] ],
    "initLibrary" : [ 'void',[] ],
    "cleanupLibrary" : [ 'void',[]  ],
    "enumerateDevices"  : [ 'string',[] ]	
});


function createWindow(){
    mainWindow = new BrowserWindow({width: 400, height: 650});

    mainWindow.setMenu(null);
    mainWindow.loadURL(`file://${__dirname}/index.html`);
    mainWindow.webContents.on('did-finish-load', () => {

      wdDeviceLibInterface.initLibrary();
      var devices=wdDeviceLibInterface.enumerateDevices();
      console.log(devices);
      mainWindow.webContents.send('refresh', devices)
      console.log('sent enumerated devices to renderer...')
      mainWindow.webContents.send('initsearch', 'ping search')
      console.log('ping search event sent')

    })
      
    //mainWindow.webContents.openDevTools();

    //let contents = window.webContents;

    mainWindow.on('closed', function() {
      mainWindow = null;
    });

    mainWindow.webContents.on('did-finish-load', () => {
      console.log('did-finish-load....')
      
    })

    mainWindow.on('closed', function() {
    wdDeviceLibInterface.cleanupLibrary();
    console.log('cleanup Lib success')
    mainWindow = null
    })
}

app.on('ready', function(){
    console.log('The application is ready.')
    createWindow();
});

function setFeature(sDeviceId,data)
{
  console.log('setFeature called...')
  var res=wdDeviceLibInterface.setFeature(sDeviceId, data);
  console.log(res)
  return res;

}

function releaseMemory(address)
{
  console.log('releaseMemory called...')
  wdDeviceLibInterface.releaseMemory(address);
  return;
}

  function setLED(sDeviceId,data)
  {
    console.log(sDeviceId+' setLed: '+data);
    var cmdReq = {};
    cmdReq["feature"] = "led";
    cmdReq["setvalue"] = data;
    
    var jsonPacket = JSON.stringify(cmdReq);
    console.log(jsonPacket);

    var response1 = wdDeviceLibInterface.setFeature(sDeviceId, jsonPacket)
    if (response1)
    {
      console.log(response1);
      var jsonObject = JSON.parse(response1);
      if(jsonObject.hasOwnProperty('address'))
        wdDeviceLibInterface.releaseMemory(jsonObject.address)
    }
    else
    {
      console.log('failed setLED!!!')
    }
  }

  function setUAS(sDeviceId,data)
  {
    console.log(sDeviceId+' setUAS: '+data);
    var cmdReq = {};
    cmdReq["feature"] = "uas";
    cmdReq["setvalue"] = data;
    
    var jsonPacket = JSON.stringify(cmdReq);
    console.log(jsonPacket);

    var response1 = wdDeviceLibInterface.setFeature(sDeviceId, jsonPacket)
    if (response1)
    {
      console.log(response1);
      var jsonObject = JSON.parse(response1);

      if(jsonObject.hasOwnProperty('address'))
          wdDeviceLibInterface.releaseMemory(jsonObject.address)
  
    }
    else
    {
      console.log('failed setUAS!!!')
    }
  }

  function getFeature(sDeviceId,data)
  {
    console.log('getFeature: sDeviceId='+sDeviceId+'data='+data);
    var cmdReq = {};
    cmdReq["feature"] = "led";
    cmdReq["setvalue"] = data;
    
    var jsonPacket = JSON.stringify(cmdReq);
    console.log(jsonPacket);

    var response = wdDeviceLibInterface.getFeature(sDeviceId, jsonPacket)
    if (response)
    {
      var jsonObject = JSON.parse(response);
      if(jsonObject.hasOwnProperty('address'))
        wdDeviceLibInterface.releaseMemory(jsonObject.address)
    }
    else
    {
      console.log('getFeature response null');
    }
    return response;
  }

  function getDeviceFeatureList(sDeviceID)
  {
    console.log("GetDeviceFeatures called!")
    
    var response = wdDeviceLibInterface.getFeatureList(sDeviceID);
    if (response)
    {
          console.log(response);
          var jsonObject = JSON.parse(response);
          if(jsonObject.hasOwnProperty('address'))
            wdDeviceLibInterface.releaseMemory(jsonObject.address)
    }
    else
    {
          console.log('response is null')
    }
    return response;
  }
 function E_enumerateDevices(){

  return wdDeviceLibInterface.enumerateDevices();

 }

 function getFeatureHealth(sDeviceId)
 {
   console.log('getFeature: sDeviceId='+sDeviceId);
   var cmdReq = {};
   cmdReq["feature"] = "health";
   
   var jsonPacket = JSON.stringify(cmdReq);
   console.log(jsonPacket);

   var response = wdDeviceLibInterface.getFeature(sDeviceId, jsonPacket)
   if (response)
   {
     var jsonObject = JSON.parse(response);
     if(jsonObject.hasOwnProperty('address'))
       wdDeviceLibInterface.releaseMemory(jsonObject.address)
   }
   else
   {
     console.log('getFeature response null');
   }
   return response;
 }

 function test(msg)
 {
    console.log(msg);
 }

 function createLocalhostClient()
 {
    global.sharedObj.elasticclient2 = new elasticsearch.Client({ host: 'localhost:9200', log: 'error' });

    global.sharedObj.elasticclient2.ping({
        requestTimeout: 30000,
    }, function (error) {
        if (error) {
            console.error('{WD Loopback Cluster} is also down. Exiting App!');
            process.exit();
        } else {
          console.log('Connected with {WD Loopback CLuster}!');
        }
    });
 }

  exports.setLED=setLED
  exports.setUAS=setUAS
  exports.getDeviceFeatureList=getDeviceFeatureList
  exports.getFeature=getFeature
  exports.setFeature=setFeature
  exports.releaseMemory=releaseMemory
  exports.E_enumerateDevices=E_enumerateDevices
  exports.getFeatureHealth=getFeatureHealth
  exports.test=test
  exports.createLocalhostClient=createLocalhostClient
  

  

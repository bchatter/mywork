var curSecurityIndex=0;
var enableAutoUnlock=false;
var enableAutoUnlock2=false;
function changeToSetPasswordPage(id)
{
        curSecurityIndex=id;
        var idx = $('#id_security_set')
        $.mobile.changePage($(idx), {transition : "slide"});
        $('#popup').hide()        

}

function changeToEditPasswordPage(id)
{
        curSecurityIndex=id;
        var idx = $('#id_security_edit')
        $.mobile.changePage($(idx), {transition : "slide"});
        $('#popup').hide()

}

function removePasswordPage()
{
        var idx = $('#id_rm_pwd')
        $.mobile.changePage($(idx), {transition : "slide"});

}
function changePasswordPage()
{
        var idx = $('#id_ch_pwd')
        $.mobile.changePage($(idx), {transition : "slide"});
}
function disableAutoUnlockPage()
{
        var idx = $('#id_unlock_pwd')
        $.mobile.changePage($(idx), {transition : "slide"});

}
$('#id_setPwdcbk').click(function(){
        if($(this).prop("checked") == true){
                enableAutoUnlock=true;
        }
        else if($(this).prop("checked") == false){
                enableAutoUnlock=false;
        }
    });

$('#id_set_pwd').click(function (event) {
        /*var form=document.querySelector('#form_setpwd');
        mainProcess.test('processing set Password')*/
        var password= $("#id_setPwd1").val();
        var newpassword= $("#id_setPwd2").val();
        var passwordHint= $("#id_setPwd3").val();
        setPassword(password,newpassword,passwordHint)
        event.preventDefault();
        return false;
       });

function setPassword(password,newPassword,passwordHint)
{
        var TestVar=[]
        TestVar.push(password);
        TestVar.push(newPassword);
        TestVar.push(passwordHint);
        TestVar.push(enableAutoUnlock);

        //TestVar.push(true)//Checkbox value
        mainProcess.test('set password values: '+TestVar);
        
        var cmdReq={}
        cmdReq["feature"] = "security";
        cmdReq["op"] = "setPassword";
        cmdReq["password"] = TestVar[0];
        cmdReq["confirmPassword"] = TestVar[1];
        cmdReq["passwordHint"] = TestVar[2];
        cmdReq["enableAutoUnlock"] = TestVar[3];

        var jsonPacket = JSON.stringify(cmdReq);

        var devId=gDeviceIds[curSecurityIndex]
        var response=mainProcess.setFeature(devId,jsonPacket);
        if (response)
        {
          var jsonObject = JSON.parse(response);
          
          mainProcess.releaseMemory(jsonObject.address)
          if(jsonObject['status']['returnCode']==0)
          {
                //return goToHome();
                alert('set Password success')
                goToHome();
          }
          else
          {
                alert('set Password failed.')
          }
        }
        else
        {
          console.log('failed set password!!!')
        }

        return;
}

$('#id_rm_passwd').click(function () {
        var password= $("#rm_passwd").val();
        removePassword(password)
       });

function removePassword(passcode)
{
        var TestVar=[]
        TestVar.push(passcode)
        mainProcess.test('remove password values: '+TestVar);


        var cmdReq={}
        cmdReq["feature"] = "security";
        cmdReq["op"] = "removePassword";
        cmdReq["password"] = TestVar[0];

        var jsonPacket = JSON.stringify(cmdReq);

        var devId=gDeviceIds[curSecurityIndex]
        var response=mainProcess.setFeature(devId,jsonPacket);
        if (response)
        {
          var jsonObject = JSON.parse(response);
          mainProcess.releaseMemory(jsonObject.address)
          if(jsonObject['status']['returnCode']==0)
          {
                alert('remove Password success')
                goToHome();
          }
          else
          {
                alert('remove Password failed.')
          }

        }
        else
        {
          console.log('failed remove password!!!')
          alert('remove password failed...')
        }

        return false;

}


$('#id_change_passwd').click(function () {
        /*var form=document.querySelector('#form_setpwd');
        mainProcess.test('processing set Password')*/
        var password= $("#id_chgPwd1").val();
        var newpassword= $("#id_chgPwd2").val();
        var newpasswordRepeat= $("#id_chgPwd3").val();
        var passwordHint= $("#id_chgPwd4").val();
        changePassword(password,newpassword,newpasswordRepeat,passwordHint)
       });

$('#cbk_1').click(function(){
        if($(this).prop("checked") == true){
                enableAutoUnlock2=true;
        }
        else if($(this).prop("checked") == false){
                enableAutoUnlock2=false;
        }
    });
function changePassword(in1,in2,in3,in4)
{
        var TestVar=[]
        TestVar.push(in1)
        TestVar.push(in2)
        TestVar.push(in3)
        TestVar.push(in4)
        TestVar.push(enableAutoUnlock2)

        mainProcess.test('Change password values: '+TestVar);

        var cmdReq={}
        cmdReq["feature"] = "security";
        cmdReq["op"] = "changePassword";
        cmdReq["oldPassword"] = TestVar[0];
        cmdReq["newPassword"] = TestVar[1];
        cmdReq["confirmPassword"] = TestVar[2];
        cmdReq["passwordHint"] = TestVar[3];
        cmdReq["enableAutoUnlock"] = TestVar[4];

        var jsonPacket = JSON.stringify(cmdReq);

        var devId=gDeviceIds[curSecurityIndex]
        var response=mainProcess.setFeature(devId,jsonPacket);
        if (response)
        {
          var jsonObject = JSON.parse(response);
          mainProcess.releaseMemory(jsonObject.address)
          if(jsonObject['status']['returnCode']==0)
          {
                alert('change Password success')
                goToHome();
          }
          else
          {
                alert('change Password failed.')
          }

        }
        else
        {
          console.log('failed changePassword!!!')
          alert('change Password failed...')
        }

}
$('#id_disableAuto').click(function () {
        var password= $("#disPwd").val();
        disableAutoUnlock(password)
       });


function disableAutoUnlock(input)
{
        var TestVar=[]
        TestVar.push(input)
        mainProcess.test('Disable auto unlock'+TestVar);

        var cmdReq={}
        cmdReq["feature"] = "security";
        cmdReq["op"] = "disableAutoUnlock";
        cmdReq["password"] = TestVar[0];

        var jsonPacket = JSON.stringify(cmdReq);

        var devId=gDeviceIds[curSecurityIndex]
        var response=mainProcess.setFeature(devId,jsonPacket);
        if (response)
        {
          var jsonObject = JSON.parse(response);
          mainProcess.releaseMemory(jsonObject.address)
          if(jsonObject['status']['returnCode']==0)
          {
                alert('disable auto unlock success')
                goToHome();
          }
          else
          {
                alert('disable auto unlock failed.')
          }
        }
        else
        {
          console.log('failed disable auto unlock!!!')
          alert('disable auto unlock failed...')
        }

}
//===================================================================================================================================================
//
//
//  Author : Biswapratap Chatterjee
//  Company: Western Digital
//  Product: Discovery Search Engine
//
//
//===================================================================================================================================================

const os = require('os');
const cluster = require('cluster');
var sleep = require('sleep');

//===================================================================================================================================================

numOfCPUs = 0;
indexList = [];
completedTaskIndex = 0;

//===================================================================================================================================================

function setNumberOfCPUs() {
    numOfCPUs = os.cpus().length;
    console.log("Num of CPU : " + numOfCPUs);
}

//===================================================================================================================================================

function clusterEngine(t) {
    var task_index = 0;
    completedTaskIndex = 0;
    if (cluster.isMaster) {
        for (var i = 0; i < ((numOfCPUs - 1) / 2); i++) {
            if (task_index >= indexList.length) {
                break;
            }

            var directory = indexList[task_index];
            try {
                cluster.setupMaster({
                    execArgv: ['indexer.js', directory],
                    silent: false
                });

                console.log("[" + t + "] Forking for [num of cpu loop] --> " + directory);
                cluster.fork();
                sleep.sleep(1);
                task_index++;
            } catch (err) {
                console.log("Fork [num of cpu loop] exception : " + err);
            }
        }

        cluster.on('exit', (worker, code, signal) => {
            completedTaskIndex++;

            if (completedTaskIndex >= indexList.length) {
                console.log("[" + t + "] EXITING CLUSTER !!");
                process.exit();
            }

            if (task_index < indexList.length) {
                var directory = indexList[task_index];

                try {
                    cluster.setupMaster({
                        execArgv: ['indexer.js', directory],
                        silent: false
                    });
    
                    console.log("[" + t + "] Forking for [on exit] --> " + directory);
                    sleep.sleep(1);
                    cluster.fork();
                    task_index++;
                } catch (err) {
                    console.log("Fork [on exit] exception : " + err);
                }
            }
        });
    } else {
    	// Worker thread area
    }
}

//===================================================================================================================================================

function indexDirectoryList(l, t) {
	setNumberOfCPUs();
	indexList = l.split(";");
	clusterEngine(t);
}

//===================================================================================================================================================

console.log("[WD Cluster] ==>");
indexDirectoryList(process.argv[2], process.argv[3]);

//===================================================================================================================================================

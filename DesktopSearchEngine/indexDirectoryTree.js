//===============================================================================================================================
//
//
//
//
//
//
//
//===============================================================================================================================

const os = require('os');
const fsUtils = require("nodejs-fs-utils");
const firstBy = require('thenby');
const winattr = require('winattr');
const cluster = require('cluster');

//===============================================================================================================================

completedTaskIndex = 0;
indexList = [];
indexList.push({
        directoryName: "", 
        directoryPath: "", 
        lastAccessTime: 0, 
        lastModificationTime: 0, 
        creationTime: 0, 
        directortyWeight: 0
    });

//===============================================================================================================================

function setNumberOfCPUs() {
    numOfCPUs = os.cpus().length;
}

function setOS() {
    OS = process.platform;
}

//===============================================================================================================================

function getNumericalTime(t) {
    var T = String(t);
    var end =  T.indexOf("GMT") - 1;
    T = T.substr(0, end);
    var splits = T.split(' ');
    var date = parseInt(splits[2]);
    var month = 0;
    var monthstr = splits[1];

    if (monthstr.toLowerCase() === "jan") {
        month = 1;
    } else if (monthstr.toLowerCase() === "feb") {
        month = 2;
    } else if (monthstr.toLowerCase() === "mar") {
        month = 3;
    } else if (monthstr.toLowerCase() === "apr") {
        month = 4;
    } else if (monthstr.toLowerCase() === "may") {
        month = 5;
    } else if (monthstr.toLowerCase() === "jun") {
        month = 6;
    } else if (monthstr.toLowerCase() === "jul") {
        month = 7;
    } else if (monthstr.toLowerCase() === "aug") {
        month = 8;
    } else if (monthstr.toLowerCase() === "sep") {
        month = 9;
    } else if (monthstr.toLowerCase() === "oct") {
        month = 10;
    } else if (monthstr.toLowerCase() === "nov") {
        month = 11;
    } else if (monthstr.toLowerCase() === "dec") {
        month = 12;
    } else {
        month = 999;
    }

    var year = parseInt(splits[3]);
    var time = splits[4]
    var time_splits = time.split(':');
    var hour = parseInt(time_splits[0]);
    var min = parseInt(time_splits[1]);
    var sec = parseInt(time_splits[2]);

    var numericalTime = parseInt("" + year + month + date + hour + min + sec);

    return numericalTime;
}

//===============================================================================================================================

function setMacDirectoryInfo(p, s) {

}

function setWindowsDirectoryInfo(p, s) {
    directory_weight = 0;
    var attrs = winattr.getSync(p);

    if (String(attrs.hidden) === "true") {
        directory_weight = directory_weight + 10;
    }

    if (String(attrs.system) === "true") {
        directory_weight = directory_weight + 15;
    }

    var dir_name = p.split('\\').pop().split('/').pop();             

    if (dir_name.startsWith(".")) {
        directory_weight = directory_weight + 8;
    } else if (dir_name.match(/^\d/)) {
        directory_weight = directory_weight + 6;
    } else if (dir_name.startsWith("_")) {
        directory_weight = directory_weight + 4;
    } else {
        directory_weight = directory_weight + 3;
    }

    //console.log(p + " : " + directory_weight);

    var aTime = getNumericalTime(s.atime);
    var mTime = getNumericalTime(s.mtime);
    var cTime = getNumericalTime(s.ctime);

    indexList.push({
        directoryName: dir_name, 
        directoryPath: p, 
        lastAccessTime: aTime, 
        lastModificationTime: mTime, 
        creationTime: cTime, 
        directortyWeight: directory_weight
    })
}

//===============================================================================================================================

function forkClusterProcesses() {
    var task_index = 1;
    if (cluster.isMaster) {

        for (var i = 0; i < numOfCPUs; i++) {
            if (task_index > (indexList.length - 1)) {
                break;
            }

            var directory = indexList[task_index].directoryPath;

            cluster.setupMaster({
                execArgv: ['indexbuilder.js', directory],
                silent: false
            });

            cluster.fork();
            task_index++;
        }

        cluster.on('exit', (worker, code, signal) => {
            completedTaskIndex++;

            if (completedTaskIndex == (indexList.length - 1)) {
                process.exit();
            }

            if (task_index <= (indexList.length - 1)) {
                var directory = indexList[task_index].directoryPath;
                
                cluster.setupMaster({
                    execArgv: ['indexbuilder.js', directory],
                    silent: false
                });

                cluster.fork();
                task_index++;
            }
        });
    } else {

    }
}

//===============================================================================================================================

function indexDirectoryTree(d) {
    var numOdDirs = 0;
    completedTaskIndex = 0;

    setNumberOfCPUs();
    setOS();

    var starttime = new Date().toLocaleString();
    //console.log("[indexDirectoryTree.js] Index List Creation for directory tree : " + d + ", Started At : " + starttime);

    fsUtils.walkSync(d, {
        skipErrors  : true,
        logErrors   : false
    }, function (err, path, stats, next, cache) {
        if (!err) {
            var dir_name = path.split('\\').pop().split('/').pop();
            if (stats.isDirectory() && (path.toLowerCase().indexOf(".git") == -1) && (path.toLowerCase().indexOf(".svn") == -1)) {
                numOdDirs++
                if (OS === "win32") { // For Windows
                    setWindowsDirectoryInfo(path, stats);
                } else { // For Mac
                    setMacDirectoryInfo(path, stats);
                }
            } else {
                // Do not consider files here
            }
        } else {
            // Ignore item error
        }
        next();
    });

    indexList.sort(
        firstBy("directortyWeight", 1)
        .thenBy("lastAccessTime", -1)
    );

    var finishtime = new Date().toLocaleString();
    //console.log("[indexDirectoryTree.js] Index List Creation for directory tree : " + d + ", Finished At : " + starttime);

    forkClusterProcesses();
}

//===============================================================================================================================

//console.log("[indexDirectoryTree.js] Inside indexDirectoryTree.js ==> " + process.argv[2]);
indexDirectoryTree(process.argv[2]);

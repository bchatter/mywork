@echo off
CALL npm install
CALL "C:\Program Files\7-Zip\7z.exe" x opencv_win.zip
CALL RMDIR node_modules\opencv /S /Q
CALL move opencv node_modules/.
CALL move package.json package_backup.json
CALL move package-with-opencv-for-win.json package.json
CALL node_modules\.bin\electron-rebuild.cmd
CALL move package.json package-with-opencv-for-win.json
CALL move package_backup.json package.json

@echo off
echo "Checking Node js installation"
CALL node --version
if errorlevel 0 (
   echo "Node js Found!"
) else (
  echo "Node js Not Found. Installing Node js ..."
  CALL node-v8.9.0-x86.msi
)
echo "Installing node modules ..."
CALL npm install -g windows-build-tool
CALL npm install
CALL .\node_modules\.bin\electron-rebuild.cmd
echo "Installing Elastic Search at C:\ ..."
CALL "C:\Program Files\7-Zip\7z.exe" x Elastic-MasterEligibleNode.zip
CALL move Elastic-MasterEligibleNode C:\.
CALL "C:\Program Files\7-Zip\7z.exe" x Installers.zip

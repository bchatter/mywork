which -s brew
if [[ $? != 0 ]] ; then
    # Install Homebrew
    echo "Brew not found! Installing brew ..."
    echo "Please wait this may take several minutes ..."
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
else
    echo "Brew found! Updating brew ..."
    echo "Please wait this may take several minutes ..."
    brew update
fi

"Setting Java 8 Runtime Environment for Discovery Search Service ..."
echo "Please wait this may take several minutes ..."
brew cask install java

brew install elasticsearch

brew services restart elasticsearch

which -s curl
if [[ $? != 0 ]] ; then
    # Install Curl
    echo "Curl not found! Installing curl ..."
    brew install curl
else
    echo "Curl found!"
fi
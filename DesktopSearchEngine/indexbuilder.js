const textract = require('textract');
const textrank = require('textrank-node');
const fsUtils = require("nodejs-fs-utils");
const firstBy = require('thenby');
const spawn = require('child_process').spawn;
const username = require('username');
const fs = require('fs');
var prefix_indexName = "wdd";
var indextype = "fd";
var indexName = "";

MAX = 28000;
level_depth = 0;

function setCUrrentUserAndIndexName() {
    UN = username.sync()
    indexName = prefix_indexName + "_" + UN;
}

function buildIndex(dirpath) {
    //console.log("Entering indexbuilder() : " + dirstr);

    var bodystr = "";
    var total_files_processed_until_now = 0;

    level_depth = 0;
    //console.log("[indexbuilder.js] Started for directory : " + dirpath); 
    fsUtils.walkSync(dirpath, {
      skipErrors  : true,
      logErrors   : false
    }, function (err, path, stats, next, cache) {
        if (!err  && level_depth > 0) {
            var isdirectory = 0;
            var isfile = 0;
            if (stats.isDirectory()) {
                isdirectory = 1;
                isfile = 0;
            } else {
                isfile = 1;
                isdirectory = 0;
            }
            var _name = path.split('\\').pop().split('/').pop();
            var _path = path.split("\\").join("/");
            var _size = 0;
            var _extstr = "";
            var _ext = 0;

            if (isfile) {
                _size = fsUtils.fsizeSync(path);
                _extstr = _name.split('.').pop();
                _ext = 0;

                if ((_extstr.toUpperCase().indexOf("ATOM") != -1) ||  (_extstr.toUpperCase().indexOf("RSS") != -1)) {
                    _ext = 12;
                } else if (_extstr.toUpperCase().indexOf("PDF") != -1) {
                    _ext = 11;
                } else if ((_extstr.toUpperCase().indexOf("XLS") != -1) ||  (_extstr.toUpperCase().indexOf("XLSX") != -1) ||  (_extstr.toUpperCase().indexOf("XLSB") != -1) ||  (_extstr.toUpperCase().indexOf("XLSM") != -1) ||  (_extstr.toUpperCase().indexOf("XLTX") != -1)) {
                    _ext = 10;
                } else if ((_extstr.toUpperCase().indexOf("PPTX") != -1) ||  (_extstr.toUpperCase().indexOf("POTX") != -1)) {
                    _ext = 9;
                } else if ((_extstr.toUpperCase().indexOf("DOC") != -1) ||  (_extstr.toUpperCase().indexOf("DOCX") != -1)) {
                    _ext = 8;
                } else if (_extstr.toUpperCase().indexOf("RTF") != -1) {
                    _ext = 7;
                } else if ((_extstr.toUpperCase().indexOf("PNG") != -1) ||  (_extstr.toUpperCase().indexOf("JPG") != -1) ||  (_extstr.toUpperCase().indexOf("JPEG") != -1) ||  (_extstr.toUpperCase().indexOf("GIF") != -1)) {
                    _ext = 6;
                } else if (_extstr.toUpperCase().indexOf("XML") != -1) {
                    _ext = 5;
                } else if ((_extstr.toUpperCase().indexOf("HTML") != -1) ||  (_extstr.toUpperCase().indexOf("HTM") != -1)) {
                    _ext = 4;
                } else if (_extstr.toUpperCase().indexOf("JS") != -1) {
                    _ext = 3;
                } else if ((_extstr.toUpperCase().indexOf("CPP") != -1) ||  (_extstr.toUpperCase().indexOf("C") != -1) ||  (_extstr.toUpperCase().indexOf("PY") != -1) ||  (_extstr.toUpperCase().indexOf("H") != -1)) {
                    _ext = 2;
                } else if ((_extstr.toUpperCase().indexOf("TEXT") != -1) || (_extstr.toUpperCase().indexOf("TXT") != -1)) {
                    _ext = 1;
                } else {
                    _ext = 0;
                }

                bodystr = bodystr + '{ "index" :  { "_index" : "' + indexName + '", "_type" : "' + indextype + '", "_id" : "' + fs.statSync(_path).ino + '" } },\n' + 
                                    '{ "fileabsolutepath" : "' + _path + '", "filename" : "' + _name + '", "type" : "File", "size" : "' + _size + '" }\n';
            } else {
                bodystr = bodystr + '{ "index" :  { "_index" : "' + indexName + '", "_type" : "' + indextype + '", "_id" : "' + fs.statSync(_path).ino + '" } },\n' + 
                                    '{ "fileabsolutepath" : "' + _path + '", "filename" : "' + _name + '", "type" : "Folder", "size" : "NA" }\n';
            }

            total_files_processed_until_now++;

            if (bodystr.length > MAX) {
                //console.log("[indexbuilder.js] Inside [1]");
                const indexer = spawn('curl', ['-XPOST', 'localhost:9200/_bulk?pretty', '-H', 'Content-Type:application/json', '-d', bodystr]);
                indexer.stdout.on('data', (data) => {
                    //console.log(data.toString());
                });

                indexer.stderr.on('data', (data) => {
                    //console.log(data.toString());
                });

                indexer.on('exit', (code) => {
                    //console.log(`[indexbuilder.js] [1] Indexer process exited with code ${code}`);
                });

                total_files_processed_until_now = 0;
                bodystr = "";
            }
        }
        level_depth++;
        if (level_depth < 2) {
            next();
        }
    });


    if (bodystr.length > 0) {
        //console.log("[indexbuilder.js] Inside [2]");
        const indexer = spawn('curl', ['-XPOST', 'localhost:9200/_bulk?pretty', '-H', 'Content-Type:application/json', '-d', bodystr]);
        indexer.stdout.on('data', (data) => {
            //console.log(data.toString());
        });

        indexer.stderr.on('data', (data) => {
            //console.log(data.toString());
        });

        indexer.on('exit', (code) => {
            //console.log("[indexbuilder.js] Finished for directory : " + dirpath);
            process.exit();
        });

        total_files_processed_until_now = 0;
        bodystr = "";
    }
}

setCUrrentUserAndIndexName();
buildIndex(process.argv[2]);


var curUASIndex=0;
function changeToUASPage(id) 
{
    curUASIndex=id;
    $('#uasDevName').text(gDevNames[id])
    if(gUASstate)
    {
        document.querySelector("#uasCheckbox").checked = true;
    }
    else
    {
        document.querySelector("#uasCheckbox").checked = false;
    }
    var idx = $('#id_uas')
    $.mobile.changePage($(idx), {transition : "slide"});
    $('#popup').hide()
      
}

function ChangeUASState()
{
    var devId=gDeviceIds[curUASIndex];
    mainProcess.test('ChangeUASState called for .. '+devId)
    if(gUASstate)
    {
        mainProcess.setUAS(devId,false);
        gUASstate=0;
    }
    else
    {
        mainProcess.setUAS(devId,true);
        gUASstate=1;
    }
}


const electron = require('electron')
const remote = electron.remote
const mainProcess = remote.require('./app')
const ipc = electron.ipcRenderer
const pids_path='./pids.json' // change to actual pids.json path
var pids = JSON.parse(require('fs').readFileSync(pids_path, 'utf8'));

var gDeviceIds=[];
var gLEDstate=0;
var gUASstate=0;
var gLEDstr='OFF';
var gDevNames=[];
var gSetPwd=0;

function createListView(data) 
{
    gDeviceIds=data;
    var deviceIDs=data;
    var xx=getDeviceNamesAndPaths(deviceIDs);
    var devNames=xx[0]
    var imagePaths=xx[1]
     gDevNames=devNames;
    mainProcess.test(deviceIDs)
    mainProcess.test(devNames);
    
    $('#result-listview').empty();

    for(var index=0;index<deviceIDs.length;++index)
    {
        var A=getDevListItem(index,deviceIDs[index],devNames[index],imagePaths[index]);
        $('#result-listview').append(A).trigger('create');
        $(index).page();
    }
    
    $('#result-listview').listview('refresh')
    

}
  
ipc.on('refresh', (event, message) => 
{
    mainProcess.test('Received refresh event...')
    var sDevs=[]
    var jsonObject=JSON.parse(message)
    Object.keys(jsonObject).forEach(function(key){
        var value = jsonObject[key];
        sDevs.push(value)
    });
    mainProcess.test(sDevs);
    searchBarShow();
    createListView(sDevs);
    updateHealth();
        
})

function refreshEnumDeviceList()
{
    var message=mainProcess.E_enumerateDevices()
    mainProcess.test(message);
    var sDevs=[]
    var jsonObject=JSON.parse(message)
    Object.keys(jsonObject).forEach(function(key){
        var value = jsonObject[key];
        sDevs.push(value)
    });
    createListView(sDevs);
}

function getDeviceNamesAndPaths(devIds)
{
    var ret=[],retA=[],retB=[];   
    for(var i=0;i<devIds.length;++i)
    {
        var name=devIds[i].split("_");
        var PID=name[2];
        if(PID.length!=4)
        {
                if(PID.length==3)
                    PID='0'+PID;
                if(PID.length==2)
                    PID='00'+PID;
        }
        var x=pids[PID]['productName'];
        var y=pids[PID]['images']['icon'];
        retA.push(x);
        retB.push(y)
    }
    ret.push(retA)
    ret.push(retB)
    return ret;
}


function getDevListItem(index,sDevId,sDevName,sPaths)
{
    var devId="WDDevice_"+sDevId;
    var content='<li class="list-group-item "  > ' +
   
    '<div class="media-body">' +
        '<img class="img media-object pull-left" src="./'+sPaths+'">' +
            '<label class="font-weight-normal" style="width:250px;" >&nbsp' + sDevName + '</label>' +
            '<img class="settings_icon"  src="./assets/settings.png"  id="'+devId+'" onclick = "popUP('+ index+');" width="15" height="15" alt="Image">'+
    '</div>'+
    '<div class="health">Health: <label id="id_health_str'+index+'">good </label><img  src="./assets/good.png" id="id_health'+index+'" width="15" height="15" left="15" alt="Image"></div>'+
    '</li>';
    return content;
}

function goToHome()
{
    //refreshEnumDeviceList();
    var idx = $('#home')
    $.mobile.changePage($(idx), null,false,true);

}

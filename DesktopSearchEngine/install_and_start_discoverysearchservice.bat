@echo off
set sd=%1

echo "Installing Discovery Search Service"
CALL %sd%\\Elastic-MasterEligibleNode\\bin\\elasticsearch-service.bat install DiscoverySearchService

echo "Starting Discovery Search Service"
CALL %sd%\\Elastic-MasterEligibleNode\\bin\\elasticsearch-service.bat start DiscoverySearchService

echo "Waiting for Service to start ..."
SLEEP 30
echo "Service Started!"

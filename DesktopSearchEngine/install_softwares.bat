@echo off
set sd=%1

echo "Installing Java 8 Rumtime environment. This might take several minutes ..."
REM CALL "%sd%\\Installers\\JavaInstaller\\win\\jre-8u151-windows-x64.exe"

echo "Installing Discovery Search Service"
IF DEFINED JAVA_HOME (
  echo "JAVA_HOME already set!"
) ELSE (
  SETX /M JAVA_HOME "C:\Program Files\Java\jre1.8.0_151"
)

echo "Checking Curl installation"
CALL curl -V
if errorlevel 0 (
   echo "Curl Found!"
) else (
  echo "Curl Not Found. Installing Curl ..."
  CALL "%sd%\\Installers\\CurlInstaller\\win\\curl-7.46.0-win64.exe"
)


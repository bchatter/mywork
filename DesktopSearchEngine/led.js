
var curLedIndex=0;
function changeToLEDPage(id) 
{
    curLedIndex=id;
    $('#ledDevName').text(gDevNames[id])
    $('#ledstr').text(gLEDstr)
    if(gLEDstate)
    {
        document.querySelector("#ledCheckbox").checked = true;
    }
    else
    {
        document.querySelector("#ledCheckbox").checked = false;
    }
    var idx = $('#id_led')
    $.mobile.changePage($(idx), {transition : "slide"});
    $('#popup').hide()
      
}

function ChangeLEDState()
{
    var devId=gDeviceIds[curLedIndex];
    mainProcess.test('ChangeLEDState called for ..'+devId)
    if(gLEDstate)
    {
        mainProcess.setLED(devId,false);
        gLEDstate=0;
        gLEDstr='OFF'
    }
    else
    {
        mainProcess.setLED(devId,true);
        gLEDstate=1;
        gLEDstr='ON'

    }
    $('#ledstr').text(gLEDstr)  
}


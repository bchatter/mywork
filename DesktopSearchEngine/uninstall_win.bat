@echo off
echo "UnInstalling. Please wait as it can take several minutes ..."
RMDIR C:\Elastic-MasterEligibleNode /S /Q
RMDIR Installers /S /Q
RMDIR node_modules /S /Q



function updateHealth() {
  setInterval(processHealth, 5000); //Drive health check interval set to 5000 milliseconds.
}

function processHealth() 
{
    mainProcess.test('updating health...')
    for(var id=0;id<gDeviceIds.length;++id)
    {
        var devId=gDeviceIds[id];
        var ret=mainProcess.getFeatureHealth(devId);
        mainProcess.test('Health response: '+ret);
        var jsonObject = JSON.parse(ret);
        var st=jsonObject['health']['state']

        var imgID='id_health'+id;
        var txtID='id_health_str'+id
        mainProcess.test(st);
        if(st=='good')
        { 
            $('#'+imgID).attr('src','./assets/good.png')
            $('#'+txtID).text('good')
        }
        else if(st=='bad')
        { 
            $('#'+imgID).attr('src','./assets/bad.png')
            $('#'+txtID).text('bad')
        }
        else if(st=='unknown')
        { 
            $('#'+imgID).attr('src','./assets/unknown.png')
            $('#'+txtID).text('unknown')
        }
        else
        {

        }
    }
}